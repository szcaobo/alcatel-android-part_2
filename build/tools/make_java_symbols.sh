#!/bin/bash

TARGET_PRODUCT=$1
PLF_PATH="out/target/product/${TARGET_PRODUCT}/plf"
SDMID=`sed -n 's|<SDMID>\([a-zA-Z0-9_]*\).*|\1|gp' $PLF_PATH/isdm_framework-res.plf`
MTYPE=`sed -n 's|<METATYPE>\([a-zA-Z0-9_]*\).*|\1|gp' $PLF_PATH/isdm_framework-res.plf`
MTYPE=${MTYPE//Boolean/bool}
MTYPE=${MTYPE//Sdword/integer}
MTYPE=${MTYPE//Sword/integer}
MTYPE=${MTYPE//Sbyte/integer}
MTYPE=${MTYPE//Byte/integer}
MTYPE=${MTYPE//Dword/integer}
MTYPE=${MTYPE//Word/integer}
MTYPE=${MTYPE//AsciiString/string}
MTYPE=${MTYPE//StringWLen/string}

out_symbols_path="frameworks/base/core/res/res/values"
mkdir -p ${out_symbols_path}
out_symbols_file="${out_symbols_path}/sdm_symbols.xml"

echo -e "<?xml version=\"1.0\" encoding=\"utf-8\"?>" > ${out_symbols_file}
echo -e "<resources>"  >> ${out_symbols_file}
echo -e "   <private-symbols package=\"com.android.internal\" />"  >> ${out_symbols_file}
index=0
for LINE in `echo "${MTYPE}" | cat`
do
  index=$(($index+1))
  type=`echo -e "$MTYPE" | sed -n "${index}p"`
  name=`echo -e "$SDMID" | sed -n "${index}p"` 
  echo -e "   <java-symbol type=\"${type}\" name=\"${name}\"/>"  >> ${out_symbols_file}
done
echo -e "</resources>"  >> ${out_symbols_file}


