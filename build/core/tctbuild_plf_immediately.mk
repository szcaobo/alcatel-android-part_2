PLF_FILE_DIR := $(LOCAL_PLF_MODULE_DIR)
PLF_MRG_FILE_DIR := $(PRODUCT_OUT)/plf
PLF_MRG_FILE := $(PLF_MRG_FILE_DIR)/isdm_$(LOCAL_PLF_MODULE).plf
ifeq ($(TCT_BUILD_PERSO),true)
PLF_TARGET_XML := $(JRD_CUSTOM_RES)/$(LOCAL_PLF_MODULE_DIR)/res/values/isdm_$(LOCAL_PLF_MODULE)_defaults.xml
else
PLF_TARGET_XML := $(LOCAL_PATH)/res/values/isdm_$(LOCAL_PLF_MODULE)_defaults.xml
endif

ifeq ("$(filter perso,$(MAKECMDGOALS))","perso")
PLF_IS_MAKE_PERSO := 1
else
PLF_IS_MAKE_PERSO := 0
endif

$(shell mergeplf $(PLF_IS_MAKE_PERSO) $(PLF_FILE_DIR) $(PLF_MRG_FILE_DIR) $(TARGET_PRODUCT) )

PLF_TOOLS_DIR := ./device/tct/common/perso/tools/prd2xml
$(shell export LD_LIBRARY_PATH=$(PLF_TOOLS_DIR) ; \
        $(PLF_TOOLS_DIR)/prd2h  --def $(PLF_TOOLS_DIR)/prd2h_def.xml  --dest $(TOP)  $(PLF_MRG_FILE) ; \
        mkdir -p $(dir $(PLF_TARGET_XML)) ; \
        mv $(TOP)/isdm_$(LOCAL_PLF_MODULE)_android.xml $(PLF_TARGET_XML) ; \
        rm $(TOP)/isdm_$(LOCAL_PLF_MODULE)_*.* \
 )

#Clear Variable
LOCAL_PLF_MODULE:=
LOCAL_PLF_MODULE_DIR:=
PLF_FILE_DIR:=
PLF_MRG_FILE_DIR:=
PLF_MRG_FILE:=
PLF_TARGET_XML:=
PLF_IS_MAKE_PERSO:=

