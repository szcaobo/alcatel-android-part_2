LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional

LOCAL_SRC_FILES := $(call all-subdir-java-files)

LOCAL_JAVA_LIBRARIES := telephony-common

LOCAL_CERTIFICATE := platform

LOCAL_PACKAGE_NAME := DrmProvider

include $(BUILD_PACKAGE)
