/******************************************************************************/
/*                                                               Date:06/2013 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2013 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 09/03/2013|       Bo.Yu          |      FR-487411       |[Egro]Message]    */
/*               [Common]DNA Message egro development,porting from bug 435888 */
/* ----------|----------------------|----------------------|----------------- */
/* 01/12/2015|      qingyi.he       |      PR-890581       |[SMS]The SMS Service*/
/*           |                      |                      |Center is wrong when*/
/*           |                      |                      | hot Swap SIM card*/
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package com.android.providers.telephony;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneFactory;
import android.os.AsyncResult;

//[BUGFIX]-Add-BEGIN by TSCD.qingyi.he,01/12/2015,PR-890581,
//[SMS]The SMS Service Center is wrong when hot Swap SIM card.
import android.telephony.TelephonyManager;
import android.telephony.SubscriptionManager;
//[BUGFIX]-Add-END by TSCD.qingyi.he,01/12/2015,PR-890581

public class SmsServiceCenterProvider extends ContentProvider {
    private static final String TAG = "SmsServiceCenterProvider";
    private static final String AUTHORITY = "com.android.providers.telephony";
    private static final String COL_ID = "_id";
    private static final String COL_SMSCADDR = "smsc_addr";
    private static final String COL_SMSCINDEX = "smsc_index";
    private static final int MSG_QUERY_SMSC = 1;
    private static final int MSG_UPDATE_SMSC = 2;
    private static final int EVENT_QUERY_SMSC_DONE = 1005;
    private static final int TIMEOUT = 20 * 1000;
    private static final int URI_SMSC = 1;
    private static final int URI_SMSC_ID = 2;

    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    private static final ArrayBlockingQueue<String> sGetSmscQueue = new ArrayBlockingQueue<String>(
            2);

    private static Handler sHandler = null;
    private static Handler sMainHandler = null;
    private static Phone sPhone = null;
    private static String[] sSmscAddr = new String[] {
            "", ""
    };
    private static String[] sSmscIndex = new String[] {
            "", ""
    };
    private Cursor mCursor = null;

    static {
        sUriMatcher.addURI(AUTHORITY, "smsc", URI_SMSC);
        sUriMatcher.addURI(AUTHORITY, "smsc/#", URI_SMSC_ID);

        sMainHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case MSG_QUERY_SMSC:
                        try {
                            //[BUGFIX]-Add-BEGIN by TSCD.qingyi.he,01/12/2015,PR-890581,
                            //[SMS]The SMS Service Center is wrong when hot Swap SIM card.
                            //sPhone = PhoneFactory.getDefaultPhone();
                            if (TelephonyManager.getDefault().isMultiSimEnabled()) {
                                sPhone = PhoneFactory.getPhone(msg.arg1);
                            } else {
                                sPhone = PhoneFactory.getDefaultPhone();
                            }
                            //[BUGFIX]-Add-END by TSCD.qingyi.he,01/12/2015,PR-890581
                            sPhone.getSmscAddress(sHandler.obtainMessage(EVENT_QUERY_SMSC_DONE));
                        } catch (Exception e) {
                            Log.i(TAG, "exception to getPhone");
                        }
                        break;
                    case MSG_UPDATE_SMSC:
                        try {
                            //[BUGFIX]-Add-BEGIN by TSCD.qingyi.he,01/12/2015,PR-890581,
                            //[SMS]The SMS Service Center is wrong when hot Swap SIM card.
                            //sPhone = PhoneFactory.getDefaultPhone();
                            if (TelephonyManager.getDefault().isMultiSimEnabled()) {
                                sPhone = PhoneFactory.getPhone(msg.arg1);
                            } else {
                                sPhone = PhoneFactory.getDefaultPhone();
                            }
                            //[BUGFIX]-Add-END by TSCD.qingyi.he,01/12/2015,PR-890581
                            String smsc = (String) msg.obj;
                            Log.i(TAG, "msg.arg1=" + msg.arg1 + "sPhone=" + sPhone + "smsc=" + smsc);
                            sPhone.setSmscAddress(smsc, null);
                        } catch (Exception e) {
                            Log.i(TAG, "exception to getPhone");
                        }
                        break;
                }
            }
        };

        new Thread(new Runnable() {
            public void run() {
                Looper.prepare();
                sHandler = new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        AsyncResult ar = null;
                        switch (msg.what) {
                            case EVENT_QUERY_SMSC_DONE:
                                ar = (AsyncResult) msg.obj;
                                try {
                                    if (ar.exception != null) {
                                        sGetSmscQueue.put("");
                                    } else {
                                        String scValueRead = (String) ar.result;
                                        if (scValueRead != null) {
                                            sGetSmscQueue.put(scValueRead);
                                        } else {
                                            sGetSmscQueue.put("");
                                        }
                                    }
                                } catch (InterruptedException e) {
                                }
                                break;
                        }
                    }
                };
                Looper.loop();
            }
        }).start();
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        return null;
    }

    @Override
    public boolean onCreate() {
        Log.i(TAG, "onCreate");
        return true;
    }

    private boolean getSmscAddress(int slotid) {
        String smscAddr = null;
        sGetSmscQueue.clear();
        Message msg = sMainHandler.obtainMessage(MSG_QUERY_SMSC, slotid, 0);
        sMainHandler.sendMessage(msg);
        try {
            smscAddr = sGetSmscQueue.poll(TIMEOUT, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            smscAddr = null;
        }
        // split
        if (smscAddr != null && smscAddr != "") {
            sSmscAddr[slotid] = smscAddr.split(",")[0];
            sSmscIndex[slotid] = smscAddr.split(",")[1];
            Log.i(TAG, "slotid=" + slotid + " -- " + sSmscAddr[slotid] + " -- "
                    + sSmscIndex[slotid]);
            return true;
        }
        return false;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
            String sortOrder) {
        //[BUGFIX]-Add-BEGIN by TSCD.qingyi.he,01/12/2015,PR-890581,
        //[SMS]The SMS Service Center is wrong when hot Swap SIM card.
        mCursor = new MatrixCursor(new String[] {
            COL_ID, COL_SMSCADDR, COL_SMSCINDEX
        });
        //[BUGFIX]-Add-END by TSCD.qingyi.he,01/12/2015,PR-890581
        Log.i(TAG, "1");
        int match = sUriMatcher.match(uri);
        switch (match) {
            case URI_SMSC:
                //[BUGFIX]-Add-BEGIN by TSCD.qingyi.he,01/12/2015,PR-890581,
                //[SMS]The SMS Service Center is wrong when hot Swap SIM card.
                //getSmscAddress(0);
                //((MatrixCursor) mCursor).addRow(new String[] {
                //        String.valueOf(0), sSmscAddr[0], sSmscIndex[0]
                //});
                if (TelephonyManager.getDefault().isMultiSimEnabled()) {
                    for (int i=0; i<TelephonyManager.getDefault().getPhoneCount(); i++) {
                        if (getSmscAddress(i)) {
                            ((MatrixCursor)mCursor).addRow(new String[]{ String.valueOf(i),
                                                                        sSmscAddr[i],
                                                                        sSmscIndex[i]});
                        }
                    }
                } else {
                    int subscription = (int)SubscriptionManager.getDefaultSubscriptionId();
                    getSmscAddress(subscription);
                    ((MatrixCursor)mCursor).addRow(new String[]{ String.valueOf(subscription),
                            sSmscAddr[subscription],
                            sSmscIndex[subscription]});
                }
                //[BUGFIX]-Add-END by TSCD.qingyi.he,01/12/2015,PR-890581
                break;
            case URI_SMSC_ID:
                int id = Integer.valueOf(uri.getLastPathSegment());
                //[BUGFIX]-Add-BEGIN by TSCD.qingyi.he,01/12/2015,PR-890581,
                //[SMS]The SMS Service Center is wrong when hot Swap SIM card.
                //getSmscAddress(0);
                //((MatrixCursor) mCursor).addRow(new String[] {
                //        String.valueOf(0), sSmscAddr[0], sSmscIndex[0]
                //});
                if (TelephonyManager.getDefault().isMultiSimEnabled()) {
                    if (id >= 0 && id < TelephonyManager.getDefault().getPhoneCount()) {
                        Log.i(TAG,"==ThreadID" + Thread.currentThread().getId());
                        Log.i(TAG, "==myloop=" + Looper.myLooper() );
                        if (getSmscAddress(id)) {
                            ((MatrixCursor)mCursor).addRow(new String[]{ String.valueOf(id),
                                                                        sSmscAddr[id],
                                                                        sSmscIndex[id]});
                        }
                    }
                } else {
                    int subscription = id;
                    getSmscAddress(subscription);
                    ((MatrixCursor)mCursor).addRow(new String[]{ String.valueOf(subscription),
                            sSmscAddr[subscription],
                            sSmscIndex[subscription]});
                }
                //[BUGFIX]-Add-END by TSCD.qingyi.he,01/12/2015,PR-890581
                break;
            default:
                break;
        }

        return mCursor;
    }

    private int setSmscAddress(ContentValues values, int slotid) {
        String smscAddr = null;
        String smscIndex = null;

        if (values.containsKey(COL_SMSCADDR)) {
            smscAddr = "\"" + values.getAsString(COL_SMSCADDR).replace("\"", "") + "\"";
        } else {
            Log.w(TAG, "NOT contains COL_SMSCADDR!");
            return 0;
        }

        if (values.containsKey(COL_SMSCINDEX)) {
            smscIndex = values.getAsString(COL_SMSCINDEX);
        } else if (sSmscIndex[slotid] != null && sSmscIndex[slotid] != "") {
            smscIndex = sSmscIndex[slotid];
        } else if (getSmscAddress(slotid)) {
            smscIndex = sSmscIndex[slotid];
        } else {
            Log.w(TAG, "NO SMSC INDEX!");
            return 0;
        }

        Log.i(TAG, "smsc=" + smscAddr + "," + smscIndex);
        Message msg = sMainHandler.obtainMessage(MSG_UPDATE_SMSC, slotid, 0,
                (smscAddr + "," + smscIndex));
        sMainHandler.sendMessage(msg);

        return 1;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int ret = 0;
        int match = sUriMatcher.match(uri);
        switch (match) {
            case URI_SMSC_ID:
                //[BUGFIX]-Add-BEGIN by TSCD.qingyi.he,01/12/2015,PR-890581,
                //[SMS]The SMS Service Center is wrong when hot Swap SIM card.
                //ret = setSmscAddress(values, 0);
                int id = Integer.valueOf(uri.getLastPathSegment());
                if (TelephonyManager.getDefault().isMultiSimEnabled()) {
                    if (id >= 0 && id < TelephonyManager.getDefault().getPhoneCount()) {
                        ret = setSmscAddress(values, id);
                    }
                } else {
                    ret = setSmscAddress(values, id);
                }
                //[BUGFIX]-Add-END by TSCD.qingyi.he,01/12/2015,PR-890581
                break;
            default:
                break;
        }

        return ret;
    }

}
