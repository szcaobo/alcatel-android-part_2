package com.android.providers.media;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import android.app.WallpaperManager;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.drm.DrmStore;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.provider.MediaStore;
import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;
import android.util.TctLog;

import com.tct.drm.DrmApp;
import com.tct.drm.TctDrmManagerClient;
import com.tct.drm.TctDrmStore;

/**
 *TODO observer the media uri. when the media  that the uri point was removed.remove the expire message.
 */

public class DrmTrackerAdapter {
    public static final String DRM_RING_SUFFIX = "_drm";
    public static final int MSG_DRM_RING_CHECK = 5;
    public static final int MSG_DRM_WALLPAPER_CHECK = 6;
    public static final int MSG_MEDIA_UNMOUNT = 7;
    public static final int MSG_PHONE_REBOOT = 8;
    public static final int MSG_DRM_LOCKPAPER_CHECK = 9;
    public static final int PATH_TYPE_WALLPAPER = 1000;
    public static final int PATH_TYPE_RING_TONG = 1001;
    public static final int PATH_TYPE_ALERT_ALARM = 1002;
    public static final int PATH_TYPE_NOTIFICATION = 1003;
    public static final int PATH_TYPE_LOCKPAPER = 1004;
    public static final int MSG_EXPIRE_INTERVAL = 50;

    private static final String TAG = DrmUtil.TAG;
    private static final int MSG_DRM_WALLPAPER_EXPIRE = PATH_TYPE_WALLPAPER + MSG_EXPIRE_INTERVAL;
    private static final int MSG_DRM_RINGTONE_EXPIRE = PATH_TYPE_RING_TONG + MSG_EXPIRE_INTERVAL;
    private static final int MSG_DRM_NOTIFICATION_SOUND_EXPIRE = PATH_TYPE_NOTIFICATION
            + MSG_EXPIRE_INTERVAL;
    private static final int MSG_DRM_ALARM_ALERT_EXPIRE = PATH_TYPE_ALERT_ALARM
            + MSG_EXPIRE_INTERVAL;
    private static final int MSG_DRM_LOCKPAPER_EXPIRE = PATH_TYPE_LOCKPAPER + MSG_EXPIRE_INTERVAL;

    public static final int MSG_DELETE_INTERVAL = 100;
    private static final int MSG_DRM_WALLPAPER_FILE_DELETE = PATH_TYPE_WALLPAPER
            + MSG_DELETE_INTERVAL;
    private static final int MSG_DRM_RINGTONE_DELETE = PATH_TYPE_RING_TONG + MSG_DELETE_INTERVAL;
    private static final int MSG_DRM_NOTIFICATION_SOUND_DELETE = PATH_TYPE_NOTIFICATION
            + MSG_DELETE_INTERVAL;
    private static final int MSG_DRM_ALARM_ALERT_DELETE = PATH_TYPE_ALERT_ALARM
            + MSG_DELETE_INTERVAL;
    private static final int MSG_DRM_LOCKPAPER_FILE_DELETE = PATH_TYPE_LOCKPAPER
            + MSG_DELETE_INTERVAL;

    /** the path has rights and have left time */
    private int RIGHTS_VALID_AVALIABLE_TIME = 200;
    /** the path has rights only count times */
    private int RIGHTS_VALID_ONLY_COUNT = 201;
    /** the path has rights and no time limit */
    private int RIGHTS_VALID_NO_LIMIT_TIME = 202;
    /** the path don't have valid rights */
    private int RIGHTS_INVALID = 400;
    private static final String CURR_LOCKPAPER = "curr_lockpaper";
    private TctDrmManagerClient mDrmClient = null;
    private Context mContext;
    private ContentResolver mCr;
    private DrmTaskHandler drmTaskHandler;
    private HandlerThread drmTaskThread;
    private boolean shutdown = false;
    private boolean unmounted = false;
    private static DrmTrackerAdapter sInstance;
    private final String SDPATH = "/storage/sdcard1/";
    private final String SDDIR = ".lockscreen_wallpaper/";
    private final String SDFILE_NAME = "lockscreenwallpaper";
    private final String [] SUFFIX_NAME = {".jpg",".png"};

    /**
     * store the observer tables.<Path, FileObserver> the path is Fileobserver
     * observer dir. the all add/remove operators are in the one thread with
     * queue,so don't worry the sync operators
     */
    private HashMap<String, DrmFileObserver> mObserversMap = new HashMap<String, DrmFileObserver>();

    private class Wrapper {
        DrmFileObserver observer;
        String name;
    }

    public static synchronized DrmTrackerAdapter getInstance(Context context) {
        DrmUtil.logd("Get DrmTackerAdaper -----!");
        if (sInstance == null) {
            sInstance = new DrmTrackerAdapter();
        }
        sInstance.mContext = context;
        sInstance.mCr = sInstance.mContext.getContentResolver();
        sInstance.mDrmClient = TctDrmManagerClient.getInstance(context);

        return sInstance;
    }

    private DrmTrackerAdapter() {
        drmTaskThread = new HandlerThread("Drm-Task-Adapter");
        drmTaskThread.start();
        drmTaskHandler = new DrmTaskHandler(drmTaskThread.getLooper());
        TctLog.e(TAG, "Thread [Drm-Task-tracked] ---start!");
    }

    private class RightsStatus {
        int status;
        /** if the drm file has avalite time,the time will be set. */
        long time;
    }

    /**
     * @param name the ringtone's name.that is stored in the system.db such as
     *            "Settings.System.NOTIFICATION_SOUND"
     * @param uri the drm observer uri.maybe this is dirty value so should check
     *            with the system current settings.
     * @param (war removed) sysDefault the system default uri.this is set in
     *        perso(.plf)
     */
    private void resetRing(DrmObj obj) {
        String name = DrmUtil.getRingTypeName(obj.drmType);
        String currUri = Settings.System.getString(mCr, name);
        resetMediaRingFlag(obj);
        if (currUri == null || !currUri.equals(obj.uriString)) {
            DrmUtil.logd("current ring uri:" + currUri + "is not same as drmObj uri:"
                    + obj.uriString);
            return;
        }
        Uri ringUri = null;
        if (name.equals(Settings.System.RINGTONE)) {
            ringUri = RingtoneManager.getDefaultRingtoneUri(mContext);
        } else if (name.equals(Settings.System.ALARM_ALERT)) {
            ringUri = RingtoneManager.getDefaultAlarm(mContext);
        } else {
            ringUri = RingtoneManager.getDefaultNotification(mContext);
        }
        Settings.System.putString(mCr, name, (ringUri == null) ? "" : ringUri.toString());
    }

    public boolean isDrm(String path) {
        if (path.startsWith("/custpack/")) {
            return false;
        }
        return mDrmClient.canHandle(path, null);
    }

    private String getCurrRingValue(int drmPathType) {
        String name = DrmUtil.getRingTypeName(drmPathType);
        return Settings.System.getString(mCr, name);
    }

    private void resetMediaRingFlag(DrmObj obj) {
        if(obj.uriString == null && obj.uriString.isEmpty()){
            DrmUtil.logd("resetMediaRingFlag----> path are null!");
            return;
        }
        ContentValues values = new ContentValues(3);
        values.put(MediaStore.Audio.Media.IS_RINGTONE, 0);
        values.put(MediaStore.Audio.Media.IS_ALARM, 0);
        values.put(MediaStore.Audio.Media.IS_NOTIFICATION, 0);
        try {
            mCr.update(Uri.parse(obj.uriString), values, null, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void checkDrmObjStatus(DrmObj drmObj) {
        String newPath = drmObj.path;
        DrmUtil.logd("checkWallpaperStatus---new DrmPath:" + drmObj);
        boolean isWallPaper = (drmObj.drmType == PATH_TYPE_WALLPAPER);
        boolean isLockPaper = (drmObj.drmType == PATH_TYPE_LOCKPAPER);
        boolean comingIsDrm = mDrmClient.isDrm(newPath); 
        Wrapper wrapper = getWrapperForType(drmObj.drmType);
        if (wrapper == null && !comingIsDrm) {
            DrmUtil.logd("The current observer is NULL.and the coming path is EMPTY! just return!");
            if (isWallPaper) {
                DrmUtil.logd("isWallPaper to set CURR_WALLPAPER_DRMPATH to \" \" ");
                Settings.System.putString(mCr, TctDrmStore.CURR_WALLPAPER_DRMPATH, "");
            }
            if (isLockPaper){
                DrmUtil.logd("isLockPaper to set CURR_LOCKPAPER.to \"\".");
                Settings.System.putString(mCr, CURR_LOCKPAPER, "");
            }
            return;
        }

        if (wrapper != null) {
            DrmUtil.logd("Current obser exist! Stop first! Observer:" + wrapper.observer);
            wrapper.observer.removeDrmObj(drmObj.drmType, wrapper.name);
            drmTaskHandler.removeMessages(drmObj.drmType + MSG_DELETE_INTERVAL);
            if (!(isWallPaper||isLockPaper)) {
                if (wrapper.name.equals(drmObj.name)) {
                    drmTaskHandler.removeMessages(drmObj.drmType + MSG_EXPIRE_INTERVAL);
                }
            } else if(isWallPaper) {
                drmTaskHandler.removeMessages(drmObj.drmType + MSG_EXPIRE_INTERVAL);
                DrmUtil.logd("01 isWallPaper to set CURR_WALLPAPER_DRMPATH to \" \" ");
                Settings.System.putString(mCr, TctDrmStore.CURR_WALLPAPER_DRMPATH, "");
            } else if(isLockPaper) {
                DrmUtil.logd("isLockPaper  to remove and set to \" \" ");
                drmTaskHandler.removeMessages(drmObj.drmType + MSG_EXPIRE_INTERVAL);
                Settings.System.putString(mCr, CURR_LOCKPAPER, "");
            }
            if (!comingIsDrm) {
                DrmUtil.logd("The coming path is not drm! just stop current drm path observer!");
                stopObserver(wrapper.observer);
                return;
            }
        }

        int action = (isWallPaper||isLockPaper) ? DrmStore.Action.DISPLAY : DrmStore.Action.PLAY;
        RightsStatus rigthStatus = getConstaintStatus(drmObj.path, action);
        if (rigthStatus.status == RIGHTS_INVALID || rigthStatus.status == RIGHTS_VALID_ONLY_COUNT) {
            DrmUtil.logd("The new drm Path constraints are invalid!" + drmObj);
            if (isWallPaper) {
                resetWallpaper(true);
            } else if (isLockPaper) {
                DrmUtil.logd("isLockPaper to doResetLockScreen()");
                doResetLockScreen();
            } else {
                resetRing(drmObj);
            }
            TctLog.w(TAG, "Set drm file failed!");

            if (wrapper != null) stopObserver(wrapper.observer);
            return;
        } else {
            DrmUtil.logd("Will start new drm file observer!" + drmObj);
            if (isWallPaper) {
                /**  store it to the settings.check this file after the phone reboot.*/
                DrmUtil.logd("04 isWallPaper to set CURR_WALLPAPER_DRMPATH to \" \" ");
                Settings.System.putString(mCr, TctDrmStore.CURR_WALLPAPER_DRMPATH, newPath);
            }
            if (isLockPaper) {
                /**  store it to the settings.check this file after the phone reboot.*/
                DrmUtil.logd("isLockPaper to store the file after the Phone reboot");
                Settings.System.putString(mCr, CURR_LOCKPAPER, newPath);
            }
            addFileObserver(drmObj, wrapper);

            if (rigthStatus.time > 0) {
                DrmUtil.logd("Send delay message!" + rigthStatus.time);
                Message msg = drmTaskHandler.obtainMessage(drmObj.drmType + MSG_EXPIRE_INTERVAL);
                msg.obj = drmObj;
                drmTaskHandler.sendMessageDelayed(msg, rigthStatus.time);
            } else {
                DrmUtil.logd("No time limit! dont send delay time!");
            }
        }
    }

    private void addFileObserver(DrmObj drmObj, Wrapper wrapper) {
        /** get the new file dir. */
        String[] pathName = DrmUtil.parserPath(drmObj.path);
        drmObj.name = pathName[1];
        /** initialize the file observer. */
        DrmFileObserver drmObserver = null;
        if (wrapper != null && wrapper.observer.mPath.equals(pathName[0])) {
            drmObserver = wrapper.observer;
            drmObserver.addDrmObj(drmObj);
            DrmUtil.logd("Use the current observer!");
        } else {
            if (wrapper != null)
                stopObserver(wrapper.observer);
            drmObserver = mObserversMap.get(pathName[0]);
            if (drmObserver == null) {
                drmObserver = DrmFileObserver.getInstance(pathName[0], drmTaskHandler);
                mObserversMap.put(pathName[0], drmObserver);
                drmObserver.addDrmObj(drmObj);
                drmObserver.startWatching();
                DrmUtil.logd("Don't find the same Observer! create one! Observer:" + drmObserver);
            } else {
                drmObserver.addDrmObj(drmObj);
                DrmUtil.logd("Find an same Observer! :" + drmObserver);
            }
        }
    }

    private void stopObserver(DrmFileObserver observer) {
        if (observer.isEmpty()) {
            mObserversMap.remove(observer.mPath);
            observer.stopWatching();
            DrmUtil.logd("The observer is empty! so stop watching!" + observer);
        }
    }

    private Wrapper getWrapperForType(int type) {
        Wrapper wrapper = null;
        Collection<DrmFileObserver> observers = mObserversMap.values();
        DrmUtil.logd("getWrapperForType type="+type+",and observers="+observers.toString());
        DrmObj drm;
        for (DrmFileObserver fileObserver : observers) {
            drm = fileObserver.getDrmObj(type);
            if (drm != null) {
                wrapper = new Wrapper();
                wrapper.observer = fileObserver;
                wrapper.name = drm.name;
                return wrapper;
            }
        }
        return wrapper;
    }

    public static class DrmObj {
        public DrmObj() {
        }

        public DrmObj(String filePath) {
            this.path = filePath;
        }

        String path;
        public String uriString;
        public int drmType;
        public String name;

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder("Path:");
            if (path != null) {
                sb.append(path);
            } else {
                sb.append("NULL");
            }
            sb.append("|RingType:");
            sb.append(" |type:" + drmType);
            sb.append(" |name:" + name);
            return sb.toString();
        }

        @Override
        public boolean equals(Object other) {
            if (other == null) {
                return false;
            }
            if (other == this) {
                return true;
            }
            DrmObj otherDrm = (DrmObj) other;
            if (otherDrm.drmType == otherDrm.drmType && otherDrm.path.equals(otherDrm.path)) {
                return true;
            } else {
                return false;
            }
        }
    }

    private class DrmTaskHandler extends Handler {
        public DrmTaskHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            if (unmounted || shutdown) {
                DrmUtil.logd("Unmounted  don't handle all message!");
                return;
            }
            int what = msg.what;
            DrmObj drmObj = (DrmObj) msg.obj;
            TctLog.d(TAG, "msg waht:" + what + "|drmObj:" + drmObj);
            switch (what) {
                case MSG_PHONE_REBOOT:
                    startRebootCheck();
                    break;
                case MSG_DRM_RING_CHECK:
                    DrmUtil.logd("---- MSG_DRM_RING_CHECK --");
                    checkDrmObjStatus(drmObj);
                    break;
                case MSG_DRM_WALLPAPER_CHECK:
                    DrmUtil.logd("---- MSG_DRM_WALLPAPER_CHECK --");
                    checkDrmObjStatus(drmObj);
                    break;
                case MSG_DRM_NOTIFICATION_SOUND_EXPIRE:
                case MSG_DRM_ALARM_ALERT_EXPIRE:
                case MSG_DRM_RINGTONE_EXPIRE:
                    DrmUtil.logd("---- MSG_DRM_RINGTONE_EXPIRE --");
                    handDrmObjFileEvent(drmObj, false);
                    break;
                case MSG_DRM_WALLPAPER_EXPIRE:
                    DrmUtil.logd("---- MSG_DRM_WALLPAPER_EXPIRE --");
                    handDrmObjFileEvent(drmObj, false);
                    break;
                case MSG_DRM_WALLPAPER_FILE_DELETE:
                    DrmUtil.logd("---- MSG_DRM_WALLPAPER_FILE_DELETE --");
                    handDrmObjFileEvent(drmObj, false);
                    break;
                case MSG_DRM_ALARM_ALERT_DELETE:
                case MSG_DRM_NOTIFICATION_SOUND_DELETE:
                case MSG_DRM_RINGTONE_DELETE:
                    DrmUtil.logd("---- MSG_DRM_RINGTONE_DELETE --");
                    handDrmObjFileEvent(drmObj, false);
                    break;
                case MSG_MEDIA_UNMOUNT:
                    DrmUtil.logd("---- MSG_MEDIA_UNMOUNT --");
                    if (unmounted == true) {
                        return;
                    }
                    unmounted = true;
                    handMediaUnmount();
                    break;
                case MSG_DRM_LOCKPAPER_FILE_DELETE:
                    DrmUtil.logd("---- MSG_DRM_LOCKPAPER_FILE_DELETE --");
                    handDrmObjFileEvent(drmObj, false);
                    break;
                case MSG_DRM_LOCKPAPER_EXPIRE:
                    DrmUtil.logd("---- MSG_DRM_LOCKPAPER_EXPIRE --");
                    handDrmObjFileEvent(drmObj, true);
                    break;
                case MSG_DRM_LOCKPAPER_CHECK:
                    DrmUtil.logd("---- MSG_DRM_LOCKPAPER_CHECK --");
                    checkDrmObjStatus(drmObj);
                    break;
                default:
                    break;
            }
        }
    }

    private void handMediaUnmount() {
        Collection<DrmFileObserver> observers = mObserversMap.values();
        if (observers.isEmpty()) {
            return;
        }
        Collection<DrmObj> drmObjs = null;
        for (DrmFileObserver drmObserver : observers) {
            drmObserver.stopWatching();
            drmObjs = drmObserver.removeAllDrmObj();
            for (DrmObj drmObj : drmObjs) {
                DrmUtil.logd("Unmount----> reset about the drm Obj:" + drmObj);
                drmTaskHandler.removeMessages(drmObj.drmType + MSG_DELETE_INTERVAL);
                drmTaskHandler.removeMessages(drmObj.drmType + MSG_EXPIRE_INTERVAL);
                if (drmObj.drmType == PATH_TYPE_WALLPAPER) {
                    resetWallpaper(true);
                    showWallperToast(drmObj, false);
                } else if(drmObj.drmType == PATH_TYPE_LOCKPAPER) {
                    DrmUtil.logd("isLockPaper handMediaUnmount ..");
                    doResetLockScreen();
                    showWallperToast(drmObj, false);
                } else {
                    resetRing(drmObj);
                    showRingToast(drmObj, false);
                }
            }
            drmObjs.clear();
        }
        mObserversMap.clear();
    }

    private void handDrmObjFileEvent(DrmObj drmObj, boolean isExpire) {
        String[] pathName = DrmUtil.parserPath(drmObj.path);
        DrmUtil.logd("handle drm file expire:" + isExpire + "| drmObj:" + drmObj);
        DrmFileObserver fileObserver = null ;
        if((fileObserver= mObserversMap.get(pathName[0])) != null){
            if(!fileObserver.containFile(pathName[1])){
                fileObserver = null;
            }
        }
        boolean isWallpaper = (drmObj.drmType == PATH_TYPE_WALLPAPER);
        boolean isLockPaper = (drmObj.drmType == PATH_TYPE_LOCKPAPER);
        if (fileObserver == null) {
            if (!isExpire) {
                TctLog.e(TAG, "Logic !!!!! Get the path's observer,but it's NULL!" + drmObj);
                TctLog.e(TAG, "Observer-Map:" + mObserversMap);
                return;
            }
        }
        String currValues = null;
        if (isWallpaper) {
            currValues = Settings.System.getString(mCr, TctDrmStore.CURR_WALLPAPER_DRMPATH);
            DrmUtil.logd("isWallPaper to get CURR_WALLPAPER_DRMPATH ="+currValues);
        }else if(isLockPaper){
            currValues = Settings.System.getString(mCr, CURR_LOCKPAPER);
            DrmUtil.logd("isLockPaper to get path="+currValues);
        } else {
            /** get the ring current valuse; */
            currValues = getCurrRingValue(drmObj.drmType);
        }
        DrmUtil.logd("Handle an drm file expire:" + isExpire + "| drmObj:" + drmObj.uriString
                + "|currSystemValue:" + currValues);
        if (fileObserver != null && (currValues == null || !currValues.equals(drmObj.uriString))) {
            DrmUtil.logd("The expire/delete drm file are not current! remove the drm observer!");
            fileObserver.removeDrmObj(drmObj.drmType, drmObj.name);
            stopObserver(fileObserver);
            return;
        }
        /** if it is delete message.should remove the expire message. */
        if (!isExpire && fileObserver != null) {
            DrmUtil.logd(" to stop the Listener ...isWallpaper="+isWallpaper+",isLockPaper="+isLockPaper);
            forceEndDrmMonitor(drmObj, isExpire, fileObserver, isWallpaper,isLockPaper,true);
            drmTaskHandler.removeMessages(drmObj.drmType + MSG_EXPIRE_INTERVAL);
            DrmUtil.logd("The delete msg handler done!" + drmObj);
            return;
        }
        /** here the expire drm obj ,check it has new rights. */
        RightsStatus rightStatus = getConstaintStatus(drmObj.path,
                ((isWallpaper||isLockPaper) ? DrmStore.Action.DISPLAY : DrmStore.Action.PLAY));
        if (rightStatus.status == RIGHTS_INVALID || rightStatus.status == RIGHTS_VALID_ONLY_COUNT) {
            if(fileObserver != null){
            forceEndDrmMonitor(drmObj, true, fileObserver, isWallpaper,isLockPaper,false);
            drmTaskHandler.removeMessages(drmObj.drmType + MSG_DELETE_INTERVAL);
            } else {
                if (!(isWallpaper||isLockPaper)) {
                    DrmUtil.logd("Drm Obj expire message come! but not in observer! drmObj:" +drmObj);
                    resetMediaRingFlag(drmObj);
                }
            }
        } else {
            /** right is ok. just check need send the message or not */
            if (rightStatus.time > 0) {
                Message msg = drmTaskHandler.obtainMessage(drmObj.drmType + MSG_EXPIRE_INTERVAL);
                msg.obj = drmObj;
                drmTaskHandler.sendMessageDelayed(msg, rightStatus.time);
            }
            DrmUtil.logd("The drm expired!but it has new rights and new expire time is delay:"
                    + rightStatus.time);
        }
    }

    private void forceEndDrmMonitor(DrmObj drmObj, boolean isExpire, DrmFileObserver fileObserver,
            boolean isWallpaper,boolean isLockPaper,boolean isNeedResetPaper) {
        fileObserver.removeDrmObj(drmObj.drmType, drmObj.name);
        stopObserver(fileObserver);
        if (isWallpaper) {
            resetWallpaper((isNeedResetPaper && isWallpaper));
            showWallperToast(drmObj, isExpire);
        }else if(isLockPaper){
            doResetLockScreen();
            showWallperToast(drmObj, isExpire);
        } else {
            resetRing(drmObj);
            showRingToast(drmObj, isExpire);
        }
    }

    private void showRingToast(DrmObj drmObj, boolean isExpire) {
        if (isExpire) {
            new DrmApp(mContext).showToast(com.android.internal.R.string.drm_ringtone_expired,
                    drmObj.path);
        } else {
            new DrmApp(mContext).showToast(com.android.internal.R.string.drm_ringtone_deleted,
                    drmObj.path);
        }
    }

    private RightsStatus getConstaintStatus(String path, int action) {
        DrmUtil.logd("Get constraintStatus --- path:" + path + "| action:" + action);
        ContentValues cv = mDrmClient.getConstraints(path, action);
        RightsStatus rs = new RightsStatus();
        if (cv == null) {
            rs.status = RIGHTS_INVALID;
            DrmUtil.logd("Get constraintStatus --- exit status = invalid!");
            return rs;
        }

        if (cv.getAsInteger(DrmStore.ConstraintsColumns.REMAINING_REPEAT_COUNT) != null) {
            rs.status = RIGHTS_VALID_ONLY_COUNT;
            DrmUtil.logd("Get constraintStatus --- exit status = only count!");
            return rs;
        }
        String endTimeStr = cv.getAsString(DrmStore.ConstraintsColumns.LICENSE_EXPIRY_TIME);
        String startTimeStr = cv.getAsString(DrmStore.ConstraintsColumns.LICENSE_START_TIME);
        DrmUtil.logd("Get constraintStatus---> startTime:" +startTimeStr);
        DrmUtil.logd("Get constraintStatus---> endTimeStr:" +endTimeStr);
        if (endTimeStr == null) {
            if (startTimeStr == null) {
                DrmUtil.logd("Get constraintStatus --- NO_LIMIT_TIME!");
                rs.status = RIGHTS_VALID_NO_LIMIT_TIME;
            } else if (isStartTiemValid(startTimeStr)) {
                rs.status = RIGHTS_VALID_NO_LIMIT_TIME;
                DrmUtil.logd("Get constraintStatus ---isStartTiemValid() True NO_LIMIT_TIME!");
            } else {
                rs.status = RIGHTS_INVALID;
                DrmUtil.logd("Get constraintStatus ---isStartTiemValid() False INValid!");
            }
        } else {
            long delay = DrmUtil.getAvaliableTime(endTimeStr);
            if (delay > 0) {
                rs.status = RIGHTS_VALID_AVALIABLE_TIME;
                rs.time = delay;
            } else {
                rs.status = RIGHTS_INVALID;
                DrmUtil.logd("Get constraintStatus ---delay < 0 inVALID!");
            }
        }
        DrmUtil.logd("Get constraintStatus --- exit status = " + rs.status 
                + "|delay:time" + rs.time);
        return rs;
    }

    private boolean isStartTiemValid(String startTimeStr) {
        long delay = DrmUtil.getAvaliableTime(startTimeStr);
        if (delay < 0) {
            return true;
        }
        return false;
    }

    private void showWallperToast(DrmObj drmObj, boolean isExpire) {
        if (isExpire) {
            new DrmApp(mContext).showToast(com.android.internal.R.string.drm_wallpaper_expired,
                    drmObj.path);
        } else {
            new DrmApp(mContext).showToast(com.android.internal.R.string.drm_wallpaper_deleted,
                    drmObj.path);
        }
    }

    private void resetWallpaper(boolean isNeedResetPaper) {
        int flagLockScreen = 0;
        try {
            flagLockScreen = Settings.System.getInt(mCr, TctDrmStore.DRMFILE_LOCKSCREEN);
        } catch(SettingNotFoundException e) {
            e.printStackTrace();
        }
        TctLog.d("DrmUtil","flagLockScreen = "+flagLockScreen);

        if (flagLockScreen == 1) {
            doResetLockScreen();
        } else if(flagLockScreen == 2) {
            doResetLockScreen();
            doResetWallPaper();
        } else if((flagLockScreen == 3) || (isNeedResetPaper)) {
            doResetWallPaper();
        }
    }

    private void doResetWallPaper() {
        WallpaperManager wm = WallpaperManager.getInstance(mContext);
        try {
            wm.setResource(com.android.internal.R.drawable.default_wallpaper);
        } catch (IOException e) {
            e.printStackTrace();
        }
        DrmUtil.logd("isWallPaper  doResetWallPaper to get CURR_WALLPAPER_DRMPATH to \" \"");
        Settings.System.putString(mCr, TctDrmStore.CURR_WALLPAPER_DRMPATH, "");
    }

    private void doResetLockScreen() {
        int i = 0 ;
        while (i< SUFFIX_NAME.length) {
             String lockScreenPath = SDPATH + SDDIR + SDFILE_NAME+ SUFFIX_NAME[i];
             File lockFile = new File(lockScreenPath);
             if (lockFile.exists()) {
                 lockFile.delete();
                 break;
             }
        }

        Intent weatherIntent = new Intent("com.android.jrdcom.lockscreen_wallpaper_changed");
        if (null != mContext) mContext.sendBroadcast(weatherIntent);
        Settings.System.putInt(mCr,TctDrmStore.DRMFILE_LOCKSCREEN, 0);
        Settings.System.putString(mCr, CURR_LOCKPAPER, "");
    }

    public void startRebootCheck() {
        String path = Settings.System.getString(mCr, TctDrmStore.CURR_WALLPAPER_DRMPATH);
        String lockPath = Settings.System.getString(mCr, CURR_LOCKPAPER);
        if ((path != null && path.length() != 0) || (lockPath != null && lockPath.length() != 0)) {
            DrmUtil.logd("----ERROR----path="+path+",lockPath="+lockPath);
        }

        Collection<DrmFileObserver> observers = mObserversMap.values();
        DrmUtil.logd("startRebootCheck,get the All Observers. observers = " + observers);
        DrmObj drm =null;
        DrmObj lockDrm=null;
        for (DrmFileObserver fileObserver:observers) {
            drm = fileObserver.getDrmObj(PATH_TYPE_WALLPAPER);
            lockDrm = fileObserver.getDrmObj(PATH_TYPE_LOCKPAPER);
        }
        DrmUtil.logd("startRebootCheck,drm = " + drm + "  lockDrm = " + lockDrm);
        sendDrmWallpaerCheck(path);
        sendDrmLockPaperCheck(lockPath);
        String[] ringtypes = {
                Settings.System.RINGTONE, Settings.System.ALARM_ALERT,
                Settings.System.NOTIFICATION_SOUND
        };
        for (String ringType : ringtypes) {
            sendRebootRingChecker(mContext, this, ringType);
        }
    }

    public boolean sendRebootRingChecker(Context context, DrmTrackerAdapter trackerAdapter,
            String ringType) {
        String path;
        String uriString = Settings.System.getString(context.getContentResolver(), ringType);
        path = DrmUtil.getPathFromUri(context, uriString);
        if (uriString != null && !uriString.isEmpty() && path == null) {
            return false;
        }

        if (path != null && !path.isEmpty() && isDrm(path)) {
            DrmUtil.logd("will sendRebootRing msg --");
            sendDrmRingCheck(ringType, uriString, path);
        }
        return true;
    }

    public Message sendDrmRingCheck(String ringType, String uriString, String path) {
        Message msg;
        DrmObj drmObj;
        drmObj = new DrmObj();
        drmObj.uriString = uriString;
        drmObj.path = path;
        drmObj.drmType = DrmUtil.getDrmObjType(ringType);
        msg = drmTaskHandler.obtainMessage(MSG_DRM_RING_CHECK);
        msg.obj = drmObj;
        drmTaskHandler.sendMessageAtFrontOfQueue(msg);
        return msg;
    }

    public void sendDrmWallpaerCheck(String startDrmPath) {
        Message msg;
        DrmObj drmObj;
        msg = drmTaskHandler.obtainMessage(MSG_DRM_WALLPAPER_CHECK);
        drmObj = new DrmObj(startDrmPath);
        drmObj.drmType = PATH_TYPE_WALLPAPER;
        drmObj.uriString = startDrmPath;
        drmObj.path = startDrmPath;
        msg.obj = drmObj;
        drmTaskHandler.sendMessageAtFrontOfQueue(msg);
    }

    public void sendDrmLockPaperCheck(String startDrmPath){
        Message msg;
        DrmObj drmObj;
        msg = drmTaskHandler.obtainMessage(MSG_DRM_LOCKPAPER_CHECK);
        drmObj = new DrmObj(startDrmPath);
        drmObj.drmType = PATH_TYPE_LOCKPAPER;
        drmObj.uriString = startDrmPath;
        drmObj.path = startDrmPath;
        msg.obj = drmObj;
        drmTaskHandler.sendMessageAtFrontOfQueue(msg);
    }

    public void mounted() {
        unmounted = false;
        DrmUtil.logd("Media is mounted!");
    }

    public Message obtainMessage(int what) {
        return drmTaskHandler.obtainMessage(what);
    }

    public void sendMessageAtFrontOfQueue(Message msg) {
        drmTaskHandler.sendMessage(msg);
    }

    public void sendMessage(Message msg) {
        drmTaskHandler.sendMessage(msg);
    }

    public void sendEmptyMessage(int what) {
        Message msg = drmTaskHandler.obtainMessage(what);
        drmTaskHandler.sendMessage(msg);
    }

    public void sendMessageDelayed(int what, long delay) {
        Message msg = drmTaskHandler.obtainMessage(what);
        drmTaskHandler.sendMessageDelayed(msg, delay);
    }

    public void shutDown() {
        DrmUtil.logd("Phone shut down!");
        shutdown = true;
    }

    public boolean hasShutDown() {
        return shutdown;
    }
}
