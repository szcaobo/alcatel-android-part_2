/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.fingerprintprovider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class FingerprintDbHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "muitifingerprint.db";
    private static final int DATABASE_VERSION = 1;
    private static FingerprintDbHelper mInstance = null;

    public FingerprintDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public synchronized static FingerprintDbHelper getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new FingerprintDbHelper(context);
        }
        return mInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE muiti_fp_ids (" + "_id INTEGER PRIMARY KEY,"
                + "TAG TEXT," + "fingerprintID INTEGER," + "userID INTEGER" + ");"); // MODIFIED by jianguang.sun, 2016-08-19,BUG-2740734
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub

    }

}
