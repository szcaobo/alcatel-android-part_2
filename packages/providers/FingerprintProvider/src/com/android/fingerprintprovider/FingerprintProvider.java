/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.fingerprintprovider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.util.Log;

public class FingerprintProvider extends ContentProvider{

    private static final String ALIAUTHORITY = "fingerprint.information";
    private static final int FP_CLASSIFYS = 1;
    private static final String FPCLASSIFYTABLE = "muiti_fp_ids";
    private String TAG = "FingerprintProvider";
    private FingerprintDbHelper mDbHelper;
    private static final UriMatcher sURLMatcher = new UriMatcher(
            UriMatcher.NO_MATCH);
    static {
        sURLMatcher.addURI(ALIAUTHORITY, "classifys", FP_CLASSIFYS);
    }

    @Override
    public boolean onCreate() {
        mDbHelper = FingerprintDbHelper.getInstance(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        SQLiteDatabase database = mDbHelper.getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        switch (sURLMatcher.match(uri)) {
        case FP_CLASSIFYS:
            qb.setTables(FPCLASSIFYTABLE);
            break;
        default:
            throw new IllegalArgumentException("Unknow Uri:" + uri);
        }
        Cursor ret;
        try {
            ret = qb.query(database, projection, selection, selectionArgs,
                    null, null, sortOrder);
            if (ret == null) {
                return null;
            }
            ret.setNotificationUri(getContext().getContentResolver(), uri);
        } catch (RuntimeException e) {
            Log.e(TAG, " query failed: uri=" + uri);
            return null;
        }
        return ret;
    }

    @Override
    public String getType(Uri uri) {
        switch (sURLMatcher.match(uri)) {
        case FP_CLASSIFYS:
            return "vnd.android.cursor.dir/fingerprint.information";
        default:
            throw new IllegalArgumentException("Unknow Uri:" + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        switch (sURLMatcher.match(uri)) {
        case FP_CLASSIFYS:
            long rowId = db.insert(FPCLASSIFYTABLE, null, values);
            if (rowId > 0) {
                Uri itemUri = ContentUris.withAppendedId(uri, rowId);
                getContext().getContentResolver().notifyChange(itemUri, null);
                return itemUri;
            }
            break;
        default:
            throw new IllegalArgumentException("Unknow Uri:" + uri);
        }
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        int num = 0;
        switch (sURLMatcher.match(uri)) {
        case FP_CLASSIFYS:
            num = db.delete(FPCLASSIFYTABLE, selection, selectionArgs);
            break;
        default:
            throw new IllegalArgumentException("Unknow Uri:" + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return num;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        int num = 0;
        switch (sURLMatcher.match(uri)) {
        case FP_CLASSIFYS:
            num = db.update(FPCLASSIFYTABLE, values, selection, selectionArgs);
            break;
        default:
            throw new IllegalArgumentException("Unknow Uri:" + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return num;
    }

}
