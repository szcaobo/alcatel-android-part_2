package com.android.providers.contacts.util;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.ContactsContract.PhoneLookup;
import android.provider.ContactsContract.RawContacts;
import android.util.Log;
import com.android.providers.contacts.R;

public class PresetContactUtil {

    private static final String TAG = "PresetContactUtil";

       private static byte[] compressBitmap(Bitmap bitmap) {
      ByteArrayOutputStream baos = new ByteArrayOutputStream(4 * (bitmap.getWidth() * bitmap.getHeight()));
      try {
          bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
          baos.flush();
          baos.close();
          return  baos.toByteArray();
      } catch (Exception e) {
          android.util.Log.e(TAG, "Unable to serialize photo: " + e.getMessage());
      }
      return null;
    }

    private static void insertAdnToDatabase(Context context, String displayName, String phoneNumber) {
        ContentResolver resolver = context.getContentResolver();
        final ArrayList<ContentProviderOperation> operationList = new ArrayList<ContentProviderOperation>();
        ContentProviderOperation.Builder builder = ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI);
        builder.withValue("account_name", "PHONE");
        builder.withValue("account_type", "com.android.localphone");
        builder.withValue("display_name", displayName);
        builder.withValue("aggregation_mode", Integer.valueOf(3));
        operationList.add(builder.build());

        ContentProviderOperation.Builder nameBuilder = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
        nameBuilder.withValueBackReference("raw_contact_id", 0);
        nameBuilder.withValue("mimetype", "vnd.android.cursor.item/name");
        nameBuilder.withValue("data3", displayName);
        operationList.add(nameBuilder.build());

        ContentProviderOperation.Builder phoneBuilder = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
        phoneBuilder.withValueBackReference("raw_contact_id", 0);
        phoneBuilder.withValue("mimetype", "vnd.android.cursor.item/phone_v2");
        phoneBuilder.withValue("data2", Integer.valueOf(2));
        phoneBuilder.withValue("data1", phoneNumber);
        phoneBuilder.withValue("is_super_primary", Integer.valueOf(1));
        operationList.add(phoneBuilder.build());
        try {
            resolver.applyBatch(ContactsContract.AUTHORITY, operationList);
            return;
        } catch (Exception e) {
            android.util.Log.e(TAG, "insertAdnToDatabase() -- " + e.getMessage());
            return;
        }
    }

    private static ArrayList<PresetContact> mPresetContacts = new ArrayList<PresetContact>();
    private static class PresetContact {
        private String presetName;
        private String presetNumber;
        public PresetContact(String name, String number) {
            this.presetName = name;
            this.presetNumber = number;
        }
    }

    private static void initPresetContact(Context context) {
        mPresetContacts.clear();
        /*              SDMID:
         *              feature_contactsprovider_preset_on
                        def_digitel_moc_service_x(x,1~20)
         * */
        if (context.getResources().getBoolean(R.bool.feature_contactsprovider_preset_on)){
            android.content.res.Resources res = context.getResources();
            String[] info;
            for (int i = 1; i <= 20; i++){
                String identifier = "def_digitel_moc_service_" + i;
                int id = res.getIdentifier(identifier, "string", "com.android.providers.contacts");
                String value = context.getResources().getString(id);
                if (value != null && !value.equals("")){
                    try{
                        info  = value.split(":");
                        PresetContact presetContact = new PresetContact(info[0], info[1]);
                        mPresetContacts.add(presetContact);
                    }catch(Exception e){
                        android.util.Log.d(TAG, "format fatal exception");
                        continue;
                    }
                }
            }
        }
    }

    public static void insertPresetContactIfNotExist(final Context context){
        initPresetContact(context);
        Log.i(TAG, "insertPresetContactIfNotExist(), mPresetContacts.size = " + mPresetContacts.size());
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (PresetContact presetContact : mPresetContacts) {
                    Uri uri = Uri.withAppendedPath(
                            PhoneLookup.CONTENT_FILTER_URI,
                            Uri.encode(presetContact.presetNumber));
                    Log.i(TAG, "getContactInfoByPhoneNumbers(), uri = " + uri);

                    Cursor contactCursor = context.getContentResolver().query(
                            uri,
                            new String[] {PhoneLookup.DISPLAY_NAME,PhoneLookup.PHOTO_ID},
                            null,
                            null,
                            null);
                    try{
                        if (contactCursor != null && contactCursor.getCount() > 0) {
                            Log.i(TAG, "insertPresetContactIfNotExist(), preset contact is exist.........");
                            return;
                        } else {
                            Log.i(TAG, "insertPresetContactIfNotExist(), to insert preset contact");
                            insertAdnToDatabase(context, presetContact.presetName, presetContact.presetNumber);
                        }
                    } catch(Exception e) {
                        android.util.Log.e(TAG, "insertPresetContactIfNotExist()" + e.getMessage());
                    } finally {
                        if (contactCursor != null) {
                            contactCursor.close();
                        }
                    }
                }
            }
        }).start();
    }
}
