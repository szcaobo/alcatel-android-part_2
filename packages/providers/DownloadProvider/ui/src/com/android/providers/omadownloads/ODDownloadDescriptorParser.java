/******************************************************************************/
/*                                                               Date:11/2014 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2012 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  Qiushou.Liu                                                      */
/*  Email  :  Qiushou.Liu@tcl-mobile.com                                       */
/*  Role   :                                                                  */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     :                                                                */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 11/04/2014|qiushou.liu           |     FR736884         |[HOMO][Orange][28]*/
/*           |                      |                      | 11 - DOWNLOAD_01 */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package com.android.providers.downloads.ui.omadownloads;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import android.content.Context;
import android.util.TctLog;
import android.widget.Toast;

import com.android.providers.downloads.ui.R;


/**
 * Class ODDownloadDescriptorParser
 */
public class ODDownloadDescriptorParser {
    /**
     * mObject[0]:the information to show for user mObject[1]:the media object
     * download uri mObject[2]:the installNotifyURI mObject[3]:the media object
     * size mObject[6]:the media NextUrl mObject[7]:the mediaTypes
     * mObject[4]:check the mandatory attributes is missing or not
     * mObject[5]:check the DDVersion attribute is exist or not,if exist,and is
     * be supported by devices,the value is true,other else is false;if not
     * exist,the value is true.
     */
    private Object[] mObject = new Object[9];//[BUGFIX]-Mod by TCTNB.Rui.Liu,03/13/2014,617185,
    //[FEATURE]-Mod-BEGIN by TCTNB.guoju.yao,06/18/2014,704961
    private ODStatusReportor mRequestAndResponseStatus;
    //[FEATURE]-Mod-END by TCTNB.guoju.yao
    /** total download seconds */
    private BigDecimal mDownloadTimeSecondAll;

    /** download hours */
    private BigDecimal mDownloadTimeHour;

    /** download minutes */
    private BigDecimal mDownloadTimeMinute;

    /** download seconds */
    private BigDecimal mDownloadTimeSecond;

    private String[] installNotifyURI;

    // This sb used to save informations which can show for user
    private StringBuffer mSb = new StringBuffer();
    private StringBuffer mMediaTypes = new StringBuffer();
    private String mValue;
    private String mObjectURL;// media object uri
    private String mInstallNotifyURI;// the uri need to post the status report
    private String mDdVersion;// the DDVersion
    private String mNextUrl;// the NextUrl
    private String mObjectName;//[BUGFIX]-Add by TCTNB.Rui.Liu,03/13/2014,617185,
    private Context mContext;

    /**
     * This array means the count of every attribute turns out.
     * count[0]:ObjectURI; count[1]:size; count[2]:type; count[3]:name;
     * count[4]:vendor; count[5]:description; count[6]:installNotifyURI;
     * count[7]:DDVersion
     */
    private int[] mCount = { 0, 0, 0, 0, 0, 0, 0, 0, 0 };

    public ODDownloadDescriptorParser() {
    }

    /**
     * parse the dd file
     *
     * @param f
     *            ---the dd file
     * @param context
     *            ---the context of this Activity
     * @return Object[]---the dd file informations after parsing it
     */
    public Object[] parseXml(Context aContext, final File aFile) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        mContext = aContext;
        //[FEATURE]-Mod-BEGIN by TCTNB.guoju.yao,06/18/2014,704961
        mRequestAndResponseStatus = new ODStatusReportor(mContext);
        //[FEATURE]-Mod-END by TCTNB.guoju.yao
        /**
         * It's start to parse a DD file.A valid DD file must have the <media>
         * attribute as root,and must have three mandatory
         * attributes:objectURI,type,size.if it has a DDVersion attribute,it
         * must be supported by this device;if it has a installNotifyURI
         * attribute,it means the client must have to send a status report to
         * the server.
         */
        try {
            TctLog.d("ParseXml", "file name is: " + aFile.getName());
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(aFile);
            Element root = doc.getDocumentElement();
            TctLog.d("ParseXml", "gen: " + root.getNodeName());

            /**
             * check the file whether a really dd file first,whether has a
             * <media> tag.
             */
            if (root != null
                    && ODDefines.DD_ATTRIBUTE_MEDIA.equals(root.getNodeName())) {
                NodeList children = root.getChildNodes();
                int length = children.getLength();

                // parse the DD file
                findAttributeForNodeList(aContext, children, length);

                // get the download time of a Media Object

                int hoursId = (mDownloadTimeHour != null) ? mDownloadTimeHour
                        .compareTo(new BigDecimal(1)) : 0;
                int minutesId = (mDownloadTimeMinute != null) ? mDownloadTimeMinute
                        .compareTo(new BigDecimal(1)) : 0;
                int secondsId = (mDownloadTimeSecond != null) ? mDownloadTimeSecond
                        .compareTo(new BigDecimal(1)) : 0;
                int hourId = (mDownloadTimeHour != null) ? mDownloadTimeHour
                        .compareTo(new BigDecimal(0)) : 0;
                int minuteId = (mDownloadTimeMinute != null) ? mDownloadTimeMinute
                        .compareTo(new BigDecimal(0)) : 0;
                int secondId = (mDownloadTimeSecond != null) ? mDownloadTimeSecond
                        .compareTo(new BigDecimal(0)) : 0;
                int secondAllId = (mDownloadTimeSecondAll != null) ? mDownloadTimeSecondAll
                        .compareTo(new BigDecimal(0)) : 0;

                /*String estimateHours = hoursId > 0 ? mDownloadTimeHour
                        + aContext.getResources().getString(
                                com.android.internal.R.string.hours)
                        : (hourId > 0 ? mDownloadTimeHour
                                + aContext.getResources().getString(
                                        com.android.internal.R.string.hour)
                                : "");
                String estimateMinutes = minutesId > 0 ? mDownloadTimeMinute
                        + aContext.getResources().getString(
                                com.android.internal.R.string.minutes)
                        : (minuteId > 0 ? mDownloadTimeMinute
                                + aContext.getResources().getString(
                                        com.android.internal.R.string.minute)
                                : "");
                String estimateSenconds = secondsId > 0 ? mDownloadTimeSecond
                        + aContext.getResources().getString(
                                com.android.internal.R.string.seconds)
                        : (secondId > 0 ? mDownloadTimeSecond
                                + aContext.getResources().getString(
                                        com.android.internal.R.string.second)
                                : "");
                String estimateTime = aContext.getResources().getString(
                        R.string.download_confirmation_dlg_content_media_time)
                        + "\n\t"
                        + estimateHours
                        + estimateMinutes
                        + estimateSenconds;
                        */
                // estimated download time
                //mSb.append(secondAllId > 0 ? estimateTime : "");

            } // end if

            mObject[0] = mSb;
            mObject[1] = mObjectURL;
            mObject[2] = mInstallNotifyURI;
            mObject[6] = mNextUrl;
            mObject[7] = mMediaTypes;
            mObject[8] = mObjectName;//[BUGFIX]-Add by TCTNB.Rui.Liu,03/13/2014,617185,

            /**
             * if three mandatory attributes all existed,give the object a true
             * value; other else if need to send a status report,send a
             * "906 Invalid Descriptor";
             */
            if (mCount[0] >= 1 && mCount[1] >= 1 && mCount[2] >= 1) {
                mObject[4] = true;
            } else {
                mObject[4] = false;

                if (mInstallNotifyURI != null) {
                    new Thread(new Runnable() {

                        public void run() {
                            mRequestAndResponseStatus
                                    .sendAndRecevie(
                                            aFile,
                                            mInstallNotifyURI,
                                            ODDefines.INSTALLNOTIFY_REPORT_INVALID_DESCRIPTOR);
                        }
                    }).start();
                }
            }// end if

            /**
             * if the DDVersion is not exist,if the DDVersion exist,check the
             * value whether be supported by this devices if it be
             * supported,give the object[5] a true value,else give a false value
             * and while need to send a status report,send
             * "951 Invalid DDVersion" to the server.
             */
            if (mDdVersion != null) {
                if (checkDDVersion(mDdVersion)) {
                    mObject[5] = true;
                } else {
                    mObject[5] = false;
                    if (mInstallNotifyURI != null) {
                        new Thread(new Runnable() {

                            public void run() {
                                mRequestAndResponseStatus
                                        .sendAndRecevie(
                                                aFile,
                                                mInstallNotifyURI,
                                                ODDefines.INSTALLNOTIFY_REPORT_INVALID_DDVERSION);
                            }
                        }).start();
                    }
                }
            } else {
                mObject[5] = true;
            }// end if
            return mObject;
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            checkDDValidIsNeedTOSend(aFile);
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }// end try

        // return mObject;
        return null;
    }

    private void findAttributeForNodeList(Context aContext, NodeList aChildren,
            int aLength) {
        for (int i = 0; i < aLength; i++) {
            Node child = aChildren.item(i);

            if (child instanceof Element) {
                String childname = child.getNodeName();
                Node d = child.getFirstChild();
                mValue = d != null ? d.getNodeValue().trim(): null;

                int id = 0;
                id = ODDefines.DD_ATTRIBUTE_OBJECTURL.equals(childname.trim()) ? 1
                        : id; // ObjectURL
                id = ODDefines.DD_ATTRIBUTE_SIZE.equals(childname.trim()) ? 2
                        : id; // Size
                id = ODDefines.DD_ATTRIBUTE_TYPE.equals(childname.trim()) ? 3
                        : id; // Type
                id = ODDefines.DD_ATTRIBUTE_NAME.equals(childname.trim()) ? 4
                        : id; // Name
                id = ODDefines.DD_ATTRIBUTE_VENDOR.equals(childname.trim()) ? 5
                        : id; // Vendor
                id = ODDefines.DD_ATTRIBUTE_DESCRIPTION
                        .equals(childname.trim()) ? 6 : id; // Description
                id = ODDefines.DD_ATTRIBUTE_INSTALLNOTIFYURI.equals(childname
                        .trim()) ? 7 : id; // InstallNotifyURI
                id = ODDefines.DD_ATTRIBUTE_DDVERSION.equals(childname.trim()) ? 8
                        : id; // DDVersion
                id = ODDefines.DD_ATTRIBUTE_NEXTURL.equals(childname.trim()) ? 9
                        : id; // nextURL
                id = ODDefines.DD_ATTRIBUTE_INFOURL.equals(childname.trim()) ? 10
                        : id; // infoURL
                id = ODDefines.DD_ATTRIBUTE_ICONURL.equals(childname.trim()) ? 11
                        : id; // iconURL
                id = ODDefines.DD_ATTRIBUTE_INSTALLPARAM.equals(childname
                        .trim()) ? 12 : id; // installParam

                switch (id) {
                case 1: // get the ObjectURL of MediaObject
                    mCount[0]++;

                    if (mCount[0] == 1) {
//[BUGFIX]-Add-BEGIN by TCTNB.Rui.Liu,11/20/2013,PR556224,
//reason [OMA Download][Forceclose]Media force close when url in DD file include Illegal character
                        if (!isValidURL(mValue)) {
                            continue;
                           }
//[BUGFIX]-Add-END by TCTNB.Rui.Liu
                        mObjectURL = mValue;
                    }

                    break;
                case 2: // get the Size of MediaObject
                    mCount[1]++;

                    if (mCount[1] == 1) {

                        if (mValue != null) {

                            try {

                                TctLog.d("ParseXml", "childname: " + childname);
                                TctLog.d("ParseXml", "mValue: " + mValue);
                                TctLog.d("ParseXml", "id: " + id);

                                String str = "^[1-9]\\d*$";
                                Pattern pattern = Pattern.compile(str);
                                Matcher matcher = pattern.matcher(mValue);

                                if (!matcher.matches()) {
                                    TctLog.d("ParseXml", "the size is not number!");
                                    continue;
                                }

                                BigDecimal in = new BigDecimal(mValue);
                                BigDecimal mas = new BigDecimal(55000.362);
                                BigDecimal dd = in.divide(mas, 0,
                                        BigDecimal.ROUND_CEILING);

                                // Integer in = Integer.valueOf(mValue);
                                // Double dd = Math.ceil(in / 60086.924);
                                // mDownloadTimeSecondAll = dd.intValue();
                                mDownloadTimeSecondAll = dd;
                                mDownloadTimeSecond = mDownloadTimeSecondAll;

                                int hourId = mDownloadTimeSecond.subtract(
                                        new BigDecimal(3600)).compareTo(
                                        new BigDecimal(0));
                                mDownloadTimeHour = hourId > 0 ? mDownloadTimeSecond
                                        .divideToIntegralValue(new BigDecimal(
                                                3600)) : new BigDecimal(0);

                                int minuteId = mDownloadTimeSecond.subtract(
                                        new BigDecimal(3600)).compareTo(
                                        new BigDecimal(0));
                                mDownloadTimeMinute = minuteId > 0 ? mDownloadTimeSecond
                                        .remainder(new BigDecimal(3600))
                                        .divideToIntegralValue(
                                                new BigDecimal(60))
                                        : mDownloadTimeSecond
                                                .divideToIntegralValue(new BigDecimal(
                                                        60));

                                int secondId = mDownloadTimeSecond
                                        .compareTo(new BigDecimal(1));
                                mDownloadTimeSecond = secondId < 0 ? new BigDecimal(
                                        1) : mDownloadTimeSecond
                                        .remainder(new BigDecimal(60));

                                BigDecimal in2 = in.divide(
                                        new BigDecimal(1024), 1,
                                        BigDecimal.ROUND_CEILING);
                                BigDecimal in3 = in2
                                        .multiply(new BigDecimal(10));
                                BigDecimal ini = in3.divide(new BigDecimal(10),
                                        1, BigDecimal.ROUND_CEILING);

                                String ss = ini.toString();
                                mObject[3] = in; // object_size
                                mSb.append(aContext
                                        .getResources()
                                        .getString(
                                                R.string.download_confirmation_dlg_content_media_size)
                                        .trim()
//[FEATURE]-Mod-BEGIN by TCTNB.xihe.lu,10/23/2013,537084,
//[HOMO][Orange][28] 11 - DOWNLOAD_01
                                        + "\t"
//[FEATURE]-Mod-END by TCTNB.xihe.lu
                                        + ss
                                        + aContext
                                                .getResources()
                                                .getString(
                                                        com.android.internal.R.string.kilobyteShort)
                                                .trim() + "\n");
                            } catch (NullPointerException e) {
                                e.printStackTrace();
                            } catch (ArithmeticException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    break;
                case 3: // get the type of MediaObject
                    mCount[2]++;
                    mMediaTypes.append(mValue + ",");
                    mSb.append(aContext
                            .getResources()
                            .getString(
                                    R.string.download_confirmation_dlg_content_media_type)
                            .trim()
//[FEATURE]-Mod-BEGIN by TCTNB.xihe.lu,10/23/2013,537084,
//[HOMO][Orange][28] 11 - DOWNLOAD_01
                            + "\t" + mValue + "\n");
//[FEATURE]-Mod-END by TCTNB.xihe.lu
                    break;
                case 4: // get the name of MediaObject
                    mCount[3]++;

                    if (mCount[3] == 1) {
                        mSb.append(aContext
                                .getResources()
                                .getString(
                                        R.string.download_confirmation_dlg_content_media_name)
                                .trim()
//[FEATURE]-Mod-BEGIN by TCTNB.xihe.lu,10/23/2013,537084,
//[HOMO][Orange][28] 11 - DOWNLOAD_01
                                + "\t" + mValue + "\n");
//[FEATURE]-Mod-END by TCTNB.xihe.lu
                    }
                    mObjectName = mValue;//[BUGFIX]-Add by TCTNB.Rui.Liu,03/13/2014,617185,
                    break;
                case 5: // get the vendor of MediaObject
                    mCount[4]++;

                    if (mCount[4] == 1) {
                        mSb.append(aContext
                                .getResources()
                                .getString(
                                        R.string.download_confirmation_dlg_content_media_operator)
                                .trim()
//[FEATURE]-Mod-BEGIN by TCTNB.xihe.lu,10/23/2013,537084,
//[HOMO][Orange][28] 11 - DOWNLOAD_01
                                + "\t" + mValue + "\n");
//[FEATURE]-Mod-END by TCTNB.xihe.lu
                    }

                    break;
                case 6: // get the description of MediaObject
                    mCount[5]++;

                    if (mCount[5] == 1) {
                        mSb.append(aContext
                                .getResources()
                                .getString(
                                        R.string.download_confirmation_dlg_content_media_des)
                                .trim()
//[FEATURE]-Mod-BEGIN by TCTNB.xihe.lu,10/23/2013,537084,
//[HOMO][Orange][28] 11 - DOWNLOAD_01
                                + "\t" + mValue + "\n");
//[FEATURE]-Mod-END by TCTNB.xihe.lu
                    }

                    break;
                case 7: // get the installNotifyURI of MediaObject
                    mCount[6]++;

                    if (mCount[6] == 1) {
//[BUGFIX]-Add-BEGIN by TCTNB.Rui.Liu,11/20/2013,PR556224,
//reason [OMA Download][Forceclose]Media force close when url in DD file include Illegal character
                        if (!isValidURL(mValue)) {
                            continue;
                           }
//[BUGFIX]-Add-END by TCTNB.Rui.Liu
                        mInstallNotifyURI = mValue;
                    }

                    break;
                case 8: // get the DDVerion of MediaObject
                    mCount[7]++;

                    if (mCount[7] == 1) {
                        mDdVersion = mValue;
                    }

                    break;

                case 9: // get the NextUrl of MediaObject
                    mCount[8]++;

                    if (mCount[8] == 1) {
//[BUGFIX]-Add-BEGIN by TCTNB.Rui.Liu,11/20/2013,PR556224,
//reason [OMA Download][Forceclose]Media force close when url in DD file include Illegal character
                        if (!isValidURL(mValue)) {
                            continue;
                           }
//[BUGFIX]-Add-END by TCTNB.Rui.Liu
                        mNextUrl = mValue;
                    }

                    break;

                default:
                    break;
                }// end switch
            }// end if
        }// end for
    }

//[BUGFIX]-Add-BEGIN by TCTNB.Rui.Liu,11/20/2013,PR556224,
//reason [OMA Download][Forceclose]Media force close when url in DD file include Illegal character
    /**
     * check URL is normal or not.
     * if normal,return true else false.
     */
    private boolean isValidURL(String url){
           //[BUGFIX]-Add-BEGIN by TCTNJ.(guoju.yao),12/16/2013, PR-570979,
           //[Download]Media has stop when download "miss_ObjectUrl"
           if(url == null) {
               return true;
           }
           //[BUGFIX]-Add-END  by TCTNJ.(guoju.yao)
           Pattern pattern = Pattern.compile(ODDefines.VALID_URL_REGULAR_EXPRESSION);
           Matcher matcher = pattern.matcher(url.toLowerCase());
           return matcher.matches();
    }
//[BUGFIX]-Add-END by TCTNB.Rui.Liu
    /**
     * check DDVersion is exist or not.if exist,check is it be supported by the
     * device.
     */
    public boolean checkDDVersion(String aDdVersion) {
        String[] majorminor = null;

        /**
         * the DDVersion value spited by ".",and get the first part, if it is
         * equal 1 means it can be supported by the device Our device can
         * support "1.0" version.
         */
        if (aDdVersion.contains(".")) {
            majorminor = aDdVersion.split("\\.");
            if (Integer.parseInt(majorminor[0]) == 1) {
                return true;
            }
        }
        return false;
    }

    /**
     * while the DD file has syntax error,if there is a <installNotifyURI> tag,
     * send a status code "906 Invalid Descriptor",delete the DD file at the
     * same time.
     *
     * @param aFile
     *            :DD file
     */
    public void checkDDValidIsNeedTOSend(final File aFile) {
        FileReader fileReader;
        String tempBuffer = null;

        try {
            fileReader = new FileReader(aFile);
            BufferedReader reader = new BufferedReader(fileReader);
            String line;

            // get the content of DD file
            try {
                String tempString = "";
                while ((line = reader.readLine()) != null) {
                    tempBuffer = tempString + line;
                    tempString = tempBuffer;
                }

                /**
                 * if the content is not null,then check it whether has a
                 * <installNotifyURI> tag; if has it,send a status report
                 * "906 Invalid Descriptor"to the server.
                 */
                if (tempBuffer != null
                        && tempBuffer
                                .contains(ODDefines.DD_INSTALLNOTIFYURI_ATTRIBUTE_START_FLAG)) {
                    String[] temp = tempBuffer
                            .split(ODDefines.DD_INSTALLNOTIFYURI_ATTRIBUTE_START_FLAG);

                    if (temp[1]
                            .contains(ODDefines.DD_INSTALLNOTIFYURI_ATTRIBUTE_END_FLAG)) {
                        installNotifyURI = temp[1]
                                .split(ODDefines.DD_INSTALLNOTIFYURI_ATTRIBUTE_END_FLAG);
                        new Thread(new Runnable() {

                            public void run() {
                                mRequestAndResponseStatus
                                        .sendAndRecevie(
                                                aFile,
                                                installNotifyURI[0],
                                                ODDefines.INSTALLNOTIFY_REPORT_INVALID_DESCRIPTOR);
                            }
                        }).start();

                        if (mContext != null) {
                            Toast.makeText(mContext,
                                    R.string.download_invalid_descriptor,
                                    Toast.LENGTH_SHORT).show();
                        }

                        if (aFile.exists()) {
                            aFile.delete();
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
