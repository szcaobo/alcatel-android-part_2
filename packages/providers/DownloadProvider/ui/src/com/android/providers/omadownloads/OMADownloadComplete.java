/******************************************************************************/
/*                                                               Date:11/2014 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2012 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  Qiushou.Liu                                                      */
/*  Email  :  Qiushou.Liu@tcl-mobile.com                                       */
/*  Role   :                                                                  */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     :                                                                */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 11/04/2014|qiushou.liu           |     FR736884         |[HOMO][Orange][28]*/
/*           |                      |                      | 11 - DOWNLOAD_01 */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package com.android.providers.downloads.ui.omadownloads;

import java.io.File;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.Environment;
import android.os.StatFs;
import android.preference.PreferenceManager;
import android.provider.Downloads;
import android.text.TextUtils;
import android.util.TctLog;
import android.widget.Toast;
import android.provider.Settings;


/**
 * Class OMADownloadComplete
 */
public class OMADownloadComplete extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent i = intent;
        int status = 0;
        if (i.getAction().equals("android.intent.action.DOWNLOAD_COMPLETE")) {
            long id = i.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
            //PR954996-Li-Zhao begin
            if (id < 0) {
                return;
            }
            //PR954996-Li-Zhao end

            int mFinalStatus=i.getIntExtra("download_finalstatus" , -1);

            Uri uri = ContentUris.withAppendedId(Downloads.Impl.ALL_DOWNLOADS_CONTENT_URI, id);

            Cursor c = context.getContentResolver()
                    .query(uri,
                            new String[] { Downloads.Impl._DATA,
                                    Downloads.Impl.COLUMN_MIME_TYPE }, null,
                            null, null);

            if (c != null) {
                 if(c.getCount()==0){

                } else if(c.moveToFirst()){
                    String fileName = c.getString(0);
                    String mimetype = c.getString(1);
                    c.close();
                    if (fileName != null) {
                        if (!TextUtils.isEmpty(fileName)) {
                            if (ensureIesDD(fileName)) {
                                //[BUGFIX]-Modify-BEGIN-TCTNB.Peng.Cao(07/02/2013)
                                Cursor ddUrlCursor = null;
                                String ddUrlStr="";
                                String notificationPackage="";//PR995716-Li-Zhao
                                try{
                                    ddUrlCursor=context.getContentResolver().query(Downloads.Impl.CONTENT_URI,
                                        new String[]{Downloads.Impl.COLUMN_URI, Downloads.Impl.COLUMN_NOTIFICATION_PACKAGE},//PR995716-Li-Zhao
                                        Downloads.Impl._DATA + " = '" + fileName + "'", null, null);
                                    if(ddUrlCursor!=null && ddUrlCursor.moveToFirst()){
                                        ddUrlStr=ddUrlCursor.getString(0);
                                        notificationPackage = ddUrlCursor.getString(1);//PR995716-Li-Zhao
                                        ddUrlCursor.close();
                                    }else{
                                         ddUrlStr = fileName;
                                    }
                                }catch(SQLiteException e){
                                    TctLog.e("OMADownloadComplete","ddUrlCursor error = "+e);
                                }finally{
                                    if(null != ddUrlCursor)
                                        ddUrlCursor.close();
                                }
                                //PR995716-Li-Zhao begin
                                if ("com.portal".equals(notificationPackage)) {
                                    context.getContentResolver().delete(
                                            Downloads.Impl.ALL_DOWNLOADS_CONTENT_URI,
                                            Downloads.Impl._DATA + " = '"
                                                    + fileName + "'", null);

                                    File ddFile = new File(fileName);
                                    if (ddFile.exists()) {
                                        ddFile.delete();
                                    }
                                } else {
                                    //[BUGFIX]-Modify-END-TCTNB.Peng.Cao(07/02/2013)
                                    Intent it = new Intent();
                                    it.putExtra(ODDefines.DD_FILEPATH_IDENTIFIER,
                                            fileName);
                                    it.putExtra("DDURL", ddUrlStr);
                                    it.putExtra(ODDefines.SD_CARD_AVAILABLE_SIZE,
                                            computeSDcardSpace());
                                    it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_MULTIPLE_TASK);//PR-306358,Modified by yanli.zhang.
                                    it.setClass(context, ODDownloadEngine.class);
                                    context.startActivity(it);
                                    // context.notify();
                                    // OMADownloadComplete.this.clearAbortBroadcast();
                                }
                                //PR995716-Li-Zhao end
                            }
                            else {
                                //[FEATURE]-Mod-BEGIN by TCTNB.guoju.yao,07/14/2014,704961
                                //String strInstallNotifyURL = ODDownloadEngine
                                //        .getInstallNotifyURL();
                                // fuwenliang, Defect: 982588 , begin
                                //String strInstallNotifyURL = Settings.System.getString(context.getContentResolver(), "ODNotifyURI");
                                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
                                String strInstallNotifyURL = sp.getString("ODNotifyURI", null);
                                // fuwenliang, Defect: 982588 , end
                                TctLog.d("OMADownloadComplete","strInstallNotifyURL = "+strInstallNotifyURL);
                                //[FEATURE]-Mod-END by TCTNB.guoju.yao
                                if (strInstallNotifyURL != null) {
                                    //[FEATURE]-Mod-BEGIN by TCTNB.guoju.yao,06/18/2014,704961
                                    new ODStatusReportor(
                                            strInstallNotifyURL,
                                            ODDefines.INSTALLNOTIFY_REPORT_DOWNLOAD_SUCCESS,context)
                                            .start();
                                    //[FEATURE]-Mod-END by TCTNB.guoju.yao
                                }
                            }
                        }
                    } else {
                        TctLog.d("OMADownloadComplete", "Low on space");
                      if(mFinalStatus == Downloads.Impl.STATUS_INSUFFICIENT_SPACE_ERROR )
                       {
                        Toast.makeText(context, context.getResources().getString(
                                com.android.internal.R.string.low_internal_storage_view_title),
                                        Toast.LENGTH_SHORT).show();
                        }
                       }
                }

            }
        } else if (i.getAction().equals(
                "android.intent.action.DOWNLOAD_NOTIFICATION_CLICKED")) {
            long id = i.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
            // Uri uri = ContentUris
            // .withAppendedId(Downloads.Impl.CONTENT_URI, id);
            String packageName = context.getPackageName();
            Intent intentDownload = new Intent(
                    Downloads.Impl.ACTION_NOTIFICATION_CLICKED);
            Uri contentUri = ContentUris.withAppendedId(
                    Downloads.Impl.CONTENT_URI, id);
            // Uri contentUri = ContentUris
            // .withAppendedId(Downloads.Impl.ALL_DOWNLOADS_CONTENT_URI, id);
            intentDownload.setData(contentUri);
            intentDownload.setPackage(packageName);
            context.sendBroadcast(intent);
        }

    }

    // this function is used to
    // ensure a file is a dd file
    public boolean ensureIesDD(String filename) {

        if (filename.contains(".")) {
            String[] nametemp = filename.split("\\.");
            //[PLATFORM]-Mod by TCTNB.(QiuRuifeng), 2013/9/20 PR-504324, reason [ID card][OMA DRM]Need support OMA DRM 1.0 (FW Lock + Separate Delivery).
            if (nametemp[nametemp.length - 1].equals("dd")||nametemp[nametemp.length - 1].equals("dm")) {
                TctLog.i("oma", "this is a dd");
                return true;
            } else {
                TctLog.i("oma", "this is not a dd");
                return false;
            }
        } else {
            TctLog.i("oma", "this is not a dd");
        }
        return false;
    }

    private long computeSDcardSpace() {
        File path = Environment.getExternalStorageDirectory();
        StatFs statFs = new StatFs(path.getPath());
        long blockSize = statFs.getBlockSize();
        long totalBlocks = statFs.getBlockCount();
        long availableBlocks = statFs.getAvailableBlocks();
        return availableBlocks * blockSize;
    }
}
