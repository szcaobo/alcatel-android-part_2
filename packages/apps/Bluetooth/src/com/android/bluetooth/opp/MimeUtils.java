package com.android.bluetooth.opp;

import java.util.HashMap;
import java.util.Map;

public final class MimeUtils {

    private static Map<String, String> mimeTypeMap;

    static {
        mimeTypeMap = new HashMap<String, String>();

        // for audio
        mimeTypeMap.put("mp3", "audio/mp3");
        mimeTypeMap.put("wav", "audio/*");
        mimeTypeMap.put("ogg", "audio/ogg");
        mimeTypeMap.put("mid", "audio/*");
        mimeTypeMap.put("spm", "audio/*");
        mimeTypeMap.put("wma", "audio/x-ms-wma");
        mimeTypeMap.put("amr", "audio/*");
        mimeTypeMap.put("aac", "audio/*");
        mimeTypeMap.put("m4a", "audio/*");
        mimeTypeMap.put("midi", "audio/*");
        mimeTypeMap.put("awb", "audio/amr-wb");
        mimeTypeMap.put("mpga", "audio/mpeg");
        mimeTypeMap.put("xmf", "audio/xmf");
        mimeTypeMap.put("flac", "audio/*");
        mimeTypeMap.put("imy", "audio/*");
        mimeTypeMap.put("diff", "audio/*");
        mimeTypeMap.put("gsm", "audio/*");
        mimeTypeMap.put("ape", "audio/x-ape");

        // for video
        mimeTypeMap.put("avi", "video/*");
        mimeTypeMap.put("wmv", "video/*");
        mimeTypeMap.put("mov", "video/*");
        mimeTypeMap.put("rmvb", "video/*");
        mimeTypeMap.put("mp4", "video/*");
        mimeTypeMap.put("mpeg", "video/*");
        mimeTypeMap.put("3gp", "video/*");
        mimeTypeMap.put("3g2", "video/*");
        mimeTypeMap.put("flv", "video/*");
        mimeTypeMap.put("m4v", "video/*");
        mimeTypeMap.put("mkv", "video/*");
        mimeTypeMap.put("mpg", "video/*");
        mimeTypeMap.put("3gpp", "video/*");

        // for application
        mimeTypeMap.put("sdp", "application/sdp");
        mimeTypeMap.put("jar", "application/java-archive");
        mimeTypeMap.put("jad", "application/java-archive");
        mimeTypeMap.put("zip", "application/zip");
        mimeTypeMap.put("rar", "application/x-rar-compressed");
        mimeTypeMap.put("tar", "application/x-tar");
        mimeTypeMap.put("7z", "application/x-7z-compressed");
        mimeTypeMap.put("gz", "application/x-gzip");
        mimeTypeMap.put("apk", "application/vnd.android.package-archive");
        mimeTypeMap.put("pdf", "application/pdf");
        mimeTypeMap.put("doc", "application/msword");
        mimeTypeMap.put("xls", "application/vnd.ms-excel");
        mimeTypeMap.put("ppt", "application/vnd.ms-powerpoint");
        mimeTypeMap.put("docx", "application/msword");
        mimeTypeMap.put("xlsx", "application/vnd.ms-excel");
        mimeTypeMap.put("pptx", "application/vnd.ms-powerpoint");
        mimeTypeMap.put("eml", "application/eml");
        mimeTypeMap.put("xlsm", "application/vnd.ms-excel");
        mimeTypeMap.put("rtf", "application/msword");
        mimeTypeMap.put("keynote", "application/vnd.ms-powerpoint");
        mimeTypeMap.put("numbers", "application/vnd.ms-powerpoint");

        // for webfile
        mimeTypeMap.put("htm", "text/html");
        mimeTypeMap.put("html", "text/html");
        mimeTypeMap.put("xml", "text/html");
        mimeTypeMap.put("php", "application/vnd.wap.xhtml+xml");
        mimeTypeMap.put("url", "text/html");

        // for image
        mimeTypeMap.put("png", "image/*");
        mimeTypeMap.put("jpg", "image/*");
        mimeTypeMap.put("gif", "image/*");
        mimeTypeMap.put("bmp", "image/*");
        mimeTypeMap.put("jpeg", "image/*");
        mimeTypeMap.put("dm", "image/*");
        mimeTypeMap.put("dcf", "image/*");
        mimeTypeMap.put("wbmp", "image/*");
        mimeTypeMap.put("webp", "image/*");

        // for text
        mimeTypeMap.put("rc", "text/plain");
        mimeTypeMap.put("txt", "text/plain");
        mimeTypeMap.put("sh", "text/plain");
        mimeTypeMap.put("vcf", "text/x-vcard");
        mimeTypeMap.put("vcs", "text/x-vcalendar");
        mimeTypeMap.put("ics", "text/calendar");
        mimeTypeMap.put("ICZ", "text/calendar");
    }

    public static String getMimeTypeFromExtension(String extension) {
        if (extension == null || extension.isEmpty()) {
            return null;
        }

        String type = mimeTypeMap.get(extension);
        return type;
    }
}
