LOCAL_PATH:= $(call my-dir)

#[BUGFIX]-Add-BEGIN by TCTNB.Qianbo.Pan,10/20/2014, PR 764323
#make prebuild
include $(CLEAR_VARS)
LOCAL_PREBUILT_STATIC_JAVA_LIBRARIES := emailcommon:libs/emailcommon.jar
include $(BUILD_MULTI_PREBUILT)
#[BUGFIX]-Add-END by TCTNB.Qianbo.Pan

include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional

LOCAL_SRC_FILES := \
        $(call all-java-files-under, lib/mapapi)

LOCAL_MODULE := bluetooth.mapsapi
LOCAL_MULTILIB := 32
include $(BUILD_STATIC_JAVA_LIBRARY)

include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional


#[FEATURE]-Add-BEGIN by TCTNB.dongdong.gong,10/31/2016,3082231,
#change bluetooth dialog
ifeq ($(SMCN_ROM_CONTROL_FLAG), true)
LOCAL_SRC_FILES := \
        $(call all-java-files-under, src) \
        $(call all-proto-files-under, src) \
        $(call all-java-files-under, lon-src)
else
LOCAL_SRC_FILES := \
        $(call all-java-files-under, src) \
        $(call all-proto-files-under, src) \
        $(call all-java-files-under, norm-src)
endif
#[FEATURE]-Add-END by TCTNB.dongdong.gong

LOCAL_PACKAGE_NAME := Bluetooth
LOCAL_CERTIFICATE := platform

LOCAL_JNI_SHARED_LIBRARIES := libbluetooth_jni
LOCAL_JAVA_LIBRARIES := javax.obex telephony-common libprotobuf-java-micro services.net
LOCAL_STATIC_JAVA_LIBRARIES := com.android.vcard bluetooth.mapsapi sap-api-java-static android-support-v4 services.net

#[FEATURE]-Add-BEGIN by TCTNB.dongdong.gong,10/31/2016,3082231,
#change bluetooth dialog
ifeq ($(SMCN_ROM_CONTROL_FLAG), true)
LOCAL_JAVA_LIBRARIES += mst-framework
endif
#[FEATURE]-Add-END by TCTNB.dongdong.gong

#[BUGFIX]-Mod-BEGIN by TCTNB.Qianbo.Pan,10/20/2014, PR 764323
#LOCAL_STATIC_JAVA_LIBRARIES += com.android.emailcommon
LOCAL_STATIC_JAVA_LIBRARIES += emailcommon
#[BUGFIX]-Mod-END by TCTNB.Qianbo.Pan
LOCAL_PROTOC_OPTIMIZE_TYPE := micro

LOCAL_REQUIRED_MODULES := bluetooth.default
LOCAL_MULTILIB := 32

LOCAL_PROGUARD_ENABLED := disabled

include $(BUILD_PACKAGE)

include $(call all-makefiles-under,$(LOCAL_PATH))
