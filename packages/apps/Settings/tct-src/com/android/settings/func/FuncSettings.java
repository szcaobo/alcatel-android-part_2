package com.android.settings.func;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
/* MODIFIED-BEGIN by song.huan, 2016-10-18,BUG-3088432*/
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.UserHandle;
import android.provider.ContactsContract;
/* MODIFIED-END by song.huan,BUG-3088432*/
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ScrollView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.internal.logging.MetricsProto.MetricsEvent;
import com.android.settings.R;
import com.android.settings.SettingsActivity;
import com.android.settings.SettingsPreferenceFragment;
import com.android.settings.func.adapter.AlternativeShortcutsAdapter;
import com.android.settings.func.adapter.AlternativeShortcutsAdapter.FuncAddListener;
import com.android.settings.func.adapter.DragAdapter;
import com.android.settings.func.adapter.DragAdapter.FuncEditItemListener;
import com.android.settings.func.view.AlternativeShortcutslistView;
import com.android.settings.func.view.DragSortListView;
import com.android.settings.func.view.DragSortListView.RemoveListener;
import com.android.settings.widget.SwitchBar;

import java.util.ArrayList;

public class FuncSettings extends SettingsPreferenceFragment implements
        SwitchBar.OnSwitchChangeListener {

    private static final String TAG = "FuncSettings";
    private static final String CONTACT_NUMBER = "contactNumber"; // MODIFIED by song.huan, 2016-10-18,BUG-3088432
    private SwitchBar mSwitchBar;
    private Context mContext;
    private ContentResolver cr;
    private ArrayList<String> choosedLists;
    private ArrayList<String> alternativeList;
    private ScrollView func_scrollview;
    private TextView func_illustrationtv;
    private DragSortListView dragSortlistView;
    private TextView zeroshowingtv;
    private DragAdapter dragAdapter;
    private AlternativeShortcutslistView alternativeShortcutslistview;
    private AlternativeShortcutsAdapter alternativeShortcutsAdapter;
    private View addAppshortcutstv;
    private static final String SP_NAME = "funcSettings_preferences";
    private static final String SHOWREMOVEFUNCDIALOG = "showRemoveFuncDialog";
    private boolean mChecked;
    private SharedPreferences mSharedPreferences;
    private boolean isShowRemoveFuncDialog;

    private static int MIDDLE_ITEM_NUM = 5;
    private Toast showToast;
    private PackageIntentReceiver mPackageIntentReceiver = null;
    private Handler mHandler = new Handler();
    private boolean mIsSaved = false;

    private class PackageIntentReceiver extends BroadcastReceiver {

        void registerReceiver() {
            IntentFilter filter = new IntentFilter(
                    FuncUtil.UNINSTALLACTION);
            mContext.registerReceiver(this, filter);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            String actionStr = intent.getAction();
            Log.i(TAG, "onReceive actionStr=" + actionStr);
            if (dragSortlistView == null) {
                Log.i(TAG, "onReceive: dragSortlistView has not been initialized yet, just return!");
                return;
            }
            if (null == actionStr) {
                return;
            }
            if (FuncUtil.UNINSTALLACTION.equals(actionStr)) {
                initData();
                setAdapterAndListenners();
            }
        }

        void unregisterReceiver() {
            mContext.unregisterReceiver(this);
        }

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK
                && requestCode == FuncUtil.GOFuncAppsListActivity) {
            Bundle bundle = data.getExtras();
            if (bundle != null) {
                String pkgName = bundle.getString("packageName");
                if (!TextUtils.isEmpty(pkgName)) {
                    choosedLists.add(pkgName);
                    dragAdapter.notifyDataSetChanged();
                    isChoosedListsEmpty();
                    func_scrollview.fullScroll(ScrollView.FOCUS_UP);
                }
            }

        /* MODIFIED-BEGIN by song.huan, 2016-10-18,BUG-3088432*/
        } else if(resultCode == Activity.RESULT_OK
                && requestCode == FuncUtil.PICK_CONTACT_REQUEST) {
            Uri contactUri = data.getData();
            String[] projection = {ContactsContract.CommonDataKinds.Phone.NUMBER};
            try (Cursor cursor = getContentResolver().query(contactUri, projection, null, null, null)){
              if(cursor != null && cursor.moveToFirst()) {
                int column = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                String number = cursor.getString(column);
                Settings.System.putString(cr, CONTACT_NUMBER, number);
              }
            } catch (Exception e) {
              e.printStackTrace();
            }
            /* MODIFIED-END by song.huan,BUG-3088432*/
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        cr = getContentResolver();
        mPackageIntentReceiver = new PackageIntentReceiver();
        mPackageIntentReceiver.registerReceiver();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View result = inflater.inflate(R.layout.funcsettings, container, false);
        initView(result);
        return result;

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        final SettingsActivity activity = (SettingsActivity) getActivity();
        mSwitchBar = activity.getSwitchBar();
        mSwitchBar.setChecked(mChecked);
        mSwitchBar.show();
        initData();
        setAdapterAndListenners();
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            mSwitchBar.addOnSwitchChangeListener(this);
        } catch (Exception e) {
            Log.e(TAG, "Cannot add twice the same OnSwitchChangeListener");
        }

    }

    @Override
    public void onStop() {
        super.onStop();
        try {
            if (mSwitchBar != null)
                mSwitchBar.removeOnSwitchChangeListener(this);
        } catch (IllegalStateException e) {
            Log.e(TAG, "Cannot remove OnSwitchChangeListener");
        }
        if (!mIsSaved) {
            saveFuncSettingData();
        }
        mIsSaved = false;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mSwitchBar.hide();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (null != mPackageIntentReceiver) {
            mPackageIntentReceiver.unregisterReceiver();
            mPackageIntentReceiver = null;
        }
    }

    private void initView(View hostView) {
        func_scrollview = (ScrollView) hostView.findViewById(R.id.func_scrollview);
        func_illustrationtv = (TextView) hostView.findViewById(R.id.func_illustrationtv);
        mSharedPreferences = mContext.getSharedPreferences(SP_NAME,
                Context.MODE_PRIVATE);
        isShowRemoveFuncDialog = mSharedPreferences.getBoolean(
                SHOWREMOVEFUNCDIALOG, true);
        mChecked = Settings.System.getInt(mContext.getContentResolver(),
                Settings.System.TCT_FUNC, 1) == 1;
        if (mChecked) {
            func_scrollview.setVisibility(View.VISIBLE);
            func_illustrationtv.setVisibility(View.GONE);
        } else {
            func_scrollview.setVisibility(View.GONE);
            func_illustrationtv.setVisibility(View.VISIBLE);
        }
        dragSortlistView = (DragSortListView) hostView.findViewById(android.R.id.list);
        zeroshowingtv = (TextView) hostView.findViewById(R.id.zeroshowingtv);
        alternativeShortcutslistview = (AlternativeShortcutslistView) hostView
                .findViewById(R.id.alternativeShortcutslistview);
        addAppshortcutstv = hostView.findViewById(R.id.addAppshortcutstv);
        addAppshortcutstv.setOnClickListener((view) -> {
            if (choosedLists.size() < 5) {
                saveFuncSettingData();
                mIsSaved = true;
                Intent intent = new Intent(mContext, FuncAppsListActivity.class);
                intent.putStringArrayListExtra(FuncUtil.EXTRA_CHOOSED, choosedLists);
                startActivityForResult(intent, FuncUtil.GOFuncAppsListActivity);
            } else {
                if (showToast != null) {
                    showToast.cancel();
                }
                showToast = Toast.makeText(mContext, R.string.func_AlternativeShortcuts_warning,
                        Toast.LENGTH_SHORT);
                showToast.show();
            }
        });
    }

    private void setAdapterAndListenners() {
        dragAdapter = new DragAdapter(mContext, choosedLists);
        dragSortlistView.setAdapter(dragAdapter);
        dragSortlistView.setDropListener(onDrop);
        dragSortlistView.setRemoveListener(onRemove);
        dragAdapter.setFuncEditItemListener(funcEditItemListener);
        alternativeShortcutsAdapter = new AlternativeShortcutsAdapter(mContext,
                alternativeList);
        alternativeShortcutslistview.setAdapter(alternativeShortcutsAdapter);
        alternativeShortcutsAdapter.setFuncAddListener(onAddFuncItemListener);
        isChoosedListsEmpty();
    }

    private DragSortListView.DropListener onDrop = new DragSortListView.DropListener() {
        @Override
        public void drop(int from, int to) {
            if (from != to) {
                String item = dragAdapter.getItem(from);
                choosedLists.remove(item);
                choosedLists.add(to, item);
                dragAdapter.notifyDataSetChanged();
                dragSortlistView.moveCheckState(from, to);
                saveFuncSettingData(); // MODIFIED by zheng.ding, 2016-10-21,BUG-3078647
            }
        }
    };

    private FuncEditItemListener funcEditItemListener = new FuncEditItemListener() {

        @Override
        public void editFuncItem(int goType) {
            /* MODIFIED-BEGIN by song.huan, 2016-10-18,BUG-3088432*/
            if (goType == com.android.internal.R.id.func_dial) {
                Intent mIntent = new Intent(Intent.ACTION_PICK, Uri.parse("content://contacts"));
                mIntent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                startActivityForResult(mIntent, FuncUtil.PICK_CONTACT_REQUEST);
                /* MODIFIED-END by song.huan,BUG-3088432*/
            }
        }

    };

    private RemoveListener onRemove = new DragSortListView.RemoveListener() {
        @Override
        public void remove(int which, boolean flag) {

            String item = dragAdapter.getItem(which);
            if (item != null) {
                int id = getResources().getIdentifier("id/" + item, null, "android");
                if (id != 0) {
                    if (id == com.android.internal.R.id.func_settings && flag) {
                        showRemoveFuncSettingDialog(which);
                    } else {
                        choosedLists.remove(item);
                        dragAdapter.notifyDataSetChanged();
                        alternativeList.add(0, item);
                        alternativeShortcutsAdapter.notifyDataSetChanged();
                        saveFuncSettingData(); // MODIFIED by zheng.ding, 2016-10-21,BUG-3078647
                    }
                } else {
                    choosedLists.remove(item);
                    dragAdapter.notifyDataSetChanged();
                }
            }
            isChoosedListsEmpty();
        }
    };

    private FuncAddListener onAddFuncItemListener = new FuncAddListener() {

        @Override
        public void addFuncItem(int position) {
            if (choosedLists.size() < 5) {
                String item = alternativeShortcutsAdapter.getItem(position);
                choosedLists.add(item);
                dragAdapter.notifyDataSetChanged();
                alternativeList.remove(item);
                alternativeShortcutsAdapter.notifyDataSetChanged();
                isChoosedListsEmpty();
                saveFuncSettingData(); // MODIFIED by zheng.ding, 2016-10-21,BUG-3078647
            } else {
                if (showToast != null) {
                    showToast.cancel();
                }
                showToast = Toast.makeText(mContext, R.string.func_AlternativeShortcuts_warning,
                        Toast.LENGTH_SHORT);
                showToast.show();
            }

        }
    };

    private void initData() {
        String choosed = Settings.System.getStringForUser(mContext.getContentResolver(),
                "choosed_list", UserHandle.USER_CURRENT);
        if (TextUtils.isEmpty(choosed)) {
            choosed = getResources().getString(com.android.internal.R.string.def_func_list_default);
        }

        choosedLists = FuncUtil.stringToList(choosed);

        String alter = mSharedPreferences.getString("alternative_list", null);
        if (TextUtils.isEmpty(alter)) {
            alternativeList = new ArrayList<>();
            String funcList = getResources().getString(R.string.def_func_list_total);
            String[] tmp = funcList.split(FuncUtil.DELIMITER);
            for (String item : tmp) {
                if (choosedLists.contains(item)) continue;
                alternativeList.add(item);
            }
        } else {
            alternativeList = FuncUtil.stringToList(alter);
        }
    }

    public void isChoosedListsEmpty() {
        if (choosedLists != null && choosedLists.size() <= 0) {
            zeroshowingtv.setVisibility(View.VISIBLE);
            dragSortlistView.setVisibility(View.GONE);
        } else {
            zeroshowingtv.setVisibility(View.GONE);
            dragSortlistView.setVisibility(View.VISIBLE);
        }
        mHandler.post(() -> {
            if (func_scrollview != null)
                func_scrollview.scrollTo(0, 0);
        });
    }

    private void saveFuncSettingData() {
         if (choosedLists != null && !choosedLists.isEmpty()) {
            String choosed_list = FuncUtil.listToString(choosedLists);
            Settings.System.putString(cr, "choosed_list", choosed_list);
         } else {
            Settings.System.putString(cr, "choosed_list", FuncUtil.DELIMITER);//store delimiter for empty
         }
         if (alternativeList != null && !alternativeList.isEmpty()) {
             SharedPreferences.Editor editor = mSharedPreferences.edit();
             editor.putString("alternative_list", FuncUtil.listToString(alternativeList));
             editor.apply();
         }

    }

    public void showRemoveFuncSettingDialog(final int position) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setCancelable(true /* cancelable */);
        builder.setMessage(R.string.func_showremovedialogmsg);
        builder.setPositiveButton(android.R.string.ok, (dialog, which) -> {
            dialog.dismiss();
            isShowRemoveFuncDialog = false;
            dragSortlistView.removeItem(position);
            SharedPreferences.Editor editor = mSharedPreferences.edit();
            editor.putBoolean(SHOWREMOVEFUNCDIALOG, false);
            editor.commit();
        });
        builder.create().show();
    }

    @Override
    public void onSwitchChanged(Switch switchView, boolean isChecked) {
        if (isChecked) {
            func_scrollview.setVisibility(View.VISIBLE);
            func_illustrationtv.setVisibility(View.GONE);
        } else {
            func_scrollview.setVisibility(View.GONE);
            func_illustrationtv.setVisibility(View.VISIBLE);
        }
        Settings.System.putInt(mContext.getContentResolver(), Settings.System.TCT_FUNC,
                (isChecked ? 1 : 0));
    }

    @Override
    protected int getMetricsCategory() {
        return MetricsEvent.LOCKSCREEN;
    }

}
