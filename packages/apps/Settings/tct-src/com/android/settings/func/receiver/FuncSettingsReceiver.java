/* ----------|----------------------|----------------------|----------------- */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* -------------------------------------------------------------------------- */
/* 9/29/2015 |     rurong.zhang     |        CR 674472     | Going directly   */
/*           |                      |                      | into a specific  */
/*           |                      |                      | function of      */
/*           |                      |                      | an app           */
/*           |                      |                      |                  */
/******************************************************************************/

package com.android.settings.func.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.UserHandle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;

import com.android.settings.func.FuncUtil;

import java.util.List;

public class FuncSettingsReceiver extends BroadcastReceiver {

    private final String TAG = this.getClass().getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        PackageManager pm = context.getPackageManager();
        String packageName = intent.getData().getSchemeSpecificPart();
        PackageInfo packageinfo = null;
        boolean isEnable =true;
        try {
            packageinfo = pm.getPackageInfo(packageName, 0);
            isEnable = packageinfo.applicationInfo.enabled;
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }
	    Log.e(TAG, "Uninstall or PACKAGE_CHANGED APP:>>" + packageName+",isEnable:>>"+isEnable);
        if (TextUtils.equals(intent.getAction(), Intent.ACTION_PACKAGE_REMOVED)
                || TextUtils.equals(intent.getAction(),
                        Intent.ACTION_PACKAGE_CHANGED) && !isEnable) {
            if (intent.getBooleanExtra(Intent.EXTRA_REPLACING, false)) {
                Log.e(TAG, "Uninstall >> EXTRA_REPLACING");
                return;
            }
            if (TextUtils.isEmpty(packageName)) return;
            String choosedList = Settings.System.getStringForUser(context.getContentResolver(),
                    "choosed_list", UserHandle.USER_CURRENT);
            if (TextUtils.isEmpty(choosedList) || FuncUtil.DELIMITER.equals(choosedList)) {
                return;
            }

            String result = choosedList.replaceAll(packageName + FuncUtil.DELIMITER, "");
            if (packageName.equals(result)) return; //this package not shown in func

            // In this case, this package had shown in func, so store result
            if (TextUtils.isEmpty(result)) {
                Settings.System.putString(context.getContentResolver(),
                        "choosed_list", FuncUtil.DELIMITER);
            } else {
                Settings.System.putString(context.getContentResolver(),
                        "choosed_list", result);
            }
            Intent uninstallAppIntent = new Intent();
            uninstallAppIntent.setAction(FuncUtil.UNINSTALLACTION);
            context.sendBroadcast(uninstallAppIntent);
            /* MODIFIED-BEGIN by jianguang.sun, 2016-10-19,BUG-2740734*/
            maybeResetFpapps(packageName, context);
        }
    }

    private void maybeResetFpapps(String packageName, Context context) {
        int fpMax = context
                .getResources()
                .getInteger(
                        com.android.internal.R.integer.config_fingerprintMaxTemplatesPerUser);
        for (int i = 0; i < fpMax; i++) {
            String funcNumString = Settings.System.TCT_FINGERPRINT_FUNC_NUM + i;
            String fpInfo = Settings.System.getString(context.getContentResolver(),
                    funcNumString);
            if (fpInfo == null) {
                continue;
            }
            if (fpInfo != null && fpInfo.contains(packageName)) {
                Log.d(TAG, "fpInfo:   " + fpInfo);
                Settings.System.putString(context.getContentResolver(), funcNumString,
                        null);
            }

        }

    }
    /* MODIFIED-END by jianguang.sun,BUG-2740734*/
}
