/*
 * Copyright (C)  The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* ----------|----------------------|----------------------|----------------- */
/*    date   |        Author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 10/14/2014|    siyan.chen        |       FR  718186     |[HOMO][HOMOLOGAT- */
/*           |                      |                      |ION] [APN] [PROF- */
/*           |                      |                      |ILE] apn list mu- */
/*           |                      |                      |st be pop up / it */
/*           |                      |                      | must be possible */
/*           |                      |                      | to define defau- */
/*           |                      |                      |lt apn selected   */
/*           |                      |                      |and order         */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package com.android.settings;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.SubscriptionManager;
import android.util.Log;

import com.android.internal.telephony.IccCardConstants;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.TelephonyIntents;

public class ApnCheckReceiver extends BroadcastReceiver {

    private static final String TAG = "ApnCheckReceiver";
    private static int currentDDSSubId = -1;
    public static boolean isBlockService = false; // MODIFIED by changwei.chi-nb, 2016-05-14,BUG-2009473

    @Override
    public void onReceive(Context context, Intent intent) {
        //[BUGFIX]-Add-BEGIN by TCTNB.changwei.chi,01/23/2016,defect-1275418,
        //Without select the data SIM Card while insert the dual SIM, the APN list can
        //be displayed
        if (context.getResources().getBoolean(
                com.android.internal.R.bool.feature_tctfw_dds_auto_switch)) {
            if (TelephonyIntents.ACTION_DEFAULT_DATA_SUBSCRIPTION_CHANGED
                    .equals(intent.getAction())) {
                int subId = intent.getIntExtra(PhoneConstants.SUBSCRIPTION_KEY,
                        SubscriptionManager.getDefaultDataSubscriptionId());
                Log.d(TAG, "Received ACTION_DEFAULT_DATA_SUBSCRIPTION_CHANGED: current subId is "
                        + currentDDSSubId + ", the new subid is " + subId);
                if (subId != currentDDSSubId) {
                    currentDDSSubId = subId;
                } else {
                    return;
                }
                if (SubscriptionManager.isValidSubscriptionId(currentDDSSubId)) {
                    intent.setClass(context, ApnCheckService.class);
                    intent.putExtra(PhoneConstants.SUBSCRIPTION_KEY, currentDDSSubId);
                    context.stopService(intent);
                    context.startService(intent);
                }
            // [BUGFIX]-Mod-BEGIN by TCTNB.changwei.chi,05/14/2016,2009473,
            // add SIM_STATE_CHANGED, if IMSI is not done, and the APN check service was blocked,
            // this action should re-start this service.
            } else if (TelephonyIntents.ACTION_SIM_STATE_CHANGED.equals(intent.getAction())) {
                String stateExtra = intent.getStringExtra(IccCardConstants.INTENT_KEY_ICC_STATE);
                if (IccCardConstants.INTENT_VALUE_ICC_IMSI.equals(stateExtra)
                        || IccCardConstants.INTENT_VALUE_ICC_LOADED.equals(stateExtra)) {
                    Log.d(TAG, "Received ACTION_SIM_STATE_CHANGED, stateExtra:" + stateExtra
                            + " isBlockService:" + isBlockService);
                    if (isBlockService) {
                        intent.setClass(context, ApnCheckService.class);
                        context.stopService(intent);
                        context.startService(intent);
                    }
                }
            }
            // [BUGFIX]-Mod-END by TCTNB.changwei.chi
        }else{
            if (TelephonyIntents.ACTION_SIM_STATE_CHANGED.equals(intent.getAction())) {
                String stateExtra = intent.getStringExtra(IccCardConstants.INTENT_KEY_ICC_STATE);
                Log.d(TAG,
                        "onReceive, stateExtra = " + stateExtra + ", action=" + intent.getAction());
                if (TelephonyIntents.ACTION_SIM_STATE_CHANGED.equals(intent.getAction())) {
                    if ((stateExtra == null)
                            || (!stateExtra.equals(IccCardConstants.INTENT_VALUE_ICC_LOADED))) {
                        Log.d(TAG, "return, because sim no ready");
                        return;
                    }
                }
                intent.setClass(context, ApnCheckService.class);
                context.stopService(intent);
                context.startService(intent);
            }
        }
        //[BUGFIX]-Add-END by TCTNB.changwei.chi
    }
}
