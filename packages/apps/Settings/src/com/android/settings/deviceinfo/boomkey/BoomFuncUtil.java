/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.settings.deviceinfo.boomkey;

import com.android.settings.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//[SOLUTION]-Created by TCTNB(Guoqiang.Qiu), 2016-9-20, Solution-2699164

public class BoomFuncUtil {
    public static final String DELIMITER = ";";
    public static final String EXTRA_CHOOSED = "choosed_list";
    public static final String UNINSTALLACTION = "com.android.settings.func_uninstallappaction";
    public static final int PICK_CONTACT_REQUEST = 4; // MODIFIED by song.huan, 2016-10-18,BUG-3088432

    public static final int GOFuncAppsListActivity = 2;

    public static ArrayList<String> stringToList(String in) {
        if (in != null) {
            return new ArrayList<>(Arrays.asList(in.split(DELIMITER)));
        }
        return null;
    }

    public static String listToString(List<String> in) {
        if (in != null) {
            StringBuilder out = new StringBuilder();
            for(String item : in) {
                out.append(item + DELIMITER);
            }
            return out.toString();
        }
        return null;
    }

}
