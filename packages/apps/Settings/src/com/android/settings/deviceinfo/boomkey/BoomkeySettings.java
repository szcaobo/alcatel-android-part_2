/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.settings.deviceinfo.boomkey;

import java.util.ArrayList;
/* MODIFIED-BEGIN by beibei.yang, 2016-09-18,BUG-2830318*/
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException; // MODIFIED by beibei.yang, 2016-09-18,BUG-2830318
import android.content.pm.ResolveInfo;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v14.preference.SwitchPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.Preference.OnPreferenceChangeListener;
import android.support.v7.preference.Preference.OnPreferenceClickListener;
import android.provider.Settings;
import android.util.Log;
import android.util.TctLog;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.os.UserHandle;
import com.android.settings.R;
import com.android.settings.SettingsPreferenceFragment;
import com.android.settings.deviceinfo.MyRadioButtonPreference;
import com.android.settings.deviceinfo.boomkey.BoomGuideImagePreference;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.SimpleAdapter.ViewBinder;
import android.widget.TextView;
public class BoomkeySettings extends SettingsPreferenceFragment implements
        OnPreferenceChangeListener, OnPreferenceClickListener,MyRadioButtonPreference.OnClickListener {

    private static final String TAG = "BoomkeySettings";

    private Context mContext;
    private BoomGuideImagePreference mBoomIntroduction;
    private MyRadioButtonPreference mTriggerApplication;
    private MyRadioButtonPreference mTriggerFunc;
    private MyRadioButtonPreference mDoNothing;

    private final int PRESS_BOOM_FIRST_TIME = 0 ; // MODIFIED by beibei.yang, 2016-11-03,BUG-3286444
    private final int TriggerApplication = 3 ;
    private final int TriggerFunc = 4 ;
    private final int DoNothing = 5 ;
    /* MODIFIED-END by beibei.yang,BUG-2830318*/

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        addPreferencesFromResource(R.xml.boomkey_settings);
        initPreference();
        mContext = getActivity();
    }

    private void initPreference() {

        mTriggerApplication = (MyRadioButtonPreference)findPreference("trigger_application");
        mTriggerFunc = (MyRadioButtonPreference)findPreference("trigger_func");
        mDoNothing = (MyRadioButtonPreference)findPreference("do_nothing");

        //init UI
        /* MODIFIED-BEGIN by beibei.yang, 2016-11-03,BUG-3286444*/
        int onFlag = Settings.System.getIntForUser(getContentResolver(),Settings.System.BOOM_KEY_ACTION, PRESS_BOOM_FIRST_TIME,UserHandle.USER_CURRENT);
        if (onFlag == PRESS_BOOM_FIRST_TIME) {
            onFlag = TriggerApplication ;
            Settings.System.putIntForUser(getContentResolver(), Settings.System.BOOM_KEY_ACTION, TriggerApplication,UserHandle.USER_CURRENT);
            Settings.System.putString(getContentResolver(), Settings.System.BOOM_KEY_APP_INFO, getResources().getString(R.string.def_app_list_default));
        }
        /* MODIFIED-END by beibei.yang,BUG-3286444*/
        /* MODIFIED-BEGIN by beibei.yang, 2016-09-18,BUG-2830318*/
        mTriggerFunc.setChecked(onFlag == TriggerFunc);
        mTriggerApplication.setChecked(onFlag == TriggerApplication);
        mDoNothing.setChecked(onFlag == DoNothing);
        /* MODIFIED-END by beibei.yang,BUG-2830318*/

        //add listeners
        mTriggerApplication.setOnClickListener(this);
        mDoNothing.setOnClickListener(this);
        mTriggerFunc.setOnClickListener(this);
    }

    @Override
    public boolean onPreferenceClick(Preference arg0) {
        return false;
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        //Boom key locked status
        return false;
    }

    @Override
    protected int getMetricsCategory() {
        // TODO Auto-generated method stub
        return 2;
    }

    @Override
    public void onRadioButtonClicked(MyRadioButtonPreference emiter) {


        if(emiter == mTriggerApplication){
            Settings.System.putIntForUser(getContentResolver(), Settings.System.BOOM_KEY_ACTION, TriggerApplication,UserHandle.USER_CURRENT);
        }else if(emiter == mTriggerFunc){
            Settings.System.putIntForUser(getContentResolver(), Settings.System.BOOM_KEY_ACTION, TriggerFunc,UserHandle.USER_CURRENT);
        }else if(emiter == mDoNothing){
            Settings.System.putIntForUser(getContentResolver(), Settings.System.BOOM_KEY_ACTION, DoNothing,UserHandle.USER_CURRENT);
            /* MODIFIED-END by beibei.yang,BUG-2830318*/
        }
        updateRadioButtonPreferenceStatus(emiter);
    }

    protected  void updateRadioButtonPreferenceStatus(MyRadioButtonPreference emiter) {
        resetRadioButton();
        if(emiter == mTriggerApplication){
            mTriggerApplication.setChecked(true);
            Intent intent = new Intent ();
            intent.setClassName("com.android.settings",
                    "com.android.settings.SubSettings$BoomAppSettingsActivity"); // MODIFIED by beibei.yang, 2016-10-31,BUG-2830318
            mContext.startActivity(intent);
        }else if(emiter == mDoNothing){
            mDoNothing.setChecked(true);
        }else if(emiter == mTriggerFunc){
            mTriggerFunc.setChecked(true);
            Intent intent = new Intent ();
            intent.setClassName("com.android.settings",
                    "com.android.settings.SubSettings$BoomFuncSettingsActivity"); // MODIFIED by beibei.yang, 2016-10-31,BUG-2830318
            mContext.startActivity(intent);
        }
    }

    private void resetRadioButton () {
        mTriggerApplication.setChecked(false);
        mDoNothing.setChecked(false);
        mTriggerFunc.setChecked(false);
    }
    public BoomkeySettings() {
        // TODO Auto-generated constructor stub
    }



    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
//        this.getListView().setDivider(null);
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
    }



}
