/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.settings.deviceinfo.boomkey;

import android.content.Context;
import android.support.v7.preference.Preference;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.android.settings.R;

public class BoomGuideImagePreference extends Preference {
    private Context mContext;
    public BoomGuideImagePreference(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        // TODO Auto-generated constructor stub
        mContext = context;
    }

    public BoomGuideImagePreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
        setLayoutResource(R.layout.boom_guide_image);
        mContext = context;
    }

    public BoomGuideImagePreference(Context context) {
        super(context);
        // TODO Auto-generated constructor stub
        mContext = context;
    }

//    @Override
//    protected View onCreateView(ViewGroup parent) {
//        LayoutInflater mInflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        View mLayout = mInflater.inflate(R.layout.boom_guide_image, parent,false);
//        return mLayout;
//    }
}
