/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.settings.deviceinfo.boomkey.adapter;

import java.util.List;

import com.android.settings.R;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class BoomAlternativeAppShortcutsAdapter extends BaseAdapter {

    private final static String TAG = "DragAdapter";
    private boolean isItemShow = false;
    private Context context;
    private int holdPosition;
    private boolean isChanged = false;
    boolean isVisible = true;
    public List<String> chooseListData;
    private TextView item_text;
    public int remove_position = -1;
    private FuncAddListener funcAddListener;
    private List<ResolveInfo> apps = null;
    PackageManager pm = null;

    public BoomAlternativeAppShortcutsAdapter(Context context,
            List<String> chooseListData,List<ResolveInfo> apps) {
        this.context = context;
        this.chooseListData = chooseListData;
        this.apps = apps ;
        this.pm = context.getPackageManager();
    }

    @Override
    public int getCount() {
        return apps == null ? 0 : apps.size();
    }

    @Override
    public Object getItem(int position) {
        if (apps != null && apps.size() != 0) {
            return apps.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolderNormal holderNormal = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(
                    R.layout.alternativeshortcuts_listview_item, null);
            holderNormal = new ViewHolderNormal();
            holderNormal.funcIcon = (ImageView) convertView
                    .findViewById(R.id.funcicon);
            holderNormal.funcText = (TextView) convertView
                    .findViewById(R.id.functext);
            holderNormal.func_addIcon = (ImageView) convertView
                    .findViewById(R.id.func_add);
            convertView.setTag(holderNormal);
        } else {
            holderNormal = (ViewHolderNormal) convertView.getTag();
        }
        ResolveInfo item = apps.get(position);
//        Resources res = context.getResources();
//        int id = res.getIdentifier("id/" + item, null, "android");
//
//        if (id != 0) {
//            int descId = res.getIdentifier("string/" + item, null, context.getPackageName());
//            holderNormal..setText(descId != 0 ? res.getString(descId) : item);
//            int imgId = res.getIdentifier("drawable/" + item, null, context.getPackageName());
//            if (imgId != 0) {
//                holderNormal.funcIcon.setImageResource(imgId);
//            }
//        }
        ActivityInfo info = item.activityInfo;
        Log.d("boom", "alteradapter info ="+info.packageName +"isChoosedShortCuts(info.packageName) ="+isChoosedShortCuts(info.packageName));
        if(isChoosedShortCuts(info.packageName)){
            Log.d("test","alteradapter info ="+info.packageName+ "  black");
            convertView.setBackgroundColor(Color.LTGRAY);
        }else {
            convertView.setBackgroundColor(Color.WHITE);
            Log.d("test","alteradapter info ="+info.packageName+ "  white");
        }
        holderNormal.funcText.setText(item.loadLabel(pm));
        holderNormal.funcIcon.setImageDrawable(item.loadIcon(pm));
        final int clickposition = position;
        holderNormal.func_addIcon.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (funcAddListener != null) {
                    funcAddListener.addFuncItem(clickposition);
                }
            }
        });
        return convertView;
    }

    public void setFuncAddListener(FuncAddListener funcAddListener) {
        this.funcAddListener = funcAddListener;
    }

    static class ViewHolderNormal {
        ImageView funcIcon;
        TextView funcText;
        ImageView func_addIcon;
    }

    public interface FuncAddListener {
        void addFuncItem(int position);
    }
    private boolean isChoosedShortCuts(String packageName) {
        if (chooseListData == null || chooseListData.isEmpty()) {
            return false;
        }
        boolean isChoosed = false;
        for (String item : chooseListData) {
            if (item.equals(packageName)) {
                isChoosed = true;
                break;
            }
        }
        return isChoosed;
    }
}
