/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.settings.deviceinfo.boomkey.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.settings.R;

import java.util.List;

public class BoomDragAdapter extends BaseAdapter {
    private final static String TAG = "DragAdapter";
    private Context context;
    private Resources res;
    public List<String> listData;
    private FuncEditItemListener funcEditItemListener;
    private final PackageManager pm;

    public BoomDragAdapter(Context context, List<String> listData) {
        this.context = context;
        this.listData = listData;
        res = context.getResources();
        pm = context.getPackageManager();
    }

    @Override
    public int getCount() {
        return listData == null ? 0 : listData.size();
    }

    @Override
    public String getItem(int position) {
        if (listData != null && listData.size() != 0
                && position < listData.size()) {
            return listData.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolderNormal holderNormal = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(
                    R.layout.drag_listview_item, null);
            holderNormal = new ViewHolderNormal();
            holderNormal.funcIcon = (ImageView) convertView
                    .findViewById(R.id.funcicon);
            holderNormal.funcText = (TextView) convertView
                    .findViewById(R.id.functext);
            holderNormal.fun_editIcon = (ImageView) convertView
                    .findViewById(R.id.fun_edit);
            holderNormal.func_removeIcon = (ImageView) convertView
                    .findViewById(R.id.click_remove);
            convertView.setTag(holderNormal);
        } else {
            holderNormal = (ViewHolderNormal) convertView.getTag();
        }

        String item = listData.get(position);
        int id = res.getIdentifier("id/" + item, null, "android");

        int type = -1;
        if (id != 0 || item.equals("func_screenshot")) { // MODIFIED by beibei.yang, 2016-10-28,BUG-2830318
            int descId = res.getIdentifier("string/" + item, null, context.getPackageName());
            holderNormal.funcText.setText(descId != 0 ? res.getString(descId) : item);
            int imgId = res.getIdentifier("drawable/" + item, null, context.getPackageName());
            if (imgId != 0) {
                holderNormal.funcIcon.setImageResource(imgId);
            }
            if (id == com.android.internal.R.id.func_dial) { // MODIFIED by beibei.yang, 2016-11-03,BUG-3304204
                holderNormal.fun_editIcon.setVisibility(View.VISIBLE);
                type = id;
            } else {
                holderNormal.fun_editIcon.setVisibility(View.GONE);
            }
        } else {
            CharSequence tip;
            try {
                ApplicationInfo info = pm.getApplicationInfo(item, PackageManager.GET_META_DATA);
                tip = pm.getApplicationLabel(info);
                Drawable drawable = pm.getApplicationIcon(item);
                holderNormal.funcIcon.setImageDrawable(drawable);
            } catch (Exception e) {
                tip = item;
            }
            holderNormal.funcText.setText(tip);
            holderNormal.fun_editIcon.setVisibility(View.GONE);
        }
        final int goType = type;
        holderNormal.fun_editIcon.setOnClickListener(view -> {
            if (funcEditItemListener != null && goType != -1) {
                funcEditItemListener.editFuncItem(goType);
            }
        });
        return convertView;
    }

    public void setFuncEditItemListener(
            FuncEditItemListener funcEditItemListener) {
        this.funcEditItemListener = funcEditItemListener;
    }

    static class ViewHolderNormal {
        ImageView funcIcon;
        TextView funcText;
        ImageView fun_editIcon;
        ImageView func_removeIcon;
    }

    public interface FuncEditItemListener {
        void editFuncItem(int type);
    }
}
