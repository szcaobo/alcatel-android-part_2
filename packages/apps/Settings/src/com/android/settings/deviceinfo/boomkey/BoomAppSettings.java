/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.settings.deviceinfo.boomkey;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.UserHandle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.SearchView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.internal.logging.MetricsProto.MetricsEvent;
import com.android.settings.R;
import com.android.settings.SettingsActivity;
import com.android.settings.SettingsPreferenceFragment;
/* MODIFIED-BEGIN by beibei.yang, 2016-10-28,BUG-2830318*/
import com.android.settings.deviceinfo.boomkey.adapter.BoomAlternativeAppShortcutsAdapter;
import com.android.settings.deviceinfo.boomkey.adapter.BoomAlternativeAppShortcutsAdapter.FuncAddListener;
import com.android.settings.deviceinfo.boomkey.adapter.BoomDragAdapter;
import com.android.settings.deviceinfo.boomkey.adapter.BoomDragAdapter.FuncEditItemListener;
import com.android.settings.deviceinfo.boomkey.view.BoomAlternativeShortcutslistView;
import com.android.settings.deviceinfo.boomkey.view.BoomDragSortListView;
import com.android.settings.deviceinfo.boomkey.view.BoomDragSortListView.RemoveListener;
import com.android.settings.deviceinfo.boomkey.BoomFuncUtil;
/* MODIFIED-END by beibei.yang,BUG-2830318*/

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BoomAppSettings extends SettingsPreferenceFragment implements
         SearchView.OnQueryTextListener, SearchView.OnCloseListener{

    /* MODIFIED-BEGIN by beibei.yang, 2016-10-28,BUG-2830318*/
    private static final String TAG = "BoomAppSettings";
    private Context mContext;
    private ContentResolver cr;
    private ArrayList<String> choosedLists;
    private List<ResolveInfo> alternativeList;
    private ScrollView func_scrollview;
    private TextView func_illustrationtv;
    private BoomDragSortListView dragSortlistView;
    private TextView zeroshowingtv;
    private BoomDragAdapter dragAdapter;
    private BoomAlternativeShortcutslistView alternativeShortcutslistview;
    private BoomAlternativeAppShortcutsAdapter alternativeShortcutsAdapter;
    /* MODIFIED-END by beibei.yang,BUG-2830318*/
    private static final String SP_NAME = "funcSettings_preferences";
    private static final String SHOWREMOVEFUNCDIALOG = "showRemoveFuncDialog";
    private boolean mChecked;
    private SharedPreferences mSharedPreferences;
    private boolean isShowRemoveFuncDialog;

    private static int MIDDLE_ITEM_NUM = 3;
    private Toast showToast;
    private PackageIntentReceiver mPackageIntentReceiver = null;
    private Handler mHandler = new Handler();
    private boolean mIsSaved = false;
    PackageManager pm ;
    ImageView searchImageView;
    SearchView searchView;
    private BoomAlternativeShortcutslistView mSearchResultsView = null; // MODIFIED by beibei.yang, 2016-10-28,BUG-2830318

    private class PackageIntentReceiver extends BroadcastReceiver {

        void registerReceiver() {
            IntentFilter filter = new IntentFilter(Intent.ACTION_PACKAGE_ADDED);
            filter.addAction(Intent.ACTION_PACKAGE_REMOVED);
            filter.addAction(Intent.ACTION_PACKAGE_CHANGED);
            filter.addDataScheme("package");
            mContext.registerReceiver(this, filter);
            // Register for events related to sdcard installation.
            IntentFilter sdFilter = new IntentFilter();
            sdFilter.addAction(Intent.ACTION_EXTERNAL_APPLICATIONS_AVAILABLE);
            sdFilter.addAction(Intent.ACTION_EXTERNAL_APPLICATIONS_UNAVAILABLE);
            sdFilter.addAction(BoomFuncUtil.UNINSTALLACTION); // MODIFIED by beibei.yang, 2016-11-02,BUG-3304419
            mContext.registerReceiver(this, sdFilter);
        }

        void unregisterReceiver() {
            mContext.unregisterReceiver(this);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            /* MODIFIED-BEGIN by beibei.yang, 2016-11-02,BUG-3304419*/
            Log.d(TAG, "onReceive actionStr="+intent.getAction());
            String actionStr = intent.getAction();
            if (null == actionStr) {
                return;
            }
            if (dragSortlistView == null) {
                Log.i(TAG, "onReceive: dragSortlistView has not been initialized yet, just return!");
                return;
            }
            if (BoomFuncUtil.UNINSTALLACTION.equals(actionStr)) {
                initData();
                setAdapterAndListenners();
            }
            /* MODIFIED-END by beibei.yang,BUG-3304419*/
            if (Intent.ACTION_PACKAGE_ADDED.equals(actionStr)
                    || Intent.ACTION_PACKAGE_CHANGED.equals(actionStr)
                    || Intent.ACTION_PACKAGE_REMOVED.equals(actionStr)) {
                doPackageChanged();
            } else if (Intent.ACTION_EXTERNAL_APPLICATIONS_AVAILABLE
                    .equals(actionStr)
                    || Intent.ACTION_EXTERNAL_APPLICATIONS_UNAVAILABLE
                            .equals(actionStr)) {
                // When applications become available or unavailable (perhaps
                // because
                // the SD card was inserted or ejected) we need to refresh the
                // AppInfo with new label, icon and size information as
                // appropriate
                // given the newfound (un)availability of the application.
                // A simple way to do that is to treat the refresh as a package
                // removal followed by a package addition.
                String pkgList[] = intent
                        .getStringArrayExtra(Intent.EXTRA_CHANGED_PACKAGE_LIST);
                if (pkgList == null || pkgList.length == 0) {
                    // Ignore
                    return;
                }
                boolean avail = Intent.ACTION_EXTERNAL_APPLICATIONS_AVAILABLE
                        .equals(actionStr);
                if (avail) {
                    doPackageChanged();
                }
            }
        }

    }

    private void doPackageChanged() {
        alternativeList.clear();
        alternativeList = getAllPackages();
        Log.d(TAG, "mItems.size-->" + alternativeList.size());
        alternativeShortcutslistview.setAdapter(null);
        /* MODIFIED-BEGIN by beibei.yang, 2016-10-28,BUG-2830318*/
        alternativeShortcutsAdapter = new BoomAlternativeAppShortcutsAdapter(mContext,choosedLists,alternativeList);
        alternativeShortcutslistview.setAdapter(alternativeShortcutsAdapter);
        alternativeShortcutsAdapter.notifyDataSetChanged();
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK
                && requestCode == BoomFuncUtil.GOFuncAppsListActivity) {
                /* MODIFIED-END by beibei.yang,BUG-2830318*/
            Bundle bundle = data.getExtras();
            if (bundle != null) {
                String pkgName = bundle.getString("packageName");
                if (!TextUtils.isEmpty(pkgName)) {
                    choosedLists.add(pkgName);
                    dragAdapter.notifyDataSetChanged();
                    isChoosedListsEmpty();
                    func_scrollview.fullScroll(ScrollView.FOCUS_UP);
                }
            }

        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        pm = mContext.getPackageManager();
        cr = getContentResolver();
        mPackageIntentReceiver = new PackageIntentReceiver();
        mPackageIntentReceiver.registerReceiver();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View result = inflater.inflate(R.layout.boom_appsettings, container, false);
        initView(result);
        return result;

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        final SettingsActivity activity = (SettingsActivity) getActivity();
        initData();
        setAdapterAndListenners();
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onStop() {
        super.onStop();
        if (!mIsSaved) {
            saveFuncSettingData();
        }
        mIsSaved = false;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (null != mPackageIntentReceiver) {
            mPackageIntentReceiver.unregisterReceiver();
            mPackageIntentReceiver = null;
        }
    }

    private void initView(View hostView) {
        func_scrollview = (ScrollView) hostView.findViewById(R.id.func_scrollview);
        func_illustrationtv = (TextView) hostView.findViewById(R.id.func_illustrationtv);
        mSharedPreferences = mContext.getSharedPreferences(SP_NAME,
                Context.MODE_PRIVATE);
        isShowRemoveFuncDialog = mSharedPreferences.getBoolean(
                SHOWREMOVEFUNCDIALOG, true);
        mChecked = true;//MOD by chao.hu for bug 3647375
        if (mChecked) {
            func_scrollview.setVisibility(View.VISIBLE);
            func_illustrationtv.setVisibility(View.GONE);
        } else {
            func_scrollview.setVisibility(View.GONE);
            func_illustrationtv.setVisibility(View.VISIBLE);
        }
        /* MODIFIED-BEGIN by beibei.yang, 2016-10-28,BUG-2830318*/
        dragSortlistView = (BoomDragSortListView) hostView.findViewById(android.R.id.list);
        zeroshowingtv = (TextView) hostView.findViewById(R.id.zeroshowingtv);
        alternativeShortcutslistview = (BoomAlternativeShortcutslistView) hostView
        /* MODIFIED-END by beibei.yang,BUG-2830318*/
                .findViewById(R.id.alternativeShortcutslistview);

        searchImageView = (ImageView)hostView.findViewById(R.id.search);
        searchView = (SearchView)hostView.findViewById(R.id.search_view);
        searchImageView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                searchView.setVisibility(View.VISIBLE);
                searchView.setBackgroundColor(Color.WHITE);
                v.setVisibility(View.GONE);
            }
        });
        searchView.setOnQueryTextListener(this);
        searchView.setOnCloseListener(this);
        mSearchResultsView = (BoomAlternativeShortcutslistView)hostView.findViewById(R.id.search_app_list);
    }

    private void setAdapterAndListenners() {
        dragAdapter = new BoomDragAdapter(mContext, choosedLists);
        dragSortlistView.setAdapter(dragAdapter);
        dragSortlistView.setDropListener(onDrop);
        dragSortlistView.setRemoveListener(onRemove);
        dragAdapter.setFuncEditItemListener(funcEditItemListener);
        alternativeShortcutsAdapter = new BoomAlternativeAppShortcutsAdapter(mContext,
                choosedLists,alternativeList);
        alternativeShortcutslistview.setAdapter(alternativeShortcutsAdapter);
        alternativeShortcutsAdapter.setFuncAddListener(onAddFuncItemListener);
        isChoosedListsEmpty();
    }

    private BoomDragSortListView.DropListener onDrop = new BoomDragSortListView.DropListener() {
    /* MODIFIED-END by beibei.yang,BUG-2830318*/
        @Override
        public void drop(int from, int to) {
            if (from != to) {
                String item = dragAdapter.getItem(from);
                choosedLists.remove(item);
                choosedLists.add(to, item);
                dragAdapter.notifyDataSetChanged();
                dragSortlistView.moveCheckState(from, to);
            }
        }
    };

    private FuncEditItemListener funcEditItemListener = new FuncEditItemListener() {

        @Override
        public void editFuncItem(int goType) {
            if (goType == com.android.internal.R.id.func_music) {
                Intent mIntent = new Intent();
                mIntent.setAction("com.tct.mix.action.FUNCLOCKSCREEN");
                mIntent.putExtra("funcSettings", true);
                mContext.sendBroadcast(mIntent);
            } else if (goType == com.android.internal.R.id.func_navigate) {
                Intent mIntent = new Intent(
                        "android.settings.NAVIGATE_HOME_SETTINGS");
                startActivity(mIntent);
            }
        }

    };

    private RemoveListener onRemove = new BoomDragSortListView.RemoveListener() { // MODIFIED by beibei.yang, 2016-10-28,BUG-2830318
        @Override
        public void remove(int which, boolean flag) {

            String item = dragAdapter.getItem(which);
            if (item != null) {
                int id = getResources().getIdentifier("id/" + item, null, "android");
                Log.d("boom2", "id= "+id);
                if (id != 0) {
                    if (id == com.android.internal.R.id.func_settings && flag) {
                        showRemoveFuncSettingDialog(which);
                    } else {
                        choosedLists.remove(item);
                        dragAdapter.notifyDataSetChanged();
                        alternativeShortcutsAdapter.notifyDataSetChanged();
                        if (searchAdapter != null) searchAdapter.notifyDataSetChanged();
                    }
                } else {
                    choosedLists.remove(item);
                    dragAdapter.notifyDataSetChanged();
                    alternativeShortcutsAdapter.notifyDataSetChanged();
                    if (searchAdapter != null) searchAdapter.notifyDataSetChanged();
                }
            }
            isChoosedListsEmpty();
        }
    };

    private FuncAddListener onAddFuncItemListener = new FuncAddListener() {

        @Override
        public void addFuncItem(int position) {
            Log.d("app", "add listener");
            if (choosedLists.size() < 3) {
                ResolveInfo app = (ResolveInfo)alternativeShortcutsAdapter.getItem(position);
                String item = app.activityInfo.packageName;
                if (choosedLists.contains(item)) return;
                choosedLists.add(item);
                dragAdapter.notifyDataSetChanged();
                alternativeShortcutsAdapter.notifyDataSetChanged();
                isChoosedListsEmpty();
            } else {
                if (showToast != null) {
                    showToast.cancel();
                }
                showToast = Toast.makeText(mContext, R.string.func_AlternativeShortcuts_warning_boom,
                        Toast.LENGTH_SHORT);
                showToast.show();
            }

        }
    };

    private void initData() {
        String choosed = Settings.System.getStringForUser(mContext.getContentResolver(),
                Settings.System.BOOM_KEY_APP_INFO, UserHandle.USER_CURRENT);
        if (TextUtils.isEmpty(choosed)) {
            choosed = getResources().getString(R.string.def_app_list_default);
        }

        //存入app 包名
        choosedLists = BoomFuncUtil.stringToList(choosed); // MODIFIED by beibei.yang, 2016-10-28,BUG-2830318
        alternativeList = getAllPackages() ;

    }
    private List<ResolveInfo> getAllPackages() {

        Intent intent = new Intent(Intent.ACTION_MAIN,null);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        List<ResolveInfo> apps = pm.queryIntentActivities(intent, PackageManager.GET_INTENT_FILTERS );
        return apps;
    }
    public void isChoosedListsEmpty() {
        if (choosedLists != null && choosedLists.size() <= 0) {
            zeroshowingtv.setVisibility(View.VISIBLE);
            dragSortlistView.setVisibility(View.GONE);
        } else {
            zeroshowingtv.setVisibility(View.GONE);
            dragSortlistView.setVisibility(View.VISIBLE);
        }
        mHandler.post(() -> {
            if (func_scrollview != null)
                func_scrollview.scrollTo(0, 0);
        });
    }

    private void saveFuncSettingData() {
    	Log.d("boomui","choosedLists="+choosedLists);
         if (choosedLists != null && !choosedLists.isEmpty()) {
            /* MODIFIED-BEGIN by beibei.yang, 2016-10-28,BUG-2830318*/
            String choosed_list = BoomFuncUtil.listToString(choosedLists);
            Settings.System.putString(cr, Settings.System.BOOM_KEY_APP_INFO, choosed_list);
         } else {
            Settings.System.putString(cr, Settings.System.BOOM_KEY_APP_INFO, BoomFuncUtil.DELIMITER);//store delimiter for empty
            /* MODIFIED-END by beibei.yang,BUG-2830318*/
         }

    }

    public void showRemoveFuncSettingDialog(final int position) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setCancelable(true /* cancelable */);
        builder.setMessage(R.string.func_showremovedialogmsg);
        builder.setPositiveButton(android.R.string.ok, (dialog, which) -> {
            dialog.dismiss();
            isShowRemoveFuncDialog = false;
            dragSortlistView.removeItem(position);
            SharedPreferences.Editor editor = mSharedPreferences.edit();
            editor.putBoolean(SHOWREMOVEFUNCDIALOG, false);
            editor.commit();
        });
        builder.create().show();
    }


    @Override
    protected int getMetricsCategory() {
        return MetricsEvent.LOCKSCREEN;
    }

    @Override
    public boolean onClose() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (TextUtils.isEmpty(newText.trim())) {
            setResultsVisibility(false);
            alternativeShortcutslistview.clearTextFilter();
            alternativeShortcutslistview.setVisibility(View.VISIBLE);
        } else {
            setResultsVisibility(true);
            updateSearchResults(newText);
            alternativeShortcutslistview.setVisibility(View.GONE);
        }
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        // TODO Auto-generated method stub
        return false;
    }
    private void setResultsVisibility(boolean visible) {
        if (mSearchResultsView != null) {
            mSearchResultsView
                    .setVisibility(visible ? View.VISIBLE : View.GONE);
        }
    }
    private BoomAlternativeAppShortcutsAdapter searchAdapter = null; // MODIFIED by beibei.yang, 2016-10-28,BUG-2830318
    private void updateSearchResults(String text) {
        List<ResolveInfo> aList = new ArrayList<ResolveInfo>();
        for (int i = 0; i < alternativeList.size(); i++) {
            ActivityInfo aInfo = alternativeList.get(i).activityInfo;
            String label = (String) aInfo.loadLabel(pm);
            if ((label.toLowerCase()).contains(text.toLowerCase())) {
                aList.add(alternativeList.get(i));
            }
        }
        //Log.d(TAG, "aList.size-->" + aList.size());
        searchAdapter = new BoomAlternativeAppShortcutsAdapter(mContext,choosedLists,aList); // MODIFIED by beibei.yang, 2016-10-28,BUG-2830318
        mSearchResultsView.setAdapter(searchAdapter);
        searchAdapter.setFuncAddListener(new FuncAddListener(){

            @Override
            public void addFuncItem(int position) {
                // TODO Auto-generated method stub
                if (choosedLists.size() < 3) {
                    Log.d("app", "choosedLists < 3");
                    ResolveInfo app = (ResolveInfo)searchAdapter.getItem(position);
                    String item = app.activityInfo.packageName;
                    Log.d("app", "choosedLists = "+choosedLists +" item="+item);
                    if (choosedLists.contains(item)) return;
                    choosedLists.add(item);
                    dragAdapter.notifyDataSetChanged();
                    alternativeShortcutsAdapter.notifyDataSetChanged();
                    searchAdapter.notifyDataSetChanged();
                    Log.d("app", "adapter.notifyDataSetChanged();");
                    isChoosedListsEmpty();
                } else {
                    if (showToast != null) {
                        showToast.cancel();
                    }
                    showToast = Toast.makeText(mContext, R.string.func_AlternativeShortcuts_warning_boom,
                            Toast.LENGTH_SHORT);
                    showToast.show();
                }

            }

        } );
    }
}
