package com.android.settings;

import com.android.settings.applications.AppInfoWithHeader;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class TransferPermissionActivity extends Activity {

    private static final String TAG = "TransferPermissionActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        String packageName = intent.getStringExtra("android.intent.extra.tct.PACKAGE_NAME");
        if(null != packageName){
            startManagePermissionsActivity(packageName);
        } else {
            Log.w(TAG,"start permission activity's package can not be null");
        }

        finish();
    }

    private void startManagePermissionsActivity(String packageName) {
        // start new activity to manage app permissions
        Intent intent = new Intent(Intent.ACTION_MANAGE_APP_PERMISSIONS);
        intent.putExtra(Intent.EXTRA_PACKAGE_NAME, packageName);
        intent.putExtra(AppHeader.EXTRA_HIDE_INFO_BUTTON, true);
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Log.w(TAG, "No app can handle android.intent.action.MANAGE_APP_PERMISSIONS");
        }
    }
}
