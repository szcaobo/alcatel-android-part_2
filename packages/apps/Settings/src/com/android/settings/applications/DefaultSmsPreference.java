/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.android.settings.applications;

import android.content.ComponentName;
import android.content.Context;
import android.os.UserHandle;
import android.os.UserManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.AttributeSet;
import com.android.internal.telephony.SmsApplication;
import com.android.internal.telephony.SmsApplication.SmsApplicationData;
import com.android.settings.AppListPreference;
import com.android.settings.R;
import com.android.settings.SelfAvailablePreference;
import android.util.Log;
import java.util.Collection;
import java.util.Objects;

public class DefaultSmsPreference extends AppListPreference implements SelfAvailablePreference {

    private Context mContext;//ADD by Dingyi  2016/08/19 SOLUTION 2521878
    public DefaultSmsPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext=context;//ADD by Dingyi  2016/08/19 SOLUTION 2521878
        loadSmsApps();
    }

    private void loadSmsApps() {
        Collection<SmsApplicationData> smsApplications =
                SmsApplication.getApplicationCollection(getContext());
        //MODIFY-BEGIN by Dingyi  2016/08/19 SOLUTION 2521878
        boolean showSmartSuite = mContext.getResources().getBoolean(R.bool.def_settings_sms_application_showSmartSuite_enable);
        int count = smsApplications.size();
        if (!showSmartSuite) {
            for (SmsApplicationData smsApplicationData : smsApplications) {
                if (!showSmartSuite && "com.tcl.smartsuite".equals(smsApplicationData.mPackageName)) {
                    count = count - 1;
                }
            }
        }
        //MODIFY-END by Dingyi  2016/08/19 SOLUTION 2521878
        String[] packageNames = new String[count];
        int i = 0;
        for (SmsApplicationData smsApplicationData : smsApplications) {
            //ADD-BEGIN by Dingyi  2016/08/19 SOLUTION 2521878
            if(!showSmartSuite && "com.tcl.smartsuite".equals(smsApplicationData.mPackageName)){
                continue;
            }
            //ADD-END by Dingyi  2016/08/19 SOLUTION 2521878
            packageNames[i++] = smsApplicationData.mPackageName;
        }
        setPackageNames(packageNames, getDefaultPackage());
    }

    private String getDefaultPackage() {
        ComponentName appName = SmsApplication.getDefaultSmsApplication(getContext(), true);
        if (appName != null) {
            return appName.getPackageName();
        }
        return null;
    }

    @Override
    protected boolean persistString(String value) {
        if (!TextUtils.isEmpty(value) && !Objects.equals(value, getDefaultPackage())) {
            SmsApplication.setDefaultApplication(value, getContext());
        }
        setSummary(getEntry());
        return true;
    }

    @Override
    public boolean isAvailable(Context context) {
        boolean isRestrictedUser =
                UserManager.get(context)
                        .getUserInfo(UserHandle.myUserId()).isRestricted();
        TelephonyManager tm =
                (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        return !isRestrictedUser && tm.isSmsCapable();
    }

    public static boolean hasSmsPreference(String pkg, Context context) {
        Collection<SmsApplicationData> smsApplications =
                SmsApplication.getApplicationCollection(context);
        for (SmsApplicationData data : smsApplications) {
            if (data.mPackageName.equals(pkg)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isSmsDefault(String pkg, Context context) {
        ComponentName appName = SmsApplication.getDefaultSmsApplication(context, true);
        return appName != null && appName.getPackageName().equals(pkg);
    }
}
