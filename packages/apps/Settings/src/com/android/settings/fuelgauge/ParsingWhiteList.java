package com.android.settings.fuelgauge;

import java.io.IOException;
import java.util.ArrayList;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import com.android.settings.R;

import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.util.Log;

public class ParsingWhiteList {

    private ArrayList<String> packageName;
    private Resources mResources;
    public ParsingWhiteList(Resources resources) {
        this.mResources = resources;
        packageName = new ArrayList<String>();
    }

    public ArrayList<String> parsingXml(){
        XmlResourceParser parser = mResources.getXml(R.xml.white_list_highpower);
        try {
            parser.next();
        int eventType = parser.getEventType();
        while(eventType != XmlPullParser.END_DOCUMENT){
            if(eventType == XmlPullParser.START_TAG){
                String elemName = parser.getName();
                if(elemName.equals("package")){
                    Log.d("the parsed white list package : ", parser.getAttributeValue(null, "name"));
                   packageName.add(parser.getAttributeValue(null, "name"));
                }
            }
            eventType = parser.next();
        }
        } catch (XmlPullParserException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Log.d("all white list packageName : ", packageName.toString());
        return packageName;
    }
}
