/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.settings;

import java.util.ArrayList;

import android.app.ListActivity;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;


public class ChooseActivity extends ListActivity {

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		Intent intent = new Intent();
		switch(position){
		case 0:
			intent.setAction("android.settings.APN_SETTINGS");
			intent.putExtra("apn_editable", true);
			break;
		//lina.yan@tcl.com remove it for Alto no need this menus,pr854555,792436,20150106,start
		/*case 1:
			intent.setComponent(new ComponentName("com.jrdcom.namprogram","com.jrdcom.namprogram.NamItems"));
			break;
		case 2:
		    	intent.setComponent(new ComponentName("com.jrdcom.namprogram","com.jrdcom.namprogram.UatiView"));
		    	break;
		*/
		//lina.yan@tcl.com remove it for Alto no need this menus,pr854555,792436,20150106,end

		//PR716468 removed item "NV/RUIM setting" by wangxiuqin on 20140625 begin
		//case 3:
		//	intent.setComponent(new ComponentName("com.jrdcom.namprogram","com.jrdcom.namprogram.RtreSettingActivity"));
	       // 	break;
		//PR716468 removed item "NV/RUIM setting" by wangxiuqin on 20140625 end

		//PR814082 add for MCR by wangxiuqin on 20141111 begin
		case 1:
			intent.setComponent(new ComponentName("com.android.settings","com.android.settings.CarrierSettingActivity"));
	        	break;

               default:
			break;
		//PR814082 add for MCR by wangxiuqin on 20141111 begin end

		}
		startActivity(intent);
//		finish();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setTitle("Data Profile");
		ListView view = getListView();
		String[] listItem = getResources().getStringArray(R.array.choose);
		ArrayList<String> arrayList = new ArrayList<String>();
		for(String string:listItem){
			arrayList.add(string);
		}
		view.setAdapter(new ArrayAdapter(this,android.R.layout.simple_list_item_1,listItem));
	}

}
