/*
 * Copyright (c) 2013 Qualcomm Technologies, Inc.  All Rights Reserved.
 * Qualcomm Technologies Proprietary and Confidential.
 *
 * Not a Contribution, Apache license notifications and license are retained
 * for attribution purposes only.
 */

/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.settings.wifi;

import java.util.ArrayList;
import java.util.List;
import android.app.Activity;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.provider.Settings;
import android.os.Handler;
import android.os.Message;
import android.telephony.TelephonyManager;
import com.android.internal.telephony.IccCardConstants;
import com.android.internal.telephony.TelephonyIntents;
import android.text.TextUtils;
import android.util.Log;
import com.android.settings.R;

public class SSIDPreloadReceiver extends BroadcastReceiver {

    private static final String TAG = "SSIDPreloadReceiver";
    private static final Boolean DEBUG = true;

  //[BUGFIX]MOD-BEGIN by TCTNB.jianhong.yang@tcl.com,2015/12/26,defect 1198579
    public static final String SHARE_PREFERENCE_FILE_NAME = "wifi_ssid_preload";
    public static final String SHARE_PREFERENCE_PRELOAD_FLAG_KEY = "wifi_ssid_preload_flag";
    public static final String NETWORK_FACTORY_RESET_ACTION = "com.android.settings.wifi.NETWORK_FACTORY_RESET_ACTION";
  //[BUGFIX]MOD-END by TCTNB.jianhong.yang

    private static final String SHARE_PREFERENCE_SSV_INDEX_KEY = "wifi_ssid_ssv_index";
    private Context mContext;
    private TelephonyManager mTelephonyManager;
    private WifiManager mWifiManager;

    @Override
    public void onReceive(Context context, Intent intent) {
        mContext = context;

        if (mContext.getResources().getBoolean(R.bool.def_Settings_wifi_preloadSsid_enable) == false) {
            if (DEBUG) Log.i(TAG, "Preload wifi ssid not enabled");
            return;
        }

        mTelephonyManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        mWifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);

        String action = intent.getAction();
        if (DEBUG) Log.d(TAG,"receive action: " + action);

        if (WifiManager.WIFI_STATE_CHANGED_ACTION.equals(action)) {
            int wifiState = intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE,WifiManager.WIFI_STATE_UNKNOWN);
            if (wifiState == WifiManager.WIFI_STATE_ENABLED) {
                tryToPreloadSSID(null);
            }
        } else if (TelephonyIntents.ACTION_SIM_STATE_CHANGED.equals(action)) {
            //[BUGFIX]-ADD-BEGIN by TCTSH.yuguan.chen,2015/01/19,Defect1276162
            boolean isIccLoaded = IccCardConstants.INTENT_VALUE_ICC_LOADED.equals(intent.getStringExtra(IccCardConstants.INTENT_KEY_ICC_STATE));
            if(isIccLoaded && mTelephonyManager.isMultiSimEnabled()) {
                setSSIDPreloadStatus(false);
                if (mWifiManager.getWifiState() == WifiManager.WIFI_STATE_ENABLED) {
                    startService();
                }
            }
            //[BUGFIX]-ADD-END by TCTSH.yuguan.chen,2015/01/19,Defect1276162

            if (mContext.getResources().getBoolean(R.bool.def_Settings_wifi_preloadSsid_ssvEnable) == false) {
                return;
            }
            String stateExtra = intent.getStringExtra(IccCardConstants.INTENT_KEY_ICC_STATE);
            boolean isSimCardInserted = IccCardConstants.INTENT_VALUE_ICC_READY.equals(stateExtra)
                        || IccCardConstants.INTENT_VALUE_ICC_LOADED.equals(stateExtra);
            if (DEBUG) Log.d(TAG,"stateExtra " + stateExtra + " isSimCardInserted = " + isSimCardInserted);

            if (isSimCardInserted) {
                if (mTelephonyManager != null) {
                    String mccmnc = mTelephonyManager.getSimOperator();
                    if (mccmnc != null) tryToPreloadSSID(mccmnc);
                }
            }
        }
        //[BUGFIX]Add-BEGIN by TCTNB.jianhong.yang@tcl.com,2015/12/26,defect 1198579
        else if (NETWORK_FACTORY_RESET_ACTION.equals(action)) {
            tryToPreloadSSID(null);
        }
        //[BUGFIX]Add-END by TCTNB.jianhong.yang
    }

    private void tryToPreloadSSID(String mccmnc) {
        if (mccmnc == null) { // Wifi state change trigger this
            if (! mContext.getResources().getBoolean(R.bool.def_Settings_wifi_preloadSsid_ssvEnable)) {
                if (!isSSIDPreloaded()) {
                    if (DEBUG) Log.d(TAG,"startService to repload the ssid, ssv not enabled");
                    startService();
                }
            } else { //MCCMNC
                int ssvIndex = getPreloadSSVIndex();
                if (! isSSIDPreloaded(ssvIndex)) {
                    if (DEBUG) Log.d(TAG,"startService to repload the ssid, ssv enabled : " + ssvIndex);
                    startService();
                }
            }

        } else { // SIM Card state change trigger this
            int operatorIndex = getIndexFromMCCMNCArray(mccmnc);
            Log.d(TAG,"mccmnc = " + mccmnc + " operatorIndex = " + operatorIndex);
            if (!isSSIDPreloaded(operatorIndex)) {
                //sync share preference state
                setSSIDPreloadStatus(false);
                setPreloadSSVIndex(operatorIndex);
                if (DEBUG) Log.d(TAG,"setSSIDPreloadStatus false , will preload SSVIndex " + operatorIndex);
                if (mWifiManager.getWifiState() == WifiManager.WIFI_STATE_ENABLED) {
                    if (DEBUG) Log.d(TAG,"startService to repload the ssid, ssv mccmnc = " + mccmnc + " operatorIndex = " + operatorIndex);
                    startService();
                }
            }
        }
    }

    private int getIndexFromMCCMNCArray(String mccMncArray) {
        if (mccMncArray == null || mccMncArray.length() == 0) {
            return -1;
        }

        String mccmncList = mContext.getResources().getString(R.string.def_Settings_wifi_mccmncList);
        String[] mccmncArray = mccmncList.split(";");
        if (mccmncArray.length == 0) {
            return -1;
        } else {
            for (int i = 0; i < mccmncArray.length ; i++) {
                if (mccmncArray[i].contains(mccMncArray))
                    return i;
            }
            return -1;
        }
    }

    private void setPreloadSSVIndex(int index) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(
                SHARE_PREFERENCE_FILE_NAME,
                Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(SHARE_PREFERENCE_SSV_INDEX_KEY, index);
        editor.commit();
    }

    private int getPreloadSSVIndex() {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(
                SHARE_PREFERENCE_FILE_NAME,
                Activity.MODE_PRIVATE);
        return sharedPreferences.getInt(SHARE_PREFERENCE_SSV_INDEX_KEY, -1);
    }

    private boolean isSSIDPreloaded() {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(
                SHARE_PREFERENCE_FILE_NAME,
                Activity.MODE_PRIVATE);
        return sharedPreferences.getBoolean(SHARE_PREFERENCE_PRELOAD_FLAG_KEY, false);
    }

    private boolean isSSIDPreloaded(int ssvIndex) {
        if (ssvIndex == -1) {
            return true; // No sim card insert, Not to preload SSID
        }
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(
                SHARE_PREFERENCE_FILE_NAME,Activity.MODE_PRIVATE);
        int storeIndex = sharedPreferences.getInt(SHARE_PREFERENCE_SSV_INDEX_KEY, -1);
        if (storeIndex < 0 || storeIndex != ssvIndex) return false;

        return sharedPreferences.getBoolean(SHARE_PREFERENCE_PRELOAD_FLAG_KEY, false);
    }

    private void setSSIDPreloadStatus(boolean isPreloaded) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(
                SHARE_PREFERENCE_FILE_NAME,
                Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(SHARE_PREFERENCE_PRELOAD_FLAG_KEY, isPreloaded);
        editor.commit();
    }

    private void startService() {
        Intent i = new Intent("com.android.settings.wifi.SSIDPreloadService");
        i.setClassName(mContext.getPackageName(), SSIDPreloadService.class.getName());
        mContext.startService(i);
    }
}
