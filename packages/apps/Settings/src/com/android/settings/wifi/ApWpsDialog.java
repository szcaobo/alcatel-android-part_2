/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/******************************************************************************/
/*                                                               Date:11/2012 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2012 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  ZhangJinbo                                                      */
/*  Email  :  jinbo.zhang@tcl-mobile.com                                      */
/*  Role   :                                                                  */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments : WPS Push Button                                                */
/*  File     : packages/apps/Settings/src/com/android/settings/wifi/          */
/*              ApWpsDialog.java                                              */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 10/30/2013|   Zhang Jinbo        |        467382        |Add WiFi WPS Push */
/*           |                      |                      | Button Feature   */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/
package com.android.settings.wifi;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.net.NetworkInfo.DetailedState;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.net.wifi.WpsInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

import com.android.settings.R;
import android.util.Log;


/**
 * Dialog to show WPS progress.
 */
public class ApWpsDialog extends AlertDialog {

    private final static String TAG = "ApWpsDialog";
    //[BUGFIX] ADD-BEGIN-BY TCTNB.Ruili.Liu TASK 573037 20151110 WPS Dialog display message error when rotate screen
    private static final String DIALOG_STATE = "android:dialogState";
    private static final String DIALOG_MSG_STRING = "android:dialogMsg";
    private static final String DIALOG_WPS_COMPLETE = "android:dialogWPSComplete";
    //[BUGFIX] ADD-END-BY TCTNB.Ruili.Liu

    private View mView;
    private TextView mTextView;
    private ProgressBar mTimeoutBar;
    private ProgressBar mProgressBar;
    private Button mButton;
    private Timer mTimer;

    private static final int WPS_TIMEOUT_S = 120;

    private WifiManager mWifiManager;
    private WifiManager.WpsCallback mWpsListener;
    private int mWpsSetup;

    private Context mContext;
    private Handler mHandler = new Handler();
    //[BUGFIX] ADD-BEGIN-BY TCTNB.Ruili.Liu TASK 573037 20151110 WPS Dialog display message error when rotate screen
    private String mMsgString = "";
    private boolean mDialogWPSComplete = false;
    //[BUGFIX] ADD-END-BY TCTNB.Ruili.Liu

    private enum DialogState {
        WPS_INIT,
        WPS_START,
        WPS_COMPLETE,
        WPS_FAILED
    }
    DialogState mDialogState = DialogState.WPS_INIT;

    public ApWpsDialog(Context context, int wpsSetup) {
        super(context);
        mContext = context;
        mWpsSetup = wpsSetup;

        class WpsListener extends WifiManager.WpsCallback {
            public void onStarted(String pin) {
                updateDialog(DialogState.WPS_START, mContext.getString(
                            R.string.wifi_ap_wps_onstart_pbc));
            }
            public void onSucceeded() {
                updateDialog(DialogState.WPS_COMPLETE,
                        mContext.getString(R.string.wifi_ap_wps_complete));
                mDialogWPSComplete = true;
            }

            public void onFailed(int reason) {
                String msg;
                mDialogWPSComplete = true;
                switch (reason) {
                    case WifiManager.WPS_OVERLAP_ERROR:
                        msg = mContext.getString(R.string.wifi_wps_failed_overlap);
                        break;
                    case WifiManager.WPS_WEP_PROHIBITED:
                        msg = mContext.getString(R.string.wifi_wps_failed_wep);
                        break;
                    case WifiManager.WPS_TKIP_ONLY_PROHIBITED:
                        msg = mContext.getString(R.string.wifi_wps_failed_tkip);
                        break;
                    case WifiManager.IN_PROGRESS:
                        msg = mContext.getString(R.string.wifi_wps_in_progress);
                        break;
                    default:
                        msg = mContext.getString(R.string.wifi_wps_failed_generic);
                        break;
                }
                updateDialog(DialogState.WPS_FAILED, msg);
            }
        }

        mWpsListener = new WpsListener();
        setCanceledOnTouchOutside(false);
    }
    //[BUGFIX] ADD-BEGIN-BY TCTNB.Ruili.Liu TASK 573037 20151110 WPS Dialog display message error when rotate screen
    @Override
    public Bundle onSaveInstanceState () {
        Bundle bundle  = super.onSaveInstanceState();
        bundle.putString(DIALOG_STATE, mDialogState.toString());
        bundle.putString(DIALOG_MSG_STRING, mMsgString.toString());
        bundle.putBoolean(DIALOG_WPS_COMPLETE, mDialogWPSComplete);
        return bundle;
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            DialogState dialogState = mDialogState.valueOf(savedInstanceState.getString(DIALOG_STATE));
            String msg = savedInstanceState.getString(DIALOG_MSG_STRING);
            mDialogWPSComplete = savedInstanceState.getBoolean(DIALOG_WPS_COMPLETE);
            super.onRestoreInstanceState(savedInstanceState);
            updateDialog(dialogState, msg);
        }
    }
    //[BUGFIX] ADD-END-BY TCTNB.Ruili.Liu

    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	Log.d(TAG,"onCreate");
        mView = getLayoutInflater().inflate(R.layout.wifi_wps_dialog, null);

        mTextView = (TextView) mView.findViewById(R.id.wps_dialog_txt);
        mTextView.setText(R.string.wifi_wps_setup_msg);

        mTimeoutBar = ((ProgressBar) mView.findViewById(R.id.wps_timeout_bar));
        mTimeoutBar.setMax(WPS_TIMEOUT_S);
        mTimeoutBar.setProgress(0);

        mProgressBar = ((ProgressBar) mView.findViewById(R.id.wps_progress_bar));
        mProgressBar.setVisibility(View.GONE);

        mButton = ((Button) mView.findViewById(R.id.wps_dialog_btn));
        mButton.setText(R.string.wifi_cancel);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        mWifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);

        setView(mView);
        //[BUGFIX] ADD-BEGIN-BY TCTNB.Ruili.Liu TASK 573037 20151110 WPS Dialog display message error when rotate screen
        if (!mDialogWPSComplete) {
        	Log.d(TAG,"startWps");
            WpsInfo wpsConfig = new WpsInfo();
            wpsConfig.setup = mWpsSetup;
            mWifiManager.startWps(wpsConfig, mWpsListener);
        }
        //[BUGFIX] ADD-END-BY TCTNB.Ruili.Liu

        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        /*
         * increment timeout bar per second.
         */
    	Log.d(TAG,"onStart");
        mTimer = new Timer(false);
        mTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                mHandler.post(new Runnable() {

                    @Override
                    public void run() {
                        mTimeoutBar.incrementProgressBy(1);
                        //[BUGFIX] ADD-BEGIN-BY TCTNB.Ruili.Liu TASK 573037 20151110 WPS Dialog display message error when rotate screen
                        if (mTimeoutBar.getProgress() == WPS_TIMEOUT_S){
                            updateDialog(DialogState.WPS_FAILED,mContext.getString(R.string.wifi_wps_failed_generic));
                            mWifiManager.cancelWps(null);
                            cancel();
                        }
                        //[BUGFIX] ADD-END-BY TCTNB.Ruili.Liu
                    }
                });
            }
        }, 1000, 1000);
    }

    @Override
    protected void onStop() {
    	Log.d(TAG,"onStop");
        //if (mDialogState != DialogState.WPS_COMPLETE) {
            mWifiManager.cancelWps(null);
        //}

        if (mTimer != null) {
            mTimer.cancel();
        }
    }

    private void updateDialog(final DialogState state, final String msg) {
        if (mDialogState.ordinal() >= state.ordinal()) {
            //ignore.
            return;
        }
        mDialogState = state;
        mMsgString = msg;
        mHandler.post(new Runnable() {
                @Override
                public void run() {
                    switch(state) {
                        case WPS_COMPLETE:
                            mButton.setText(mContext.getString(R.string.dlg_ok));
                            mTimeoutBar.setVisibility(View.GONE);
                            mProgressBar.setVisibility(View.GONE);
                            break;
                        case WPS_FAILED:
                            mButton.setText(mContext.getString(R.string.dlg_ok));
                            mTimeoutBar.setVisibility(View.GONE);
                            mProgressBar.setVisibility(View.GONE);
                            break;
                         default:
                            break;
                    }
                    mTextView.setText(msg);
                }
            });
    }

}
