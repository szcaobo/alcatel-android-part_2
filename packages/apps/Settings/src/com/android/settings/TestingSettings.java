/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.settings;

import android.os.Bundle;
import android.os.UserManager;
import android.support.v7.preference.PreferenceScreen;
/* MODIFIED-BEGIN by sunyandong, 2016-08-18,BUG-2759046*/
import android.content.Context;
import android.hardware.usb.UsbManager;
import android.support.v7.preference.CheckBoxPreference;
import android.support.v7.preference.Preference;
import android.widget.Toast;
import android.text.TextUtils;
import android.os.SystemProperties;

import com.android.internal.logging.MetricsProto.MetricsEvent;

public class TestingSettings extends SettingsPreferenceFragment {

    private CheckBoxPreference rmnetPreference;
    private static final String ENABLE_NET_WWAN_PREF = "enable_net_wwan_pref";
    private Context mContext;

    /* MODIFIED-BEGIN by sunyandong, 2016-08-24,BUG-2789611*/
    //Open RNDIS+MODEM port
    private static final String ENABLE_RNDS_MODEM_PREF = "enable_rnds_modem_pref";
    /* MODIFIED-END by sunyandong,BUG-2789611*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.testing_settings);

        mContext = getContext();
        final UserManager um = UserManager.get(getContext());
        if (!um.isAdminUser()) {
            PreferenceScreen preferenceScreen = (PreferenceScreen)
                    findPreference("radio_info_settings");
            getPreferenceScreen().removePreference(preferenceScreen);
        }

        rmnetPreference = (CheckBoxPreference)findPreference(ENABLE_NET_WWAN_PREF);
        rmnetPreference.setChecked(isRMNETEnable());
    }

    @Override
    protected int getMetricsCategory() {
        return MetricsEvent.TESTING;
    }

    public boolean isRMNETEnable()
    {
        boolean enable=false;
        String func = SystemProperties.get("persist.sys.usb.config",UsbManager.USB_FUNCTION_NONE);

        if(!TextUtils.isEmpty(func)){
          //"rmnet_ipa" for 8952/8976/8996 new qcom platform, "rmnet_bam" for old qcom platform
          if(func.contains("rmnet_ipa")||func.contains("rmnet_bam"))
            enable=true;
        }
        return enable;
    }

    @Override
    @Deprecated
    public boolean onPreferenceTreeClick(Preference preference) {
        // TODO Auto-generated method stub
        if (ENABLE_NET_WWAN_PREF.equals(preference.getKey())) {
            this.setFunction(!((CheckBoxPreference)preference).isChecked());
        }

        /* MODIFIED-BEGIN by sunyandong, 2016-08-24,BUG-2789611*/
        //Open RNDIS+MODEM port
        if (ENABLE_RNDS_MODEM_PREF.equalsIgnoreCase(preference.getKey())) {
            this.setFunctionRNDIS(!((CheckBoxPreference)preference).isChecked());
        }
        /* MODIFIED-END by sunyandong,BUG-2789611*/
        return super.onPreferenceTreeClick(preference);
    }

    private void setFunction(boolean isDefault) {
        /* MODIFIED-BEGIN by sunyandong, 2016-09-07,BUG-2759149*/
        String usbConfig = "diag,serial_smd,rmnet_bam,adb"; //msm8916
        String defaultUsbConfig = "default,mass_storage,diag,serial_smd,serial_tty";
        UsbManager mUsbManager = (UsbManager) mContext.getSystemService(Context.USB_SERVICE);

        //Match the correct RMNET port parameter for the different qcom platform
        String platform = SystemProperties.get("ro.board.platform","");
        if(!TextUtils.isEmpty(platform))
        {
            if(platform.equalsIgnoreCase("msm8952")
                || platform.equalsIgnoreCase("msm8953")
                || platform.equalsIgnoreCase("msm8976")){
                usbConfig = "diag,serial_smd,rmnet_ipa,adb";
                defaultUsbConfig = "mtp,diag,adb";
            }else if(platform.equalsIgnoreCase("msm8996")){
                usbConfig = "diag,serial_cdev,serial_tty,rmnet_ipa,mass_storage,adb";
                defaultUsbConfig = "mtp,diag,adb";
                /* MODIFIED-END by sunyandong,BUG-2759149*/
            }
        }

        if (isDefault) {
            // change to default usb config
            mUsbManager.setCurrentFunction(defaultUsbConfig);
            Toast.makeText(mContext, "Disable RMNET", Toast.LENGTH_LONG).show();
        } else {
            mUsbManager.setCurrentFunction(usbConfig);
            Toast.makeText(mContext, "Enable RMNET", Toast.LENGTH_LONG).show();
        }
    }
    /* MODIFIED-END by sunyandong,BUG-2759046*/

    /* MODIFIED-BEGIN by sunyandong, 2016-08-24,BUG-2789611*/
    //Open RNDIS+MODEM port
    private void setFunctionRNDIS(boolean isDefault) {
        String usbConfig = "rndis,serial_smd,diag,adb";
        String defaultUsbConfig = "default,mass_storage,diag,serial_smd,serial_tty";
        UsbManager mUsbManager = (UsbManager) mContext.getSystemService(Context.USB_SERVICE);
        if (isDefault) {
            // change to default usb config
            mUsbManager.setCurrentFunction(defaultUsbConfig);
            Toast.makeText(mContext, "Disable RNDIS+MODEM", Toast.LENGTH_SHORT).show();
        } else {
            mUsbManager.setCurrentFunction(usbConfig);
            Toast.makeText(mContext, "Enable RNDIS+MODEM", Toast.LENGTH_SHORT).show();
        }
    }
    /* MODIFIED-END by sunyandong,BUG-2789611*/

}
