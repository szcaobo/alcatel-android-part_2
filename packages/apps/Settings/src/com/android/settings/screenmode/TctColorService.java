package com.android.settings.screenmode;

import android.R.integer;
import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;
import com.android.settings.R;

//qualcomm sdk
import com.qti.snapdragon.sdk.display.ColorManager;
import com.qti.snapdragon.sdk.display.ColorManager.ColorManagerListener;
import com.qti.snapdragon.sdk.display.ColorManager.DCM_DISPLAY_TYPE;
import com.qti.snapdragon.sdk.display.ColorManager.DCM_FEATURE;
import com.qti.snapdragon.sdk.display.ColorManager.MODE_TYPE;
import com.qti.snapdragon.sdk.display.ModeInfo;

public class TctColorService extends Service {

    private static String TAG = "TctColorService";

    private ColorManager cMgr;

    private Handler mTctColorServiceHandler;
    private HandlerThread mHandlerThread;

    private static final String COLOR_TEMPERATURE = "color_temperature";
    private static final String COLOR_MODE = "color_mode";

    private boolean isActive = false;
    private static final long ACTIVETIME = 1 * 60 * 1000;

    private static final int MSG_STOP_SERVICE = 0;
    private static final int MSG_SET_COLOR_TEMPERATURE = 1;
    private static final int MSG_SET_COLOR_MODE = 2;
    private static final int MSG_GET_ALL_MODES = 3;

    private static final String ACTION_START_COLOR_SERVICE = "com.android.settings.TCT_COLOR_SERVICE";
    private static final String ACTION_SET_COLOR_MODE = "com.android.settings.TCT_COLOR_SERVICE.SET_COLOR_MODE";
    private static final String ACTION_SET_COLOR_TEMPERATURE = "com.android.settings.TCT_COLOR_SERVICE.SET_COLOR_TEMPERATURE";

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
        initHandler();
        connectColorManager();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    private void initHandler() {
        mHandlerThread = new HandlerThread("tct_service_handler_thread");
        mHandlerThread.start();
        mTctColorServiceHandler = new TctColorServiceHandler(
                mHandlerThread.getLooper());
        Message msg = mTctColorServiceHandler.obtainMessage(MSG_STOP_SERVICE);
        mTctColorServiceHandler.sendMessageDelayed(msg, ACTIVETIME);
    }

    private void connectColorManager() {
        ColorManagerListener colorinterface = new ColorManagerListener() {
            @Override
            public void onConnected() {
                getColorManagerInstance();
            }
        };
        int retVal = ColorManager.connect(this, colorinterface);
        if (retVal != ColorManager.RET_SUCCESS) {
            Log.e(TAG, "Connection failed");
        }
    }

    private void getColorManagerInstance() {
        cMgr = ColorManager.getInstance(getApplication(), this,
                DCM_DISPLAY_TYPE.DISP_PRIMARY);
        getAllModes();
    }

    private class TctColorServiceHandler extends Handler {

        public TctColorServiceHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
            Intent intent = (Intent) msg.obj;
            switch (msg.what) {
            case MSG_STOP_SERVICE:
                {
                    if (isActive) {
                        this.sendEmptyMessageDelayed(MSG_STOP_SERVICE,
                                ACTIVETIME);
                        isActive = false;
                    } else {
                        stopSelf();
                        Log.i(TAG, "stop self");
                    }
                    break;

                }
            case MSG_SET_COLOR_TEMPERATURE:
                {
                    setColorTemperature(intent);
                    break;
                }
            case MSG_SET_COLOR_MODE:
                {
                    setColorMode(intent);
                    break;
                }
            case MSG_GET_ALL_MODES:
                {
                    getAllModes();
                    break;
                }
            default:
                break;
            }
        }
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        releaseColorManager();
    }

    private void releaseColorManager() {
        if (cMgr != null)
            cMgr.release();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // TODO Auto-generated method stub
        if (intent != null) {
            handleIntent(intent);
        }
        return super.onStartCommand(intent, flags, startId);
    }

    private void handleIntent(Intent intent) {
        // TODO Auto-generated method stub
        String action = intent.getAction();
        Log.v(TAG, "action : " + action);
        isActive = true;
        if (action.equals(ACTION_START_COLOR_SERVICE)) {
            return;
        }
        int what = MSG_GET_ALL_MODES;
        if (action.equals(ACTION_SET_COLOR_TEMPERATURE)) {
            what = MSG_SET_COLOR_TEMPERATURE;
        } else if (action.equals(ACTION_SET_COLOR_MODE)) {
            what = MSG_SET_COLOR_MODE;
        }
        Message msg = mTctColorServiceHandler.obtainMessage(what);
        msg.obj = intent;
        msg.sendToTarget();
    }

    private void sendTctMessage(int what, Intent intent, long time) {
        // TODO Auto-generated method stub
        Message msg = mTctColorServiceHandler.obtainMessage(what);
        msg.obj = intent;
        mTctColorServiceHandler.sendMessageDelayed(msg, time);
    }

    private void getAllModes() {
        if (cMgr != null) {
            cMgr.getModes(MODE_TYPE.MODE_ALL);
            int mDefaultModeID = cMgr.getDefaultMode();
        } else {
            Log.e(TAG, "cMgr is null");
            sendTctMessage(MSG_GET_ALL_MODES, null, 100);
        }
    }

    private void setColorTemperature(Intent intent) {
        // TODO Auto-generated method stub
        int colorTemperature = intent.getIntExtra(COLOR_TEMPERATURE, 3);
        int defMode = getDefaultMode();
        int mode = colorTemperature * 2 + defMode % 2;
        if (cMgr != null) {
            cMgr.setActiveMode(mode);
            cMgr.setDefaultMode(mode);
            setDefaultMode(mode);
        } else {
            Log.e(TAG, "cMgr is null");
            sendTctMessage(MSG_SET_COLOR_TEMPERATURE, intent, 100);
        }

    }

    private void setColorMode(Intent intent) {
        // TODO Auto-generated method stub
        int colorMode = intent.getIntExtra(COLOR_MODE, 0);
        int defMode = getDefaultMode();
        int mode = defMode;
        if (colorMode == 0) {
            mode = mode / 2 * 2 ;
        } else {
            mode = mode / 2 * 2 + 1;
        }
        if (cMgr != null) {
            cMgr.setActiveMode(mode);
            cMgr.setDefaultMode(mode);
            setDefaultMode(mode);
        } else {
            Log.e(TAG, "cMgr is null");
            sendTctMessage(MSG_SET_COLOR_MODE, intent, 100);
        }
    }

    private int getDefaultMode() {
        // TODO Auto-generated method stub
        int defaultMode = this.getResources().getInteger(
                com.android.internal.R.integer.def_tctfw_screenmode_id);
        return Settings.System.getInt(this.getContentResolver(),
                Settings.System.TCT_COLORSERVICE_SCREEN_MODE, defaultMode);
    }

    private void setDefaultMode(int mode) {
        // TODO Auto-generated method stub
        Settings.System.putInt(this.getContentResolver(),
                Settings.System.TCT_COLORSERVICE_SCREEN_MODE, mode);
    }
}
