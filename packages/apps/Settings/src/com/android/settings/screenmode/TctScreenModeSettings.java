package com.android.settings.screenmode;

import android.content.Context;
import android.os.Bundle;

import com.android.settings.SettingsPreferenceFragment;
import com.android.settings.R;

import com.android.internal.logging.MetricsProto.MetricsEvent;

public class TctScreenModeSettings extends SettingsPreferenceFragment {

    private static final String TAG = "ScreenModeSettings";

    @Override
    protected int getMetricsCategory() {
        // TODO Auto-generated method stub
        return MetricsEvent.TCT_SCREEN_MODE;
    }

    @Override
    public void onCreate(Bundle icicle) {
        // TODO Auto-generated method stub
        super.onCreate(icicle);
        addPreferencesFromResource(R.xml.tct_screen_mode_settings);
    }

}
