package com.android.settings.screenmode;

import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceViewHolder;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

import com.android.settings.R;

public class TctScreenModePreference extends Preference {
    private static final String TAG = "TctColorModePreference";

    private Context mContext;

    private SeekBar mSeekBar;
    private ListView mColorModelistView;
    private LinearLayout mColorModeLinearLayout;

    public TctScreenModePreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        setLayoutResource(R.layout.preference_tct_screen_mode);
        startTctColorService(mContext);
    }

    @Override
    public void onBindViewHolder(PreferenceViewHolder view) {
        super.onBindViewHolder(view);
        createColorConfiguration(view);
        createColorModeConfiguration(view);
    }

    private void createColorConfiguration(PreferenceViewHolder view) {
        mSeekBar = (SeekBar) view.findViewById(R.id.color_seekbar);
        initSeekbarProgress(getDefaultMode(), mSeekBar);
        mSeekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int seekProgress = seekBar.getProgress();
                updateSeekBarState(seekProgress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                    boolean fromUser) {
                setColorTemperature(progress);
            }
        });

    }

    private void createColorModeConfiguration(PreferenceViewHolder view) {
        mColorModeLinearLayout = (LinearLayout) view
                .findViewById(R.id.color_mode_linearlayout);
        if (!mContext.getResources().getBoolean(
                com.android.internal.R.bool.feature_tctfw_imagedisplay_on)) {
            mColorModeLinearLayout.setVisibility(View.GONE);
        }
        mColorModelistView = (ListView) view
                .findViewById(R.id.color_mode_listview);
        mColorModelistView.setAdapter(new ArrayAdapter<String>(mContext,
                android.R.layout.simple_list_item_single_choice, mContext
                        .getResources().getStringArray(R.array.color_modes)));
        mColorModelistView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> list, View v, int position,
                    long id) {
                setColorModeByService(position);
            }
        });
        initColorModeListView();
    }

    private void initColorModeListView() {
        int mode = getDefaultMode() % 2;
        mColorModelistView.setItemChecked(mode, true);
    }

    private void updateSeekBarState(int progress) {
        if ((progress >= 0) && (progress <= 20)) {
            mSeekBar.setProgress(0);
        } else if ((progress > 20) && (progress <= 61)) {
            mSeekBar.setProgress(41);
        } else if ((progress > 61) && (progress <= 102)) {
            mSeekBar.setProgress(82);
        } else if ((progress > 102) && (progress <= 143)) {
            mSeekBar.setProgress(123);
        } else if ((progress > 143) && (progress <= 164)) {
            mSeekBar.setProgress(164);
        }
    }

    private void setColorTemperature(int progress) {
        if (progress == 0) {
            setColorTemperatureByService(0);
        } else if (progress == 41) {
            setColorTemperatureByService(1);
        } else if (progress == 82) {
            setColorTemperatureByService(2);
        } else if (progress == 123) {
            setColorTemperatureByService(3);
        } else if (progress == 164) {
            setColorTemperatureByService(4);
        }
    }

    private void initSeekbarProgress(int mode, SeekBar seekBar) {
        int colorTemperature = mode / 2;
        if (colorTemperature == 0) {
            seekBar.setProgress(0);
        } else if (colorTemperature == 1) {
            seekBar.setProgress(41);
        } else if (colorTemperature == 2) {
            seekBar.setProgress(82);
        } else if (colorTemperature == 3) {
            seekBar.setProgress(123);
        } else if (colorTemperature == 4) {
            seekBar.setProgress(164);
        }
    }

    private void startTctColorService(Context mContext) {
        Intent intent = new Intent("com.android.settings.TCT_COLOR_SERVICE");
        intent.setPackage("com.android.settings");
        mContext.startService(intent);
    }

    private void setColorTemperatureByService(int colorTemperature) {
        Intent intent = new Intent(
                "com.android.settings.TCT_COLOR_SERVICE.SET_COLOR_TEMPERATURE");
        intent.setPackage("com.android.settings");
        intent.putExtra("color_temperature", colorTemperature);
        mContext.startService(intent);
    }

    private void setColorModeByService(int colorMode) {
        Intent intent = new Intent(
                "com.android.settings.TCT_COLOR_SERVICE.SET_COLOR_MODE");
        intent.setPackage("com.android.settings");
        intent.putExtra("color_mode", colorMode);
        mContext.startService(intent);
    }

    private int getDefaultMode() {
        int defaultMode = mContext.getResources().getInteger(
                com.android.internal.R.integer.def_tctfw_screenmode_id);
        return Settings.System.getInt(mContext.getContentResolver(),
                Settings.System.TCT_COLORSERVICE_SCREEN_MODE, defaultMode);
    }
}
