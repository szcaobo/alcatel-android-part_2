/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.settings.aod;

import android.content.Context;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceViewHolder;
import android.provider.Settings;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.AdapterView.OnItemClickListener;

import com.android.settings.R;
import com.android.settings.aod.view.BackgroundStyleAdapter;

import static android.provider.Settings.System.AOD_BG_STYLE;

public class BackgroundStylePreference extends Preference implements
        OnItemClickListener, View.OnTouchListener {

    private Context mContext;

    private GridView mBackgroundStyleGridView;
    private BackgroundStyleAdapter mBackgroundStyleAdapter;

    private int[] mBackgroundStyle = { R.drawable.always_on_a,
            R.drawable.always_on_b, R.drawable.always_on_c,
            R.drawable.always_on_d,R.drawable.always_on_e,R.drawable.always_on_f };

    public BackgroundStylePreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
        mContext = context;
        setLayoutResource(R.layout.background_style_preference);
    }

    @Override
    public void onBindViewHolder(PreferenceViewHolder view) {
        super.onBindViewHolder(view);
        // TODO Auto-generated method stub
        initGridView(view);
    }

    private void initGridView(PreferenceViewHolder view) {
        // TODO Auto-generated method stub
        mBackgroundStyleGridView = (GridView) view
                .findViewById(R.id.background_style_grid_view);
        mBackgroundStyleAdapter = new BackgroundStyleAdapter(mContext,
                mBackgroundStyle);
        mBackgroundStyleGridView.setAdapter(mBackgroundStyleAdapter);
        mBackgroundStyleAdapter.setFocusPosition(Settings.System.getInt(
                mContext.getContentResolver(), AOD_BG_STYLE, 0));
        mBackgroundStyleGridView.setOnItemClickListener(this);
        mBackgroundStyleGridView.setOnTouchListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View view, int position, long id) {
        // TODO Auto-generated method stub
        Settings.System.putInt(mContext.getContentResolver(), AOD_BG_STYLE,
                position);
        mBackgroundStyleAdapter.setFocusPosition(position);
    }

    @Override
    public boolean onTouch(View arg0, MotionEvent arg1) {
        // TODO Auto-generated method stub
        mBackgroundStyleGridView.getParent()
                .requestDisallowInterceptTouchEvent(true);
        return false;
    }

}
