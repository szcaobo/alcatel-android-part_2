/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.settings.fingerprint;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.hardware.fingerprint.Fingerprint;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Bundle;
import android.os.UserHandle;
import android.provider.Settings;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TctLog;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.android.settings.ChooseLockSettingsHelper;
import com.android.settings.R;
import com.android.settings.SettingsPreferenceFragment;
import com.android.settings.Settings.FingerprintFuncActivity;
import com.android.settings.deviceinfo.MyRadioButtonPreference;
import com.android.internal.logging.MetricsProto.MetricsEvent;
import com.android.settings.func.FuncUtil;

import android.support.v7.preference.PreferenceCategory;
import android.support.v7.preference.PreferenceScreen;

/* MODIFIED-BEGIN by jianguang.sun, 2016-11-05,BUG-2740734*/
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
/* MODIFIED-END by jianguang.sun,BUG-2740734*/


public class FingerprintFunc extends SettingsPreferenceFragment implements MyRadioButtonPreference.OnClickListener {

    private static final String TAG = "FingerprintFunc_main";
    private Context mContext;
    private ArrayList<String> alternativeList;
    private final String KEY_FINGERPRINT_UNLOCK_DEVICE = "unlock_device";
    private final String KEY_FINGERPRINT_QUICK_LAUNCH_FUNC = "quick_lunch_func";
    private MyRadioButtonPreference mUnlockDevice;
    private PreferenceCategory mQuickLaunchFunc;
    private byte[] mToken;
    private Fingerprint mFingerprint;
    private int mFingerId;
    private int mFpFuncSaveNum;
    private final int APP_CHOOSE_RESULT = 101;
    private int mFpMax;
    private static final String FUNC_APPS = "func_apps";
    private int mTotleFuncFp = 0;
    private FingerprintManager mFp;
    private boolean mIsNewFpFuncNum = false;

    /* MODIFIED-BEGIN by jianguang.sun, 2016-11-05,BUG-2740734*/
    private static final String FP_CONTACT_NUMBER = "fp_contactNumber";
    private static final String FUNC_DIAL = "func_dial";
    private final int PICK_CONTACT_REQUEST = 102;
    /* MODIFIED-END by jianguang.sun,BUG-2740734*/

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        addPreferencesFromResource(R.xml.fingerprint_func_settings);
        mContext = getActivity();
        getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
        mToken = getIntent().getByteArrayExtra(
                ChooseLockSettingsHelper.EXTRA_KEY_CHALLENGE_TOKEN);
        if (mToken == null) {
            finish();
        }
        mFp = (FingerprintManager) mContext.getSystemService(
                Context.FINGERPRINT_SERVICE);
        mFingerprint = (Fingerprint) getIntent().getParcelableExtra(
                "fingerprint");
        mFingerId = mFingerprint.getFingerId();
        mFpMax = getContext().getResources().getInteger(
                com.android.internal.R.integer.config_fingerprintMaxTemplatesPerUser);
        mFpFuncSaveNum = getFpFuncSaveNum();
        if (mFingerprint != null) {
            getActivity().setTitle(mFingerprint.getName());
        }else {
            finish();
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        initPreference();
    }

    private void initPreference() {
        PreferenceScreen root = getPreferenceScreen();
        mUnlockDevice = (MyRadioButtonPreference)root.findPreference(KEY_FINGERPRINT_UNLOCK_DEVICE);
        mUnlockDevice.setOnClickListener(this);

        mQuickLaunchFunc = (PreferenceCategory)root.findPreference(KEY_FINGERPRINT_QUICK_LAUNCH_FUNC);
        alternativeList = new ArrayList<>();
        String funcList = getResources().getString(R.string.def_fp_func_list);
        Log.d(TAG, "funcList:   " + funcList);
        String[] tmp = funcList.split(FuncUtil.DELIMITER);
        for (String item : tmp) {
            alternativeList.add(item);
        }
        if (mQuickLaunchFunc != null) {
            mQuickLaunchFunc.removeAll();
        }
        String func = getFuncById(mFingerId);
        Log.d(TAG, "getFuncById:" + func);
        mUnlockDevice.setChecked(func == null);
        final int funcnum = alternativeList.size();
        for (int i = 0; i < funcnum; i++) {
            FuncRadioButtonPreference funcItem = new FuncRadioButtonPreference(getActivity(), null);
            String funcStr = alternativeList.get(i);
            Resources res = getActivity().getResources();
            int id = res.getIdentifier("id/" + funcStr, null, "android");
            if (id != 0) {
                int descId = res.getIdentifier("string/" + funcStr, null, getActivity().getPackageName());
                funcItem.setTitle(descId);
            }
            funcItem.setFunc(funcStr);
            funcItem.setChecked(false);
            if (func != null && func.equals(funcStr)) {
                Log.d(TAG, "getFuncById:" + func + " setChecked" );
                funcItem.setChecked(true);
            }
            if (funcStr != null && funcStr.equals(FUNC_APPS)) {
                String summary = getSelectedApp();
                if (!"".equals(summary)) {
                    funcItem.setSummary(getActivity().getString(R.string.func_apps_summery, summary));
                }
            }

            /* MODIFIED-BEGIN by jianguang.sun, 2016-11-05,BUG-2740734*/
            if (funcStr != null && funcStr.equals(func) && funcStr.equals(FUNC_DIAL)) {
                String summary = getSelectContact();
                if (!"".equals(summary)) {
                    funcItem.setSummary(summary);
                }
            }
            /* MODIFIED-END by jianguang.sun,BUG-2740734*/
            mQuickLaunchFunc.addPreference(funcItem);
            funcItem.setOnClickListener(this);
        }
    }

    private String getFuncById(int fpId) {
        String func;
        String fpInfo = Settings.System.getString(getContentResolver(),
                Settings.System.TCT_FINGERPRINT_FUNC_NUM + mFpFuncSaveNum);
        if (fpInfo != null && fpInfo.contains("" + fpId)) {
            Log.d(TAG, "fpinfo:" + fpInfo);
            String[] tmp = fpInfo.split(":");
            for (int i = 0; i < tmp.length; i++) {
            }
            if (tmp.length < 2) {
                Log.e(TAG, "wrong fpInfo");
            } else {
                return tmp[1];
            }
        }
        return null;
    }

    private String getSelectedApp(){
        String appName = "";
        String fpInfo = Settings.System.getString(getContentResolver(),
                Settings.System.TCT_FINGERPRINT_FUNC_NUM + mFpFuncSaveNum);
        if (fpInfo != null){
            String[] tmp = fpInfo.split(":");
            if (tmp.length == 3) {
               PackageManager pm = getPackageManager();
               try {
                    appName = pm.getApplicationLabel(
                            pm.getApplicationInfo(tmp[2],
                                    PackageManager.GET_META_DATA)).toString();
                } catch (NameNotFoundException e) {
                  e.printStackTrace();
                }
            }
        }
        Log.d(TAG, "appName:   " + appName);
        return appName;
    }


    /* MODIFIED-BEGIN by jianguang.sun, 2016-11-05,BUG-2740734*/
    private String getSelectContact() {
        String[] projection = {ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME};
        Uri contactUri = Uri.parse("content://com.android.contacts/data/");
        String ContactName = "";
        String fpInfo = Settings.System.getString(getContentResolver(),
                Settings.System.TCT_FINGERPRINT_FUNC_NUM + mFpFuncSaveNum);
        if (fpInfo != null) {
            Log.d(TAG, "getSelectContact fpInfo:   " + fpInfo);
            String[] tmp = fpInfo.split(":");
            if (tmp.length == 3) {
                try (Cursor cursor = getContentResolver().query(contactUri,
                        projection,
                        ContactsContract.CommonDataKinds.Phone.NUMBER + "=?",
                        new String[] { tmp[2] }, null)) {
                    if(cursor != null && cursor.moveToFirst()) {
                        int nameColumn = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
                        ContactName = cursor.getString(nameColumn);
                        Log.d(TAG, "ContactName:" + ContactName);
                        cursor.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ContactName;

    }
    /* MODIFIED-END by jianguang.sun,BUG-2740734*/

    private int getFpFuncSaveNum(){
        int result = 0;
        for (int i = 0; i < mFpMax; i++) {
            String funcNumString = Settings.System.TCT_FINGERPRINT_FUNC_NUM + i;
            String fpInfo = Settings.System.getString(getContentResolver(),
                    funcNumString);
            Log.d(TAG, "fpInfo:   " + fpInfo);
            if (fpInfo!=null) {
                mTotleFuncFp ++;
            }
            if (fpInfo == null) {
                result = i;
                mIsNewFpFuncNum = true;
                continue;
            }
            if (fpInfo != null && fpInfo.contains("" + mFingerId)) {
                result = i;
                mIsNewFpFuncNum = false;
            }
        }
        Log.d(TAG, "getFpFuncSaveNum:" + result);
        return result;
    }

    private boolean setFunc(String func, String packageName){
        boolean result;
        String fpfunc;
        if (func == null) {
            fpfunc = null;
        }else if (packageName == null) {
            fpfunc = mFingerId + ":" + func;
        }else {
            fpfunc = mFingerId + ":" + func + ":" + packageName;
        }
        Log.d(TAG, "fpfunc" + fpfunc + " mFpFuncSaveNum:" + mFpFuncSaveNum);
        result = Settings.System.putString(getContentResolver(),
                Settings.System.TCT_FINGERPRINT_FUNC_NUM + mFpFuncSaveNum, fpfunc);
        return result;
    }

    @Override
    public void onRadioButtonClicked(MyRadioButtonPreference emiter) {
        final String func;
        final List<Fingerprint> items = mFp.tctGetEnrolledFingerprints(UserHandle.myUserId(), FingerprintManager.FP_TAG_COMMON);
        final int fingerprintCount = items != null ? items.size() : 0;
        if (emiter == mUnlockDevice) {
            func = null;
            if (setFunc(func, null)) {
                initPreference();
            }
            return;
        } else{
            FuncRadioButtonPreference funcPreference = (FuncRadioButtonPreference)emiter;
            Log.d(TAG, "func clicked:"+  funcPreference.getFunc());
            func = funcPreference.getFunc();
        }
        switch (func) {
        case FUNC_APPS:
            Log.d(TAG, FUNC_APPS);
            Intent intent = new Intent();
            intent.setClassName("com.android.settings",
                    "com.android.settings.func.FuncAppsListActivity");
            startActivityForResult(intent, APP_CHOOSE_RESULT);
            return;

        /* MODIFIED-BEGIN by jianguang.sun, 2016-11-05,BUG-2740734*/
        case FUNC_DIAL:
            Intent mIntent = new Intent(Intent.ACTION_PICK, Uri.parse("content://contacts"));
            mIntent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
            startActivityForResult(mIntent, PICK_CONTACT_REQUEST);
            return;
            /* MODIFIED-END by jianguang.sun,BUG-2740734*/

        default:
            Log.d(TAG, "fingerprintCount :" + fingerprintCount + "  mTotleFuncFp:" + mTotleFuncFp + "  mIsNewFpFuncNum" + mIsNewFpFuncNum);
            if ((fingerprintCount == mTotleFuncFp + 1) && mIsNewFpFuncNum) {
                 ConfirmSetAllFpFuncDialog(func, null);
                 return;
            }
            break;
        }
        if (setFunc(func, null)) {
            initPreference();
        }
    }

    private void ConfirmSetAllFpFuncDialog(String Func, String PackageName) {
            final String func = Func;
            final String packageName = PackageName;
            AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
            .setTitle(R.string.fp_all_setfunc_title)
            .setMessage(R.string.fp_all_setfunc_message)
            .setPositiveButton(R.string.fp_all_setfunc_positive,
            new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (setFunc(func, packageName)) {
                        mTotleFuncFp ++;
                        initPreference();
                    }
                }
            }).setNegativeButton(R.string.fp_all_setfunc_cancel,
            new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    initPreference();
                    dialog.dismiss();
                }
            }).show();
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        /* MODIFIED-BEGIN by jianguang.sun, 2016-11-05,BUG-2740734*/
        final List<Fingerprint> items = mFp.tctGetEnrolledFingerprints(UserHandle.myUserId(), FingerprintManager.FP_TAG_COMMON);
        final int fingerprintCount = items != null ? items.size() : 0;
        /* MODIFIED-END by jianguang.sun,BUG-2740734*/
        if (resultCode == Activity.RESULT_OK
                && requestCode == APP_CHOOSE_RESULT) {
            Bundle bundle = data.getExtras();
            if (bundle != null) {
                String pkgName = bundle.getString("packageName");
                Log.d(TAG, "pkgName:" + pkgName);
                if ((fingerprintCount == mTotleFuncFp + 1) && mIsNewFpFuncNum) {
                    ConfirmSetAllFpFuncDialog(FUNC_APPS,pkgName);
                    return;
                }
                if (setFunc(FUNC_APPS, pkgName)) {
                    initPreference();
                }
            }

        /* MODIFIED-BEGIN by jianguang.sun, 2016-11-05,BUG-2740734*/
        }else if(resultCode == Activity.RESULT_OK
                && requestCode == PICK_CONTACT_REQUEST) {
            Uri contactUri = data.getData();
            String[] projection = {ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME};
            try (
              Cursor cursor = getContentResolver().query(contactUri, projection, null, null, null)){
              if(cursor != null && cursor.moveToFirst()) {
                int column = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                String number = cursor.getString(column);

                cursor.close();
                if ((fingerprintCount == mTotleFuncFp + 1) && mIsNewFpFuncNum) {
                    ConfirmSetAllFpFuncDialog(FUNC_DIAL,number);
                    return;
                }
                if (setFunc(FUNC_DIAL, number)) {
                    initPreference();
                }
              }
            } catch (Exception e) {
              e.printStackTrace();
            }
            /* MODIFIED-END by jianguang.sun,BUG-2740734*/
        }
    }


    @Override
    protected int getMetricsCategory() {
        // TODO Auto-generated method stub
        return MetricsEvent.FINGERPRINT;
    }

    public static class FuncRadioButtonPreference extends MyRadioButtonPreference{

        private String mFunc;
        public FuncRadioButtonPreference(Context context, AttributeSet attrs) {
            super(context, attrs);
            // TODO Auto-generated constructor stub
        }
        public void setFunc(String func){
            mFunc = func;
        }
        public String getFunc(){
            return mFunc;
        }
    }

}
