/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.android.settings.fingerprint;

import android.annotation.Nullable;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.UserHandle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.android.settings.ChooseLockSettingsHelper;
import com.android.settings.InstrumentedActivity;
import com.android.settings.R;
import com.android.setupwizardlib.SetupWizardLayout;
import com.android.setupwizardlib.view.NavigationBar;

/**
 * Base activity for all fingerprint enrollment steps.
 */
public abstract class FingerprintEnrollBase extends InstrumentedActivity
        implements View.OnClickListener {
    static final int RESULT_FINISHED = FingerprintSettings.RESULT_FINISHED;
    static final int RESULT_SKIP = FingerprintSettings.RESULT_SKIP;
    static final int RESULT_TIMEOUT = FingerprintSettings.RESULT_TIMEOUT;

    protected byte[] mToken;
    protected int mUserId;
    protected int mEnrollTag; // MODIFIED by jianguang.sun, 2016-09-27,BUG-2740734
    //[FEATURE]-Add-BEGIN by TCTNB.caixia.chen,10/20/2016,task 2854067
    protected boolean mShowSkipButton;
    protected boolean mFromPrivateMode;
    protected long mChallenge;
    //[FEATURE]-Add-END by TCTNB.caixia.chen

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.Theme_FingerprintEnroll);
        mToken = getIntent().getByteArrayExtra(
                ChooseLockSettingsHelper.EXTRA_KEY_CHALLENGE_TOKEN);
        /* MODIFIED-BEGIN by jianguang.sun, 2016-09-27,BUG-2740734*/
        mEnrollTag = getIntent().getIntExtra(
                ChooseLockSettingsHelper.EXTRA_KEY_ENROLL_FINGERPRINT_TAG, 0);
                /* MODIFIED-END by jianguang.sun,BUG-2740734*/
        if (savedInstanceState != null && mToken == null) {
            mToken = savedInstanceState.getByteArray(
                    ChooseLockSettingsHelper.EXTRA_KEY_CHALLENGE_TOKEN);
        }
        mUserId = getIntent().getIntExtra(Intent.EXTRA_USER_ID, UserHandle.myUserId());
        //[FEATURE]-Add-BEGIN by TCTNB.caixia.chen,10/20/2016,task 2854067
        mShowSkipButton = getIntent().getBooleanExtra(ChooseLockSettingsHelper.EXTRA_KEY_SHOW_SKIP_BUTTON, false);
        mFromPrivateMode = getIntent().getBooleanExtra(ChooseLockSettingsHelper.EXTRA_KEY_FROM_PRIVATE_MODE_SETTINGS, false);
        if (mFromPrivateMode) {
            mChallenge = getIntent().getLongExtra(
                    ChooseLockSettingsHelper.EXTRA_KEY_CHALLENGE, 0);
        }
        //[FEATURE]-Add-END by TCTNB.caixia.chen
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putByteArray(ChooseLockSettingsHelper.EXTRA_KEY_CHALLENGE_TOKEN, mToken);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        initViews();
    }

    protected void initViews() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS |
                WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN |
                WindowManager.LayoutParams.FLAG_LAYOUT_INSET_DECOR);

        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);

        getWindow().setStatusBarColor(Color.TRANSPARENT);
        getNavigationBar().setVisibility(View.GONE);
        Button nextButton = getNextButton();
        if (nextButton != null) {
            nextButton.setOnClickListener(this);
        }
        //[FEATURE]-Add-BEGIN by TCTNB.caixia.chen,10/20/2016,task 2854067
        Button skipButton = getSkipButton();
        if (skipButton != null) {
            skipButton.setOnClickListener(this);
        }
        //[FEATURE]-Add-END by TCTNB.caixia.chen
    }

    protected NavigationBar getNavigationBar() {
        return (NavigationBar) findViewById(R.id.suw_layout_navigation_bar);
    }

    protected SetupWizardLayout getSetupWizardLayout() {
        return (SetupWizardLayout) findViewById(R.id.setup_wizard_layout);
    }

    protected void setHeaderText(int resId, boolean force) {
        TextView layoutTitle = getSetupWizardLayout().getHeaderTextView();
        CharSequence previousTitle = layoutTitle.getText();
        CharSequence title = getText(resId);
        if (previousTitle != title || force) {
            if (!TextUtils.isEmpty(previousTitle)) {
                layoutTitle.setAccessibilityLiveRegion(View.ACCESSIBILITY_LIVE_REGION_POLITE);
            }
            getSetupWizardLayout().setHeaderText(title);
            setTitle(title);
        }
    }

    protected void setHeaderText(int resId) {
        setHeaderText(resId, false /* force */);
    }

    protected Button getNextButton() {
        return (Button) findViewById(R.id.next_button);
    }

    @Override
    public void onClick(View v) {
        if (v == getNextButton()) {
            onNextButtonClick();
        } else if (v == getSkipButton()) {
            onSkipButtonClick();//[FEATURE]-Add by TCTNB.caixia.chen,10/20/2016,task 2854067
        }
    }

    protected void onNextButtonClick() {
    }

    //[FEATURE]-Add-BEGIN by TCTNB.caixia.chen,10/20/2016,task 2854067
    protected void onSkipButtonClick() {
    }

    protected Button getSkipButton() {
        return (Button) findViewById(R.id.skip_button);
    }
    //[FEATURE]-Add-END by TCTNB.caixia.chen

    protected Intent getEnrollingIntent() {
        Intent intent = new Intent();
        intent.setClassName("com.android.settings", FingerprintEnrollEnrolling.class.getName());
        intent.putExtra(ChooseLockSettingsHelper.EXTRA_KEY_ENROLL_FINGERPRINT_TAG, mEnrollTag); // MODIFIED by jianguang.sun, 2016-09-27,BUG-2740734
        intent.putExtra(ChooseLockSettingsHelper.EXTRA_KEY_CHALLENGE_TOKEN, mToken);
        if (mUserId != UserHandle.USER_NULL) {
            intent.putExtra(Intent.EXTRA_USER_ID, mUserId);
        }
        return intent;
    }
}
