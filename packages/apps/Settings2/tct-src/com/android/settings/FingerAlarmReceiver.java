/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.settings;

import android.app.Activity;
import android.app.AlarmManager; // MODIFIED by wanshun.ni, 2016-09-28,BUG-2827925
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
/* MODIFIED-BEGIN by wanshun.ni, 2016-09-27,BUG-2827925*/
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
/* MODIFIED-END by wanshun.ni,BUG-2827925*/
// import android.location.Address;
// import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
/* MODIFIED-BEGIN by wanshun.ni, 2016-10-28,BUG-2827925*/
import android.net.Uri;
import android.os.BatteryManager; // MODIFIED by wanshun.ni, 2016-09-28,BUG-2827925
import android.os.Bundle;
import android.os.UserHandle; // MODIFIED by wanshun.ni, 2016-10-14,BUG-2827925
import android.os.Vibrator;
import android.provider.CallLog; // MODIFIED by wanshun.ni, 2016-09-27,BUG-2827925
import android.provider.Settings;
import android.provider.Telephony.Sms;
/* MODIFIED-END by wanshun.ni,BUG-2827925*/
import android.telephony.SmsManager;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.android.settings.R;
import com.android.settings.Settings.SecuritySettingsActivity; // MODIFIED by wanshun.ni, 2016-10-14,BUG-2827925
import com.google.android.mms.util.SqliteWrapper;

import java.io.IOException;
/* MODIFIED-BEGIN by wanshun.ni, 2016-09-27,BUG-2827925*/
import java.text.SimpleDateFormat;
import java.util.ArrayList; // MODIFIED by wanshun.ni, 2016-10-25,BUG-2827925
import java.util.Date;
/* MODIFIED-END by wanshun.ni,BUG-2827925*/
import java.util.List;
// import java.util.Locale;
import java.util.Set;

/* Create by TCTNB(Guoqiang.Qiu)
 * This BroadcastReceiver to send emergency message when get a broadcast from framework
 */

public class FingerAlarmReceiver extends BroadcastReceiver {

    // [BUGFIX]-Mod-BEGIN by TCTNB.yusong.xuan,03/23/2016,task-1840050 // MODIFIED by wanshun.ni, 2016-09-27,BUG-2827925
    private static String TAG = "FingerAlarmReceiver";
    private LocationManager locationManager;
    private Location mLocation;
    private int locationMode;
    /* MODIFIED-BEGIN by wanshun.ni, 2016-09-28,BUG-2827925*/
    private int batteryCapacity;
    private Boolean sendSmsAlways = true;
    /* MODIFIED-END by wanshun.ni,BUG-2827925*/
    private Context mContext;
    SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss"); // MODIFIED by wanshun.ni, 2016-10-28,BUG-2827925
    private Boolean locationChecked,callRecord,electricity,netWorkCar; // MODIFIED by wanshun.ni, 2016-09-29,BUG-2827925
    private LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
            mLocation = location;
        }

        public void onProviderDisabled(String arg0) {

        }

        public void onProviderEnabled(String arg0) {
        }

        public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
        }

    };
    // [BUGFIX]-Mod-END by TCTNB.yusong.xuan // MODIFIED by wanshun.ni, 2016-09-27,BUG-2827925

    private static int sum = 0;
    private static boolean success = false;
    private static int sendCount = 0; // MODIFIED by wanshun.ni, 2016-10-14,BUG-2827925

    public FingerAlarmReceiver() {
    }

    @Override
    /* MODIFIED-BEGIN by wanshun.ni, 2016-09-27,BUG-2827925*/
    public void onReceive(final Context context, Intent intent) { // MODIFIED by
                                                                    // yusong.xuan,
                                                                    // 2016-03-30,BUG-1690228
        mContext = context;
        /* MODIFIED-BEGIN by wanshun.ni, 2016-10-10,BUG-2827925*/
        Log.d(TAG, "FingerAlarmReceiver receive a broadcast "+intent.getAction());
        if("com.tct.fingerprint.alarm.cancel".equals(intent.getAction())){
            AlarmManager am = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
            Intent myintent = new Intent();
            myintent.setAction(Config.ACTION_ONE_KEY_ALARM);
            PendingIntent pi = PendingIntent.getBroadcast(context,0,myintent,0);
            am.cancel(pi);
            return;
        }
        /* MODIFIED-END by wanshun.ni,BUG-2827925*/
        if (Config.ACTION_ONE_KEY_ALARM.equals(intent.getAction())) {
            /* MODIFIED-BEGIN by wanshun.ni, 2016-10-11,BUG-2827925*/
            if (Settings.System.getInt(context.getContentResolver(),
                    "tct_one_key_alarm", 0) == 0) { // MODIFIED by wanshun.ni,
                // 2016-09-08,BUG-2827925
                return;
            }
            /* MODIFIED-END by wanshun.ni,BUG-2827925*/
            // [FEATURE]-ADD-BEGIN by TCTNB.yusong.xuan,03/23/2016,task-1840050

            /* MODIFIED-BEGIN by wanshun.ni, 2016-09-28,BUG-2827925*/
            //Get current battery capacity
            BatteryManager batteryManager = (BatteryManager) mContext.getSystemService(Context.BATTERY_SERVICE);
            batteryCapacity = batteryManager.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);

            if(sendSmsAlways){
                //set time ,every 25min send ACTION_ONE_KEY_ALARM once
                AlarmManager am = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
                Intent myintent = new Intent();
                myintent.setAction(Config.ACTION_ONE_KEY_ALARM);
                PendingIntent pi = PendingIntent.getBroadcast(context,0,myintent,0);
                am.setExact(0,System.currentTimeMillis()+1000*60*25, pi); // MODIFIED by wanshun.ni, 2016-09-30,BUG-2827925
            }
            if(batteryCapacity < 10){
                sendSmsAlways = false;
            }
            /* MODIFIED-END by wanshun.ni,BUG-2827925*/
            setMobileData(true);
            // [FEATURE]-ADD-END by TCTNB.yusong.xuan,03/23/2016,task-1840050
            SharedPreferences file = context.getSharedPreferences(
                    Config.PREF_NAME, Context.MODE_PRIVATE);
            final Set<String> contactSet = file.getStringSet(
                    Config.KEY_CONTACTS, null);
            if (contactSet == null || contactSet.isEmpty()) {
                return;
            }
            /* MODIFIED-BEGIN by wanshun.ni, 2016-09-29,BUG-2827925*/
            locationChecked = file.getBoolean(Config.KEY_LOCATION, true);
            callRecord = file.getBoolean(Config.KEY_CALLRECORD, true);
            electricity = file.getBoolean(Config.KEY_ELECTRITY, true);
            netWorkCar = file.getBoolean(Config.KEY_NETWORKCAR, true);

            final String msg = file.getString(Config.KEY_MESSAGE,
                    context.getString(R.string.default_message));
                    /* MODIFIED-END by wanshun.ni,BUG-2827925*/

            // [BUGFIX]-Mod-BEGIN by TCTNB.yusong.xuan,03/23/2016,task-1840050
            locationMode = Settings.Secure.getInt(context.getContentResolver(),
                    Settings.Secure.LOCATION_MODE,
                    Settings.Secure.LOCATION_MODE_OFF);
            changeLocationMode(locationMode, 3);
            Log.d(TAG, "change locationMode to High accuracy!");
            if (initLocation()/* && !"".equals(getLocationInformation()) */) {
                sendMessage(contactSet, msg);
            } else {
                Log.d(TAG, "locationIsNullNow, Waiting......");
                new Thread(new Runnable() {
                    public void run() {
                        for (int i = 0; i < 15 && mLocation == null; i++) { // MODIFIED by wanshun.ni, 2016-09-29,BUG-2827925

                            /* MODIFIED-END by wanshun.ni,BUG-2827925*/
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }

                        /* MODIFIED-BEGIN by wanshun.ni, 2016-09-29,BUG-2827925*/
                        if(null == mLocation){
                            /* MODIFIED-BEGIN by wanshun.ni, 2016-09-30,BUG-2827925*/
                            new Thread(new Runnable() {

                                @Override
                                public void run() {
                                    // TODO Auto-generated method stub
                                    for (int j = 0; j < 60 && mLocation == null; j++) {
                                        // "".equals(getLocationInformation())
                                        try {
                                            Thread.sleep(2000);
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    if(mLocation != null) {
                                        sendLocationMessage(contactSet);
                                    }
                                }
                            }).start();
                        }

                        sendMessage(contactSet, msg);
                    }
                }).start();
            }
            // [BUGFIX]-Mod-END by TCTNB.yusong.xuan
        }
        if(Config.ACTION_ONE_KEY_MMS.equals(intent.getAction())){
        /* MODIFIED-END by wanshun.ni,BUG-2827925*/
            sum--;
            if (getResultCode() == Activity.RESULT_OK) {
                success = true;
            }
            if (sum == 0) {
                if (success) {
                    /* MODIFIED-BEGIN by wanshun.ni, 2016-09-27,BUG-2827925*/
                    Vibrator vibrator = (Vibrator) context
                            .getSystemService(Context.VIBRATOR_SERVICE);
                    /* MODIFIED-BEGIN by wanshun.ni, 2016-10-14,BUG-2827925*/
                    vibrator.vibrate(3000);

                    sendCount++;
                    NotificationManager mManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
                    Intent mintent = new Intent();
                    mintent.setClassName("com.android.settings", "com.android.settings.SecuritySettingsAlarm");
                    mintent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    PendingIntent mPendingIntent = PendingIntent.getActivity(mContext, 0, mintent, 0);
                    Notification.Builder notificationBuilder = new Notification.Builder(mContext);
                    notificationBuilder.setSmallIcon(R.drawable.emergency)
                            .setVisibility(Notification.VISIBILITY_SECRET) // MODIFIED by wanshun.ni, 2016-10-28,BUG-2827925
                            .setContentTitle(mContext.getString(R.string.one_finger_alarm))
                            .setContentText(mContext.getString(R.string.alarm_counts)+":" + sendCount)
                            .setContentIntent(mPendingIntent);
                    Notification notification = notificationBuilder.getNotification();
                    notification.flags |= Notification.FLAG_AUTO_CANCEL;
                    mManager.notifyAsUser(null, R.drawable.emergency,notificationBuilder.build(), UserHandle.ALL);
                    /* MODIFIED-END by wanshun.ni,BUG-2827925*/
                    Log.d(TAG, "sendMessageSuccess");
                } else {
                    Log.d(TAG, "sendMessageFailure");
                    /* MODIFIED-END by wanshun.ni,BUG-2827925*/
                }
                success = false;
            }
        }
    }

    private String getLocationUrl() {
        if (mLocation == null) {
            return null;
        }
        /* MODIFIED-BEGIN by wanshun.ni, 2016-09-27,BUG-2827925*/
        return String.format(Config.URL_FORMAT, mLocation.getLatitude(),
                mLocation.getLongitude());
    }

    private void sendMessage(final Set<String> contactSet, String message) {
        final SmsManager smsManager = SmsManager
                .getSmsManagerForSubscriptionId(getSmsManagerSubId());
        // String address = getLocationInformation();
        /* MODIFIED-BEGIN by wanshun.ni, 2016-09-29,BUG-2827925*/

        Date curDate = new Date(System.currentTimeMillis());
        String devices_battery = mContext.getString(R.string.electricity) + ":" + batteryCapacity +"%\n"; // MODIFIED by wanshun.ni, 2016-10-11,BUG-2827925

        /* MODIFIED-BEGIN by wanshun.ni, 2016-10-25,BUG-2827925*/
        String devices_info = mContext.getString(R.string.devices_info) + mContext.getString(R.string.alarm_message_date)+format.format(curDate) +"\n";

        /* MODIFIED-BEGIN by wanshun.ni, 2016-09-30,BUG-2827925*/
        ArrayList<String> msgList = smsManager.divideMessage(message + "\n");
        msgList.add(devices_info);
        if(electricity){
            msgList.add(devices_battery);
        }

        if(callRecord) {
            List<String> callRrcord = smsManager.divideMessage(mContext.getString(R.string.last_five_call) + getCallRecord());
            /* MODIFIED-END by wanshun.ni,BUG-2827925*/
            msgList.addAll(callRrcord);
        }

        /* MODIFIED-BEGIN by wanshun.ni, 2016-10-28,BUG-2827925*/
        if (netWorkCar) {
            List<String> netWorkCarCall = smsManager.divideMessage(mContext
                    .getString(R.string.last_five_networtcar)
                    + getCarMessageCount());
            msgList.addAll(netWorkCarCall);
        }
        /* MODIFIED-END by wanshun.ni,BUG-2827925*/

        final String url = getLocationUrl();
        /* MODIFIED-END by wanshun.ni,BUG-2827925*/
        if(locationChecked){
            if (url != null) {
                Log.d(TAG, url);
                msgList.add(mContext.getString(R.string.devices_current_location) + mContext.getString(R.string.map_url) + url);
            } else {
                msgList.add(mContext.getString(R.string.devices_current_location) + mContext.getString(R.string.can_not_locate));
            }
            /* MODIFIED-END by wanshun.ni,BUG-2827925*/
        }
        /* MODIFIED-BEGIN by wanshun.ni, 2016-10-25,BUG-2827925*/
        sum = contactSet.size();
        Intent msgIntent = new Intent(Config.ACTION_ONE_KEY_MMS);
        /* MODIFIED-BEGIN by wanshun.ni, 2016-09-27,BUG-2827925*/
        final PendingIntent pendingIntent = PendingIntent.getBroadcast(
                mContext, 0, msgIntent, 0);
        ArrayList<PendingIntent> sentPendingIntents = new ArrayList<PendingIntent>();
        for (int i = 0; i < msgList.size(); i++) {
            sentPendingIntents.add(i, pendingIntent);
        }
        for (String number : contactSet) {
            /* MODIFIED-BEGIN by wanshun.ni, 2016-10-28,BUG-2827925*/
            try {
                smsManager.sendMultipartTextMessage(number, null, msgList,
                        sentPendingIntents, null);
            } catch (Exception e) {
                Log.e(TAG, "SmsMessageSender.sendMessage: caught", e);
            }
            /* MODIFIED-END by wanshun.ni,BUG-2827925*/

        }
        if(url != null) {
            changeLocationMode(3, locationMode);
            locationManager.removeUpdates(locationListener);
            Log.d(TAG, "remove location listener!");
        }
    }

    private void sendLocationMessage(final Set<String> contactSet) {
        final SmsManager smsManager = SmsManager
                .getSmsManagerForSubscriptionId(getSmsManagerSubId());
        // String address = getLocationInformation();
        Log.d(TAG, "send location message");

        Intent msgIntent = new Intent(Config.ACTION_ONE_KEY_MMS);
        final PendingIntent pendingIntent = PendingIntent.getBroadcast(
                mContext, 0, msgIntent, 0);

        if(locationChecked && null != getLocationUrl()) {
            for (String number : contactSet) {
                smsManager.sendTextMessage(number, null, mContext.getString(R.string.devices_current_location)+getLocationUrl(), pendingIntent,
                        null);
                        /* MODIFIED-END by wanshun.ni,BUG-2827925*/
            }
        }
        changeLocationMode(3, locationMode);
        locationManager.removeUpdates(locationListener);
        Log.d(TAG, "remove location listener!");
    }

    private int getSmsManagerSubId() {
        int subId = SubscriptionManager.getDefaultSmsSubscriptionId();
        /* MODIFIED-BEGIN by wanshun.ni, 2016-09-27,BUG-2827925*/
        TelephonyManager telephonyManager = (TelephonyManager) mContext
                .getSystemService(Context.TELEPHONY_SERVICE);
        if (telephonyManager.isMultiSimEnabled()) {
            int slotId = SubscriptionManager.getSlotId(subId);
            Log.d(TAG, "slotId->" + slotId);
            /* MODIFIED-END by wanshun.ni,BUG-2827925*/
            int simState = telephonyManager.getSimState(slotId);
            boolean active = (simState != TelephonyManager.SIM_STATE_ABSENT)
                    && (simState != TelephonyManager.SIM_STATE_UNKNOWN);
            if (active && telephonyManager.hasIccCard(slotId)) {
                return subId;
            } else {
                int slot2Id = slotId == 1 ? 0 : 1;
                int sub2Id[] = SubscriptionManager.getSubId(slot2Id);
                if (sub2Id != null) {
                    subId = sub2Id[0];
                }
            }
        }
        return subId;
    }

    /* MODIFIED-BEGIN by wanshun.ni, 2016-09-27,BUG-2827925*/
    // [BUGFIX]-Mod-BEGIN by TCTNB.yusong.xuan,03/23/2016,task-1840050
    private boolean initLocation() {
        locationManager = (LocationManager) mContext
                .getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(
                LocationManager.NETWORK_PROVIDER, 500, 0, locationListener);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                500, 0, locationListener);
        mLocation = locationManager
                .getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (mLocation == null) {
            mLocation = locationManager
                    .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    /* MODIFIED-END by wanshun.ni,BUG-2827925*/
        }
        return mLocation != null;
    }

    private void setMobileData(boolean enabled) {
        // Do not make mobile data on/off if airplane mode on or has no sim card
        /* MODIFIED-BEGIN by wanshun.ni, 2016-09-27,BUG-2827925*/
        TelephonyManager mTelephonyManager = (TelephonyManager) mContext
                .getSystemService(Context.TELEPHONY_SERVICE);
        if (Settings.Global.getInt(mContext.getContentResolver(),
                Settings.Global.AIRPLANE_MODE_ON, 0) != 0
                || !hasIccCard(mTelephonyManager)) {
            return;
        }
        /* MODIFIED-BEGIN by wanshun.ni, 2016-09-30,BUG-2827925*/
        mTelephonyManager.setDataEnabled(enabled);
/* MODIFIED-END by wanshun.ni,BUG-2827925*/
    }

    private boolean hasIccCard(final TelephonyManager telephonyManager) {
        if (telephonyManager.isMultiSimEnabled()) {
            int prfDataSlotId = SubscriptionManager
                    .getSlotId(SubscriptionManager
                            .getDefaultDataSubscriptionId());
                            /* MODIFIED-END by wanshun.ni,BUG-2827925*/
            int simState = telephonyManager.getSimState(prfDataSlotId);
            boolean active = (simState != TelephonyManager.SIM_STATE_ABSENT)
                    && (simState != TelephonyManager.SIM_STATE_UNKNOWN);
            return active && telephonyManager.hasIccCard(prfDataSlotId);
        } else {
            return telephonyManager.hasIccCard();
        }
    }

    public void changeLocationMode(int oldMode, int newMode) {
        /* MODIFIED-BEGIN by wanshun.ni, 2016-09-27,BUG-2827925*/
        Settings.Secure.putInt(mContext.getContentResolver(),
                Settings.Secure.LOCATION_MODE, newMode);
    }

    // [BUGFIX]-Mod-END by TCTNB.yusong.xuan

    // get call record
    private String getCallRecord() {
        ContentResolver cr = mContext.getContentResolver();
        String callHistory = ""; // MODIFIED by wanshun.ni,
                                 // 2016-09-29,BUG-2827925
        Cursor cs = null;
        try {
            cs = cr.query(CallLog.Calls.CONTENT_URI, new String[] {
                    CallLog.Calls.NUMBER, CallLog.Calls.TYPE,
                    CallLog.Calls.DATE, CallLog.Calls.DURATION }, null, null,
                    CallLog.Calls.DEFAULT_SORT_ORDER);
            int i = 0;
            if (cs != null && cs.getCount() > 0) {
                for (cs.moveToFirst(); !cs.isAfterLast() & i < 5; cs
                        .moveToNext()) {
                    String callNum = cs.getString(0);
                    // call type
                    int callType = Integer.parseInt(cs.getString(1));
                    String callTypeStr = "";
                    switch (callType) {
                    case CallLog.Calls.INCOMING_TYPE:
                        callTypeStr = mContext
                                .getString(R.string.call_incoming);
                        break;
                    case CallLog.Calls.OUTGOING_TYPE:
                        callTypeStr = mContext.getString(R.string.call_outging);
                        break;
                    case CallLog.Calls.MISSED_TYPE:
                        callTypeStr = mContext.getString(R.string.call_missed);
                        break;
                    }
                    // call date
                    SimpleDateFormat sdf = new SimpleDateFormat("MM-dd HH:mm");
                    Date callDate = new Date(Long.parseLong(cs.getString(2)));

                    String callDateStr = sdf.format(callDate);
                    // call during

                    int callDuring = Integer.parseInt(cs.getString(3));
                    String callState = "";
                    if (callDuring == 0) {
                        callState = mContext.getString(R.string.call_missed);
                    } else {
                        int hour = callDuring / 3600;
                        int min = callDuring % 3600 / 60;
                        int sec = callDuring % 60;
                        callState = mContext.getString(R.string.call_time)
                                + hour + ":" + min + ":" + sec;
                    }

                    String callOne = "" + callDateStr + " " + callTypeStr + " "
                            + callNum + " " + callState + ";\n"; // MODIFIED by wanshun.ni, 2016-09-29,BUG-2827925
                    callHistory += callOne;
                    i++;
                }
            } else {
                callHistory = mContext.getString(R.string.no_call_record);
            }
        } catch (Exception e) {
            Log.e(TAG, "getCallRecord() Exception: \n", e);
        } finally {
            if (cs != null) {
                cs.close();
            }
        }
        return callHistory;
        /* MODIFIED-END by wanshun.ni,BUG-2827925 */
    }

    private String getCarMessageCount() {
        Uri CONTENT_CAR_URI = Uri.parse("content://sms/car");
        String carInfoString = "";
        Cursor cursor = null;
        int count = 0;
        try {
            cursor = SqliteWrapper.query(mContext,
                    mContext.getContentResolver(), CONTENT_CAR_URI, null, null,
                    null, null);
            if (null != cursor && cursor.getCount() > 0) {
                int i = 0;
                for (cursor.moveToFirst(); !cursor.isAfterLast() & i < 5; cursor
                        .moveToNext()) {
                    Date callDateOfCar = new Date(Long.parseLong(cursor
                            .getString(4)));
                    String callDateStr = format.format(callDateOfCar);
                    String body = cursor.getString(12);
                    carInfoString += callDateStr + body + "\n";
                    i++;
                }
            } else {
                carInfoString = mContext.getString(R.string.no_networt_car);
            }
        } catch (Exception e) {
            Log.e(TAG, "getCarMessageCount() Exception: \n", e);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return carInfoString;
    }

}
