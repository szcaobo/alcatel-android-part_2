/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.settings;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;


public class AlarmDateBaseHelper extends SQLiteOpenHelper {
    public static final String Create_sql = "create table alarmContact (name text, number integer)";
    private Context mcontext;

    public AlarmDateBaseHelper(Context context, String name,
            SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
        mcontext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Create_sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
