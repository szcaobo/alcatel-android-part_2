/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.settings;

public class ContactsContent {
    public String title;
    public String summary;
    public ContactsContent(String title,String summary){
        super();
        this.summary = summary;
        this.title = title;
    }
}
