package com.android.settings.lockapp.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.android.settings.R;

public class LetterView extends View {
    private String[] mLetters = null;
    private int mChoose = -1;
    private Paint textPaint;
    private Paint textHighLightPaint;
    private boolean mShowBg = false;
    private int indexHeight;
    private PopupWindow mPopupWindow;
    private TextView mPopupTextView;
    private int height;

    private Handler mHandler = new Handler();

    private OnLetterClickListener mOnLetterClickListener;

    private static final long POPUP_DELAY_MILLIS = 800;
    private final int IndexHighLightColor = 0xff27b8af;
    private final int IndexColor = 0xff666666;
    private int mTextSize;
    private int width;
    private int sectionLength;

    private int MAX_TEXTSIZE = 31 ;//this value maybe difference in different project
    private int MIN_TEXTSIZE = 16 ;//this value maybe difference in different project

    private final static Typeface M_TYPEFACE = Typeface.DEFAULT ;
    public LetterView(Context context) {
        super(context);
        init(context);
    }

    public LetterView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public LetterView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public void setOnLetterClickListener(OnLetterClickListener listener) {
        mOnLetterClickListener = listener;
    }

    private void init(Context context) {
        mLetters = getResources().getStringArray(R.array.sections);
        sectionLength = mLetters.length;
        mTextSize = getResources().getDimensionPixelSize(R.dimen.list_search_bar_text_size);
        textPaint = new Paint();
        textPaint.setColor(IndexColor);
        textPaint.setTextSize(mTextSize);
        textPaint.setTypeface(M_TYPEFACE);
        textPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
        textHighLightPaint = new Paint();
        textHighLightPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
        textHighLightPaint.setColor(IndexHighLightColor);
        textHighLightPaint.setTextSize(mTextSize);
        textHighLightPaint.setTypeface(M_TYPEFACE);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (mShowBg) {// 显示按下时选择框背景
            canvas.drawColor(Color.parseColor("#40000000"));
        }

        for (int i = 0; i < mLetters.length; i++) {
            float paddingLeft = (width - textPaint.measureText(mLetters[i])) / 2;

            if (i == mChoose) {//选中状态下
                canvas.drawText(mLetters[i],paddingLeft, (i + 4/3) * indexHeight,
                        textHighLightPaint);
            } else {
                canvas.drawText(mLetters[i], paddingLeft, (i + 4/3) * indexHeight,
                        textPaint);
            }
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        final int action = event.getAction();
        final float y = event.getY();
        final int oldChoose = mChoose;
        final int c = (int) (y / getHeight() * mLetters.length);

        switch (action) {
            case MotionEvent.ACTION_DOWN:
                mShowBg = false;
                if (oldChoose != c) {
                    if (c >= 0 && c < mLetters.length) {
                        performItemClicked(c);
                        mChoose = c;
                        invalidate();
                    }
                }
                break;
            case MotionEvent.ACTION_MOVE:
                if (oldChoose != c) {
                    if (c >= 0 && c < mLetters.length) {
                        performItemClicked(c);
                        mChoose = c;
                        invalidate();
                    }
                }
                break;
            case MotionEvent.ACTION_UP:
                mShowBg = false;
                mChoose = -1;
                dismissPopup();
                invalidate();
                break;
        }
        return true;
    }

    private void showPopup(int item) {
        if (mPopupWindow == null) {
            mHandler.removeCallbacks(mDismissRunnable);
            mPopupTextView = new TextView(getContext());
            mPopupTextView.setBackgroundColor(Color.parseColor("#b4000000"));
            mPopupTextView.setGravity(Gravity.CENTER);
            mPopupTextView.setTextColor(Color.WHITE);
            mPopupWindow = new PopupWindow(mPopupTextView);
        }

        String text;
        if (item == 0) {
            text = "已加锁";
        } else if (item == 1){
            text = "建议加锁";
        } else {
            text = mLetters[item];
        }
        mPopupTextView.setText(text);
        mPopupTextView.setTextSize(28);

//        if (mPopupWindow.isShowing()) {
//            mPopupWindow.update();
//        } else {
            mPopupWindow.setWidth(200);
            mPopupWindow.setHeight(200);
            mPopupWindow.showAtLocation(getRootView(), Gravity.CENTER, 0, 0);
//        }
    }

    private void dismissPopup() {
        mHandler.postDelayed(mDismissRunnable, POPUP_DELAY_MILLIS);
    }

    private Runnable mDismissRunnable = new Runnable() {
        @Override
        public void run() {
            if (mPopupWindow != null) {
                mPopupWindow.dismiss();
            }
        }
    };

    private void performItemClicked(int item) {
        if (mOnLetterClickListener != null) {
//            String s;
//            if (item == 0) {
//                s = "#";// “加锁应用”的占位符设定的为"#"
//            } else {
//                s = mLetters[item];
//            }
            String s = mLetters[item];
            mOnLetterClickListener.onLetterClick(s);
            //showPopup(item);
        }
    }

    public interface OnLetterClickListener {
        void onLetterClick(String letter);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        indexHeight = h / sectionLength;
        mTextSize = h / sectionLength;
        boolean isPortrait = getResources().getConfiguration().orientation == 1;
        if (isPortrait && mTextSize < MIN_TEXTSIZE) {
            mTextSize = MAX_TEXTSIZE;
            if(indexHeight<MAX_TEXTSIZE){
                indexHeight = MAX_TEXTSIZE;
            }
        }
        if (!isPortrait && mTextSize < MIN_TEXTSIZE) {
            mTextSize = MIN_TEXTSIZE;
            indexHeight = MIN_TEXTSIZE;
        }
        if (mTextSize > MAX_TEXTSIZE) {
            mTextSize = MAX_TEXTSIZE;
        }
        textHighLightPaint.setTextSize(mTextSize);
        textPaint.setTextSize(mTextSize);
        super.onSizeChanged(w, h, oldw, oldh);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        height = getMeasuredHeight();
        width = getWidth();
        if(width==0){
            width = getResources().getDimensionPixelSize(R.dimen.list_search_bar_default_width);
        }
    }
}
