package com.android.settings.lockapp.receiver;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.provider.Settings;
import android.provider.Settings.System;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.android.settings.lockapp.ui.AppInfoParams;
import com.android.settings.lockapp.ui.PasswordDialog;
import com.android.settings.lockapp.ui.PatternDialog;
import com.android.settings.lockapp.ui.PatternLockActivity;
import com.android.settings.lockapp.ui.PasswordLockActivity;
import com.android.settings.lockapp.utils.ApplicationLockUtils;
import com.android.settings.lockapp.utils.SharedPreferenceUtil;
import com.android.settings.R;

public class AppLockedReceiver extends BroadcastReceiver{
    private final static String TAG = "SecurityCenter.AppLockedReceiver ";
    private IntentFilter intentFilter;
    private WindowManager windowManager;
    private Window window;
    private WindowManager.LayoutParams layoutParams;
    private static boolean mShouldShowDialog = true;
    private final static int MSG_SHOW_NUM_DIALOG = 1;
    private final static int MSG_SHOW_PATTERN_DIALOG = 2;
    private Context mContext;
    private PackageManager localPackageManager;

    @Override
    public void onReceive(Context context, Intent intent) {
        mContext = context;
        if (intent.getAction().equals("com.tct.action.UNLOCK_APP")) {
            Log.d(TAG, "xz-topActivity: " + getTopActivity());
            if ("com.android.settings.lockapp.ui.PasswordLockActivity".equals(getTopActivity())
                || "com.android.settings.lockapp.ui.PatternLockActivity".equals(getTopActivity())) {
                Log.d(TAG, "xz- LockScreen has shown, ignore");
                return;
            }

            //[BUGFIX]-Add-BEGIN by TCTNB.caixia.chen,06/12/2016,2114657
            if (ApplicationLockUtils.isApplicationKilled(context, intent.getStringExtra("packageName"))) {
                Log.w(TAG, "application is killed, ignore");
                return;
            }
            //[BUGFIX]-Add-END by TCTNB.caixia.chen

            Log.d(TAG, "xz-should show LockScreen");
 //           if (mShouldShowDialog) {
            String packageName = intent.getStringExtra("packageName");
            intent.putExtra("package_name", packageName);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setClassName("com.android.settings", "com.android.settings.JumpActivity");
            Log.i(TAG, "start JumpActivity");
            mContext.startActivity(intent);
        }
    }


    private String getTopActivity () {
         ActivityManager manager = (ActivityManager)mContext.getSystemService(Context.ACTIVITY_SERVICE);
         List<RunningTaskInfo> runningTaskInfos = manager.getRunningTasks(1);
         if(runningTaskInfos != null) {
            return (runningTaskInfos.get(0).topActivity).getClassName().toString();
         } else {
           return null;
         }
    }
}
