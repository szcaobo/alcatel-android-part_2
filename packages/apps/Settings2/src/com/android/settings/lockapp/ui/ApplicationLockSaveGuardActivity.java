package com.android.settings.lockapp.ui;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TextView.OnEditorActionListener;

import com.android.settings.R;
import com.android.settings.lockapp.activity.ApplicationLockActivity;
import com.android.settings.lockapp.utils.SharedPreferenceUtil;

public class ApplicationLockSaveGuardActivity extends Activity implements
OnEditorActionListener, TextWatcher {

    private Button mCompleteButton;
    private Button mSetLaterButton;
    private EditText mPasswordEntry;
    private Spinner mSpinner;
    private static int mChoose;
    private Boolean isFromApp = false;
    private TextView mspinner_text;
    private Boolean isFromSettings = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        getWindow().setStatusBarColor(Color.parseColor("#f8f8f8"));
        setContentView(R.layout.activity_application_lock_savegurad);
        getActionBar().setTitle(R.string.saveguard);
        isFromApp = getIntent().getBooleanExtra("from_app_forgot", false);
        isFromSettings = getIntent().getBooleanExtra("from_settings", false);
        mSpinner = ((Spinner) findViewById(R.id.spinner));
        mspinner_text = ((TextView) findViewById(R.id.spinner_text));
        mPasswordEntry = ((EditText) findViewById(R.id.password_entry));
        mPasswordEntry.setOnEditorActionListener(this);
        mPasswordEntry.addTextChangedListener(this);
        mSetLaterButton = ((Button) findViewById(R.id.setlater_button));
        mCompleteButton = ((Button) findViewById(R.id.next_button));
        mCompleteButton.setEnabled(false);
        if (!SharedPreferenceUtil.readIsFirst(this)) {
            mSetLaterButton.setVisibility(View.GONE);
        }
        if(isFromApp) {
            mSpinner.setVisibility(View.GONE);
            mspinner_text.setVisibility(View.VISIBLE);
            mspinner_text.setText(getResources()
                    .getStringArray(R.array.password_questions)[SharedPreferenceUtil.readQuestion(this)]);
        }
        ArrayAdapter<String> localArrayAdapter = new ArrayAdapter<String>(this,
                R.layout.simple_spinner_item, getResources()
                        .getStringArray(R.array.password_questions));
        localArrayAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(localArrayAdapter);
        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                    int position, long id) {
                mChoose = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mSetLaterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isFromSettings) {
                    Intent intent = new Intent(ApplicationLockSaveGuardActivity.this, ApplicationLockActivity.class);
                    startActivity(intent);
                }
                SharedPreferenceUtil.editIsFirst(ApplicationLockSaveGuardActivity.this, false);
                ((InputMethodManager) ApplicationLockSaveGuardActivity.this
                        .getSystemService(Context.INPUT_METHOD_SERVICE))
                        .hideSoftInputFromWindow(mPasswordEntry.getWindowToken(), 0);
                ApplicationLockSaveGuardActivity.this.finish();
            }
        });

        mCompleteButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View paramView) {
                final String answer = mPasswordEntry.getText().toString();
                if (SharedPreferenceUtil.readIsFirst(ApplicationLockSaveGuardActivity.this)) {
                    if (answer != null && answer != "") {
                        if (isFromSettings) {
                            SharedPreferenceUtil.editIsFirst(ApplicationLockSaveGuardActivity.this, false);
                            savePasswordQuestion(mChoose, answer);
                            ((InputMethodManager) ApplicationLockSaveGuardActivity.this
                                    .getSystemService(Context.INPUT_METHOD_SERVICE))
                                    .hideSoftInputFromWindow(mPasswordEntry.getWindowToken(), 0);
                            setResult(Activity.RESULT_OK);
                            ApplicationLockSaveGuardActivity.this.finish();
                        } else {
                            Intent intent = new Intent(ApplicationLockSaveGuardActivity.this, ApplicationLockActivity.class);
                            startActivity(intent);
                            SharedPreferenceUtil.editIsFirst(ApplicationLockSaveGuardActivity.this, false);
                            savePasswordQuestion(mChoose, answer);
                            ((InputMethodManager) ApplicationLockSaveGuardActivity.this
                                    .getSystemService(Context.INPUT_METHOD_SERVICE))
                                    .hideSoftInputFromWindow(mPasswordEntry.getWindowToken(), 0);
                            ApplicationLockSaveGuardActivity.this.finish();
                        }
                    }
                } else if (isFromApp) {
                    if (answer.equals(SharedPreferenceUtil.readQuestionAnswer(ApplicationLockSaveGuardActivity.this)))  {
                        Intent intent = new Intent(ApplicationLockSaveGuardActivity.this, ChooseLockPatternActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        SharedPreferenceUtil.editShouldReturn(ApplicationLockSaveGuardActivity.this, true);
                        ApplicationLockSaveGuardActivity.this.finish();
                    } else {
                        //Toast.makeText(ApplicationLockSaveGuardActivity.this, "密保问题错误", Toast.LENGTH_SHORT).show();
                        Toast.makeText(ApplicationLockSaveGuardActivity.this,
                                ApplicationLockSaveGuardActivity.this.getResources().
                                getString(R.string.wrong_saveguard),Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (answer != null && answer != "") {
                        savePasswordQuestion(mChoose, answer);
                        ApplicationLockSaveGuardActivity.this.finish();
                    }
                }
            }
        });

        new Timer().schedule(new TimerTask() {
            public void run() {
                ((InputMethodManager) ApplicationLockSaveGuardActivity.this
                        .getSystemService("input_method")).showSoftInput(
                        mPasswordEntry, 0);
            }
        }, 500);
    }

    @Override
    protected void onStop() {
        super.onStop();
        finish();
    }

    private void savePasswordQuestion(int position, String str) {
        SharedPreferenceUtil.editQuestion(this, position);
        SharedPreferenceUtil.editQuestionAnswer(this, str);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count,
            int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable s) {
        String password = mPasswordEntry.getText().toString();
        if (password != null && password != "") {
            mCompleteButton.setEnabled(true);
        }
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        return false;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK ) {
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

}
