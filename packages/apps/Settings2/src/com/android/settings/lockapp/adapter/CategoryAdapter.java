package com.android.settings.lockapp.adapter;

import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.settings.R;
import com.android.settings.lockapp.activity.ApplicationLockActivity;
import com.android.settings.lockapp.ui.AppInfoParams;
import com.android.settings.lockapp.ui.AppSectionIndexer;

import java.util.List;

public class CategoryAdapter extends BaseAdapter {

    private final static String TAG = "CategoryAdapter";
    private static int lockedAppNum = 0;
    private PackageManager localPackageManager;
    private Context mContext;
    private List<Category> mListData;
    private LayoutInflater mInflater;
    private String mFirstLetter;
    private String mPreFirstLetter;
    private AppSectionIndexer mIndexer;
    private static final int TYPE_CATEGORY_ITEM = 0;
    private static final int TYPE_ITEM = 1;


    public CategoryAdapter(Context paramContext, List<Category> paramList,
                           PackageManager paramPackageManager, AppSectionIndexer indexer) {
        this.mContext = paramContext;
        this.mListData = paramList;
        this.localPackageManager = paramPackageManager;
        this.mInflater = LayoutInflater.from(this.mContext);
        this.mIndexer = indexer;
    }

    @Override
    public int getCount() {
        int count = 0;

        if (null != mListData) {
            //  所有分类中item的总和是ListVIew  Item的总个数
            for (Category category : mListData) {
                count += category.getItemCount();
            }
        }

        return count;
    }

    @Override
    public Object getItem(int position) {

        // 异常情况处理
        if (null == mListData || position <  0|| position > getCount()) {
            return null;
        }

        // 同一分类内，第一个元素的索引值
        int categroyFirstIndex = 0;

        for (Category category : mListData) {
            int size = category.getItemCount();
            // 在当前分类中的索引值
            int categoryIndex = position - categroyFirstIndex;
            // item在当前分类内
            if (categoryIndex < size) {
                Log.d("yshlist","position: "+position+ "categoryIndex: "+categoryIndex+"");
                return  category.getItem(categoryIndex);
            }

            // 索引移动到当前分类结尾，即下一个分类第一个元素索引
            categroyFirstIndex += size;
        }

        return null;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        int itemViewType = getItemViewType(position);


        switch (itemViewType) {
            case TYPE_CATEGORY_ITEM:
                if (null == convertView) {
                    convertView = mInflater.inflate(R.layout.listview_item_header, null);
                }
                TextView textView = (TextView) convertView.findViewById(R.id.header);
                AppInfoParams titleApp = (AppInfoParams) getItem(position);
                if ("*".equals(String.valueOf(titleApp.letter.charAt(0)))) {
                    textView.setText(R.string.has_locked);
                } else if ("#".equals(String.valueOf(titleApp.letter.charAt(0)))) {
                    textView.setText(R.string.suggest_lock);
                } else {
                    textView.setText(String.valueOf(titleApp.letter.charAt(0)));
                }
                break;

            case TYPE_ITEM:
                AppInfoViewHolder localAppInfoViewHolder = null;
                if (null == convertView) {
                    localAppInfoViewHolder = new AppInfoViewHolder();
                    convertView = mInflater.inflate(com.mst.R.layout.list_item_1_line_with_icon_multiple_choice, null);
                    localAppInfoViewHolder.iv_app_icon = (ImageView) convertView
                            .findViewById(android.R.id.icon);
                    localAppInfoViewHolder.tv_app_name = (TextView) convertView
                            .findViewById(android.R.id.text2);
                    localAppInfoViewHolder.checkBox = (CheckBox) convertView
                            .findViewById(android.R.id.button1);
                    localAppInfoViewHolder.checkBox.setButtonDrawable(R.drawable.checkbox_selector);
                    convertView.setTag(localAppInfoViewHolder);
                } else {
                    localAppInfoViewHolder = (AppInfoViewHolder) convertView.getTag();
                }

                AppInfoParams localAppInfoParams = (AppInfoParams) getItem(position);
                localAppInfoViewHolder.tv_app_name.setText(localAppInfoParams.appName);
                localAppInfoViewHolder.iv_app_icon.setImageDrawable(localAppInfoParams.appIcon);
                localAppInfoViewHolder.checkBox.setOnCheckedChangeListener(null); // MODIFIED by zheng.ding, 2016-10-10,BUG-2825800
                boolean isChecked = isPackageLocked(localAppInfoParams.packageName,
                        position);
                localAppInfoViewHolder.checkBox.setChecked(isChecked);

//                int section = mIndexer.getSectionForPosition(position);//position对应的索引

               // setChecked(position, isChecked);
//                final int pos = position;

//                localAppInfoViewHolder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                    @Override
//                    public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
//                        if (checked) {
//                            setChecked(pos, true);
//                            Log.d(TAG, "package name" + localAppInfoParams.packageName
//                                    + "setLocked");
//                            localPackageManager.setApplicationLockStatus(localAppInfoParams.packageName, 1);
//                            if (mContext instanceof ApplicationLockActivity) {
//                                ((ApplicationLockActivity) mContext).updateLockedNum();
//                            }
//                        } else {
//                            setChecked(pos, false);
//                            localPackageManager.setApplicationLockStatus(localAppInfoParams.packageName, 0);
//                            if (mContext instanceof ApplicationLockActivity) {
//                                ((ApplicationLockActivity) mContext).updateLockedNum();
//                            }
//                        }
//                    }
//                });
                break;
        }
        return convertView;
    }

    private boolean isPackageLocked(String paramString, int paramInt) {
        try {
            if (localPackageManager != null) {
                if (localPackageManager.getApplicationLockStatus(paramString) != 0) {
                    Log.d(TAG, "package name " + paramString + " true");
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception localException) {
        }
        return false;
    }

//    public void setChecked(int paramInt, boolean paramBoolean) {
//        AppInfoParams localAppInfoParams = (AppInfoParams) this.mListData
//                .get(paramInt);
//        if (localAppInfoParams == null)
//            return;
//        localAppInfoParams.setChecked(paramBoolean);
//    }

    public class AppInfoViewHolder {
        ImageView iv_app_icon;
        TextView tv_app_name;
        TextView tv_letter;
        //public ToggleSwitch tv_app_switch;
        public CheckBox checkBox;
    }
    @Override
    public int getItemViewType(int position) {
        // 异常情况处理
        if (null == mListData || position <  0|| position > getCount()) {
            return TYPE_ITEM;
        }

        int categroyFirstIndex = 0;

        for (Category category : mListData) {
            int size = category.getItemCount();
            // 在当前分类中的索引值
            int categoryIndex = position - categroyFirstIndex;
            if (categoryIndex == 0) {
                return TYPE_CATEGORY_ITEM;
            }

            categroyFirstIndex += size;
        }

        return TYPE_ITEM;
    }
}
