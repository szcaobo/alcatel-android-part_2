package com.android.settings.lockapp.ui;

import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.drawable.Drawable;

public class AppInfoParams {
    public Drawable appIcon;
    public String appName;
    public boolean isChecked = false;
    public String packageName;
    public int size;
    public int versionCode;
    public String versionName;
    public String letter;//分组用首字母，如A
    public int type;//0 category; 1 item

    public boolean isChecked() {
        return this.isChecked;
    }

    public void setChecked(boolean paramBoolean) {
        this.isChecked = paramBoolean;
    }

    public JSONObject toJSONObject() {
        JSONObject localJSONObject = new JSONObject();
        try {
            localJSONObject.put("package_name", this.packageName);
            localJSONObject.put("app_name", this.appName);
            localJSONObject.put("version_code", this.versionCode);
            localJSONObject.put("version_name", this.versionName);
            return localJSONObject;
        } catch (JSONException localJSONException) {
            localJSONException.printStackTrace();
        }
        return localJSONObject;
    }
}
