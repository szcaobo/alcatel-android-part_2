/************************************************************************************************************/
/*                                                                           Date : 05/09/2016 */
/*                                      PRESENTATION                                           */
/*                        Copyright (c) 2013 JRD Communications, Inc.                          */
/************************************************************************************************************/
/*                                                                                                          */
/*              This material is company confidential, cannot be reproduced in any             */
/*              form without the written permission of JRD Communications, Inc.                */
/*                                                                                                          */
/*==========================================================================================================*/
/*   Author :                                                                                 */
/*   Role :    Setting                                                                        */
/*   Reference documents : None                                                               */
/*==========================================================================================================*/
/* Comments :                                                                                 */
/*     file    :                                                                              */
/*     Labels  :                                                                              */
/*==========================================================================================================*/
/* Modifications   (month/day/year)                                                           */
/*==========================================================================================================*/
/* date    | author       |FeatureID                                 |modification            */
/*=========|==============|==========================================|======================================*/

/*==========================================================================================================*/
/* Problems Report(PR/CR)                                                                     */
/*==========================================================================================================*/
/* date    | author    | PR #                               |                                 */
/*==========|==============|=========================================|======================================*/

package com.android.settings;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.app.ActivityManagerNative;
import mst.app.dialog.AlertDialog;
import android.app.Dialog;
import android.widget.SpinnerPopupDialog;
import android.app.admin.DevicePolicyManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.Toast;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.telecom.PhoneAccountHandle;
import android.telecom.TelecomManager;
import android.telephony.PhoneStateListener;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import android.preference.PreferenceCategory;
import com.android.internal.logging.MetricsLogger;
import android.os.SystemProperties;
import org.codeaurora.internal.IExtTelephony;
import com.android.internal.telephony.TelephonyIntents;
import com.android.ims.ImsManager;
import android.provider.SearchIndexableResource;
import com.android.settings.search.Indexable;
import com.android.settings.search.BaseSearchIndexProvider;

import mst.preference.SwitchPreference;
import mst.preference.ListPreference;
import mst.preference.Preference;
import mst.preference.Preference.OnPreferenceChangeListener;
import mst.preference.Preference.OnPreferenceClickListener;
import mst.preference.PreferenceGroup;
import mst.preference.PreferenceScreen;
import com.android.internal.logging.MetricsProto.MetricsEvent;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.TelephonyProperties;

public class TctDualSimNetworkSettings extends SettingsPreferenceFragment implements
Indexable,OnPreferenceChangeListener,OnPreferenceClickListener {

    private static final String TAG = "TctDualSimNetworkSettings";

    private static final int PROVISIONED = 1;
    private static final int NOT_PROVISIONED = 0;
    private static final int INVALID_STATE = -1;
    private static final int CARD_NOT_PRESENT = -2;
    private static final String EXTRA_NEW_PROVISION_STATE = "newProvisionState";
    public static final String EXTRA_SLOT_ID = "slot_id";

    private static final String KEY_DUALSIM_CELLULAR_DATA_PREFERENCE = "dualsim_cellular_data_preference";
    private static final String KEY_DUALSIM_4G_NETWORK_PREFERENCE = "dualsim_4G_network_preference";
    private static final String KEY_DUALSIM_VOLTE_PREFERENCE = "dualsim_volte_preference";
    private static final String KEY_DEFAULT_SIM_PREFERENCE = "default_sim";
    private static final String KEY_SIMCARD1_PREFERENCE = "simcard1";
    private static final String KEY_SIMCARD2_PREFERENCE = "simcard2";
    //[BUGFIX]-Add-BEGIN by TCTNB.Xijun.Zhang,11/09/2016,Task-3337917
    private static final String KEY_TCT_MOBILE_NETWORK_SETTINGS = "tct_mobile_network_settings";
    //[BUGFIX]-Add-END by TCTNB.Xijun.Zhang,11/09/2016,Task-3337917
    private static final String KEY_HIGHSPEED_RAIL_MODE_PREFERENCE = "highspeed_rail_mode_preference";

    private SwitchPreference mCellularDataPreference;
    private SwitchPreference m4GNetworkPreference;
    private SwitchPreference mVoltePreference;
    private TctV7WidgetPreference mDefaultSimPreference;
    private TctV7WidgetPreference mSim1Preference;
    private TctV7WidgetPreference mSim2Preference;
    //[BUGFIX]-Add-BEGIN by TCTNB.Xijun.Zhang,11/09/2016,Task-3337917
    private TctV7WidgetPreference mNetworkSettingsPreference;
    //[BUGFIX]-Add-END by TCTNB.Xijun.Zhang,11/09/2016,Task-3337917
    private SwitchPreference mHighspeedPreference;

    private Dialog mSimDialog=null;
    private MyHandler mHandler;
    private final Configuration mCurConfig = new Configuration();
    private static final String ACTION_UICC_MANUAL_PROVISION_STATUS_CHANGED =
            "org.codeaurora.intent.action.ACTION_UICC_MANUAL_PROVISION_STATUS_CHANGED";
    private static final String SETTING_USER_PREF_DATA_SUB = "user_preferred_data_sub";
    private int mPhoneCount = TelephonyManager.getDefault().getPhoneCount();
    private PhoneStateListener[] mPhoneStateListener = new PhoneStateListener[mPhoneCount];
    private int[] mCallState = new int[mPhoneCount];
    private int[] mUiccProvisionStatus = new int[mPhoneCount];
    private List<SubscriptionInfo> mSelectableSubInfos = null;
    private int mNumSlots;
    private boolean simcard1_exist = false;
    private boolean simcard2_exist = false;
    private boolean isairplaneMode = false;
    private SubscriptionManager mSubscriptionManager;
    private TelephonyManager mTelephonyManager;
    private Context mContext;
    private int mClickedDialogEntryIndex;
    // ADD-BEGIN by zhiqianghu, 11/11/2016, for 3438729
    private boolean mShowVoLTEOnlyCMCC;
    // ADD-END by zhiqianghu, 11/11/2016, for 3438729

    @Override
    protected int getMetricsCategory() {
        return MetricsEvent.TCT_DUALSIM_AND_NETWORK;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        addPreferencesFromResource(R.xml.tct_dualsim_network_settings);
        mSubscriptionManager = SubscriptionManager.from(getActivity());
        mTelephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        mNumSlots = mTelephonyManager.getSimCount();

        mCellularDataPreference = (SwitchPreference)findPreference(KEY_DUALSIM_CELLULAR_DATA_PREFERENCE);
        mCellularDataPreference.setOnPreferenceChangeListener(this);
        m4GNetworkPreference = (SwitchPreference)findPreference(KEY_DUALSIM_4G_NETWORK_PREFERENCE);
        m4GNetworkPreference.setOnPreferenceChangeListener(this);
        mVoltePreference = (SwitchPreference)findPreference(KEY_DUALSIM_VOLTE_PREFERENCE);
        mVoltePreference.setOnPreferenceChangeListener(this);

        mDefaultSimPreference = (TctV7WidgetPreference)findPreference(KEY_DEFAULT_SIM_PREFERENCE);
        mSim1Preference = (TctV7WidgetPreference)findPreference(KEY_SIMCARD1_PREFERENCE);
        mSim1Preference.setTitle(String.format(mContext.getResources().getString(R.string.tct_dualsim_simcard_title),1));
        mSim2Preference = (TctV7WidgetPreference)findPreference(KEY_SIMCARD2_PREFERENCE);
        mSim2Preference.setTitle(String.format(mContext.getResources().getString(R.string.tct_dualsim_simcard_title),2));
        //[BUGFIX]-Add-BEGIN by TCTNB.Xijun.Zhang,11/09/2016,Task-3337917
        //secret codes to set network mode list customization
        mNetworkSettingsPreference = (TctV7WidgetPreference)findPreference(KEY_TCT_MOBILE_NETWORK_SETTINGS);
        //[BUGFIX]-Add-END by TCTNB.Xijun.Zhang,11/09/2016,Task-3337917
        mHighspeedPreference = (SwitchPreference)findPreference(KEY_HIGHSPEED_RAIL_MODE_PREFERENCE);
        mHighspeedPreference.setOnPreferenceChangeListener(this);

        mHandler = new MyHandler();
        mSelectableSubInfos = new ArrayList<SubscriptionInfo>();
        IntentFilter intentFilter = new IntentFilter(ACTION_UICC_MANUAL_PROVISION_STATUS_CHANGED);
        intentFilter.addAction(TelephonyIntents.ACTION_ANY_DATA_CONNECTION_STATE_CHANGED);
        mContext.registerReceiver(mReceiver, intentFilter);

        // ADD-BEGIN by zhiqianghu, 11/11/2016, for 3438729
        mShowVoLTEOnlyCMCC = mContext.getResources().getBoolean(R.bool.feature_settings_show_volte_only_cmcc);
        // ADD-END by zhiqianghu, 11/11/2016, for 3438729
    }

    private boolean isAirplaneModeOn() {
        return Settings.Global.getInt(mContext.getContentResolver(),
                Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
    }

    private boolean hasCard(int slodId) {
        return TelephonyManager.getDefault().hasIccCard(slodId);
    }

    private boolean isSubProvisioned(int slotId) {
        boolean retVal = false;

        if (mUiccProvisionStatus[slotId] == PROVISIONED) retVal = true;
        return retVal;
    }

    private void updateall() {
        mSelectableSubInfos.clear();
        for (int i = 0; i < mNumSlots; ++i) {
            final SubscriptionInfo sir = mSubscriptionManager.getActiveSubscriptionInfoForSimSlotIndex(i);
            try {
                IExtTelephony extTelephony =
                        IExtTelephony.Stub.asInterface(ServiceManager.getService("extphone"));
                //get current provision state of the SIM.
                mUiccProvisionStatus[i] =
                        extTelephony.getCurrentUiccCardProvisioningStatus(i);
            } catch (RemoteException ex) {
                mUiccProvisionStatus[i] = INVALID_STATE;
                Log.d(TAG,"Failed to get pref, slotId: "+ i +" Exception: " + ex);
            } catch (NullPointerException ex) {
                mUiccProvisionStatus[i] = INVALID_STATE;
                Log.d(TAG,"Failed to get pref, slotId: "+ i +" Exception: " + ex);
            }
            if (sir != null && (isSubProvisioned(i))) {
                mSelectableSubInfos.add(sir);
            }
        }
        simcard1_exist = hasCard(0);
        simcard2_exist = hasCard(1);
        isairplaneMode = isAirplaneModeOn();
        updateCellular();
        update4GNetwork();
        updateVolte();
        updateNetwordSettingsTest(); //[BUGFIX]-Add by TCTNB.Xijun.Zhang,11/09/2016,Task-3337917
        updateDefaultCellularDataValues();
        // MOD-BEGIN by zhiqianghu, 11/11/2016, for 3438729
        updateSim1Preference();
        updateSim2Preference();
        // MOD-END by zhiqianghu, 11/11/2016, for 3438729
        updateHighSpeed();

    }

    private void updateCellular(){
        int mCellularData = Settings.System.getInt(getContentResolver(),Settings.System.TCT_DUALSIM_CELLULAR_DATA_ENABLE, 1);
        mCellularDataPreference.setChecked(mCellularData == 1);
        boolean enable= (simcard1_exist || simcard2_exist) && !isairplaneMode;
        mCellularDataPreference.setEnabled(enable);
    }

    //[BUGFIX]-Mod-BEGIN by TCTNB.Xijun.Zhang,11/09/2016,Task-3337917
    //secret codes to set network mode list customization
    private void update4GNetwork(){
        int m4GNetwork = Settings.System.getInt(getContentResolver(),Settings.System.TCT_DUALSIM_4G_NETWORK_ENABLE, 1);
        m4GNetworkPreference.setChecked(m4GNetwork == 1);
        boolean enable= (simcard1_exist || simcard2_exist) && !isairplaneMode;
        int networkSettingTestFlag = Settings.System.getInt(getContentResolver(), "network_settings_test_mode_flag", 0);
        if (networkSettingTestFlag == 1) {
            enable = false;
        }
        m4GNetworkPreference.setEnabled(enable);
    }

    private void updateVolte(){
        int m4GNetwork = Settings.System.getInt(getContentResolver(),Settings.System.TCT_DUALSIM_4G_NETWORK_ENABLE, 1);
        boolean m4Genable = (m4GNetwork==1) ? true : false;
        int mVolte = Settings.System.getInt(getContentResolver(),Settings.System.TCT_DUALSIM_VOLTE_ENABLE, 1);
        mVoltePreference.setChecked(mVolte == 1);
        boolean enable= (simcard1_exist || simcard2_exist) && !isairplaneMode && m4Genable;
        int networkSettingTestFlag = Settings.System.getInt(getContentResolver(), "network_settings_test_mode_flag", 0);
        if (networkSettingTestFlag == 1) {
            enable = false;
        }
        mVoltePreference.setEnabled(enable);

        // ADD-BEGIN by zhiqianghu, 11/11/2016, for 3438729
        if (mShowVoLTEOnlyCMCC) {
            String defaultDataPLMN =
                    TelephonyManager.getDefault().getNetworkOperator(SubscriptionManager.getDefaultDataSubscriptionId());
            if ("46000".equals(defaultDataPLMN) ||
                    "46002".equals(defaultDataPLMN) ||
                    "46007".equals(defaultDataPLMN)) {
                getPreferenceScreen().addPreference(mVoltePreference);
            } else {
                getPreferenceScreen().removePreference(mVoltePreference);
            }
        }
        // ADD-END by zhiqianghu, 11/11/2016, for 3438729
    }

    private void updateNetwordSettingsTest() {
        boolean enable= (simcard1_exist || simcard2_exist) && !isairplaneMode;
        mNetworkSettingsPreference.setEnabled(enable);
        int networkSettingTestFlag = Settings.System.getInt(getContentResolver(), "network_settings_test_mode_flag", 0);
        if (networkSettingTestFlag == 1) {
            //mOtherNetworkCategory.addPreference(mNetworkSettingsPreference);
        } else {
            getPreferenceScreen().removePreference(mNetworkSettingsPreference);
        }
    }
    //[BUGFIX]-Mod-END by TCTNB.Xijun.Zhang,11/09/2016,Task-3337917

    private void updateHighSpeed(){
        int mHighspeed = Settings.System.getInt(getContentResolver(),Settings.System.TCT_HIGHSPEED_RAIL_MODE_ENABLE, 1);
        mHighspeedPreference.setChecked(mHighspeed == 1);
        boolean enable= (simcard1_exist || simcard2_exist) && !isairplaneMode;
        mHighspeedPreference.setEnabled(enable);
    }

    /* MODIFIED-BEGIN by chengbiao.hu, 2016-11-16,BUG-3462770*/
    private int getSlotProvisionStatus(int slot) {
        int provisionStatus = -1;
        try {
            //get current provision state of the SIM.
            IExtTelephony extTelephony =
                    IExtTelephony.Stub.asInterface(ServiceManager.getService("extphone"));
            provisionStatus =  extTelephony.getCurrentUiccCardProvisioningStatus(slot);
        } catch (RemoteException ex) {
            provisionStatus = INVALID_STATE;
            Log.e(TAG,"Failed to get slotId: "+ slot +" Exception: " + ex);
        } catch (NullPointerException ex) {
            provisionStatus = INVALID_STATE;
            Log.e(TAG,"Failed to get slotId: "+ slot +" Exception: " + ex);
        }
        return provisionStatus;
    }

    private void updateDefaultCellularDataValues(){
        boolean callStateIdle = isCallStateIdle();
        final boolean ecbMode = SystemProperties.getBoolean(
                TelephonyProperties.PROPERTY_INECM_MODE, false);
        int sub1_state = getSlotProvisionStatus(PhoneConstants.SUB1);
        int sub2_state = getSlotProvisionStatus(PhoneConstants.SUB2);
        Log.d(TAG,"sub1 status = " + sub1_state + ", sub2_status = " + sub2_state);
        //boolean enable= simcard1_exist && simcard2_exist && !isairplaneMode && callStateIdle && !ecbMode;
        boolean enable= (sub1_state == 1) && (sub2_state == 1) && !isairplaneMode && callStateIdle && !ecbMode;
        /* MODIFIED-END by chengbiao.hu,BUG-3462770*/
        mDefaultSimPreference.setEnabled(enable);
        int mSubId = SubscriptionManager.getDefaultDataSubscriptionId();
        int mSlotId = mSubscriptionManager.getSlotId(mSubId);
        if(SubscriptionManager.isValidSlotId(mSlotId)){
            mDefaultSimPreference.setDetail(String.format(mContext.getResources().getString(R.string.tct_dualsim_simcard_title),mSlotId+1));
            // ADD-BEGIN by zhiqianghu, 11/11/2016, for 3408734
            mClickedDialogEntryIndex = mSlotId;
            // ADD-END by zhiqianghu, 11/11/2016, for 3408734
        } else {
            mDefaultSimPreference.setDetail(String.format(mContext.getResources().getString(R.string.tct_dualsim_simcard_title),1));
            // ADD-BEGIN by zhiqianghu, 11/11/2016, for 3408734
            mClickedDialogEntryIndex = 0;
            // ADD-END by zhiqianghu, 11/11/2016, for 3408734
        }

        // ADD-BEGIN by zhiqianghu, 11/11/2016, for 3438729
        if (mShowVoLTEOnlyCMCC) {
            updateVolte();
        }
        // ADD-END by zhiqianghu, 11/11/2016, for 3438729
    }
    // MOD-BEGIN by zhiqianghu, 11/11/2016, for 3438729
    private void updateSim1Preference(){
        boolean enable= simcard1_exist && !isairplaneMode;
        mSim1Preference.setEnabled(enable);

        SubscriptionInfo sir = mSubscriptionManager.getActiveSubscriptionInfoForSimSlotIndex(0);
        if (sir != null) {
            if(sir.getDisplayName()!=null){
                mSim1Preference.setDetail(sir.getDisplayName().toString());
            }
        }
    }
    private void updateSim2Preference(){
        boolean enable= simcard2_exist && !isairplaneMode;
        mSim2Preference.setEnabled(enable);

        SubscriptionInfo sir = mSubscriptionManager.getActiveSubscriptionInfoForSimSlotIndex(1);
        if (sir != null) {
            if(sir.getDisplayName()!=null){
                mSim2Preference.setDetail(sir.getDisplayName().toString());
            }
        }
    }
    // MOD-END by zhiqianghu, 11/11/2016, for 3438729

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mCurConfig.updateFrom(newConfig);
    }

    private void listen() {
        TelephonyManager tm =
                (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        if (mSelectableSubInfos.size() > 1) {
            Log.d(TAG, "Register for call state change");
            for (int i = 0; i < mPhoneCount; i++) {
                int subId = mSelectableSubInfos.get(i).getSubscriptionId();
                tm.listen(getPhoneStateListener(i, subId),
                        PhoneStateListener.LISTEN_CALL_STATE);
            }
        }
    }
    private void unRegisterPhoneStateListener() {
        TelephonyManager tm =
                (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        for (int i = 0; i < mPhoneCount; i++) {
            if (mPhoneStateListener[i] != null) {
                tm.listen(mPhoneStateListener[i], PhoneStateListener.LISTEN_NONE);
                mPhoneStateListener[i] = null;
            }
        }
    }

    private PhoneStateListener getPhoneStateListener(int phoneId, int subId) {
        // Disable Sim selection for Data when voice call is going on as changing the default data
        // sim causes a modem reset currently and call gets disconnected
        // ToDo : Add subtext on disabled preference to let user know that default data sim cannot
        // be changed while call is going on
        final int i = phoneId;
        mPhoneStateListener[phoneId]  = new PhoneStateListener(subId) {
            @Override
            public void onCallStateChanged(int state, String incomingNumber) {
                Log.d(TAG,"PhoneStateListener.onCallStateChanged: state=" + state);
                mCallState[i] = state;
                updateDefaultCellularDataValues();
            }
        };
        return mPhoneStateListener[phoneId];
    }

    private boolean isCallStateIdle() {
        boolean callStateIdle = true;
        for (int i = 0; i < mCallState.length; i++) {
            if (TelephonyManager.CALL_STATE_IDLE != mCallState[i]) {
                callStateIdle = false;
            }
        }
        Log.d(TAG, "isCallStateIdle " + callStateIdle);
        return callStateIdle;
    }
    private void disableDataForOtherSubscriptions(int subId) {
        List<SubscriptionInfo> subInfoList = mSubscriptionManager.getActiveSubscriptionInfoList();
        if (subInfoList != null) {
            for (SubscriptionInfo subInfo : subInfoList) {
                if (subInfo.getSubscriptionId() != subId) {
                    mTelephonyManager.setDataEnabled(subInfo.getSubscriptionId(), false);
                }
            }
        }
    }
    private void setDefaultDataSubId(final Context context, final int subId) {
        final SubscriptionManager subscriptionManager = SubscriptionManager.from(context);
        subscriptionManager.setDefaultDataSubId(subId);
//        Toast.makeText(context, R.string.data_switch_started, Toast.LENGTH_LONG).show();
    }

    private void setUserPrefDataSubIdInDb(int subId) {
        android.provider.Settings.Global.putInt(mContext.getContentResolver(),
                SETTING_USER_PREF_DATA_SUB, subId);
        Log.d(TAG, "updating data subId: " + subId + " in DB");
    }

    private void set4GNetworkMode(boolean ltemode){
        int mSubId = mSubscriptionManager.getDefaultDataSubscriptionId();
        int mSlotId = mSubscriptionManager.getSlotId(mSubId);
        int settingsNetworkMode = -1;
        try {
            settingsNetworkMode = android.telephony.TelephonyManager.getIntAtIndex(
                            getActivity().getContentResolver(),
                            android.provider.Settings.Global.PREFERRED_NETWORK_MODE,
                            mSlotId);
        } catch (SettingNotFoundException snfe) {
            Log.d(TAG,"LTE Switch--  SettingNotFoundException  :" + snfe);
            settingsNetworkMode = Phone.NT_MODE_LTE_CDMA_EVDO_GSM_WCDMA;
        }

        int changeMode = settingsNetworkMode;
        Log.d(TAG,"LTE Switch--  settingnetworkmode1 = " + settingsNetworkMode);
        if (ltemode == true) {
            switch (settingsNetworkMode) {
            case Phone.NT_MODE_GSM_UMTS:
                changeMode = Phone.NT_MODE_LTE_GSM_WCDMA;
                break;
            case Phone.NT_MODE_TDSCDMA_GSM_WCDMA:
                changeMode = Phone.NT_MODE_LTE_TDSCDMA_GSM_WCDMA;
                break;
            case Phone.NT_MODE_GLOBAL:
                changeMode = Phone.NT_MODE_LTE_CDMA_EVDO_GSM_WCDMA;
                break;
            case Phone.NT_MODE_CDMA:
                changeMode = Phone.NT_MODE_LTE_CDMA_AND_EVDO;
                break;
            default:
                changeMode = Phone.NT_MODE_LTE_CDMA_EVDO_GSM_WCDMA;
                break;
            }
        } else {
            switch (settingsNetworkMode) {
            case Phone.NT_MODE_LTE_GSM_WCDMA:
                changeMode = Phone.NT_MODE_GSM_UMTS;
                break;
            case Phone.NT_MODE_LTE_TDSCDMA_GSM_WCDMA:
                changeMode = Phone.NT_MODE_TDSCDMA_GSM_WCDMA;
                break;
            case Phone.NT_MODE_LTE_CDMA_EVDO_GSM_WCDMA:
                changeMode = Phone.NT_MODE_GLOBAL;
                break;
            case Phone.NT_MODE_LTE_CDMA_AND_EVDO:
                changeMode = Phone.NT_MODE_CDMA;
                break;
            default:
                changeMode = Phone.NT_MODE_GLOBAL;
                break;
            }
        }
        if (mSlotId > -1) {
            android.telephony.TelephonyManager.putIntAtIndex(
                            getActivity().getContentResolver(),
                            android.provider.Settings.Global.PREFERRED_NETWORK_MODE,
                            mSlotId, changeMode);
        }
        mTelephonyManager.setPreferredNetworkType(SubscriptionManager.from(getActivity())
                .getDefaultDataSubscriptionId(), changeMode);
    }

    private class MyHandler extends Handler {
        static final int MESSAGE_UPDATE_DEFAULT_SIM = 0;

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_UPDATE_DEFAULT_SIM:
                    int preferredSubID = msg.arg1;
                    Log.d(TAG,"MyHandler  msg.arg1:"+msg.arg1);
                    //set data enable
                    setDefaultDataSubId(mContext, preferredSubID);
                    setUserPrefDataSubIdInDb(preferredSubID);
                    disableDataForOtherSubscriptions(preferredSubID);
                    int mCellularData = Settings.System.getInt(getContentResolver(),
                            Settings.System.TCT_DUALSIM_CELLULAR_DATA_ENABLE, 1);
                    boolean mDateEnable = (mCellularData == 1) ? true : false;
                    mTelephonyManager.setDataEnabled(preferredSubID, mDateEnable);
                    // set network mode
                    int m4GNetwork = Settings.System.getInt(getContentResolver(),
                            Settings.System.TCT_DUALSIM_4G_NETWORK_ENABLE, 1);
                    boolean m4Genable = (m4GNetwork == 1) ? true : false;
                    set4GNetworkMode(m4Genable);
                    updateDefaultCellularDataValues();
                    Toast.makeText(mContext, R.string.default_data_sim_switch_success, Toast.LENGTH_LONG).show();
                    break;
            }
        }

    }

    private void showDefaultDataSimDialog(){
        SpinnerPopupDialog defaultDataSimdialog = new SpinnerPopupDialog(mContext);

        final SubscriptionManager subscriptionManager = SubscriptionManager.from(mContext);
        final List<SubscriptionInfo> subInfoList = subscriptionManager
                .getActiveSubscriptionInfoList();
        final int selectableSubInfoLength = subInfoList == null ? 0 : subInfoList.size();
        final String[] list = new String[selectableSubInfoLength];

        for (int i = 0; i < selectableSubInfoLength; ++i) {
            final SubscriptionInfo sir = subInfoList.get(i);
            String sirDisplayName;
            if (sir != null && sir.getDisplayName() != null
                    && !TextUtils.isEmpty(sir.getDisplayName().toString())) {
                sirDisplayName = sir.getDisplayName().toString();
            } else {
                sirDisplayName = "";
            }
            String simslodstring = mContext.getResources().getString(R.string.tct_dualsim_simcard_title, i + 1);
            String listTitle;
            listTitle = simslodstring + "(" + sirDisplayName + ")";
            list[i] = listTitle;
        }

        defaultDataSimdialog.setSingleChoiceItems(list, mClickedDialogEntryIndex,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        mClickedDialogEntryIndex = which;
                        final SubscriptionInfo sir;

                        sir = subInfoList.get(which);
                        TelephonyManager tm = TelephonyManager.getDefault();
                        int defaultDataSubId = SubscriptionManager.getDefaultDataSubscriptionId();
                        final int preferredSubID = sir.getSubscriptionId();
                        if (defaultDataSubId != preferredSubID) {
                            final Message msg = mHandler
                                    .obtainMessage(MyHandler.MESSAGE_UPDATE_DEFAULT_SIM);
                            msg.arg1 = preferredSubID;
                            mHandler.sendMessage(msg);
                        }
                        dialog.dismiss();
                    }
                });
        defaultDataSimdialog.setNegativeButton(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });
        defaultDataSimdialog.create();
        defaultDataSimdialog.show();
//
//        final ArrayList<String> list = new ArrayList<String>();
//        final SubscriptionManager subscriptionManager = SubscriptionManager.from(mContext);
//        final List<SubscriptionInfo> subInfoList = subscriptionManager.getActiveSubscriptionInfoList();
//        final int selectableSubInfoLength = subInfoList == null ? 0 : subInfoList.size();
//
//        final DialogInterface.OnClickListener selectionListener = new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int value) {
//
//                final SubscriptionInfo sir;
//                boolean ddsalertDisplayed = false;
//
//                sir = subInfoList.get(value);
//                TelephonyManager tm = TelephonyManager.getDefault();
//                int defaultDataSubId = SubscriptionManager.getDefaultDataSubscriptionId();
//                final int preferredSubID = sir.getSubscriptionId();
//                if (defaultDataSubId != preferredSubID) {
//                    final Message msg = mHandler.obtainMessage(MyHandler.MESSAGE_UPDATE_DEFAULT_SIM);
//                    msg.arg1 = preferredSubID;
//                    mHandler.sendMessage(msg);
//                }
//                dialog.dismiss();
//            }
//        };
//        for (int i = 0; i < selectableSubInfoLength; ++i) {
//            final SubscriptionInfo sir = subInfoList.get(i);
//            CharSequence displayName = sir.getDisplayName();
//            if (displayName == null) {
//                displayName = "";
//            }
//            list.add(displayName.toString());
//        }
//
//        String[] arr = list.toArray(new String[0]);
//        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
//
//        ListAdapter adapter = new SelectAccountListAdapter(
//                subInfoList,
//                builder.getContext(),
//                R.layout.tct_select_default_data_list_item,
//                arr);
//
//        mSimDialog = builder.setAdapter(adapter, selectionListener)
//                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int whichButton) {
//                        dialog.dismiss();
//                    }
//                }).create();
//        mSimDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
//                        @Override
//                        public void onDismiss(DialogInterface dialog) {
//                            if(mSimDialog !=null){
//                                mSimDialog =null;
//                            }
//                        }
//                    });
//        mSimDialog.show();
    }

    private class SelectAccountListAdapter extends ArrayAdapter<String> {
        private Context mContext;
        private int mResId;
        private List<SubscriptionInfo> mSubInfoList;
        public SelectAccountListAdapter(List<SubscriptionInfo> subInfoList,
                Context context, int resource, String[] arr) {
            super(context, resource, arr);
            mContext = context;
            mResId = resource;
            mSubInfoList = subInfoList;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater)
                    mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView;
            final ViewHolder holder;

            if (convertView == null) {
                // Cache views for faster scrolling
                rowView = inflater.inflate(mResId, null);
                holder = new ViewHolder();
                holder.title = (TextView) rowView.findViewById(R.id.title);
                holder.radioButton = (RadioButton) rowView.findViewById(R.id.radiobutton);
                rowView.setTag(holder);
            } else {
                rowView = convertView;
                holder = (ViewHolder) rowView.getTag();
            }

            final SubscriptionInfo sir = mSubInfoList.get(position);
            String sirDisplayName ;
            if(sir != null && sir.getDisplayName()!=null && !TextUtils.isEmpty(sir.getDisplayName().toString())){
                sirDisplayName = sir.getDisplayName().toString();
            }else {
                sirDisplayName = "";
            }
            String slodstring=mContext.getResources().getString(R.string.tct_dualsim_simcard_title, position + 1);
            String textTitle ;
            textTitle = slodstring + "(" + sirDisplayName + ")";
            holder.title.setText(textTitle);
            int defaultDataSubId = SubscriptionManager.getDefaultDataSubscriptionId();
            if(sir.getSubscriptionId()==defaultDataSubId){
                holder.radioButton.setChecked(true);
            } else {
                holder.radioButton.setChecked(false);
            }
            return rowView;
        }

        private class ViewHolder {
            TextView title;
            RadioButton radioButton;
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        mSubscriptionManager.addOnSubscriptionsChangedListener(mOnSubscriptionsChangeListener);
        updateall();
        listen();
    }

    @Override
    public void onPause() {
        super.onPause();
        mSubscriptionManager.removeOnSubscriptionsChangedListener(mOnSubscriptionsChangeListener);
        unRegisterPhoneStateListener();
    }

    @Override
    public void onDestroy() {
        mContext.unregisterReceiver(mReceiver);
        Log.d(TAG,"on onDestroy");
        super.onDestroy();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen,
            Preference preference) {
        if (preference == mCellularDataPreference) {
            return true;

        } else if (preference == m4GNetworkPreference) {
            //we need to update VOLTE witch enable/disable if we turn on/off 4G
            updateVolte();
            return true;

        } else if(preference == mVoltePreference){
            return true;

        } else if(preference == mHighspeedPreference){
            return true;

        } else if(preference == mDefaultSimPreference){
            showDefaultDataSimDialog();
            return true;

        } else if(preference == mSim1Preference){
          Intent newIntent = new Intent(mContext, TctSimCardSettings.class);
          newIntent.putExtra(EXTRA_SLOT_ID, 0);
          startActivity(newIntent);
        } else if(preference == mSim2Preference){
          Intent newIntent = new Intent(mContext, TctSimCardSettings.class);
          newIntent.putExtra(EXTRA_SLOT_ID, 1);
          startActivity(newIntent);
        }
        //[BUGFIX]-Mod-BEGIN by TCTNB.Xijun.Zhang,11/09/2016,Task-3337917
        //secret codes to set network mode list customization
        else if(preference == mNetworkSettingsPreference){
            final Intent intent = new Intent(Intent.ACTION_MAIN);
            // prepare intent to start qti MobileNetworkSettings activity
            intent.setComponent(new ComponentName("com.qualcomm.qti.networksetting",
                   "com.qualcomm.qti.networksetting.MobileNetworkSettings"));
            startActivity(intent);
        }
        //[BUGFIX]-Mod-END by TCTNB.Xijun.Zhang,11/09/2016,Task-3337917
        return super.onPreferenceTreeClick(preferenceScreen,preference);
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object objValue) {
        if (preference == mCellularDataPreference){
            boolean cellularDataEnable = !mCellularDataPreference.isChecked();
            mCellularDataPreference.setChecked(cellularDataEnable);
            Settings.System.putInt(getContentResolver(),
                    Settings.System.TCT_DUALSIM_CELLULAR_DATA_ENABLE, cellularDataEnable ? 1 : 0);
            int mSubId = mSubscriptionManager.getDefaultDataSubscriptionId();
            int mSlotId = mSubscriptionManager.getSlotId(mSubId);
            disableDataForOtherSubscriptions(mSubId);
            mTelephonyManager.setDataEnabled(mSubId, cellularDataEnable);
            return true;
        } else if (preference == m4GNetworkPreference){
            boolean lteMode = !m4GNetworkPreference.isChecked();
            m4GNetworkPreference.setChecked(lteMode);
            Settings.System.putInt(getContentResolver(),
                  Settings.System.TCT_DUALSIM_4G_NETWORK_ENABLE, lteMode ? 1 : 0);
            set4GNetworkMode(lteMode);
            return true;

        } else if (preference == mVoltePreference){
            boolean enhanced4gMode = !mVoltePreference.isChecked();
            mVoltePreference.setChecked(enhanced4gMode);
            Settings.System.putInt(getContentResolver(),
                    Settings.System.TCT_DUALSIM_VOLTE_ENABLE, enhanced4gMode ? 1 : 0);
            ImsManager.setEnhanced4gLteModeSetting(getActivity().getBaseContext(), enhanced4gMode);
            return true;
        } else if(preference == mHighspeedPreference){
            boolean value = !mHighspeedPreference.isChecked();
            mHighspeedPreference.setChecked(value);
            Settings.System.putInt(getContentResolver(),
                    Settings.System.TCT_HIGHSPEED_RAIL_MODE_ENABLE, value ? 1 : 0);
        }
        return true;
    }

    @Override
    public boolean onPreferenceClick(Preference preference) {
        return false;
    }

    private final SubscriptionManager.OnSubscriptionsChangedListener mOnSubscriptionsChangeListener = new SubscriptionManager.OnSubscriptionsChangedListener() {
        @Override
        public void onSubscriptionsChanged() {
            Log.d(TAG,"onSubscriptionsChanged:");
            updateall();
        }
    };

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.d(TAG, "Intent received: " + action);
            if (ACTION_UICC_MANUAL_PROVISION_STATUS_CHANGED.equals(action)) {
                int phoneId = intent.getIntExtra(PhoneConstants.PHONE_KEY,
                        SubscriptionManager.INVALID_SUBSCRIPTION_ID);
                int newProvisionedState = intent.getIntExtra(EXTRA_NEW_PROVISION_STATE,
                        NOT_PROVISIONED);
                updateall();
                 Log.d(TAG, "Received ACTION_UICC_MANUAL_PROVISION_STATUS_CHANGED on phoneId: "
                         + phoneId + " new sub state " + newProvisionedState);
            }else if (TelephonyIntents.ACTION_ANY_DATA_CONNECTION_STATE_CHANGED.equals(action)) {
                updateCellular();
            }
        }
    };
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
        @Override
        public List<SearchIndexableResource> getXmlResourcesToIndex(Context context, boolean enabled) {
            List<SearchIndexableResource> result = new ArrayList<SearchIndexableResource>();

            final SearchIndexableResource sir = new SearchIndexableResource(context);
            sir.xmlResId = R.xml.tct_dualsim_network_settings;
            result.add(sir);

            return result;
        }
    };
}

