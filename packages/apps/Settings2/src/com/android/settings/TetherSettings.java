/* Copyright (C) 2016 Tcl Corporation Limited */

/******************************************************************************/
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 2016/11/17|     jianhong.yang    |     task 3476360     |  Ergo development*/
/*           |                      |                      |  create new file */
/* ----------|----------------------|----------------------|----------------- */

package com.android.settings;

import static android.net.ConnectivityManager.TETHERING_BLUETOOTH;
import static android.net.ConnectivityManager.TETHERING_WIFI;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.hardware.usb.UsbManager;
import android.net.ConnectivityManager;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiConfiguration.AuthAlgorithm;
import android.net.wifi.WifiConfiguration.KeyMgmt;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.UserManager;
import android.os.INetworkManagementService;
import android.os.IBinder;
import android.provider.Settings;
import mst.app.dialog.AlertDialog;
import mst.preference.CheckBoxPreference;
import mst.preference.EditTextPreference;
import mst.preference.SwitchPreference;
import mst.preference.PreferenceCategory;
import mst.preference.Preference.OnPreferenceClickListener;
import mst.preference.Preference.OnPreferenceChangeListener;
import mst.preference.Preference;
import mst.preference.PreferenceScreen;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;

import com.android.internal.logging.MetricsProto.MetricsEvent;
import com.android.settings.applications.LayoutPreference;
import com.android.settings.datausage.DataSaverBackend;
import com.android.settings.wifi.WifiApDialog;
import com.android.settings.wifi.WifiApEnabler;
import com.android.settings.WidgetPreference;
import com.android.settingslib.TetherUtil;

public class TetherSettings extends RestrictedSettingsFragment implements View.OnClickListener,
        TextWatcher,OnPreferenceChangeListener,DataSaverBackend.Listener{
    private static final String TAG = "TetherSettings";

    private static final String KEY_WIFI_SSID_PREFERENCE = "wifi_ssid_preference";
    private static final String KEY_PASSWORD_PREFERENCE = "password_preference";
    private static final String KEY_WIFI_AP_SECURITY = "wifi_ap_security";
    private static final String KEY_WIFI_AP_CHANNEL = "wifi_ap_channel";
    private static final String KEY_HIDE_NETWORK = "hide_network";
    public static final String KEY_RESTRICT_DATA_TRAFFIC = "restrict_data_traffic";
    private static final String KEY_DATA_USAGE_LIMIT = "data_usage_limit";
    private static final String KEY_HOTSPOT_TOGGLE_PREFERENCE = "hotspot_toggle_preference";
    //private static final String MAX_DATA_USAGE_LIMIT_NUM = "nax_data_usage_limit_num";

    public static final int OPEN_INDEX = 0;
    public static final int WPA2_INDEX = 1;

    public static final int CHANNEL_24GHZ_INDEX = 0;
    public static final int CHANNEL_5GHZ_INDEX = 1;

    private LayoutPreference mWifiSsidPreference;
    private LayoutPreference mPasswordPreference;
    private LayoutPreference mHotspotTogglePreference;
    private WidgetListPreference mWifiAPSecurity;
    private WidgetListPreference mWifiAPChannel;
    private CheckBoxPreference mHideNetwork;
    private SwitchPreference mRestrictDataTraffic;
    private WidgetPreference mDataUsageLimit;

    private int mSecurityTypeIndex = OPEN_INDEX;
    private int mChannelTypeIndex = CHANNEL_24GHZ_INDEX;
    private String[] mSecurityType;
    private String[] mChannelType;
    private int mMaxDataUsageLimitNum;

    private LinearLayout mSsidSection;
    private LinearLayout mPasswordSection;
    private EditText mSsid;
    private EditText mPassword;
    private EditText mDataUsageLimitEditText;
    private Button mHotspotToggle;

    private WifiManager mWifiManager;
    private WifiConfiguration mWifiConfig = null;
    private ConnectivityManager mCm;
    private HotSpotTrafficService mHotspotTrafficService;
    private String[] mWifiRegexs;

    private Handler mHandler = new Handler();
    private OnStartTetheringCallback mStartTetheringCallback;
    private DataSaverBackend mDataSaverBackend;
    private boolean mDataSaverEnabled;
    private boolean mRestartWifiApAfterConfigChange;

    private IntentFilter mIntentFilter;
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (WifiManager.WIFI_AP_STATE_CHANGED_ACTION.equals(action)) {
                int state = intent.getIntExtra(
                        WifiManager.EXTRA_WIFI_AP_STATE, WifiManager.WIFI_AP_STATE_FAILED);
                if (state == WifiManager.WIFI_AP_STATE_FAILED) {
                    int reason = intent.getIntExtra(WifiManager.EXTRA_WIFI_AP_FAILURE_REASON,
                            WifiManager.SAP_START_FAILURE_GENERAL);
                    handleWifiApStateChanged(state, reason);
                } else {
                    handleWifiApStateChanged(state, WifiManager.SAP_START_FAILURE_GENERAL);
                }
            } else if (ConnectivityManager.ACTION_TETHER_STATE_CHANGED.equals(action)) {
                ArrayList<String> available = intent.getStringArrayListExtra(
                        ConnectivityManager.EXTRA_AVAILABLE_TETHER);
                ArrayList<String> active = intent.getStringArrayListExtra(
                        ConnectivityManager.EXTRA_ACTIVE_TETHER);
                ArrayList<String> errored = intent.getStringArrayListExtra(
                        ConnectivityManager.EXTRA_ERRORED_TETHER);
                updateTetherState(available.toArray(), active.toArray(), errored.toArray());
            } else if (Intent.ACTION_AIRPLANE_MODE_CHANGED.equals(action)) {
                initViewsState();
            }
        }
    };

    public TetherSettings() {
        super(UserManager.DISALLOW_CONFIG_TETHERING);
    }

    @Override
    public void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        mIntentFilter = new IntentFilter(WifiManager.WIFI_AP_STATE_CHANGED_ACTION);
        mIntentFilter.addAction(ConnectivityManager.ACTION_TETHER_STATE_CHANGED);
        mIntentFilter.addAction(Intent.ACTION_AIRPLANE_MODE_CHANGED);

        getActivity().registerReceiver(mReceiver, mIntentFilter);
        //bindHotspotService();

        mStartTetheringCallback = new OnStartTetheringCallback(this);
    }

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        addPreferencesFromResource(R.xml.wifi_hotspot_settings);

        initView();
    }

    private void initView() {
        mCm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        mWifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        mWifiConfig = mWifiManager.getWifiApConfiguration();
        mDataSaverBackend = new DataSaverBackend(getContext());
        mDataSaverEnabled = mDataSaverBackend.isDataSaverEnabled();

        mWifiSsidPreference = (LayoutPreference)findPreference(KEY_WIFI_SSID_PREFERENCE);
        mPasswordPreference = (LayoutPreference)findPreference(KEY_PASSWORD_PREFERENCE);
        mHotspotTogglePreference = (LayoutPreference)findPreference(KEY_HOTSPOT_TOGGLE_PREFERENCE);

        mWifiAPSecurity = (WidgetListPreference)findPreference(KEY_WIFI_AP_SECURITY);
        mWifiAPChannel = (WidgetListPreference)findPreference(KEY_WIFI_AP_CHANNEL);
        mHideNetwork = (CheckBoxPreference)findPreference(KEY_HIDE_NETWORK);
        mRestrictDataTraffic = (SwitchPreference)findPreference(KEY_RESTRICT_DATA_TRAFFIC);
        mDataUsageLimit = (WidgetPreference)findPreference(KEY_DATA_USAGE_LIMIT);

        mSsidSection = (LinearLayout)mWifiSsidPreference.findViewById(R.id.ssid_section);
        mPasswordSection = (LinearLayout)mPasswordPreference.findViewById(R.id.password_section);
        mSsid = (EditText)mWifiSsidPreference.findViewById(R.id.ssid);
        mPassword = (EditText)mPasswordPreference.findViewById(R.id.password);
        mHotspotToggle = (Button)mHotspotTogglePreference.findViewById(R.id.hotspot_toggle);
        mSsidSection.setVisibility(View.VISIBLE);
        mPasswordSection.setVisibility(View.VISIBLE);
        mHotspotToggle.setVisibility(View.VISIBLE);

        mHideNetwork.setChecked(mWifiConfig.hiddenSSID);
        mRestrictDataTraffic.setChecked(Settings.Global.getInt(getContentResolver(),
                KEY_RESTRICT_DATA_TRAFFIC,0) == 1);
        if(mRestrictDataTraffic.isChecked()) {
            mDataUsageLimit.setEnabled(true);
        } else {
            mDataUsageLimit.setEnabled(false);
        }
        mDataUsageLimit.setSummary(formatDataUsageLimitNum());

        mWifiAPSecurity.setOnPreferenceChangeListener(this);
        mWifiAPChannel.setOnPreferenceChangeListener(this);
        mRestrictDataTraffic.setOnPreferenceChangeListener(this);
        mSsid.addTextChangedListener(this);
        mPassword.addTextChangedListener(this);
        mHotspotToggle.setOnClickListener(this);
        mDataSaverBackend.addListener(this);

        mSecurityType = getResources().getStringArray(R.array.wifi_ap_security);

        String countryCode = mWifiManager.getCountryCode();
        if (!mWifiManager.isDualBandSupported() || countryCode == null) {
            //If no country code, 5GHz AP is forbidden
            Log.i(TAG,(!mWifiManager.isDualBandSupported() ? "Device do not support 5GHz " :"")
                    + (countryCode == null ? " NO country code" :"") +  " forbid 5GHz");
            mChannelType = getResources().getStringArray(R.array.wifi_ap_band_config_2G_only);
            mWifiAPChannel.setEntries(mChannelType);
            mWifiAPChannel.setEntryValues(mChannelType);
        } else {
            mChannelType = getResources().getStringArray(R.array.wifi_ap_band_config_full);
            mWifiAPChannel.setEntries(mChannelType);
            mWifiAPChannel.setEntryValues(mChannelType);
        }

        mWifiRegexs = mCm.getTetherableWifiRegexs();
        final boolean wifiAvailable = mWifiRegexs.length != 0;
        if (wifiAvailable && !Utils.isMonkeyRunning()) {
            mSsid.setText(mWifiConfig.SSID);
            mSecurityTypeIndex = getSecurityTypeIndex();
            if (mWifiConfig.apBand == 0) {
                mChannelTypeIndex = 0;
            } else {
                mChannelTypeIndex = 1;
            }

            mWifiAPSecurity.setSummary(mSecurityType[mSecurityTypeIndex]);
            mWifiAPChannel.setSummary(mChannelType[mChannelTypeIndex]);

            if (mSecurityTypeIndex == WPA2_INDEX) {
                mPasswordSection.setVisibility(View.VISIBLE);
                mPassword.setText(mWifiConfig.preSharedKey);
            } else {
                mPasswordSection.setVisibility(View.GONE);
            }
        }
        onDataSaverChanged(mDataSaverBackend.isDataSaverEnabled());
        bindHotspotService();
    }

    public int getSecurityTypeIndex() {
        if (mWifiConfig.allowedKeyManagement.get(KeyMgmt.WPA2_PSK)) {
            return WPA2_INDEX;
        }
        return OPEN_INDEX;
    }

    @Override
    protected int getMetricsCategory() {
        return MetricsEvent.TETHER;
    }

    public WifiConfiguration getConfig() {

        WifiConfiguration config = new WifiConfiguration();

        /**
         * TODO: SSID in WifiConfiguration for soft ap
         * is being stored as a raw string without quotes.
         * This is not the case on the client side. We need to
         * make things consistent and clean it up
         */
        config.SSID = mSsid.getText().toString();

        config.apBand = mChannelTypeIndex;

        config.hiddenSSID = mHideNetwork.isChecked();

        switch (mSecurityTypeIndex) {
            case OPEN_INDEX:
                config.allowedKeyManagement.set(KeyMgmt.NONE);
                return config;

            case WPA2_INDEX:
                config.allowedKeyManagement.set(KeyMgmt.WPA2_PSK);
                config.allowedAuthAlgorithms.set(AuthAlgorithm.OPEN);
                if (mPassword.length() != 0) {
                    String password = mPassword.getText().toString();
                    config.preSharedKey = password;
                }
                return config;
        }
        return null;
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        if(mWifiManager.isWifiApEnabled()) {
            mCm.stopTethering(TETHERING_WIFI);
        } else {
            //bindHotspotService();
            WifiConfiguration newConfig = getConfig();
            if(newConfig != null) {
                if (mWifiManager.getWifiApState() == WifiManager.WIFI_AP_STATE_ENABLED) {
                    mRestartWifiApAfterConfigChange = true;
                    mCm.stopTethering(TETHERING_WIFI);
                }
                mWifiManager.setWifiApConfiguration(newConfig);
                //mWifiManager.setWifiApEnabled(newConfig, true);
            }
            mCm.startTethering(TETHERING_WIFI, true, mStartTetheringCallback, mHandler);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count,
            int after) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        // TODO Auto-generated method stub
    }

    @Override
    public void afterTextChanged(Editable s) {
        // TODO Auto-generated method stub
    }

     @Override
        public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen,
                Preference preference)  {
         String key = preference.getKey();

         if(KEY_HIDE_NETWORK.equals(key)) {
         } else if(KEY_RESTRICT_DATA_TRAFFIC.equals(key)) {
             if(mRestrictDataTraffic.isChecked()) {
                  mDataUsageLimit.setEnabled(true);
              } else {
                  mDataUsageLimit.setEnabled(false);
              }
              Settings.Global.putInt(getContentResolver(),KEY_RESTRICT_DATA_TRAFFIC,
                      mRestrictDataTraffic.isChecked() ? 1 : 0);
         } else if(KEY_DATA_USAGE_LIMIT.equals(key)) {
             createDataUsageLimitDialog();
         }

         return true;
     }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        final Context context = getActivity();
        String key = preference.getKey();

        if (KEY_WIFI_AP_SECURITY.equals(key)) {
            String stringValue = (String) newValue;
            if (mSecurityType[OPEN_INDEX].equals(stringValue)) {
                mSecurityTypeIndex = OPEN_INDEX;
                mPasswordSection.setVisibility(View.GONE);
                mWifiAPSecurity.setSummary(stringValue);
            } else if (mSecurityType[WPA2_INDEX].equals(stringValue)) {
                mSecurityTypeIndex = WPA2_INDEX;
                mPasswordSection.setVisibility(View.VISIBLE);
                mWifiAPSecurity.setSummary(stringValue);
            }
        } else if(KEY_WIFI_AP_CHANNEL.equals(key)) {
            String stringValue = (String) newValue;
            if (mChannelType[CHANNEL_24GHZ_INDEX].equals(stringValue)) {
                mChannelTypeIndex = CHANNEL_24GHZ_INDEX;
                mWifiAPChannel.setSummary(stringValue);
            } else if (mChannelType[CHANNEL_5GHZ_INDEX].equals(stringValue)) {
                mChannelTypeIndex = CHANNEL_5GHZ_INDEX;
                mWifiAPChannel.setSummary(stringValue);
            }
        }

        return true;
    }

    private static final class OnStartTetheringCallback extends
            ConnectivityManager.OnStartTetheringCallback {
        final WeakReference<TetherSettings> mTetherSettings;

        OnStartTetheringCallback(TetherSettings settings) {
            mTetherSettings = new WeakReference<TetherSettings>(
                    settings);
        }

        @Override
        public void onTetheringStarted() {
            update();
        }

        @Override
        public void onTetheringFailed() {
            update();
        }

        private void update() {
            TetherSettings settings = mTetherSettings.get();
            if (settings != null) {
                 settings.updateState();
            }
        }
    }

    public void updateState() {
        // TODO Auto-generated method stub
    }

    public void setViewsEnabled(final boolean enable) {
        mSsid.setEnabled(enable);
        mPassword.setEnabled(enable);
        mWifiAPSecurity.setEnabled(enable);
        mWifiAPChannel.setEnabled(enable);
        mHideNetwork.setEnabled(enable);
        mRestrictDataTraffic.setEnabled(enable);
        if(!enable) {
            mDataUsageLimit.setEnabled(enable);
        } else {
            if(mRestrictDataTraffic.isChecked())
                mDataUsageLimit.setEnabled(enable);
        }
    }

    private void handleWifiApStateChanged(int state, int reason) {
        switch (state) {
        case WifiManager.WIFI_AP_STATE_ENABLING:
            mHotspotToggle.setText(R.string.wifi_tether_starting);
            mHotspotToggle.setEnabled(false);
            setViewsEnabled(false);
            break;
        case WifiManager.WIFI_AP_STATE_ENABLED:
            /**
             * Summary on enable is handled by tether broadcast notice
             */
            mHotspotToggle.setText(R.string.disable_hotspot);
            /* Doesnt need the airplane check */
            mHotspotToggle.setEnabled(true/*!mDataSaverBackend.isDataSaverEnabled()*/);
            setViewsEnabled(false);
            setMaxTraffic();
            break;
        case WifiManager.WIFI_AP_STATE_DISABLING:
            mHotspotToggle.setText(R.string.wifi_tether_stopping);
            mHotspotToggle.setEnabled(false);
            setViewsEnabled(false);
            break;
        case WifiManager.WIFI_AP_STATE_DISABLED:
            mHotspotToggle.setEnabled(true);
            mHotspotToggle.setText(R.string.enable_hotspot);
            setViewsEnabled(true);
            initViewsState();
            break;
        default:
            if (reason == WifiManager.SAP_START_FAILURE_NO_CHANNEL) {
                mHotspotToggle.setText(R.string.wifi_sap_no_channel_error);
            } else {
                mHotspotToggle.setText(R.string.wifi_error);
            }
            initViewsState();
        }
    }

    private void updateTetherState(Object[] available, Object[] tethered, Object[] errored) {
        boolean wifiTethered = false;
        boolean wifiErrored = false;

        for (Object o : tethered) {
            String s = (String)o;
            for (String regex : mWifiRegexs) {
                if (s.matches(regex)) wifiTethered = true;
            }
        }
        for (Object o: errored) {
            String s = (String)o;
            for (String regex : mWifiRegexs) {
                if (s.matches(regex)) wifiErrored = true;
            }
        }

        if (wifiTethered) {
            setViewsEnabled(false);
        } else if (wifiErrored) {
            setViewsEnabled(true);
        }
    }

    private void initViewsState() {
        boolean isAirplaneMode = Settings.Global.getInt(getActivity().getContentResolver(),
                Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
        if(!isAirplaneMode) {
            setViewsEnabled(true);
        } else {
            setViewsEnabled(false);
        }
    }

    public static boolean isProvisioningNeededButUnavailable(Context context) {
        return (TetherUtil.isProvisioningNeeded(context)
                && !isIntentAvailable(context));
    }

    private static boolean isIntentAvailable(Context context) {
        String[] provisionApp = context.getResources().getStringArray(
                com.android.internal.R.array.config_mobile_hotspot_provision_app);
        final PackageManager packageManager = context.getPackageManager();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.setClassName(provisionApp[0], provisionApp[1]);

        return (packageManager.queryIntentActivities(intent,
                PackageManager.MATCH_DEFAULT_ONLY).size() > 0);
    }

    @Override
    public void onDataSaverChanged(boolean isDataSaving) {
        mDataSaverEnabled = isDataSaving;
        //setViewsEnabled(mDataSaverEnabled);
        //mHotspotToggle.setEnabled(!mDataSaverEnabled);
        //mDataSaverFooter.setVisible(mDataSaverEnabled);
    }

    @Override
    public void onWhitelistStatusChanged(int uid, boolean isWhitelisted) {
    }

    @Override
    public void onBlacklistStatusChanged(int uid, boolean isBlacklisted)  {
    }

    private void createDataUsageLimitDialog() {
        final View view = getActivity().getLayoutInflater().inflate(R.layout.wifi_hotspot_panel, null);
         LinearLayout dataUsageLimitSection = (LinearLayout) view.findViewById(R.id.data_usage_limit_section);
         dataUsageLimitSection.setVisibility(View.VISIBLE);
         mDataUsageLimitEditText = (EditText) view.findViewById(R.id.data_usage_limit);
         if (mMaxDataUsageLimitNum > 0) {
             mDataUsageLimitEditText.setText(mMaxDataUsageLimitNum + "");
         }
         mDataUsageLimitEditText.requestFocus();
         mDataUsageLimitEditText.selectAll();

         final AlertDialog dialog = new AlertDialog.Builder(getActivity())
             .setTitle(R.string.data_usage_limit)
             .setView(view)
             .setPositiveButton(R.string.dlg_ok,
                 new DialogInterface.OnClickListener() {

                     @Override
                     public void onClick(DialogInterface dialog, int which) {
                         if (mDataUsageLimitEditText != null) {
                             String num = mDataUsageLimitEditText.getText().toString();

                             if(num != null) {
                                 try {
                                     Settings.Global.putInt(getContentResolver(),
                                             HotSpotTrafficService.MAX_DATA_USAGE_LIMIT_NUM,
                                             Integer.parseInt(num));
                                 } catch (Exception e) {
                                     Toast.makeText(getContext(), R.string.set_data_usage_limit_error,
                                             Toast.LENGTH_SHORT).show();
                                     return;
                                 }
                                 mDataUsageLimit.setSummary(formatDataUsageLimitNum());
                            }
                         }
                     }
                 })
         .setNegativeButton(R.string.dlg_cancel, null)
         .show();

         if (mDataUsageLimitEditText != null) {
             dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(mDataUsageLimitEditText.getText().length() > 0);
         }

         mDataUsageLimitEditText.addTextChangedListener(new TextWatcher() {

             @Override
             public void beforeTextChanged(CharSequence s, int start,
                     int count, int after) {
                 // TODO Auto-generated method stub
             }

             @Override
             public void onTextChanged(CharSequence s, int start,
                     int before, int count) {
                 // TODO Auto-generated method stub
             }

             @Override
             public void afterTextChanged(Editable s) {
                 // TODO Auto-generated method stub
                 dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(s.length() > 0 &&
                          Integer.parseInt(mDataUsageLimitEditText.getText().toString()) != 0);
             }

         });
    }

    private String formatDataUsageLimitNum() {
        mMaxDataUsageLimitNum = Settings.Global.getInt(getContentResolver(),
                HotSpotTrafficService.MAX_DATA_USAGE_LIMIT_NUM, 0);
        Log.d(TAG,"mMaxDataUsageLimitNum = " + mMaxDataUsageLimitNum);
        return mMaxDataUsageLimitNum > 0 ? String.format("%s %s",mMaxDataUsageLimitNum,
                getResources().getString(R.string.unit_mb)):
                getResources().getString(R.string.not_limit_data_usage);
    }

    private void setMaxTraffic() {
        if(mMaxDataUsageLimitNum > 0 && mHotspotTrafficService != null && mRestrictDataTraffic.isChecked()) {
            mHotspotTrafficService.setHotspotMaxTraffic(mMaxDataUsageLimitNum);
        }
    }

    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder binder) {
            mHotspotTrafficService = ((HotSpotTrafficService.MyBinder) binder).getService();
            mHotspotTrafficService.setHotspotTrafficListener(new HotSpotTrafficListener(){
                @Override
                public void onTrafficChange(long totalBytes){
                    Log.d(TAG,"HotspotTrafficListener totalBytes : " + totalBytes);
                }
            });
        }
        @Override
        public void onServiceDisconnected(ComponentName name) {
        }
    };

    private void bindHotspotService() {
        Intent intent = new Intent(getActivity(),HotSpotTrafficService.class);
        getActivity().bindService(intent,mConnection,Context.BIND_AUTO_CREATE);
    }

    private void unbindHotspotService() {
        if (mHotspotTrafficService != null) {
            Log.d(TAG,"Remove HotspotTrafficListener");
            mHotspotTrafficService.setHotspotTrafficListener(null);
        }

        if(mConnection != null) {
            getActivity().unbindService(mConnection);
        }
    }

    @Override
    public void onStop() {
        super.onStop();

        getActivity().unregisterReceiver(mReceiver);
        mDataSaverBackend.remListener(this);

       // mReceiver = null;
        mStartTetheringCallback = null;
    }

}
