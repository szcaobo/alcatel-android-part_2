/* Copyright (C) 2016 Tcl Corporation Limited */
/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.settings.bluetooth;


import android.app.Activity;
import android.app.Dialog;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.UserManager;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.internal.logging.MetricsProto.MetricsEvent;
import com.android.settings.R;
import com.android.settings.RestrictedSettingsFragment;
import com.android.settings.WidgetPreference;
import com.android.settings.applications.LayoutPreference;
import com.android.settingslib.bluetooth.CachedBluetoothDevice;
import com.android.settingslib.bluetooth.CachedBluetoothDeviceManager;
import com.android.settingslib.bluetooth.LocalBluetoothManager;
import com.android.settingslib.bluetooth.LocalBluetoothProfile;
import com.android.settingslib.bluetooth.LocalBluetoothProfileManager;
import com.android.settingslib.bluetooth.MapProfile;
import com.android.settingslib.bluetooth.PanProfile;
import com.android.settingslib.bluetooth.PbapServerProfile;

import java.util.HashMap;

import mst.app.dialog.AlertDialog;
import mst.preference.Preference;
import mst.preference.PreferenceCategory;
import mst.preference.PreferenceScreen;
import mst.preference.SwitchPreference;
import com.android.settings.SettingsPreferenceFragment;




public class DeviceProfilesFragment extends SettingsPreferenceFragment implements
        CachedBluetoothDevice.Callback,DialogInterface.OnClickListener{
    private static final String TAG = "DeviceProfilesFragment";
    private static final String DEVICE_PROFILES_NAME = "device_profiles_name";
    public static final String ARG_DEVICE_ADDRESS = "device_address";
    public static final String DIS_PAIR = "dis_pair";

    private AlertDialog mDisconnectDialog;
    private LocalBluetoothProfileManager mProfileManager;
    PreferenceCategory profiles;
    WidgetPreference deviceName;
    private CachedBluetoothDevice mCachedDevice;
    private LocalBluetoothManager mManager;
    private Button mDisPair;
    HashMap<SwitchPreference,String> map = new HashMap<SwitchPreference,String>();

/*    public DeviceProfilesFragment() {
        super(UserManager.DISALLOW_CONFIG_TETHERING);
    }*/
    @Override
    protected int getMetricsCategory() {
        return MetricsEvent.ACCESSIBILITY;
    }

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        mManager = Utils.getLocalBtManager(getActivity());
        CachedBluetoothDeviceManager deviceManager = mManager.getCachedDeviceManager();

        //mac address
        String address = getArguments().getString(ARG_DEVICE_ADDRESS);

        BluetoothDevice remoteDevice = mManager.getBluetoothAdapter().getRemoteDevice(address);

        mCachedDevice = deviceManager.findDevice(remoteDevice);
        if (mCachedDevice == null) {
            mCachedDevice = deviceManager.addDevice(mManager.getBluetoothAdapter(),
                    mManager.getProfileManager(), remoteDevice);
        }
        String dName = mCachedDevice.getName();
//        addPreferencesFromResource(R.xml.bluetooth_device_profiles);

        mProfileManager = mManager.getProfileManager();
        addPreferencesForActivity(dName);
    }

    void addPreferencesForActivity(String dName) {

        Activity activity = getActivity();
        PreferenceScreen screen = getPreferenceManager().createPreferenceScreen(activity);
        screen.setTitle(activity.getString(R.string.bluetooth_device_profiles));
        setPreferenceScreen(screen);

        deviceName = new WidgetPreference(activity);
        deviceName.setSelectable(false);
        deviceName.setOrder(1);
        deviceName.setKey(activity.getString(R.string.bluetooth_device_profiles_name));
        deviceName.setSummary(dName);
        deviceName.setTitle(activity.getString(R.string.bluetooth_device_profiles_name));
        deviceName.setSelectable(true);
        deviceName.setOnPreferenceClickListener(mRenameClick);
        screen.addPreference(deviceName);

        profiles = new PreferenceCategory(activity);
        profiles.setKey(DEVICE_PROFILES_NAME);
        profiles.setOrder(2);
        screen.addPreference(profiles);

        LayoutPreference disPair = new LayoutPreference(activity, R.layout.dispair_bluetooth_layout);
        disPair.setKey(DIS_PAIR);
        disPair.setOrder(3);
        screen.addPreference(disPair);
        mDisPair = (Button)disPair.findViewById(R.id.dispair_bluetooth_button);
        mDisPair.setOnClickListener(new OnClickListener(){
            @Override
            public void onClick(View v) {
                mCachedDevice.unpair();
                com.android.settings.bluetooth.Utils.updateSearchIndex(getContext(),
                        BluetoothSettings.class.getName(), mCachedDevice.getName(),
                        getString(R.string.bluetooth_settings),
                        R.drawable.ic_settings_bluetooth, false);
                Log.d("GDD", "FORGET BUTTON IS CLICKED");
                DeviceProfilesFragment.this.finish();
            }
        });
    }


    @Override
    public void onDeviceAttributesChanged() {
        refresh();
    }

    private Preference.OnPreferenceClickListener mRenameClick = new Preference.OnPreferenceClickListener() {
        @Override
        public boolean onPreferenceClick(Preference preference) {
            showDialog();
            return true;
        }
    };
    private void showDialog(){
        View mChangeNameView = LayoutInflater.from(DeviceProfilesFragment.this.getContext()).inflate(R.layout.dialog_edittext,
                null);
        final EditText mDeviceNameView = (EditText) mChangeNameView.findViewById(R.id.edittext);
        mDeviceNameView.setText(mCachedDevice.getName(), TextView.BufferType.EDITABLE);
        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mCachedDevice.setName(mDeviceNameView.getText().toString());
            }
        };
        Dialog dialog = new AlertDialog.Builder(DeviceProfilesFragment.this.getContext(), android.R.style.Theme_Material_Light_Dialog_Alert)
                .setView(mChangeNameView)
                .setNegativeButton(android.R.string.cancel, null)
                .setPositiveButton(R.string.bluetooth_rename_button, listener)
                .setTitle(R.string.bluetooth_preference_paired_dialog_name_label)
                .create();
        dialog.show();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case DialogInterface.BUTTON_POSITIVE:
//[FEATURE]-Add-BEGIN by TCTNB.dongdong.gong,10/11/2016,3082231,
//London Bluetooth ROM
//                TextView deviceName = (TextView) mRootView.findViewById(R.id.name);
//                mCachedDevice.setName(deviceName.getText().toString());
//                mCachedDevice.setName("button_positive");
//[FEATURE]-Add-END by TCTNB.dongdong.gong
                break;
            case DialogInterface.BUTTON_NEGATIVE:
                mCachedDevice.unpair();
                com.android.settings.bluetooth.Utils.updateSearchIndex(getContext(),
                        BluetoothSettings.class.getName(), mCachedDevice.getName(),
                        getString(R.string.bluetooth_settings),
                        R.drawable.ic_settings_bluetooth, false);
                break;
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mDisconnectDialog != null) {
            mDisconnectDialog.dismiss();
            mDisconnectDialog = null;
        }
        if (mCachedDevice != null) {
            mCachedDevice.unregisterCallback(this);
        }
        map.clear();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume()");
        mManager.setForegroundActivity(getActivity());
        if (mCachedDevice != null) {
            mCachedDevice.registerCallback(this);
            if (mCachedDevice.getBondState() == BluetoothDevice.BOND_NONE) {
                finish();
                return;
            }
            addPreferencesForProfiles();
            refresh();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause()");
        if (mCachedDevice != null) {
            mCachedDevice.unregisterCallback(this);
        }

        mManager.setForegroundActivity(null);
    }

    private void addPreferencesForProfiles(){

        profiles.removeAll();
        for (LocalBluetoothProfile profile : mCachedDevice.getConnectableProfiles()) {
            if (!((profile instanceof PbapServerProfile) ||
                    (profile instanceof MapProfile))) {
                Log.d(TAG, "addPreferencesForProfiles = ");
                SwitchPreference PbapMapPreference = new SwitchPreference(getActivity());
                PbapMapPreference.setEnabled(true);
                PbapMapPreference.setTitle(profile.getNameResource(mCachedDevice.getDevice()));
                PbapMapPreference.setKey(getActivity().getString(profile.getNameResource(mCachedDevice.getDevice())));
                profiles.addPreference(PbapMapPreference);

                map.put(PbapMapPreference,profile.toString());
            }
        }

        final int pbapPermission = mCachedDevice.getPhonebookPermissionChoice();
        if (pbapPermission == CachedBluetoothDevice.PBAP_CONNECT_RECEIVED) {
            final PbapServerProfile psp = mManager.getProfileManager().getPbapProfile();
            SwitchPreference pspPreference = new SwitchPreference(getActivity());
            pspPreference.setEnabled(true);
            pspPreference.setTitle(psp.getNameResource(mCachedDevice.getDevice()));
            pspPreference.setKey(getActivity().getString(psp.getNameResource(mCachedDevice.getDevice())));
            profiles.addPreference(pspPreference);

            map.put(pspPreference, psp.toString());
        }

        final MapProfile mapProfile = mManager.getProfileManager().getMapProfile();
        final int mapPermission = mCachedDevice.getMessagePermissionChoice();
        if (mapPermission != CachedBluetoothDevice.ACCESS_UNKNOWN) {
            SwitchPreference mapPreference = new SwitchPreference(getActivity());
            mapPreference.setEnabled(true);
            mapPreference.setTitle(mapProfile.getNameResource(mCachedDevice.getDevice()));
            mapPreference.setKey(getActivity().getString(mapProfile.getNameResource(mCachedDevice.getDevice())));
            profiles.addPreference(mapPreference);

            map.put(mapPreference, mapProfile.toString());
        }
    }

    private void refresh() {
        WidgetPreference deviceName = (WidgetPreference) getPreferenceScreen().findPreference(getActivity().getString(R.string.bluetooth_device_profiles_name));
        Log.d(TAG, "refresh deviceName = " + deviceName);
        if(deviceName != null){
            deviceName.setSummary(mCachedDevice.getName());
        }
        refreshProfiles();
    }

    private void refreshProfiles() {
        PreferenceScreen preferenceScreen = getPreferenceScreen();
        for (LocalBluetoothProfile profile : mCachedDevice.getConnectableProfiles()) {
            SwitchPreference profilePref = (SwitchPreference) preferenceScreen.findPreference(getActivity().getString(profile.getNameResource(mCachedDevice.getDevice())));
            Log.d(TAG, "profilePref = " + profilePref);
            if (profilePref == null) {
                profilePref = new SwitchPreference(getActivity());
                profilePref.setEnabled(true);
                profilePref.setTitle(profile.getNameResource(mCachedDevice.getDevice()));
                profilePref.setKey(getActivity().getString(profile.getNameResource(mCachedDevice.getDevice())));
                profiles.addPreference(profilePref);

                map.put(profilePref, profile.toString());
            } else {
                refreshProfilePreference(profilePref, profile);
            }
        }
        for (LocalBluetoothProfile profile : mCachedDevice.getRemovedProfiles()) {
            SwitchPreference profilePref = (SwitchPreference)preferenceScreen.findPreference(getActivity().getString(profile.getNameResource(mCachedDevice.getDevice())));
            if (profilePref != null) {
                if (profile instanceof PbapServerProfile) {
                    final int pbapPermission = mCachedDevice.getPhonebookPermissionChoice();
                    Log.d(TAG, "refreshProfiles: pbapPermission = " + pbapPermission);
                    if (pbapPermission != CachedBluetoothDevice.ACCESS_UNKNOWN)
                        continue;
                }
                Log.d(TAG, "Removing " + profile.toString() + " from profile list");
                profiles.removePreference(profilePref);
            }
        }
    }

    private void refreshProfilePreference(SwitchPreference profilePref,
                                          LocalBluetoothProfile profile) {
        BluetoothDevice device = mCachedDevice.getDevice();

        profilePref.setEnabled(!mCachedDevice.isBusy());

        if (profile instanceof MapProfile) {
            profilePref.setChecked(mCachedDevice.getMessagePermissionChoice()
                    == CachedBluetoothDevice.ACCESS_ALLOWED);

        } else if (profile instanceof PbapServerProfile) {
            profilePref.setChecked(mCachedDevice.getPhonebookPermissionChoice()
                    == CachedBluetoothDevice.PBAP_CONNECT_RECEIVED);

        } else if (profile instanceof PanProfile) {
            profilePref.setChecked(profile.getConnectionStatus(device) ==
                    BluetoothProfile.STATE_CONNECTED);

        } else {
            profilePref.setChecked(profile.isPreferred(device));
        }
    }

    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen,
                                         Preference preference)  {
        /*if (preference == mUsbTether) {
            if (mUsbTether.isChecked()) {
                startTethering(TETHERING_USB);
            } else {
                mCm.stopTethering(TETHERING_USB);
            }
        } else if (preference == mBluetoothTether) {
            if (mBluetoothTether.isChecked()) {
                startTethering(TETHERING_BLUETOOTH);
            } else {
                mCm.stopTethering(TETHERING_BLUETOOTH);
                // No ACTION_TETHER_STATE_CHANGED is fired or bluetooth unless a device is
                // connected. Need to update state manually.
                updateState();
            }
        } else if (preference == mCreateNetwork) {
            showDialog(DIALOG_AP_SETTINGS);
        }*/
        if (preference instanceof SwitchPreference){
            Log.e(TAG, "The Switch is performing onProfileClicked");
            LocalBluetoothProfile prof = mProfileManager.getProfileByName(map.get(preference));
            Log.e(TAG, "onpreferencetreeclick map.size = " + map.size());
            if(prof != null){
                Log.e(TAG, "onpreferencetreeclick localbluetoothprofile = " + prof);
                onProfileClicked(prof,(SwitchPreference)preference);
            }
        }

        return super.onPreferenceTreeClick(preferenceScreen,preference);
    }

    private void onProfileClicked(LocalBluetoothProfile profile, SwitchPreference profilePref) {
        BluetoothDevice device = mCachedDevice.getDevice();

        if (!profilePref.isChecked()) {
            profilePref.setChecked(true);
            askDisconnect(mManager.getForegroundActivity(), profile);
        } else {
            if (profile instanceof MapProfile) {
                mCachedDevice.setMessagePermissionChoice(BluetoothDevice.ACCESS_ALLOWED);
            }
            /* MODIFIED-BEGIN by qianbo.pan, 2016-10-21,BUG-3175107*/
            else if (profile instanceof PbapServerProfile) {
                // Special handling for pbap server, as server cant connect to client
                mCachedDevice.setPhonebookPermissionChoice(
                        CachedBluetoothDevice.PBAP_CONNECT_RECEIVED);
                refreshProfilePreference(profilePref, profile);
                return;
            }
            /* MODIFIED-END by qianbo.pan, 2016-10-21,BUG-3175107*/
            if (profile.isPreferred(device)) {
                // profile is preferred but not connected: disable auto-connect
                if (profile instanceof PanProfile) {
                    mCachedDevice.connectProfile(profile);
                } else {
                    profile.setPreferred(device, false);
                }
            } else {
                profile.setPreferred(device, true);
                mCachedDevice.connectProfile(profile);
            }
            refreshProfilePreference(profilePref, profile);
        }
    }

    private void askDisconnect(Context context,
                               final LocalBluetoothProfile profile) {
        // local reference for callback
        final CachedBluetoothDevice device = mCachedDevice;
        String name = device.getName();
        if (TextUtils.isEmpty(name)) {
            name = context.getString(R.string.bluetooth_device);
        }

        String profileName = context.getString(profile.getNameResource(device.getDevice()));

        String title = context.getString(R.string.bluetooth_disable_profile_title);
        String message = context.getString(R.string.bluetooth_disable_profile_message,
                profileName, name);

        DialogInterface.OnClickListener disconnectListener =
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        device.disconnect(profile);
                        profile.setPreferred(device.getDevice(), false);
                        if (profile instanceof MapProfile) {
                            device.setMessagePermissionChoice(BluetoothDevice.ACCESS_REJECTED);
                        }
                        if (profile instanceof PbapServerProfile) {
                            device.setPhonebookPermissionChoice(BluetoothDevice.ACCESS_REJECTED);
                        }
                        SwitchPreference profilePref = (SwitchPreference) getPreferenceScreen().findPreference(getActivity().getString(profile.getNameResource(mCachedDevice.getDevice())));
                        refreshProfilePreference(profilePref, profile);
                    }
                };

        mDisconnectDialog = Utils.showDisconnectDialog(context,
                mDisconnectDialog, disconnectListener, title, Html.fromHtml(message));
    }
}
