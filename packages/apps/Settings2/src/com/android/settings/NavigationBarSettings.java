/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.settings;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import android.app.Activity;
import android.os.Bundle;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.view.View;
import android.os.SystemProperties;
import mst.preference.Preference;
import mst.preference.Preference.OnPreferenceChangeListener;
import mst.preference.Preference.OnPreferenceClickListener;
import mst.preference.PreferenceActivity;
import mst.preference.SwitchPreference;
import com.android.settings.AppWidgetLoader.ItemConstructor;
import com.android.settings.R;
import com.android.settings.location.RadioButtonPreference;
import com.android.settings.location.RadioButtonPreference.OnClickListener;
import mst.widget.toolbar.Toolbar;
import com.android.internal.logging.MetricsProto.MetricsEvent;

public class NavigationBarSettings extends InstrumentedActivity implements OnItemClickListener{
    private static final String TAG = "NavigationBarSettings";
    private int mCurrentSelected;
    private String[] mSoftKeyTypes = {"LEFT", "RIGHT"};
    private ListView mSoftkeyTypeListView;
    private SoftkeyListViewAdapter mAdapter;
    private SwitchPreference mHideNotiBar;
    private final String SYSTEM_SOFTKEY_STYLE_STORAGE = "softkey_style_storage";
    private final String SYSTEM_SOFTKEY_STYLE = "softkey_style";
    private static final String INTENT_SOFTKEY_CHANGE ="android.intent.action.softkey_change";
    private int[] keyImage = { R.drawable.setup_img_softkey_01, R.drawable.setup_img_softkey_02};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setMstContentView(R.layout.navigation_bar_settings);
        setTitle(R.string.virtual_key);
        showBackIcon(true);
        mSoftkeyTypeListView = (ListView) findViewById(R.id.softkey_list);
        mSoftkeyTypeListView.setOnItemClickListener(this);
        SharedPreferences softkey = getSharedPreferences(SYSTEM_SOFTKEY_STYLE_STORAGE, MODE_PRIVATE);
        mCurrentSelected = softkey.getInt(SYSTEM_SOFTKEY_STYLE, 0);
        String[] typeName = {this.getString(R.string.key_type_name_01),this.getString(R.string.key_type_name_02)};
        mAdapter = new SoftkeyListViewAdapter(this, getListItems(), keyImage,typeName);
        mSoftkeyTypeListView.setAdapter(mAdapter);
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                saveSettings();
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private List<Map<String, Object>> getListItems() {
        List<Map<String, Object>> listItems = new ArrayList<Map<String, Object>>();
        int index = 0;
        for (String t: mSoftKeyTypes) {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("text", t);
            if (index == mCurrentSelected) {
                map.put("checked", true);
            } else {
                map.put("checked", false);
            }
            index++;
            listItems.add(map);
        }
        return listItems;
    }

    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (position == mSoftKeyTypes.length) {
            return;
        }
        mCurrentSelected = position;
        int firstPos = mSoftkeyTypeListView.getFirstVisiblePosition();
        String[] typeName = {this.getString(R.string.key_type_name_01),this.getString(R.string.key_type_name_02)};
        mAdapter = new SoftkeyListViewAdapter(this, getListItems(), keyImage,typeName);
        mSoftkeyTypeListView.setAdapter(mAdapter);
        mSoftkeyTypeListView.setSelection(firstPos);
        saveSettings();
    }

    private boolean saveSettings() {
        SharedPreferences softkey = getSharedPreferences(SYSTEM_SOFTKEY_STYLE_STORAGE, MODE_PRIVATE);
        softkey.edit().putInt(SYSTEM_SOFTKEY_STYLE, mCurrentSelected).commit();
        Intent softkeyChangeIntent=new Intent(INTENT_SOFTKEY_CHANGE);
        /*
        int softkeyValue;
        if (mCurrentSelected == 0 || mCurrentSelected == 1){
            softkeyValue=0;
        }else{
            softkeyValue=1;
        }
        */
        softkeyChangeIntent.putExtra("softkey", mCurrentSelected);
        Log.d(TAG,"softkey get from intent="+softkeyChangeIntent.getIntExtra("softkey",0));
        sendBroadcast(softkeyChangeIntent);
        return true;
    }

    @Override
    protected int getMetricsCategory() {
        return MetricsEvent.FINGERPRINT;
    }
    @Override
    public void onNavigationClicked(View view) {
        super.onNavigationClicked(view);
        finish();
    }
}
