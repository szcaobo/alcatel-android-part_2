/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.settings;

import android.content.Context;
import mst.preference.Preference;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.settings.R;

public class TctWidgetPreference extends Preference {
    private TextView mTextView;
    private String mDetail;
    private ImageView mImageView;
    private boolean mShow = false;
    private boolean needsetcolor = false;
    private Context mContext = null;

    public TctWidgetPreference(Context context) {
        this(context, null);
        mContext=context;
        setWidgetLayoutResource(R.layout.tct_pref_widget);
    }

    public TctWidgetPreference(Context context, AttributeSet attrs) {
        this(context, attrs, com.android.internal.R.attr.preferenceStyle);
    }

    public TctWidgetPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public TctWidgetPreference(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        mContext=context;
        setWidgetLayoutResource(R.layout.tct_pref_widget);
    }

    @Override
    protected void onBindView(View view) {
        super.onBindView(view);

        View widgetFrame = view.findViewById(com.android.internal.R.id.widget_frame);

        if (widgetFrame != null) {
            mTextView = (TextView) widgetFrame.findViewById(R.id.pref_tv_detail);
            if (!TextUtils.isEmpty(mDetail)) {
                mTextView.setText(mDetail);
            } else {
                mTextView.setText("");
            }
            mImageView = (ImageView) widgetFrame.findViewById(R.id.pref_image_detail);
            if (mShow) {
                mImageView.setVisibility(View.GONE);
            }
            if(needsetcolor){
                mImageView.setColorFilter(mContext.getResources().getColor(R.color.text_disable));
            }
        }
    }

    public void setDetail(String detail) {
        this.mDetail = detail;
        notifyChanged();
    }

    public void setGone(boolean visible) {
        this.mShow = visible;
        notifyChanged();
    }

    public String getDetail() {
        return mDetail;
    }

    public void setDsiabledImageColorFilter(boolean needset) {
        this.needsetcolor=needset;
        notifyChanged();
    }
}
