package com.android.settings;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;
import android.os.Binder;
import android.os.Message;
import android.content.pm.ResolveInfo;
import android.content.pm.PackageManager;
import android.provider.Settings;
import android.util.Log;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.os.UserHandle;

import com.android.settings.lockapp.utils.ApplicationLockUtils;
import com.android.settings.lockapp.utils.SharedPreferenceUtil;
import com.android.settings.lockapp.ui.PatternLockActivity;
import com.android.settings.lockapp.ui.PasswordLockActivity;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


public class LockAppService extends Service {
    public static final boolean DEBUG = true;
    private static final String TAG = "LockAppService";
    private Context mContext = this;
    private PackageManager localPackageManager;
    public static boolean threadIsTerminate = false;
    private Thread dThread;
    private static boolean lockState;
    private final static int MSG_SHOW_NUM_DIALOG = 1;
    private final static int MSG_SHOW_PATTERN_DIALOG = 2;
    String topUnLockedAppName = "";
    private static final int CONFIRM_REQUEST = 155;

    ActivityManager activityManager;
    PackageManager mPM;
    String packgeName = "";
    String lastLoadPackageName = "";
    private boolean isFromScreenOff = false;
    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Intent.ACTION_SCREEN_OFF.equals(intent.getAction())) {
                Log.d(TAG, "intent.action.screen.off");
                if (Settings.System.getInt(context.getContentResolver(), "tct_lock_app_switch", 0) == 1) {

                    if (Settings.System.getInt(context.getContentResolver(), "tct_lock_app_time", 0) == 0) {
                        reSetLockState();
                    }
                }
            } else if (Intent.ACTION_USER_PRESENT.equals(intent.getAction())) {
                Log.d(TAG, "intent.action.USER_PRESENT");
                isFromScreenOff = true;
            }
        }
    };

    private Runnable checkDataRunnable = new Runnable() {

        @Override
        public void run() {

            boolean lockState = (Settings.System.getInt(getContentResolver(), "tct_lock_app_switch", 0) == 1);
            if (lockState) {
                List<RunningTaskInfo> runningTaskInfos = activityManager.getRunningTasks(1);
                if (runningTaskInfos != null) {
                    packgeName = runningTaskInfos.get(0).topActivity.getPackageName();
                } else {
                    packgeName = null;
                }
                if (Settings.System.getInt(getContentResolver(), "tct_lock_app_time", 0) == 1) {
                    if (("".equals(topUnLockedAppName)) && (ApplicationLockUtils.hasUnLockedPackageName(mPM, packgeName))) {
                        topUnLockedAppName = packgeName;
                    }
                    if ((!("".equals(topUnLockedAppName))) && (!topUnLockedAppName.equals(packgeName))) {
                        reToNeedUnLock(topUnLockedAppName);
                        topUnLockedAppName = "";
                    }
                }
                if (!packgeName.equals(lastLoadPackageName) || isFromScreenOff) {
                    lastLoadPackageName = packgeName;
                    if (ApplicationLockUtils.isLockedPackageName(mPM, packgeName)) {
                        passwordLock(packgeName);
                    }
                    isFromScreenOff = false;
                }
            }
        }
    };

//    private Runnable checkDataRunnable = new Runnable() {
//
//        @Override
//        public void run() {
//
//            // TODO Auto-generated method stub
//            Log.d(TAG, "checkDataRunnable-->run");
//            ActivityManager activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
//            PackageManager mPM = mContext.getPackageManager();
//            String packgeName = "";
//            String lastLoadPackageName = "";
//            while (!threadIsTerminate) {
//                try {
//                    boolean lockState = (Settings.System.getInt(getContentResolver(), "tct_lock_app_switch", 0) == 1);
//                    if (lockState) {
//                        List<RunningTaskInfo> runningTaskInfos = activityManager.getRunningTasks(1);
//                        if (runningTaskInfos != null && lockState) {
//                            packgeName = runningTaskInfos.get(0).topActivity.getPackageName();
//                        } else {
//                            continue;
//                        }
//                        if (Settings.System.getInt(getContentResolver(), "tct_lock_app_time", 0) == 1) {
//                            if (("".equals(topUnLockedAppName)) && (ApplicationLockUtils.hasUnLockedPackageName(mPM, packgeName))) {
//                                topUnLockedAppName = packgeName;
//                                Log.d(TAG, "topUnLockedAppName: " + packgeName);
//                            }
//                            if ((!("".equals(topUnLockedAppName))) && (!topUnLockedAppName.equals(packgeName))) {
//                                reToNeedUnLock(topUnLockedAppName);
//                                topUnLockedAppName = "";
//                            }
//                        }
//                        if (!packgeName.equals(lastLoadPackageName)) {
//                            lastLoadPackageName = packgeName;
//                            if (ApplicationLockUtils.isLockedPackageName(mPM, packgeName)) {
//                                passwordLock(packgeName);
//                                continue;
//                            }
//                        }
//                    }
//                    Thread.sleep(500);
//                } catch (InterruptedException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }
//            }
//        }
//    };


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (DEBUG) Log.d(TAG, "onStartCommand ");
        flags = START_STICKY;
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "onBind");
        return null;
    }

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate");
        super.onCreate();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_SCREEN_OFF);
        intentFilter.addAction(Intent.ACTION_SCREEN_ON);
        intentFilter.addAction(Intent.ACTION_USER_PRESENT);
        this.registerReceiver(mReceiver, intentFilter);
//        dThread = new Thread(checkDataRunnable);
//        dThread.start();
        activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        mPM = mContext.getPackageManager();
        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        scheduler.scheduleWithFixedDelay(checkDataRunnable, 0, 300, TimeUnit.MILLISECONDS);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.unregisterReceiver(mReceiver);
        startService(new Intent(this, LockAppService.class));
    }

    private void reSetLockState() {
        localPackageManager = mContext.getPackageManager();
        ActivityManager activityManager = (ActivityManager) mContext.getSystemService(ACTIVITY_SERVICE);
        Intent localIntent = new Intent("android.intent.action.MAIN", null);
        localIntent.addCategory("android.intent.category.LAUNCHER");
        List<ResolveInfo> mAllAppList = localPackageManager.queryIntentActivities(localIntent, 0);
        Collections.sort(mAllAppList, new ResolveInfo.DisplayNameComparator(localPackageManager));
        for (int i = 0; i < mAllAppList.size(); ++i) {
            ResolveInfo appInfo = (ResolveInfo) mAllAppList.get(i);
            int locaState =
                    localPackageManager.getApplicationLockStatus(appInfo.activityInfo.packageName);
            //Log.d(TAG,"locaState: "+locaState +"appInfo.activityInfo.packageName: "+appInfo.activityInfo.packageName);
            if (locaState == 2 ) {
                localPackageManager.setApplicationLockStatus(appInfo.activityInfo.packageName, 1);
            }
        }
    }

    private void reToNeedUnLock(String packageName) {
        localPackageManager = mContext.getPackageManager();
        localPackageManager.setApplicationLockStatus(packageName, 1);
    }

    private void passwordLock(String pkgName) {
        Intent intent = new Intent();
        intent.putExtra("package_name", pkgName);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setClassName("com.android.settings", "com.android.settings.JumpActivity");
        Log.i(TAG, "start JumpActivity");
        mContext.startActivity(intent);
    }

}
