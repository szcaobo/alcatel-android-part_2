/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.settings.wifi;

import android.app.Dialog;
import android.app.DialogFragment;
/* MODIFIED-BEGIN by jie.zhang, 2016-10-18,BUG-3137678*/
//WAPI+++
import android.preference.PreferenceActivity;
//WAPI---
/* MODIFIED-END by jie.zhang,BUG-3137678*/
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WpsInfo;
import android.os.Bundle;
import android.os.UserManager;
import android.provider.Settings;
import android.security.Credentials;
import mst.preference.Preference;
import mst.preference.CheckBoxPreference;
import mst.preference.ListPreference;
import mst.preference.Preference.OnPreferenceClickListener;
import mst.preference.Preference.OnPreferenceChangeListener;
import mst.preference.PreferenceScreen;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.internal.logging.MetricsProto.MetricsEvent;
import com.android.settings.R;
import com.android.settings.RestrictedSettingsFragment;
import com.android.settings.Utils;
import com.android.settingslib.RestrictedLockUtils;
//[FEATURE]Add-BEGIN TCTNB.jianhong.yang,2016/10/11,task 3046476
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.text.TextUtils;
import mst.preference.SwitchPreference;
import com.android.setupwizardlib.util.SystemBarHelper;
//[FEATURE]Add-END TCTNB.jianhong.yang

public class AdvancedWifiSettings extends RestrictedSettingsFragment
/* MODIFIED-BEGIN by jie.zhang, 2016-10-18,BUG-3137678*/
//WAPI+++
        implements OnPreferenceChangeListener, Preference.OnPreferenceClickListener {
//WAPI---
    private static final String TAG = "AdvancedWifiSettings";

    private static final String KEY_INSTALL_CREDENTIALS = "install_credentials";
    private static final String KEY_WIFI_DIRECT = "wifi_direct";
    private static final String KEY_WPS_PUSH = "wps_push_button";
    private static final String KEY_WPS_PIN = "wps_pin_entry";
    //WAPI+++
    private static final int WAPI_INSTALL_DIALOG_ID = 3;
    private static final int WAPI_UNINSTALL_DIALOG_ID = 4;
    //WAPI---
    /* MODIFIED-END by jie.zhang,BUG-3137678*/

    // Wifi extension requirement
    private static final String KEY_CURRENT_GATEWAY = "current_gateway";
    private static final String KEY_CURRENT_NETMASK = "current_netmask";
    private static final int WIFI_HS2_ENABLED = 1;
    private static final int WIFI_HS2_DISABLED = 0;

    private static final String KEY_PRIORITY_SETTINGS = "wifi_priority_settings";

    private static final String KEY_AUTO_CONNECT_ENABLE = "auto_connect_type";
    private static final String WIFI_AUTO_CONNECT_TYPE = "wifi_auto_connect_type";
    private static final int AUTO_CONNECT_ENABLED = 0;
    private static final int AUTO_CONNECT_DISABLE = 1;
    private static final int AUTO_CONNECT_DEFAULT_VALUE = AUTO_CONNECT_ENABLED;

    private static final String KEY_CELLULAR_TO_WLAN = "cellular_to_wlan";
    private static final String CELLULAR_TO_WLAN_CONNECT_TYPE = "cellular_to_wlan_type";
    private static final int CELLULAR_TO_WLAN_CONNECT_TYPE_AUTO = 0;
    private static final int CELLULAR_TO_WLAN_CONNECT_TYPE_MANUAL = 1;
    private static final int CELLULAR_TO_WLAN_CONNECT_TYPE_ASK = 2;
    private static final int CELLULAR_WLAN_DEFAULT_VALUE = CELLULAR_TO_WLAN_CONNECT_TYPE_AUTO;

    private static final String KEY_CELLULAR_TO_WLAN_HINT = "cellular_to_wlan_hint";
    private static final String CELLULAR_TO_WLAN_HINT = "cellular_to_wlan_hint";

    private static final String KEY_WLAN_TO_CELLULAR_HINT = "wlan_to_cellular_hint";
    private static final String WLAN_TO_CELLULAR_HINT = "wlan_to_cellular_hint";

    private static final String KEY_CONNECT_NOTIFY = "notify_ap_connected";
    private static final String NOTIFY_USER_CONNECT = "notify_user_when_connect_cmcc";

    //[FEATURE]Add-BEGIN TCTNB.jianhong.yang,2016/10/11,task 3046476
    private static final String KEY_MAC_ADDRESS = "mac_address";
    private static final String KEY_SAVED_NETWORKS = "saved_networks";
    private static final String KEY_MARK_PAY_AP = "mark_pay_ap";
    private static final String KEY_CURRENT_IP_ADDRESS = "current_ip_address";
    private static final String KEY_NOTIFY_OPEN_NETWORKS = "notify_open_networks";
    private static final String KEY_SLEEP_POLICY = "sleep_policy";
    private static final String KEY_ADD_NETWORK_MANUALLY = "add_network_manually";
    private WifiManager mWifiManager;
    private IntentFilter mFilter;
    private Preference mAddNetwork;
    SwitchPreference mSleepPolicyPref;

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(WifiManager.LINK_CONFIGURATION_CHANGED_ACTION) ||
                action.equals(WifiManager.NETWORK_STATE_CHANGED_ACTION)) {
                refreshWifiInfo();
            }
        }
    };
    //[FEATURE]Add-END TCTNB.jianhong.yang

    private static final int NOTIFY_USER = 0;
    private static final int DO_NOT_NOTIFY_USER = -1;

    private CheckBoxPreference mAutoConnectEnablePref;
    private CheckBoxPreference mCellularToWlanHintPref;
    private ListPreference mCellularToWlanPref;

    private boolean mUnavailable;

    public AdvancedWifiSettings() {
        super(UserManager.DISALLOW_CONFIG_WIFI);
    }

    @Override
    protected int getMetricsCategory() {
        return MetricsEvent.WIFI_ADVANCED;
    }

    /* MODIFIED-BEGIN by jie.zhang, 2016-10-18,BUG-3137678*/
    //WAPI+++
    private static final String KEY_WAPI_CERT_INSTALL = "wapi_cert_install";
    private static final String KEY_WAPI_CERT_UNINSTALL = "wapi_cert_uninstall";

    private Preference mWapiCertInstall;
    private Preference mWapiCertUninstall;
    //WAPI---
    /* MODIFIED-END by jie.zhang,BUG-3137678*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (isUiRestricted()) {
            mUnavailable = true;
            setPreferenceScreen(new PreferenceScreen(getPrefContext(), null));
        } else {
            addPreferencesFromResource(R.xml.wifi_advanced_settings);
            /* MODIFIED-BEGIN by jie.zhang, 2016-10-18,BUG-3137678*/
            //WAPI+++
            Log.e(TAG, "Oncreate findpref.");
            mWapiCertInstall = findPreference(KEY_WAPI_CERT_INSTALL);
            mWapiCertUninstall = findPreference(KEY_WAPI_CERT_UNINSTALL);
            //WAPI---
            /* MODIFIED-END by jie.zhang,BUG-3137678*/
        }

       //[FEATURE]Add-BEGIN TCTNB.jianhong.yang,2016/10/11,task 3046476
       int isSetupWizadRun = Settings.Global.getInt(getContentResolver(), Settings.Global.DEVICE_PROVISIONED, 0);
        if (isSetupWizadRun == 0) {
            View decorView = getActivity().getWindow().getDecorView();
            /* MODIFIED-BEGIN by yongliang.zhu, 2016-11-10,BUG-3213595*/
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
                    /* MODIFIED-END by yongliang.zhu,BUG-3213595*/
            decorView.setSystemUiVisibility(uiOptions);
        }
        //Add-END TCTNB.jianhong.yang
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getEmptyTextView().setText(R.string.wifi_advanced_not_available);
        if (mUnavailable) {
            getPreferenceScreen().removeAll();
        }

        //[FEATURE]Add-BEGIN TCTNB.jianhong.yang,2016/10/11,task 3046476
        mWifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        mFilter = new IntentFilter();
        mFilter.addAction(WifiManager.LINK_CONFIGURATION_CHANGED_ACTION);
        mFilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        //[FEATURE]Add-END TCTNB.jianhong.yang
    }

    @Override
    public void onResume() {
        super.onResume();
        Utils.hideNavigationBar(getActivity());
        if (!mUnavailable) {
            initPreferences();
/* MODIFIED-BEGIN by jie.zhang, 2016-10-18,BUG-3137678*/
//WAPI+++
            initWapiCertInstallPreference();
            initWapiCertUninstallPreference();
//WAPI---
/* MODIFIED-END by jie.zhang,BUG-3137678*/
            //[FEATURE]Add-BEGIN TCTNB.jianhong.yang,2016/10/11,task 3046476
            getActivity().registerReceiver(mReceiver, mFilter);
            refreshWifiInfo();
            //[FEATURE]Add-END TCTNB.jianhong.yang
        }
    }

/* MODIFIED-BEGIN by jie.zhang, 2016-10-18,BUG-3137678*/
//WAPI+++
     @Override
     public Dialog onCreateDialog(int dialogId) {
        WapiCertMgmtDialog wapiDialog;
        switch (dialogId) {
            case WAPI_INSTALL_DIALOG_ID:
                 wapiDialog = new WapiCertMgmtDialog(getActivity());
                 wapiDialog.setMode(WapiCertMgmtDialog.MODE_INSTALL);
                 wapiDialog.setTitle(R.string.wifi_wapi_cert_install);
                 return wapiDialog;

            case WAPI_UNINSTALL_DIALOG_ID:
                 wapiDialog = new WapiCertMgmtDialog(getActivity());
                 wapiDialog.setMode(WapiCertMgmtDialog.MODE_UNINSTALL);
                 wapiDialog.setTitle(R.string.wifi_wapi_cert_uninstall);
                 return wapiDialog;
         }
         return super.onCreateDialog(dialogId);
     }
//WAPI---
/* MODIFIED-END by jie.zhang,BUG-3137678*/

    private void initPreferences() {
        final Context context = getActivity();
        Intent intent = new Intent(Credentials.INSTALL_AS_USER_ACTION);
        intent.setClassName("com.android.certinstaller",
                "com.android.certinstaller.CertInstallerMain");
        intent.putExtra(Credentials.EXTRA_INSTALL_AS_UID, android.os.Process.WIFI_UID);
        Preference pref = findPreference(KEY_INSTALL_CREDENTIALS);
        pref.setIntent(intent);

       //[FEATURE]Add-BEGIN TCTNB.jianhong.yang,2016/10/11,task 3046476
        mAddNetwork = getPreferenceScreen().findPreference(KEY_ADD_NETWORK_MANUALLY);
        mAddNetwork.setEnabled(mWifiManager.isWifiEnabled());

        SwitchPreference notifyOpenNetworks =
                (SwitchPreference) findPreference(KEY_NOTIFY_OPEN_NETWORKS);
        notifyOpenNetworks.setChecked(Settings.Global.getInt(getContentResolver(),
                Settings.Global.WIFI_NETWORKS_AVAILABLE_NOTIFICATION_ON, 0) == 1);
        notifyOpenNetworks.setEnabled(mWifiManager.isWifiEnabled());

        mSleepPolicyPref = (SwitchPreference) findPreference(KEY_SLEEP_POLICY);
        if (mSleepPolicyPref != null) {
            int value = Settings.Global.getInt(getContentResolver(),
                    Settings.Global.WIFI_SLEEP_POLICY,
                    Settings.Global.WIFI_SLEEP_POLICY_NEVER);
            mSleepPolicyPref.setChecked(value != Settings.Global.WIFI_SLEEP_POLICY_NEVER);
        }

        Intent wifiDirectIntent = new Intent(context,
                com.android.settings.Settings.WifiP2pSettingsActivity.class);
        Preference wifiDirectPref = findPreference(KEY_WIFI_DIRECT);
        wifiDirectPref.setIntent(wifiDirectIntent);

        // WpsDialog: Create the dialog like WifiSettings does.
        Preference wpsPushPref = findPreference(KEY_WPS_PUSH);
        wpsPushPref.setOnPreferenceClickListener(new OnPreferenceClickListener() {
                public boolean onPreferenceClick(Preference arg0) {
                    WpsFragment wpsFragment = new WpsFragment(WpsInfo.PBC);
                    wpsFragment.show(getFragmentManager(), KEY_WPS_PUSH);
                    return true;
                }
        });

        // WpsDialog: Create the dialog like WifiSettings does.
        Preference wpsPinPref = findPreference(KEY_WPS_PIN);
        wpsPinPref.setOnPreferenceClickListener(new OnPreferenceClickListener(){
                public boolean onPreferenceClick(Preference arg0) {
                    WpsFragment wpsFragment = new WpsFragment(WpsInfo.DISPLAY);
                    wpsFragment.show(getFragmentManager(), KEY_WPS_PIN);
                    return true;
                }
        });

        //[FEATURE]Add-BEGIN TCTNB.jianhong.yang,2016/10/11,task 3046476
        if (!mWifiManager.isWifiEnabled()) {
            if (wpsPinPref != null) {
                wpsPinPref.setEnabled(false);
            }
            if (wpsPushPref != null) {
                wpsPushPref.setEnabled(false);
            }
        }
        //Add-END jianhong.yang

        // Wifi extension requirement
        Preference prioritySettingPref = findPreference(KEY_PRIORITY_SETTINGS);
        if (prioritySettingPref != null) {
            if (!getResources().getBoolean(R.bool.set_wifi_priority)) {
                getPreferenceScreen().removePreference(prioritySettingPref);
            }
        } else {
            Log.d(TAG, "Fail to get priority pref...");
        }

        mAutoConnectEnablePref =
                (CheckBoxPreference) findPreference(KEY_AUTO_CONNECT_ENABLE);
        if (mAutoConnectEnablePref != null) {
            if (getResources().getBoolean(R.bool.config_auto_connect_wifi_enabled)) {
                mAutoConnectEnablePref.setChecked(isAutoConnectEnabled());
                mAutoConnectEnablePref.setOnPreferenceChangeListener(this);
            } else {
                getPreferenceScreen().removePreference(mAutoConnectEnablePref);
            }
        }

        mCellularToWlanPref =
                (ListPreference) findPreference(KEY_CELLULAR_TO_WLAN);
        if (mCellularToWlanPref != null) {
            if (getResources().getBoolean(R.bool.cell_to_wifi)) {
                int value = getCellularToWlanValue();
                mCellularToWlanPref.setValue(String.valueOf(value));
                updateCellToWlanSummary(mCellularToWlanPref, value);
                mCellularToWlanPref.setOnPreferenceChangeListener(this);
            } else {
                getPreferenceScreen().removePreference(mCellularToWlanPref);
            }
        }

        CheckBoxPreference wlanToCellularHintPref =
                (CheckBoxPreference) findPreference(KEY_WLAN_TO_CELLULAR_HINT);
        if (wlanToCellularHintPref != null) {
            if (getResources().getBoolean(R.bool.wifi_to_cell)) {
                wlanToCellularHintPref.setChecked(isWlanToCellHintEnable());
                wlanToCellularHintPref.setOnPreferenceChangeListener(this);
            } else {
                getPreferenceScreen().removePreference(wlanToCellularHintPref);
            }
        }

        CheckBoxPreference notifyConnectedApPref =
                (CheckBoxPreference) findPreference(KEY_CONNECT_NOTIFY);
        if (notifyConnectedApPref != null) {
            if (getResources().getBoolean(R.bool.connect_to_cmcc_notify)) {
                notifyConnectedApPref.setChecked(ifNotifyConnect());
                notifyConnectedApPref.setOnPreferenceChangeListener(this);
            } else {
                getPreferenceScreen().removePreference(notifyConnectedApPref);
            }
        }

        mCellularToWlanHintPref = (CheckBoxPreference) findPreference(KEY_CELLULAR_TO_WLAN_HINT);
        if (mCellularToWlanHintPref != null) {
            if (getResources().getBoolean(R.bool.cellular_to_wlan_hint)) {
                mCellularToWlanHintPref.setChecked(isCellularToWlanHintEnable());
                mCellularToWlanHintPref.setOnPreferenceChangeListener(this);
            } else {
                getPreferenceScreen().removePreference(mCellularToWlanHintPref);
            }
        }
    }

    /* Wrapper class for the WPS dialog to properly handle life cycle events like rotation. */
    public static class WpsFragment extends DialogFragment {
        private static int mWpsSetup;

        // Public default constructor is required for rotation.
        public WpsFragment() {
            super();
        }

        public WpsFragment(int wpsSetup) {
            super();
            mWpsSetup = wpsSetup;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            return new WpsDialog(getActivity(), mWpsSetup);
        }
    }

/* MODIFIED-BEGIN by jie.zhang, 2016-10-18,BUG-3137678*/
//WAPI+++
    private void initWapiCertInstallPreference() {
        Preference pref = findPreference(KEY_WAPI_CERT_INSTALL);
        if (null != pref) {
            Log.e(TAG, "initWapiCertInstallPreference pref != null");
            pref.setOnPreferenceClickListener((Preference.OnPreferenceClickListener)this);
        } else {
            Log.e(TAG, "initWapiCertInstallPreference pref == null");
        }
    }

    private void initWapiCertUninstallPreference() {
        Preference pref = findPreference(KEY_WAPI_CERT_UNINSTALL);
        if (null != pref) {
            Log.e(TAG, "initWapiCertUninstallPreference pref != null");
            pref.setOnPreferenceClickListener((Preference.OnPreferenceClickListener)this);
        } else {
            Log.e(TAG, "initWapiCertUninstallPreference pref == null");
        }
    }

    public boolean onPreferenceClick(Preference preference) {
        String key = preference.getKey();
        Log.e(TAG, "onPreferenceClick key " + key);
        if (key == null) return true;
        //[FEATURE]Mod-BEGIN jianhong.yang,2016/10/31,task3046476
        Bundle bundle = new Bundle();
        if (key.equals(KEY_WAPI_CERT_INSTALL)) {
            Log.e(TAG, "onPreferenceClick key 1" + key);
            bundle.putInt(WapiCertificateSettings.WAPI_REQUEST_MODE,
                    WapiCertificateSettings.MODE_INSTALL);
            startFragment(this, WapiCertificateSettings.class.getCanonicalName(),
                    R.string.wifi_wapi_cert_install, -1 /* Do not request a results */,
                    bundle);
            //showDialog(WAPI_INSTALL_DIALOG_ID);

        } else if (key.equals(KEY_WAPI_CERT_UNINSTALL)) {
            Log.e(TAG, "onPreferenceClick key 2" + key);
            bundle.putInt(WapiCertificateSettings.WAPI_REQUEST_MODE,
                    WapiCertificateSettings.MODE_UNINSTALL);
            startFragment(this, WapiCertificateSettings.class.getCanonicalName(),
                    R.string.wifi_wapi_cert_uninstall, -1 /* Do not request a results */,
                    bundle);
            //showDialog(WAPI_UNINSTALL_DIALOG_ID);
            //[FEATURE]Mod-END jianhong.yang
        }
        return true;
    }
//WAPI---
/* MODIFIED-END by jie.zhang,BUG-3137678*/

    // Wifi extension requirement
    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        final Context context = getActivity();
        String key = preference.getKey();

        if (KEY_WLAN_TO_CELLULAR_HINT.equals(key)) {
            boolean checked = ((Boolean) newValue).booleanValue();
            setWlanToCellularHintEnable(checked);
        }

        if (KEY_AUTO_CONNECT_ENABLE.equals(key)) {
            boolean checked = ((Boolean) newValue).booleanValue();
            setAutoConnectTypeEnabled(checked);
            updateCellularToWifiPrefs(checked);
            if (!checked) {
                updateCellularToWlanHintPref(true);
            }
        }

        if (KEY_CELLULAR_TO_WLAN.equals(key)) {
            int value = Integer.parseInt(((String) newValue));
            setCellToWlanType(value);
            mCellularToWlanPref.setValue(String.valueOf(value));
            updateCellToWlanSummary(mCellularToWlanPref, value);
            updateAutoConnectPref(value == CELLULAR_TO_WLAN_CONNECT_TYPE_AUTO);
            if (CELLULAR_TO_WLAN_CONNECT_TYPE_AUTO != value) {
                updateCellularToWlanHintPref(true);
            }
        }

        if (KEY_CONNECT_NOTIFY.equals(key)) {
            boolean checked = ((Boolean) newValue).booleanValue();
            setApConnectedNotify(checked);
        }

        if (KEY_CELLULAR_TO_WLAN_HINT.equals(key)) {
            boolean checked = ((Boolean) newValue).booleanValue();
            setCellularToWlanHintEnable(checked);
            if(!checked) {
                Toast.makeText(getActivity(),
                        getResources().getString(R.string.cellular_to_wlan_hint_toast),
                        Toast.LENGTH_LONG).show();
            }
        }

        return true;
    }


    private boolean isCellularToWlanHintEnable() {
        return Settings.System.getInt(getActivity().getContentResolver(),
                CELLULAR_TO_WLAN_HINT, NOTIFY_USER) == NOTIFY_USER;
    }

    private boolean isWlanToCellHintEnable() {
        return Settings.System.getInt(getActivity().getContentResolver(),
                WLAN_TO_CELLULAR_HINT, NOTIFY_USER) == NOTIFY_USER;
    }

    private void setWlanToCellularHintEnable(boolean enable) {
        final int defaultValue = enable ? NOTIFY_USER : DO_NOT_NOTIFY_USER;
        Settings.System.putInt(getActivity().getContentResolver(),
                WLAN_TO_CELLULAR_HINT, defaultValue);
    }

    private boolean ifNotifyConnect() {
        return Settings.System.getInt(getActivity().getContentResolver(),
                NOTIFY_USER_CONNECT, NOTIFY_USER) == NOTIFY_USER;
    }

    private boolean isAutoConnectEnabled() {
        return Settings.System.getInt(getActivity().getContentResolver(),
                WIFI_AUTO_CONNECT_TYPE, AUTO_CONNECT_ENABLED) == AUTO_CONNECT_ENABLED;
    }

    private void setAutoConnectTypeEnabled(boolean enable) {
        final int defaultValue = enable ? AUTO_CONNECT_ENABLED : AUTO_CONNECT_DISABLE;
        Settings.System.putInt(getActivity().getContentResolver(),
                WIFI_AUTO_CONNECT_TYPE, defaultValue);
    }

    private int getCellularToWlanValue() {
        if (isAutoConnectEnabled()) {
            return CELLULAR_TO_WLAN_CONNECT_TYPE_AUTO;
        } else {
            return Settings.System.getInt(getContentResolver(), CELLULAR_TO_WLAN_CONNECT_TYPE,
                    CELLULAR_TO_WLAN_CONNECT_TYPE_AUTO);
        }
    }

    private void updateCellToWlanSummary(Preference preference, int index) {
        String[] summaries = getResources().getStringArray(R.array.cellcular2wifi_entries);
        preference.setSummary(summaries[index]);
    }

    private void updateCellularToWlanHintPref(boolean enable) {
        mCellularToWlanHintPref.setChecked(enable);
        setCellularToWlanHintEnable(enable);
    }

    private void setCellularToWlanHintEnable(boolean needNotify) {
        final int defaultValue = needNotify ? NOTIFY_USER : DO_NOT_NOTIFY_USER;
        Settings.System.putInt(getActivity().getContentResolver(),
                CELLULAR_TO_WLAN_HINT, defaultValue);
    }

    private void setApConnectedNotify(boolean needNotify) {
        final int defaultValue = needNotify ? NOTIFY_USER : DO_NOT_NOTIFY_USER;
        Settings.System.putInt(getActivity().getContentResolver(),
                NOTIFY_USER_CONNECT, defaultValue);
    }

    private void setCellToWlanType(int value) {
        try {
            Settings.System.putInt(getContentResolver(), CELLULAR_TO_WLAN_CONNECT_TYPE,
                    value);
        } catch (NumberFormatException e) {
            Toast.makeText(getActivity(), R.string.wifi_setting_connect_type_error,
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void updateCellularToWifiPrefs(boolean isAutoEnabled) {
        if (!isAutoEnabled) {
            updateCellularToWlanHintPref(true);
        }
        int defaultValue = isAutoEnabled ? CELLULAR_TO_WLAN_CONNECT_TYPE_AUTO
                : CELLULAR_TO_WLAN_CONNECT_TYPE_MANUAL;
        Settings.System.putInt(getContentResolver(), CELLULAR_TO_WLAN_CONNECT_TYPE, defaultValue);
        mCellularToWlanPref.setValue(String.valueOf(defaultValue));
        updateCellToWlanSummary(mCellularToWlanPref, defaultValue);
    }

    private void updateAutoConnectPref(boolean isAutoMode) {
        setAutoConnectTypeEnabled(isAutoMode);
        mAutoConnectEnablePref.setChecked(isAutoMode);
    }

    //[FEATURE]Add-BEGIN TCTNB.jianhong.yang,2016/10/11,task 3046476
    @Override
    public void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        getActivity().unregisterReceiver(mReceiver);
    }

    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen,
            Preference preference) {
        // TODO Auto-generated method stub
         String key = preference.getKey();

            if (KEY_NOTIFY_OPEN_NETWORKS.equals(key)) {
                Settings.Global.putInt(getContentResolver(),
                        Settings.Global.WIFI_NETWORKS_AVAILABLE_NOTIFICATION_ON,
                        ((SwitchPreference) preference).isChecked() ? 1 : 0);
            } else if(KEY_ADD_NETWORK_MANUALLY.equals(key)) {
                startActivity();
            } if (KEY_SLEEP_POLICY.equals(key)) {
                    int value = mSleepPolicyPref.isChecked() ?
                             Settings.Global.WIFI_SLEEP_POLICY_DEFAULT : Settings.Global.WIFI_SLEEP_POLICY_NEVER;
                    Settings.Global.putInt(getContentResolver(), Settings.Global.WIFI_SLEEP_POLICY,
                            value);
            } else if(KEY_MARK_PAY_AP.equals(key)) {
                 startFragment(this, MarkPayApSettings.class.getCanonicalName(),
                         R.string.mark_pay_ap, -1 /* Do not request a results */,
                         null);
            }

        return super.onPreferenceTreeClick(preferenceScreen,preference);
    }

    private void refreshWifiInfo() {
        final Context context = getActivity();
        WifiInfo wifiInfo = mWifiManager.getConnectionInfo();

        Preference wifiMacAddressPref = findPreference(KEY_MAC_ADDRESS);
        String macAddress = wifiInfo == null ? null : wifiInfo.getMacAddress();
        wifiMacAddressPref.setSummary(!TextUtils.isEmpty(macAddress) ? macAddress
                : context.getString(R.string.status_unavailable));
        wifiMacAddressPref.setSelectable(false);

        Preference wifiIpAddressPref = findPreference(KEY_CURRENT_IP_ADDRESS);
        String ipAddress = Utils.getWifiIpAddresses(context);
        wifiIpAddressPref.setSummary(ipAddress == null ?
                context.getString(R.string.status_unavailable) : ipAddress);
        wifiIpAddressPref.setSelectable(false);
    }

    private void startActivity() {
        // TODO Auto-generated method stub
        Intent intent = new Intent();
        intent.setClassName(getActivity(), "com.android.settings.wifi.AddNetwork");
        startActivity(intent);
    }
    //[FEATURE]Add END TCTNB.jianhong.yang
}
