/* Copyright (C) 2016 Tcl Corporation Limited */

/******************************************************************************/
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 2016/10/31|     jianhong.yang    |     task 3046476     |  Ergo development*/
/*           |                      |                      |  create new file */
/* ----------|----------------------|----------------------|----------------- */

package com.android.settings.wifi;

import android.os.Bundle;
import android.os.FileUtils;
import android.os.UserManager;
import android.os.storage.StorageManager;
import android.os.storage.StorageVolume;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.BufferedReader;
import java.util.ArrayList;

import mst.app.dialog.AlertDialog;
import mst.preference.Preference;
import mst.preference.PreferenceScreen;
import mst.preference.Preference.OnPreferenceClickListener;
import mst.preference.Preference.OnPreferenceChangeListener;

import android.content.Context;
import android.content.DialogInterface;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.text.format.Formatter;
import android.text.method.PasswordTransformationMethod;
import android.text.method.TransformationMethod;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.TableLayout;
import android.widget.Toast;
import android.os.Environment;
import android.os.storage.StorageManager;
import android.os.storage.StorageVolume;

import com.android.settings.WidgetListPreference;
import com.android.internal.logging.MetricsProto.MetricsEvent;
import com.android.settings.R;
import com.android.settings.RestrictedSettingsFragment;
import com.android.settings.WidgetPreference;
import com.android.settings.applications.LayoutPreference;

public class WapiCertificateSettings extends RestrictedSettingsFragment implements
        OnPreferenceChangeListener{
     private static final String TAG = "WapiCertificateSettings";

    private static final String KEY_CREATE_SUBDIR_PREFERENCE = "create_subdir_preference";
    private static final String KEY_AS_CERT_PREFERENCE = "as_cert_preference";
    private static final String KEY_USER_CERT_PREFERENCE = "user_cert_preference";

    private LayoutPreference mCreateSubDirPreference;
    private LayoutPreference mAsCertPreference;
    private LayoutPreference mUserCertPreference;

    private WidgetListPreference mUninstallWapiCertPreference;

    private static final String InstallTitle = "Install";
    private static final String UninstallTitle = "Uninstall";

    public static final String WAPI_REQUEST_MODE = "wapi_request_mode";
    public static final int MODE_INSTALL = 0;
    public static final int MODE_UNINSTALL = 1;
    private int mMode = MODE_INSTALL;
    // General views
    private View mView;

    private TextView mCreateSubdirText;
    private EditText mCreateSubdirEdit;

    private TextView mASCertText;
    private EditText mASCertEdit;

    private TextView mUserCertText;
    private EditText mUserCertEdit;

    private TextView mDeletDirText;

    private String mUninstallCerts;
    private CharSequence mCustomTitle;

    // Button positions, default to impossible values
    private int mInstallButtonPos = Integer.MAX_VALUE;
    private int mUninstallButtonPos = Integer.MAX_VALUE;
    private int mCancelButtonPos = Integer.MAX_VALUE;

    private static final String DEFAULT_CERTIFICATE_PATH =
    // "/system/wifi/wapi_certificate";
    "/data/misc/wapi_certificate";

    private static String default_sdcard_path;
    private static String external_sdcard_path;
    private static String wifi_sdcard_path;
    private static String certificate_path;
    private static String certificate_installation_path;

    private StorageManager mStorageManager;
     ArrayList<String> mCertNameList= new ArrayList<String>();

    public WapiCertificateSettings(String restrictionKey) {
        super(restrictionKey);
        // TODO Auto-generated constructor stub
    }

    public WapiCertificateSettings() {
        super(UserManager.DISALLOW_CONFIG_WIFI);
    }

    @Override
    protected int getMetricsCategory() {
        // TODO Auto-generated method stub
        return MetricsEvent.WIFI_ADVANCED;
    }

    @Override
    public void onCreate(Bundle icicle) {
        // TODO Auto-generated method stub
        super.onCreate(icicle);
        addPreferencesFromResource(R.xml.wapi_certificate_settings);

        initView();
    }

    private void initView() {
         Bundle requestMode = getArguments();
         mMode = requestMode.getInt(WAPI_REQUEST_MODE);

        if (mMode == MODE_INSTALL) {

            mCreateSubDirPreference = new LayoutPreference(getContext(),R.layout.wapi_certificate_settings_panel);
            mAsCertPreference = new LayoutPreference(getContext(),R.layout.wapi_certificate_settings_panel);
            mUserCertPreference = new LayoutPreference(getContext(),R.layout.wapi_certificate_settings_panel);
            getPreferenceScreen().addPreference(mCreateSubDirPreference);
            getPreferenceScreen().addPreference(mAsCertPreference);
            getPreferenceScreen().addPreference(mUserCertPreference);

            ((LinearLayout) mCreateSubDirPreference
                    .findViewById(R.id.create_subdir_section))
                    .setVisibility(View.VISIBLE);
            ((LinearLayout) mAsCertPreference.findViewById(R.id.as_cert_section))
                    .setVisibility(View.VISIBLE);
            ((LinearLayout) mUserCertPreference
                    .findViewById(R.id.user_cert_section))
                    .setVisibility(View.VISIBLE);

            mCreateSubdirText = (TextView) mCreateSubDirPreference
                    .findViewById(R.id.wapi_cert_create_subdir_text);
            mCreateSubdirEdit = (EditText) mCreateSubDirPreference
                    .findViewById(R.id.wapi_cert_create_subdir_edit);

            mASCertText = (TextView) mAsCertPreference
                    .findViewById(R.id.wapi_as_cert_text);
            mASCertEdit = (EditText) mAsCertPreference
                    .findViewById(R.id.wapi_as_cert_edit);

            mUserCertText = (TextView) mUserCertPreference
                    .findViewById(R.id.wapi_user_cert_text);
            mUserCertEdit = (EditText) mUserCertPreference
                    .findViewById(R.id.wapi_user_cert_edit);
        } else if (mMode == MODE_UNINSTALL) {
            mUninstallWapiCertPreference = new WidgetListPreference(getContext(),null);
            mUninstallWapiCertPreference.setTitle(R.string.wifi_wapi_cert_delet_subdir);
            getPreferenceScreen().addPreference(mUninstallWapiCertPreference);
            mUninstallWapiCertPreference.setOnPreferenceChangeListener(this);
            setDeletDirAdapter();

            if(!mCertNameList.isEmpty()) {
                mUninstallWapiCertPreference.setDetail(mCertNameList.get(0));
                handleDeletDirChange(0);
            }
        }
    }

    private boolean handleInstall() {
        Log.v(TAG, "handleInstall");

        String stringDefDir = DEFAULT_CERTIFICATE_PATH;
        File defDir = new File(stringDefDir);
        File path;
        mStorageManager = StorageManager.from(getContext());
        StorageVolume[] volumes = mStorageManager.getVolumeList();

        if (!defDir.exists()) {
            defDir.mkdir();
            if (!defDir.exists()) {
                new AlertDialog.Builder(getContext())
                        .setTitle(R.string.error_title)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setMessage("Cert. base dir create failed")
                        .setPositiveButton(android.R.string.ok, null).show();
                return false;
            }
            FileUtils.setPermissions(stringDefDir, FileUtils.S_IRWXU
                    | FileUtils.S_IRWXG | FileUtils.S_IRWXO, -1, -1);
        }

        String subdir = getInput(mCreateSubdirEdit);
        if (null == subdir || TextUtils.isEmpty(subdir)) {
            new AlertDialog.Builder(getContext())
                    .setTitle(R.string.error_title)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setMessage(
                            R.string.wifi_wapi_cert_mgmt_subdir_name_is_empty)
                    .setPositiveButton(android.R.string.ok, null).show();
            return false;
        }
        String stringDestDir = DEFAULT_CERTIFICATE_PATH + "/" + subdir;
        File destDir = new File(stringDestDir);
        if (destDir.exists()) {
            new AlertDialog.Builder(getContext())
                    .setTitle(R.string.error_title)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setMessage(R.string.wifi_wapi_cert_mgmt_subdir_exist)
                    .setPositiveButton(android.R.string.ok, null).show();
            return false;
        }
        try {
            destDir.mkdir();
        } catch (Exception e) {
            //setMessage(e.toString());
        }
        if (!destDir.exists()) {
            new AlertDialog.Builder(getContext())
                    .setTitle(R.string.error_title)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setMessage(R.string.wifi_wapi_cert_mgmt_subdir_create_fail)
                    .setPositiveButton(android.R.string.ok, null).show();
            return false;
        }

        String asCert = getInput(mASCertEdit);
        if (null == asCert || TextUtils.isEmpty(asCert)) {
            new AlertDialog.Builder(getContext())
                    .setTitle(R.string.error_title)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setMessage(R.string.wifi_wapi_cert_mgmt_as_name_is_empty)
                    .setPositiveButton(android.R.string.ok, null).show();
            deleteAll(stringDestDir);
            return false;
        }

        wifi_sdcard_path = "/system/wifi/sdcard";

        for (int ivolumes = 0; ivolumes < volumes.length; ivolumes++) {
            path = new File(volumes[ivolumes].getPath());
            Log.e("adarsh",
                    "Trying to create file at - " + path + ":: isRemovable="
                            + volumes[ivolumes].isRemovable()
                            + ", getDescription="
                            + volumes[ivolumes].getDescription(getContext())
                            + ", isEmulated=" + volumes[ivolumes].isEmulated()
                            + ", isPrimary=" + volumes[ivolumes].isPrimary());

            if (volumes[ivolumes].isPrimary() == true
                    && volumes[ivolumes].isEmulated() == true)
                default_sdcard_path = path.toString();

            if (volumes[ivolumes].isRemovable() == true)
                external_sdcard_path = path.toString();

        }

        certificate_installation_path = default_sdcard_path;
        Log.d(TAG, "default_sdcard_path: " + default_sdcard_path);
        Log.d(TAG, "asCert file:" + asCert);
        certificate_path = default_sdcard_path + "/" + asCert;
        Log.d(TAG, "certificate_path: " + certificate_path);
        File fileASCert = new File(certificate_path);
        Log.d(TAG, "fileASCert.exists(): " + fileASCert.exists());

        if (!fileASCert.exists()) {
            Log.d(TAG, "Certificate path: " + certificate_path
                    + " does not exist");
            Log.d(TAG, "Hence trying with " + external_sdcard_path);
            certificate_installation_path = external_sdcard_path;
            certificate_path = external_sdcard_path + "/" + asCert;
            fileASCert = new File(certificate_path);
            Log.d(TAG, "fileASCert.exists(): " + fileASCert.exists());

            if (!fileASCert.exists()) {
                Log.d(TAG, "Secondary certificate path: " + certificate_path
                        + " does not exist.");
                Log.d(TAG, "Hence trying with " + wifi_sdcard_path);
                certificate_installation_path = wifi_sdcard_path;
                certificate_path = wifi_sdcard_path + "/" + asCert;
                fileASCert = new File(certificate_path);
                Log.d(TAG, "fileASCert.exists(): " + fileASCert.exists());

                if (!fileASCert.exists()) {
                    Log.d(TAG, "wifi certificate path: " + certificate_path
                            + " does not exist.");
                    Log.d(TAG, "Hence ABORTING!!!!!");
                    new AlertDialog.Builder(getContext())
                            .setTitle(R.string.error_title)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setMessage(
                                    R.string.wifi_wapi_cert_mgmt_as_dont_exist)
                            .setPositiveButton(android.R.string.ok, null)
                            .show();
                    deleteAll(stringDestDir);
                    return false;
                }
            }
        }

        /* Assuming that all the certificates will be in single path */
        Log.e(TAG, "certificate is installing from "
                + certificate_installation_path);

        if (!isAsCertificate(certificate_installation_path + "/" + asCert)) {
            new AlertDialog.Builder(getContext())
                    .setTitle(R.string.error_title)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setMessage(R.string.wifi_wapi_cert_mgmt_as_format_is_wrong)
                    .setPositiveButton(android.R.string.ok, null).show();
            deleteAll(stringDestDir);
            return false;
        }
        Log.e(TAG, "handleInstall Create AS Cert: = " + asCert);
        File fileDestAS = new File(stringDestDir + "/" + "as.cer");
        try {
            fileDestAS.createNewFile();
        } catch (Exception e) {
            //setMessage(e.toString());
        }
        if (fileDestAS.exists()) {
            if (!copyFile(fileDestAS, fileASCert)) {
                deleteAll(stringDestDir);
                Toast.makeText(getActivity(),R.string.install_wapi_cert_failed,Toast.LENGTH_SHORT).show();
                return false;
            }
        } else {
            deleteAll(stringDestDir);
            Toast.makeText(getActivity(),R.string.install_wapi_cert_failed,Toast.LENGTH_SHORT).show();
            return false;
        }

        String userCert = getInput(mUserCertEdit);
        if (null == userCert || TextUtils.isEmpty(userCert)) {
            new AlertDialog.Builder(getContext())
                    .setTitle(R.string.error_title)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setMessage(R.string.wifi_wapi_cert_mgmt_user_name_is_empty)
                    .setPositiveButton(android.R.string.ok, null).show();
            deleteAll(stringDestDir);
            return false;
        }
        File fileUserCert = new File(certificate_installation_path + "/"
                + userCert);
        if (!fileUserCert.exists()) {
            new AlertDialog.Builder(getContext())
                    .setTitle(R.string.error_title)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setMessage(R.string.wifi_wapi_cert_mgmt_user_dont_exist)
                    .setPositiveButton(android.R.string.ok, null).show();
            deleteAll(stringDestDir);
            return false;
        }
        if (!isUserCertificate(certificate_installation_path + "/" + userCert)) {
            new AlertDialog.Builder(getContext())
                    .setTitle(R.string.error_title)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setMessage(
                            R.string.wifi_wapi_cert_mgmt_user_format_is_wrong)
                    .setPositiveButton(android.R.string.ok, null).show();
            deleteAll(stringDestDir);
            return false;
        }
        File fileDestUser = new File(stringDestDir + "/" + "user.cer");
        try {
            fileDestUser.createNewFile();
        } catch (Exception e) {
            //setMessage(e.toString());
        }
        if (fileDestUser.exists()) {
            if (!copyFile(fileDestUser, fileUserCert)) {
                deleteAll(stringDestDir);
                Toast.makeText(getActivity(),R.string.install_wapi_cert_failed,Toast.LENGTH_SHORT).show();
                return false;
            }
        } else {
            deleteAll(stringDestDir);
            Toast.makeText(getActivity(),R.string.install_wapi_cert_failed,Toast.LENGTH_SHORT).show();
            return false;
        }
        // FileUtils.setPermissions(stringDefDir, FileUtils.S_IRWXU|
        // FileUtils.S_IRWXG | FileUtils.S_IRWXO , -1, -1);
        FileUtils.setPermissions(stringDestDir, FileUtils.S_IRWXU
                | FileUtils.S_IRWXO, -1, -1);
        FileUtils.setPermissions(stringDestDir + "/" + "user.cer",
                FileUtils.S_IRUSR | FileUtils.S_IRGRP | FileUtils.S_IROTH, -1,
                -1);
        FileUtils.setPermissions(stringDestDir + "/" + "as.cer",
                FileUtils.S_IRUSR | FileUtils.S_IRGRP | FileUtils.S_IROTH, -1,
                -1);

        return true;
    }

    private void handleUninstall() {
        Log.v(TAG, "handleUninstall");
        if (null != mUninstallCerts) {
            deleteAll(mUninstallCerts);
        }
    }

    private void handleDeletDirChange(int deletDirIdx) {
        File certificateList [];

        //find all certificate
        File certificatePath = new File(DEFAULT_CERTIFICATE_PATH);
        try{
            if (!certificatePath.isDirectory()){
                return;
            } else {
                certificateList = certificatePath.listFiles();
                mUninstallCerts = certificateList[deletDirIdx].getAbsolutePath();
            }
        } catch (Exception e){
            //setMessage(e.toString());
        }
    }

    private void setDeletDirAdapter() {
        Context context = getContext();
        File certificateList [];
        int i;

        //find all certificate
        File certificatePath = new File(DEFAULT_CERTIFICATE_PATH);
        try {
            if (!certificatePath.isDirectory()) {
                return;
            }

            //build string array
            certificateList = certificatePath.listFiles();
            for(i=0; i < certificateList.length; i++){
                mCertNameList.add(certificateList[i].getName());
            }

            mUninstallWapiCertPreference.setEntries((String [])(mCertNameList.toArray(new String[0])));
            mUninstallWapiCertPreference.setEntryValues((String [])(mCertNameList.toArray(new String[0])));

        } catch (Exception e) {
            //setMessage(e.toString());
        }
    }

    private boolean copyFile(File fileDest, File fileSource) {
        FileInputStream fI;
        FileOutputStream fO;
        byte[] buf = new byte[1024];
        int i = 0;
        Log.v(TAG, "copyFile");
        try {
            fI = new FileInputStream(fileSource);
        } catch (Exception e) {
            //setMessage(e.toString());
            return false;
        }
        try {
            fO = new FileOutputStream(fileDest);
        } catch (Exception e) {
            //setMessage(e.toString());
            return false;
        }
        while (true) {
            try {
                i = fI.read(buf);
            } catch (Exception e) {
                //setMessage(e.toString());
                return false;
            }
            if (i == -1) {
                break;
            }
            try {
                fO.write(buf, 0, i);
            } catch (Exception e) {
                //setMessage(e.toString());
                return false;
            }
        }
        try {
            fI.close();
            fO.close();
        } catch (Exception e) {
            //setMessage(e.toString());
            return false;
        }
        return true;
    }

    public int searchString(String find_str, File file) throws Exception {
        FileReader reader = new FileReader(file);
        BufferedReader reader2 = new BufferedReader(reader, 2048);
        String s = "";
        String buffer = new String("");
        do {
            buffer += s;
        } while ((s = reader2.readLine()) != null);
        return buffer.split(find_str).length - 1;
    }

    private boolean isAsCertificate(String ascert) {
        String stringCertBegin = "BEGIN CERTIFICATE";
        String stringCertEnd = "END CERTIFICATE";
        String stringECBegin = "BEGIN EC PRIVATE KEY";
        String stringECEnd = "END EC PRIVATE KEY";
        File as = new File(ascert);
        try {
            if (1 != searchString(stringCertBegin, as)) {
                return false;
            }
        } catch (Exception e) {
            //setMessage(e.toString());
            return false;
        }
        try {
            if (1 != searchString(stringCertEnd, as)) {
                return false;
            }
        } catch (Exception e) {
            //setMessage(e.toString());
            return false;
        }
        try {
            if (searchString(stringECBegin, as) != 0) {
                return false;
            }
        } catch (Exception e) {
            //setMessage(e.toString());
            return false;
        }
        try {
            if (searchString(stringECEnd, as) != 0) {
                return false;
            }
        } catch (Exception e) {
            //setMessage(e.toString());
            return false;
        }
        return true;
    }

    private boolean isUserCertificate(String usercert) {
        String stringCertBegin = "BEGIN CERTIFICATE";
        String stringCertEnd = "END CERTIFICATE";
        String stringECBegin = "BEGIN EC PRIVATE KEY";
        String stringECEnd = "END EC PRIVATE KEY";
        File user = new File(usercert);
        try {
            if (1 != searchString(stringCertBegin, user)) {
                return false;
            }
        } catch (Exception e) {
            //setMessage(e.toString());
            return false;
        }
        try {
            if (1 != searchString(stringCertEnd, user)) {
                return false;
            }
        } catch (Exception e) {
            //setMessage(e.toString());
            return false;
        }
        try {
            if (1 != searchString(stringECBegin, user)) {
                return false;
            }
        } catch (Exception e) {
            //setMessage(e.toString());
            return false;
        }
        try {
            if (1 != searchString(stringECEnd, user)) {
                return false;
            }
        } catch (Exception e) {
            //setMessage(e.toString());
            return false;
        }
        return true;
    }

    private String getInput(EditText edit) {
        return (edit != null) ? (edit.getText().toString()) : null;
    }

    private void deleteAll(String filepath) {
        File f = new File(filepath);
        Log.v(TAG, "deleteAll filepath " + filepath);

        if (f.exists() && f.isDirectory()) {
            File delFile[] = f.listFiles();
            int fileNum = delFile.length;
            int i;
            if (fileNum == 0) {
                f.delete();
            } else {
                for (i = 0; i < fileNum; i++) {
                    String subdirectory = delFile[i].getAbsolutePath();
                    deleteAll(subdirectory);
                }
            }
            f.delete();
        } else if (f.exists()) {
            f.delete();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if(mMode == MODE_INSTALL) {
            menu.add(Menu.NONE, MODE_INSTALL, 0, R.string.wifi_wapi_cert_install_button)
            .setEnabled(true)
            .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        } else {
            menu.add(Menu.NONE, MODE_UNINSTALL, 0, R.string.wifi_wapi_cert_uninstall_button)
            .setEnabled(true)
            .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        }

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case MODE_INSTALL:
                if(handleInstall()) {
                    finish();
                }
                return true;

            case MODE_UNINSTALL:
                handleUninstall();
                //finish();
                mCertNameList.clear();
                setDeletDirAdapter();
                if(!mCertNameList.isEmpty()) {
                    mUninstallWapiCertPreference.setDetail(mCertNameList.get(0));
                    handleDeletDirChange(0);
                } else {
                    mUninstallWapiCertPreference.setDetail("");
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        final Context context = getActivity();
        String key = preference.getKey();

        if(preference == mUninstallWapiCertPreference) {
            if(mCertNameList.contains(newValue)) {
                handleDeletDirChange(mCertNameList.indexOf(newValue));
                mUninstallWapiCertPreference.setDetail((String)newValue);
            }
        }

        return true;
    }

}