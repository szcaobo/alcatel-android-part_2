/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.settings.dashboard;

import android.os.Bundle;
import com.android.settings.SettingsPreferenceFragment;
import com.android.internal.logging.MetricsProto.MetricsEvent;
import com.android.settings.R;
import com.android.settingslib.RestrictedPreference;
import android.content.Intent;
import android.content.IntentFilter;
import mst.preference.Preference;
import mst.preference.Preference.OnPreferenceClickListener;
import mst.preference.PreferenceGroup;
import mst.preference.PreferenceScreen;
import android.content.ComponentName;

/**
 * Created by zhaokai on 9/7/16.
 */
public class MoreSettings extends SettingsPreferenceFragment {

    private static final String KEY_REPLACEMENT_PHONE = "replacement_settings";
    private RestrictedPreference replacementPhone;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.more_settings);
        replacementPhone = (RestrictedPreference) findPreference(KEY_REPLACEMENT_PHONE);
        if (replacementPhone != null) {
            replacementPhone.setOnPreferenceClickListener(new OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    if (preference == replacementPhone) {
                        Intent intent=new Intent();
                        intent.setComponent(new ComponentName("cn.tcl.transfer","cn.tcl.transfer.activity.MainActivity"));
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|
                        Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                        startActivity(intent);
                        return true;
                    }
                    return false;
                }
            });
        }
    }
    @Override
    protected int getMetricsCategory() {
        return MetricsEvent.WIRELESS;
    }
}
