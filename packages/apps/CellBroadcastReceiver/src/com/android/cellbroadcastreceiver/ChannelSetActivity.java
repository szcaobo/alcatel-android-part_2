/******************************************************************************/
/*                                                               Date:04/2013 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2013 TCL Communication Technology Holdings Limited.       */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form   */
/* without the written permission of TCL Communication Technology Holdings   */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  jianglong.pan                                                   */
/*  Email  :  jianglong.pan@tcl.com                                           */
/*  Role   :                                                                  */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     :                                                                */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 04/06/2013|        bo.xu         |      FR-400302       |[SMS]Cell broadc- */
/*           |                      |                      |ast SMS support   */
/* ----------|----------------------|----------------------|----------------- */
/* 06/15/2013|        bo.xu         |      CR-451418       |Set dedicated Ce- */
/*           |                      |                      |ll broadcast MI   */
/*           |                      |                      |for Israel Progr- */
/*           |                      |                      |ams               */
/* ----------|----------------------|----------------------|----------------- */
/* 07/10/2013|        bo.xu         |      PR-477859       |[CB]CB list incr- */
/*           |                      |                      |ease new channel  */
/* ----------|----------------------|----------------------|----------------- */
/* 09/12/2014|      fujun.yang      |        772564        |new CLID variable */
/*           |                      |                      |to re-activate CB */
/*           |                      |                      |for NL            */
/* ----------|----------------------|----------------------|----------------- */
/* 08/12/2015|      dagang.yang     |       1060007        |CB channel index  */
/*           |                      |                      |name should suppo-*/
/*           |                      |                      |rt at least 12    */
/*           |                      |                      |Cyrillic character*/
/* ----------|----------------------|----------------------|----------------- */
/* 19/07/2016|    jianglong.pan     |SOLUTION-2520283      |Porting CMASS     */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package com.android.cellbroadcastreceiver;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.util.Log;


import com.android.cellbroadcastreceiver.CellBroadcast.Channel;
import com.android.internal.telephony.PhoneConstants;

import java.io.UnsupportedEncodingException;

public class ChannelSetActivity extends Activity {
    Button ok;

    Button cancel;

    EditText channelindex;
    TextView channelindexTitle;

    EditText channelname;

    CheckBox enable;

    String channelName = "";

    String channelId = "";

    String channelEnable = "";

    String channelOldIndex = "";

    String channelNewIndex = "";

    String ENABLE = "Enable";

    String DISABLE = "Disable";

    CBSUtills cbu;

    static final int EDIT_CHANNEL_REQUEST = 0;

    static final int ADD_CHANNEL_REQUEST = 1;

    final int MAXLENGTH = 14;

    boolean isAdd = true;

    private TelephonyManager mTelephonyManager;
    private int ranType;

    private static final String LOG_TAG = "ChannelSetActivity";
    //[BUGFIX]-Add-BEGIN by TCTNB.bo.xu,06/15/2013,CR-451418,
    //Set dedicated Cell broadcast MI for Israel Programs
    private String channelMode = "1";
    SharedPreferences mSettings;
    //[BUGFIX]-Add-END by TCTNB.bo.xu

    private int subDescription = PhoneConstants.SUB1;
    private boolean addChannel = false;
    private boolean mEnableSingleSIM = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_LEFT_ICON);
        setContentView(R.layout.addchannel);
        findView();
        setListener();
        cbu = new CBSUtills(this);
        //[BUGFIX]-Add-BEGIN by TCTNB.bo.xu,06/15/2013,CR-451418,
        //Set dedicated Cell broadcast MI for Israel Programs
        mSettings = this.getSharedPreferences("com.android.cellbroadcastreceiver_preferences", 0);
        //[FEATURE]-Add-BEGIN by TSCD.fujun.yang,09/12/2014,772564,new CLID variable to re-activate CB for NL
        mEnableSingleSIM = getResources().getBoolean(R.bool.def_cellbroadcastreceiver_enable_single_sim);
        log("mEnableSingleSIM=" + mEnableSingleSIM);
        if (TelephonyManager.getDefault().isMultiSimEnabled()) {
            if (subDescription == 0) {
                channelMode = mSettings.getString("pref_key_choose_channel_sim1", "1");
            } else if (subDescription == 1) {
                channelMode = mSettings.getString("pref_key_choose_channel_sim2", "1");
            }
        } else {
            channelMode = mSettings.getString("pref_key_choose_channel", "1");
        }
        //[FEATURE]-Add-END by TSCD.fujun.yang
        //[BUGFIX]-Add-END by TCTNB.bo.xu
        Intent intent = this.getIntent();
        if (TelephonyManager.getDefault().isMultiSimEnabled() && (intent != null)) {
            subDescription = intent.getIntExtra(PhoneConstants.SUBSCRIPTION_KEY, PhoneConstants.SUB1);
            if (mEnableSingleSIM) {
                subDescription = PhoneConstants.SUB1;
            }
            log("multisimcard-subDescription=" + subDescription);
            addChannel = intent.getBooleanExtra("channeladd", false);
        }
        if (!addChannel && this.getIntent().getExtras() != null) {
            isAdd = false;
            channelId = this.getIntent().getExtras().getString("id");
            Cursor c = null;
            if (TelephonyManager.getDefault().isMultiSimEnabled() && (subDescription != -1)) {
                c = cbu.queryChannel(channelId, subDescription);
            } else {
                c = cbu.queryChannel(channelId);
            }
            if (c != null && c.getCount() > 0) {
                c.moveToFirst();
                channelName = c.getString(c.getColumnIndex(Channel.NAME));
                channelOldIndex = c.getString(c.getColumnIndex(Channel.INDEX));
                channelEnable = c.getString(c.getColumnIndex(Channel.Enable));
                // init control text
                channelname.setText(channelName);
                channelindex.setText(channelOldIndex);
                enable.setChecked(channelEnable.equalsIgnoreCase(ENABLE) ? true : false);
            }
            if (c != null) {
                c.close();
            }
        } else {
            addChannel = false;
            isAdd = true;
        }
        getWindow().setFeatureDrawableResource(Window.FEATURE_LEFT_ICON,
                R.drawable.ic_dialog_alert_holo_light);

        mTelephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (mTelephonyManager.getPhoneType() == TelephonyManager.PHONE_TYPE_CDMA) {
            ranType = SmsManager.CELL_BROADCAST_RAN_TYPE_CDMA;
        } else if (mTelephonyManager.getPhoneType() == TelephonyManager.PHONE_TYPE_GSM) {
            ranType = SmsManager.CELL_BROADCAST_RAN_TYPE_GSM;
        }
    }

    void findView() {
        ok = (Button) findViewById(R.id.ok);
        cancel = (Button) findViewById(R.id.cancel);
        channelname = (EditText) findViewById(R.id.channelname);
        channelindex = (EditText) findViewById(R.id.channelindex);
        enable = (CheckBox) findViewById(R.id.channelenable);
        //[BUGFIX]-Add-BEGIN by TSCD.dagang.yang,08/12/2015,1060007
        if (getResources().getBoolean(R.bool.feature_addChannelTextLimitForRU_on)) {
            channelname.setFilters(new InputFilter[]{new InputFilter.LengthFilter(50)});
            //Begin add by qiang.sun for 1983051
            TextView channelname_Tv = (TextView)findViewById(R.id.channelname_prompt);
            String channelname_prompt = this.getResources().getString(R.string.channel_name);
            Log.i("sunqiang", "channelname_prompt = " + channelname_prompt);
            if(!TextUtils.isEmpty(channelname_prompt) && channelname_prompt.contains("10")){
                channelname_prompt = channelname_prompt.replace("10", "50");
                Log.i("sunqiang", "channelname_prompt == " + channelname_prompt);
            }
            channelname_Tv.setText(channelname_prompt);
            //End add by qiang.sun for 1983051
        }
        //[BUGFIX]-Add-END by TSCD.dagang.yang,08/12/2015,1060007
    }

    void setListener() {
        ok.setOnClickListener(mOkListener);
        cancel.setOnClickListener(mCancelListenertest);
    }

    // true: is numeric
    boolean isNumeric(String obj) {
        try {
            Integer.parseInt(obj);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    private OnClickListener mOkListener = new OnClickListener() {
        public void onClick(View v) {
            // check index is integer
            String channelName = "";
            String channelEnable = "";
            channelNewIndex = channelindex.getText().toString().trim();

            if (channelNewIndex.trim().equalsIgnoreCase("")) {
                // alert user
                Toast.makeText(ChannelSetActivity.this, R.string.ChannelIsNull, Toast.LENGTH_SHORT)
                        .show();
                return;
            }
            if (!isNumeric(channelNewIndex)) {
                // alert user
                Toast.makeText(ChannelSetActivity.this, R.string.ChannelNotNumeric,
                        Toast.LENGTH_SHORT).show();
                return;
            }

            int chIndex = Integer.parseInt(channelNewIndex);
            String sIndex = chIndex + "";

            // add this channel to db
            channelName = channelname.getText().toString().trim();
            //[BUGFIX]-Mod-BEGIN by TSCD.dagang.yang,08/12/2015,1060007
            if (!getResources().getBoolean(R.bool.feature_addChannelTextLimitForRU_on)) {
                if (isOutLength(channelName)) {
                    // alert user
                    Toast.makeText(ChannelSetActivity.this, R.string.ChannelNameOut, Toast.LENGTH_SHORT)
                            .show();
                    return;
                }
            }
            //[BUGFIX]-Mod-END by TSCD.dagang.yang,08/12/2015,1060007
            channelEnable = enable.isChecked() ? ENABLE : DISABLE;
            ContentValues values = new ContentValues();
            values.put(CellBroadcast.Channel.NAME, channelName);
            values.put(CellBroadcast.Channel.INDEX, sIndex);
            values.put(CellBroadcast.Channel.Enable, channelEnable);

            if (isAdd) {
                // set cb Channel start----------------------------
                // judge channel index is duplicate
                if (TelephonyManager.getDefault().isMultiSimEnabled() && (subDescription != -1)) {
                    if (cbu.queryChannelIndexA(sIndex, subDescription)) {
                        // alert user
                        Toast.makeText(ChannelSetActivity.this, R.string.ChannelExist,
                                Toast.LENGTH_SHORT).show();
                        return;
                    }
                    // add channel to db-----------------------
                    cbu.addChannel(values, subDescription);
                } else {
                    if (cbu.queryChannelIndexA(sIndex)) {
                        // alert user
                        Toast.makeText(ChannelSetActivity.this, R.string.ChannelExist,
                                Toast.LENGTH_SHORT).show();
                        return;
                    }
                    // add channel to db-----------------------
                    cbu.addChannel(values);
                }
                setResult(ADD_CHANNEL_REQUEST);

            } else {
                // judge channel index is duplicate
                if (TelephonyManager.getDefault().isMultiSimEnabled() && (subDescription != -1)) {
                    if (cbu.queryChannelIndexE(sIndex, channelId, subDescription)) {
                        // alert user
                        Toast.makeText(ChannelSetActivity.this, R.string.ChannelExist,
                                Toast.LENGTH_SHORT).show();
                        return;
                    }
                    //[BUGFIX]-Add-BEGIN by TCTNB.bo.xu,07/10/2013,PR-477859,
                    //[CB]CB list increase new channel
                    SmsManager manager = SmsManager.getSmsManagerForSubscriptionId(subDescription);
                    int index = Integer.parseInt(channelOldIndex);
                    manager.disableCellBroadcastRange(index, index, ranType);
                    //[BUGFIX]-Add-END by TCTNB.bo.xu
                    // update channel in to db
                    cbu.updateChannel(channelId, values, subDescription);
                } else {
                    if (cbu.queryChannelIndexE(sIndex, channelId)) {
                        // alert user
                        Toast.makeText(ChannelSetActivity.this, R.string.ChannelExist,
                                Toast.LENGTH_SHORT).show();
                        return;
                    }
                    //[BUGFIX]-Add-BEGIN by TCTNB.bo.xu,07/10/2013,PR-477859,
                    //[CB]CB list increase new channel
                    SmsManager manager = SmsManager.getDefault();
                    int index = Integer.parseInt(channelOldIndex);
                    manager.disableCellBroadcastRange(index, index, ranType);
                    //[BUGFIX]-Add-END by TCTNB.bo.xu
                    // update channel in to db
                    cbu.updateChannel(channelId, values);
                }

                setResult(EDIT_CHANNEL_REQUEST);
            }
            //[BUGFIX]-Add-BEGIN by TCTNB.bo.xu,06/15/2013,CR-451418,
            //Set dedicated Cell broadcast MI for Israel Programs
            //[FEATURE]-Add-BEGIN by TSCD.fujun.yang,09/12/2014,772564,new CLID variable to re-activate CB for NL
            if (TelephonyManager.getDefault().isMultiSimEnabled() && (subDescription != -1)) {
                if (subDescription == 0) {
                    channelMode = mSettings.getString("pref_key_choose_channel_sim1", "1");
                } else if (subDescription == 1) {
                    channelMode = mSettings.getString("pref_key_choose_channel_sim2", "1");
                }
            } else {
                channelMode = mSettings.getString("pref_key_choose_channel", "1");
            }
            //[FEATURE]-Add-END by TSCD.fujun.yang
            if ("1".equalsIgnoreCase(channelMode)) {
                setCBMConfig();
            }
            //[BUGFIX]-Add-END by TCTNB.bo.xu
            finish();

        }
    };

    void setCBMConfig() {
        log("setCbChannel");
        if (TelephonyManager.getDefault().isMultiSimEnabled() && (subDescription != -1)) {
            //Changed by JRD_Zhanglei for Idol4 Task1033485, Cause we just support singleSIM CB, 2015/12/07
            int defaultSmsSubID = SmsManager.getDefaultSmsSubscriptionId();
            log("defaultSmsSbuID = " + defaultSmsSubID + "; subDescription = " + subDescription);
            SmsManager manager = SmsManager.getSmsManagerForSubscriptionId(defaultSmsSubID);
            //Changed end for Task1033485
            com.android.internal.telephony.gsm.SmsBroadcastConfigInfo[] cbi = cbu.getSmsBroadcastConfigInfo(subDescription);
            if (cbi != null) {
                int num = cbi.length;
                for (int i = 0; i < num; i++) {
                    int index = cbi[i].getFromServiceId();
                    if (cbi[i].isSelected()) {
                        manager.enableCellBroadcastRange(index, index, ranType);
                    } else {
                        manager.disableCellBroadcastRange(index, index, ranType);
                    }
                }
            }
        } else {
            SmsManager manager = SmsManager.getDefault();
            com.android.internal.telephony.gsm.SmsBroadcastConfigInfo[] cbi = cbu.getSmsBroadcastConfigInfo();
            if (cbi != null) {
                int num = cbi.length;
                for (int i = 0; i < num; i++) {
                    int index = cbi[i].getFromServiceId();
                    if (cbi[i].isSelected()) {
                        manager.enableCellBroadcastRange(index, index, ranType);
                    } else {
                        manager.disableCellBroadcastRange(index, index, ranType);
                    }
                }
            }
        }
    }

    private OnClickListener mCancelListenertest = new OnClickListener() {
        public void onClick(View v) {
            setResult(RESULT_CANCELED);
            finish();
        }
    };

    // true: out of max length limit
    // false : not outof
    boolean isOutLength(String aString) {
        // aString = "This is a test string.";
        String anotherString = null;
        try {
            anotherString = new String(aString.getBytes("GBK"), "ISO8859_1");
            log("aString.length():  " + aString.length() + ", anotherString.length(): "
                    + anotherString.length());
            if (anotherString.length() > MAXLENGTH) {
                return true;
            }
        } catch (UnsupportedEncodingException ex) {
        }

        return false;
    }

    private boolean HasLog = true;// add by xielianghui

    void log(String mes) {
        if (HasLog)
            Log.d("ChannelSetActivity", mes + "\n");
    }

}
