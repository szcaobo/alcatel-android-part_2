/******************************************************************************/
/*                                                               Date:04/2013 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2013 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  Jianglong.pan                                                   */
/*  Email  :  Jianglong.pan@tcl.com                                           */
/*  Role   :                                                                  */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     :                                                                */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 04/06/2013|        bo.xu         |      FR-400302       |[SMS]Cell broadc- */
/*           |                      |                      |ast SMS support   */
/* ----------|----------------------|----------------------|----------------- */
/* 04/11/2013|     Dandan.Fang      |       FR400297       |CBC notification  */
/*           |                      |                      |with pop up and   */
/*           |                      |                      |tone alert + vib- */
/*           |                      |                      |rate in CHILE     */
/* ----------|----------------------|----------------------|----------------- */
/* 09/06/2013|     yugang.jia       |      FR-516039       |[SMS]Cell broadc- */
/*           |                      |                      |ast SMS support   */
/* ----------|----------------------|----------------------|----------------- */
/* 19/07/2016|    jianglong.pan     |SOLUTION-2520283      |Porting CMASS     */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package com.android.cellbroadcastreceiver;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;
import android.telephony.SmsMessage.MessageClass;
import android.text.TextUtils;

//import com.android.cb.util.TLog;
import com.android.cellbroadcastreceiver.CellBroadcast.Channel;

import java.util.ArrayList;
import java.util.HashMap;

public class CBSUtills {
    /** User data text encoding code unit size */
    /*
     * public static final int ENCODING_UNKNOWN = 0; public static final int
     * ENCODING_7BIT = 1; public static final int ENCODING_8BIT = 2; public
     * static final int ENCODING_16BIT = 3;
     */
    public static final int MAX_CBM_SIZE = 10;

    final int DATACODESCHEMA = 4095;

    String ENABLE = "Enable";

    String DISABLE = "Disable";

    String LOG_TAG = "CELLBROADCASTMESSAGE";

    int GeoScope;

    int MessageCode;

    int UpdateNO;

    int MessageId;

    int dataCodingScheme;

    int PageParm;

    int Language;

    Context context;

    private MessageClass messageClass;

    static HashMap<String, String> pageBuffer = new HashMap<String, String>();// cache
                                                                              // 15
                                                                              // pages

    static ArrayList<String> pageParam = new ArrayList<String>(15);// cache 15
                                                                   // pages

    // messageParam
    static ArrayList<String> getData = new ArrayList<String>();// already exist
                                                               // data after

    static String oldKey = "";

    CBSUtills(Context ct) {
        GeoScope = 0;// SerialNO
        MessageCode = 0;// SerialNO
        UpdateNO = 0;// SerialNO
        MessageId = 0;
        dataCodingScheme = 0;
        PageParm = 0;
        pdu = null;
        context = ct;

    }

    byte[] pdu;

    void log(String mes) {
        Log.d(LOG_TAG, mes + "\n");
    }

    void addChannel(ContentValues values) {
        context.getContentResolver().insert(Channel.CONTENT_URI, values);
    }

    void updateChannel(String channelId, ContentValues values) {
        Uri uri = ContentUris.withAppendedId(Channel.CONTENT_URI, Integer.parseInt(channelId));
        context.getContentResolver().update(uri, values, null, null);
    }

    void updateChannel(ContentValues values) {
        context.getContentResolver().update(Channel.CONTENT_URI, values, null, null);
    }

    void deleteChannel(String channelId) {
        Uri uri = ContentUris.withAppendedId(Channel.CONTENT_URI, Integer.parseInt(channelId));
        context.getContentResolver().delete(uri, null, null);
    }

    // delete all cb channel of db ,add by 20100208
    void deleteChannel() {
        context.getContentResolver().delete(Channel.CONTENT_URI, null, null);
    }

    Cursor queryChannel() {
        Cursor c = context.getContentResolver().query(Channel.CONTENT_URI, null, null, null,
                "_id asc");
        return c;
    }

    Cursor queryChannel(String channelId) {
        Uri uri = ContentUris.withAppendedId(Channel.CONTENT_URI, Integer.parseInt(channelId));
        Cursor c = context.getContentResolver().query(uri, null, null, null, "_id asc");
        return c;
    }

    // true: already has data
    // false: has nothing
    boolean queryChannelIndexA(String channelIndex) {
        String select = Channel.INDEX + " =" + sqlText(channelIndex);
        Cursor c = context.getContentResolver()
                .query(Channel.CONTENT_URI, null, select, null, null);
        int count = c.getCount();// add by fiona for pr84673
        c.close();// add by fiona for pr84673
        if (count > 0)
            return true;
        return false;
    }

    // true: already has data
    // false: has nothing
    boolean queryChannelIndexE(String channelIndexNew, String channelId) {
        String select = Channel.INDEX + "=" + sqlText(channelIndexNew) + " and " + Channel._ID
                + " !=" + channelId + "  ";

        Cursor c = context.getContentResolver()
                .query(Channel.CONTENT_URI, null, select, null, null);
        int count = c.getCount();// add by fiona for pr84673
        c.close();// add by fiona for pr84673
        if (count > 0)
            return true;
        return false;
    }

    String sqlText(String value) {
        return " '" + value.trim() + "' ";
    }

    com.android.internal.telephony.gsm.SmsBroadcastConfigInfo[] getSmsBroadcastConfigInfo() {
        boolean chEnableTmp;
        int chIndexTmp;
        int i = 0;
        // set channel index to mobile
        Cursor c = queryChannel();
        int CursorSize = c.getCount();
        com.android.internal.telephony.gsm.SmsBroadcastConfigInfo[] cbi;
        cbi = new com.android.internal.telephony.gsm.SmsBroadcastConfigInfo[CursorSize];
        if (CursorSize > 0) {
            c.moveToFirst();
            while (!c.isAfterLast()) {
                chIndexTmp = Integer.parseInt(c.getString(c.getColumnIndex(Channel.INDEX)));
                chEnableTmp = (c.getString(c.getColumnIndex(Channel.Enable)))
                        .equalsIgnoreCase(ENABLE) ? true : false;
                cbi[i] = new com.android.internal.telephony.gsm.SmsBroadcastConfigInfo(chIndexTmp, chIndexTmp, DATACODESCHEMA,
                        DATACODESCHEMA, chEnableTmp);
                i++;
                c.moveToNext();
            }
        }
        c.close();
        return cbi;
    }

    // save cb language to db
    void saveCBLanguage(String ret) {
        context.getContentResolver().delete(CellBroadcast.CBLanguage.CONTENT_URI, null, null);
        ContentValues values;
        values = new ContentValues();
        values.put(CellBroadcast.CBLanguage.CBLANGUAGE, ret);
        context.getContentResolver().insert(CellBroadcast.CBLanguage.CONTENT_URI, values);
        Log.d(LOG_TAG,"saveCBLanguage-value : "+values);
    }

    // get cb language
    int queryCBLanguage() {
        //[FEATURE]-begin-MOD by TCTNB.yugang.jia,09/06/2013,FR-516039,
        int cblanguage = 0;

        Cursor c = context.getContentResolver().query(CellBroadcast.CBLanguage.CONTENT_URI, null,
                null, null, null);
        if (c.getCount() > 0) {
            c.moveToFirst();
            cblanguage = Integer.parseInt(c.getString(c
                    .getColumnIndex(CellBroadcast.CBLanguage.CBLANGUAGE)));
        }
        c.close();
        return cblanguage;
    }

    //[FEATURE]-Add-BEGIN by TCTNB.Dandan.Fang,04/11/2013,FR400297,
    //CBC notification with pop up and tone alert + vibrate in CHILE
    //SMSCB channels should not be customizable or editable by the end user.
    boolean isForbiddenToModifyPredefinedChannels(String channelId) {
        //[FEATURE]-Mod-BEGIN by TCTNB.Tongyuan.Lv, 07/08/2013, FR-482850, add customized cell broadcast channel
        /*boolean isForbidden = context.getResources().getBoolean(
                R.bool.feature_cellbroadcastreceiver_forbiddenModifyPredefinedChannels_on);
        if (isForbidden) {*/
            /*
             * because we use the def_cellbroadcastreceiver_customized_channel_names and
             * def_cellbroadcastreceiver_customized_channel_numbers instead of
             * def_cellbroadcastreceiver_chn?name, and def_cellbroadcastreceiver_chn?number
             * to define predefined customized cell broadcast channel names and numbers,
             * so need to make a update here.
             */
            /*int channel1 = context.getResources().getInteger(
                    R.integer.def_cellbroadcastreceiver_chn1number);
            int channel2 = context.getResources().getInteger(
                    R.integer.def_cellbroadcastreceiver_chn2number);*/


            try {
                Cursor c = queryChannel(channelId);
                int currentIndex = -1;
                if (c != null && c.moveToFirst()) {
                    currentIndex = Integer.valueOf(c.getString(c.getColumnIndex(Channel.INDEX)));
                }
                c.close();
                /*if (currentIndex != 0 && (currentIndex == channel1 || currentIndex == channel2)) {
                    return true;
                }*/
                String persoChannelNumbersAndPolicy = context.getResources().getString(
                        R.string.def_cellbroadcastreceiver_customized_channel_numbers_policy);

                if (TextUtils.isEmpty(persoChannelNumbersAndPolicy.trim())) {
                    Log.i("CellBroadcastReceiver", "empty customized channel, no forbidden channel");
                    return false;
                }
                String[] channelNumbersAndPolicy = persoChannelNumbersAndPolicy.split(";");
                int len = channelNumbersAndPolicy.length;

                for (int i = 0; i < len; i++) {
                    String[] channelInfo = channelNumbersAndPolicy[i].split(",");
                    if (channelInfo.length == 2) {
                        String channelNumber = channelInfo[0].trim();
                        String channelPolicy = channelInfo[1].trim();
                        if (!TextUtils.isEmpty(channelNumber)
                                && currentIndex == Integer.parseInt(channelNumber)
                                && (channelPolicy.equals("-")||channelPolicy.equals("*"))) {//[Defect 1266096]-Mod-by gang-chen@tcl.com 2015/1/3
                            Log.i("CellBroadcastReceiver", "get matched forbidden edit channel " + channelNumber);
                            return true;
                        }
                    } else {
                        Log.e("CellBroadcastReceiver", "detected wrong number and policy, please check the perso settings");
                    }
                }
                return false;
            } catch (NumberFormatException e) {
                Log.e("CELLBROADCAST-CBMSetting", "NumberFormatException:" + e.toString());
                return false;
            }
        //}
        //return false;
        //[FEATURE]-Mod-END by TCTNB.Tongyuan.Lv
    }
   //[FEATURE]-Add-END by TCTNB.Dandan.Fang


    int queryCBLanguage(int subid) {
        //[FEATURE]-begin-MOD by TCTNB.yugang.jia,09/06/2013,FR-516039,
        int cblanguage = 0;
        Log.i(LOG_TAG,"queryCBLanguage(int subid)"+ Uri.withAppendedPath(CellBroadcast.CBLanguage.CONTENT_URI, "sub" + subid));
        Cursor c = context.getContentResolver().query(Uri.withAppendedPath(CellBroadcast.CBLanguage.CONTENT_URI, "sub" + subid), null,
                null, null, null);
        if (c.getCount() > 0) {
            c.moveToFirst();
            cblanguage = Integer.parseInt(c.getString(c
                    .getColumnIndex(CellBroadcast.CBLanguage.CBLANGUAGE)));
        }
        c.close();
        Log.d(LOG_TAG,"queryCBLanguage-cblanguage : "+cblanguage);
        return cblanguage;
    }
    Cursor queryChannel(int subid) {
        Log.i(LOG_TAG,"queryChannel(int subid)="+ Uri.withAppendedPath(Channel.CONTENT_URI, "sub" + subid));
        Cursor c = context.getContentResolver().query(Uri.withAppendedPath(Channel.CONTENT_URI, "sub" + subid), null, null, null,
                "_id asc");
        return c;
    }
    void saveCBLanguage(String ret,int subid) {
        Log.i(LOG_TAG,"saveCBLanguage(String ret,int subid)="+ Uri.withAppendedPath(CellBroadcast.CBLanguage.CONTENT_URI, "sub" + subid));
        context.getContentResolver().delete(Uri.withAppendedPath(CellBroadcast.CBLanguage.CONTENT_URI, "sub" + subid), null, null);
        ContentValues values;
        values = new ContentValues();
        values.put(CellBroadcast.CBLanguage.CBLANGUAGE, ret);
        context.getContentResolver().insert(Uri.withAppendedPath(CellBroadcast.CBLanguage.CONTENT_URI, "sub" + subid), values);
    }
    Cursor queryChannel(String channelId,long subid) {
        Log.i(LOG_TAG,"queryChannel(String channelId,int subid)="+ Uri.withAppendedPath(Channel.CONTENT_URI, "sub" + subid));
        Uri uri = ContentUris.withAppendedId(Uri.withAppendedPath(Channel.CONTENT_URI, "sub" + subid), Integer.parseInt(channelId));
        Cursor c = context.getContentResolver().query(uri, null, null, null, "_id asc");
        return c;
    }
    //[FEATURE]-Add-BEGIN by TCTNB.Dandan.Fang,04/11/2013,FR400297,
    //CBC notification with pop up and tone alert + vibrate in CHILE
    //SMSCB channels should not be customizable or editable by the end user.
    boolean isForbiddenToModifyPredefinedChannels(String channelId,int subid) {
        //[FEATURE]-Mod-BEGIN by TCTNB.Tongyuan.Lv, 07/08/2013, FR-482850, add customized cell broadcast channel
        /*boolean isForbidden = context.getResources().getBoolean(
                R.bool.feature_cellbroadcastreceiver_forbiddenModifyPredefinedChannels_on);
        if (isForbidden) {*/
            /*
             * because we use the def_cellbroadcastreceiver_customized_channel_names and
             * def_cellbroadcastreceiver_customized_channel_numbers instead of 
             * def_cellbroadcastreceiver_chn?name, and def_cellbroadcastreceiver_chn?number
             * to define predefined customized cell broadcast channel names and numbers, 
             * so need to make a update here.
             */
            /*int channel1 = context.getResources().getInteger(
                    R.integer.def_cellbroadcastreceiver_chn1number);
            int channel2 = context.getResources().getInteger(
                    R.integer.def_cellbroadcastreceiver_chn2number);*/


            try {
                Log.i(LOG_TAG,"isForbiddenToModifyPredefinedChannels-channelId="+channelId+" subid="+subid);
                Cursor c = queryChannel(channelId,subid);
                int currentIndex = -1;
                if (c != null && c.moveToFirst()) {
                    currentIndex = Integer.valueOf(c.getString(c.getColumnIndex(Channel.INDEX)));
                }
                c.close();
                /*if (currentIndex != 0 && (currentIndex == channel1 || currentIndex == channel2)) {
                    return true;
                }*/
                String persoChannelNumbersAndPolicy = context.getResources().getString(
                        R.string.def_cellbroadcastreceiver_customized_channel_numbers_policy);

                if (TextUtils.isEmpty(persoChannelNumbersAndPolicy.trim())) {
                    Log.i("CellBroadcastReceiver", "empty customized channel, no forbidden channel");
                    return false;
                }
                String[] channelNumbersAndPolicy = persoChannelNumbersAndPolicy.split(";");
                int len = channelNumbersAndPolicy.length;

                for (int i = 0; i < len; i++) {
                    String[] channelInfo = channelNumbersAndPolicy[i].split(",");
                    if (channelInfo.length == 2) {
                        String channelNumber = channelInfo[0].trim();
                        String channelPolicy = channelInfo[1].trim();
                        if (!TextUtils.isEmpty(channelNumber)
                                && currentIndex == Integer.parseInt(channelNumber)
                                && (channelPolicy.equals("-")||channelPolicy.equals("*"))) {//[Defect 1266096]-Mod-by gang-chen@tcl.com 2015/1/3
                            Log.i("CellBroadcastReceiver", "get matched forbidden edit channel " + channelNumber);
                            return true;
                        }
                    } else {
                        Log.e("CellBroadcastReceiver", "detected wrong number and policy, please check the perso settings");
                    }
                }
                return false;
            } catch (NumberFormatException e) {
                Log.e("CELLBROADCAST-CBMSetting", "NumberFormatException:" + e.toString());
                return false;
            }
        //}
        //return false;
        //[FEATURE]-Mod-END by TCTNB.Tongyuan.Lv
    }
    void updateChannel(String channelId, ContentValues values,int subid) {
        Log.i(LOG_TAG,"updateChannel(String channelId, ContentValues values,int subid)="+ Uri.withAppendedPath(Channel.CONTENT_URI, "sub" + subid));;
        Uri uri = ContentUris.withAppendedId(Uri.withAppendedPath(Channel.CONTENT_URI, "sub" + subid), Integer.parseInt(channelId));
        context.getContentResolver().update(uri, values, null, null);
    }
    void deleteChannel(String channelId,int subid) {
        Log.i(LOG_TAG,"deleteChannel(String channelId,int subid)="+ Uri.withAppendedPath(Channel.CONTENT_URI, "sub" + subid));
        Uri uri = ContentUris.withAppendedId(Uri.withAppendedPath(Channel.CONTENT_URI, "sub" + subid), Integer.parseInt(channelId));
        context.getContentResolver().delete(uri, null, null);
    }
    com.android.internal.telephony.gsm.SmsBroadcastConfigInfo[] getSmsBroadcastConfigInfo(int subid) {
        boolean chEnableTmp;
        int chIndexTmp;
        int i = 0;
        // set channel index to mobile
        Log.i(LOG_TAG,"getSmsBroadcastConfigInfo(int sub)-subid="+subid);
        Cursor c = queryChannel(subid);
        int CursorSize = c.getCount();
        com.android.internal.telephony.gsm.SmsBroadcastConfigInfo[] cbi;
        cbi = new com.android.internal.telephony.gsm.SmsBroadcastConfigInfo[CursorSize];
        if (CursorSize > 0) {
            c.moveToFirst();
            while (!c.isAfterLast()) {
                chIndexTmp = Integer.parseInt(c.getString(c.getColumnIndex(Channel.INDEX)));
                chEnableTmp = (c.getString(c.getColumnIndex(Channel.Enable)))
                        .equalsIgnoreCase(ENABLE) ? true : false;
                cbi[i] = new com.android.internal.telephony.gsm.SmsBroadcastConfigInfo(chIndexTmp, chIndexTmp, DATACODESCHEMA,
                        DATACODESCHEMA, chEnableTmp);
                i++;
                c.moveToNext();
            }
        }
        c.close();
        return cbi;
    }
    void addChannel(ContentValues values,int subid) {
        Log.i(LOG_TAG,"addChannel(ContentValues values,int subid)="+ Uri.withAppendedPath(Channel.CONTENT_URI, "sub" + subid));
        context.getContentResolver().insert(Uri.withAppendedPath(Channel.CONTENT_URI, "sub" + subid), values);
    }
    // true: already has data
    // false: has nothing
    boolean queryChannelIndexA(String channelIndex,int subid) {
        String select = Channel.INDEX + " =" + sqlText(channelIndex);
        Log.i(LOG_TAG,"queryChannelIndexA(String channelIndex,int subid)="+ Uri.withAppendedPath(Channel.CONTENT_URI, "sub" + subid));
        Cursor c = context.getContentResolver()
                .query(Uri.withAppendedPath(Channel.CONTENT_URI, "sub" + subid), null, select, null, null);
        int count = c.getCount();// add by fiona for pr84673
        c.close();// add by fiona for pr84673
        if (count > 0)
            return true;
        return false;
    }
    // true: already has data
    // false: has nothing
    boolean queryChannelIndexE(String channelIndexNew, String channelId,int subid) {
        String select = Channel.INDEX + "=" + sqlText(channelIndexNew) + " and " + Channel._ID
                + " !=" + channelId + "  ";

        Log.i(LOG_TAG,"queryChannelIndexE(String channelIndexNew, String channelId,int subid)="+ Uri.withAppendedPath(Channel.CONTENT_URI, "sub" + subid));
        Cursor c = context.getContentResolver()
                .query(Uri.withAppendedPath(Channel.CONTENT_URI, "sub" + subid), null, select, null, null);
        int count = c.getCount();// add by fiona for pr84673
        c.close();// add by fiona for pr84673
        if (count > 0)
            return true;
        return false;
    }
}
