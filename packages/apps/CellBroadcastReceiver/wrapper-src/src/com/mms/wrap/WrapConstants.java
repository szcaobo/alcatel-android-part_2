package com.mms.wrap;


class WrapConstants {
    public static final String TAG = "wrap-mms";

    //define platform constants
    public static final String PLATFORM_QCOM_NAME = "qcom";
    public static final String PLATFORM_MTK_NAME = "mtk";

    //define project constants
    public static final String PROJECT_ALTO5_NAME = "alto5";
    public static final String PROJECT_ALTO4_NAME = "alto4";

    //define common platform and project plf key
    //public static final String PLATFORM_PLF_KEY = "def_platform_name";
    public static final String PROJECT_PLF_KEY = "ro.tct.product";

    //define tct plf key
    public static final String TCT_PLF_KEY_SMS_VALIDPERIOD_ON = "feature_tctfw_smsValidPeriod_on";
    public static final String TCT_PLF_KEY_WAPPUSHMESSAGE_SETTINGMENU_ON = "feature_wappushmessage_settingmenu_on";


    public enum Platform {
        QCOM(PLATFORM_QCOM_NAME),
        MTK(PLATFORM_MTK_NAME);

        private Platform(String name) {
            mName = name;
        }

        private String mName;

        public String getName() {
            return mName;
        }
    }

    public enum Project {
        ALTO5(PROJECT_ALTO5_NAME),
        ALTO4(PROJECT_ALTO4_NAME);

        private Project(String name) {
            mName = name;
        }

        private String mName;

        public String getName() {
            return mName;
        }
    }

}
