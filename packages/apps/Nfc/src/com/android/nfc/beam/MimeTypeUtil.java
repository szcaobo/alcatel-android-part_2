/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.nfc.beam;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.webkit.MimeTypeMap;
import android.text.TextUtils; // MODIFIED by na.long, 2016-08-04,BUG-2670150

public final class MimeTypeUtil {

    private static final String TAG = "MimeTypeUtil";

    private MimeTypeUtil() {}

    public static String getMimeTypeForUri(Context context, Uri uri) {
        if (uri.getScheme() == null) return null;

        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            ContentResolver cr = context.getContentResolver();
            return cr.getType(uri);
        } else if (uri.getScheme().equals(ContentResolver.SCHEME_FILE)) {
            String extension = MimeTypeMap.getFileExtensionFromUrl(uri.getPath()).toLowerCase();
            /* MODIFIED-BEGIN by na.long, 2016-08-04,BUG-2670150*/
            if (TextUtils.isEmpty(extension)) {
                // getMimeTypeFromExtension() doesn't handle spaces in filenames nor can it handle
                // urlEncoded strings. Let's try one last time at finding the extension.
                int dotPos = uri.getPath().lastIndexOf('.');
                if (0 <= dotPos) {
                    extension = uri.getPath().substring(dotPos + 1);
                }
            }
            if (extension != null) {
                //String mimetype = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
                String mimetype = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension.toLowerCase());
                if (TextUtils.isEmpty(mimetype)) {
                    // Finally, if original mimetype is null, synthesize
                    mimetype = "application/" + extension;
                }
                return mimetype;
                /* MODIFIED-END by na.long,BUG-2670150*/
            } else {
                return null;
            }
        } else {
            Log.d(TAG, "Could not determine mime type for Uri " + uri);
            return null;
        }
    }
}
