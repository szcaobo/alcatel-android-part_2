/* Copyright (C) 2016 Tcl Corporation Limited */
#ifndef _MEDIA_LOGS_H_
#define _MEDIA_LOGS_H_

#include <inttypes.h>
#include <utils/Log.h>


/*
 * Change logging-level at runtime with "persist.debug.media.logs.level"
 *
 * level     MediaLOGV      MediaLOGD
 * ----------------------------------
 * 0         silent          silent
 * 1         silent          printed
 * 2         printed         printed
 *
 * MediaLOGI/W/E are printed always
 */

extern uint32_t gMediaLogLevel;

#ifdef TCT_DEBUG_MEDIALOG

#define MediaLOGE(format, args...) ALOGE_IF((gMediaLogLevel > 4), format, ##args)
#define MediaLOGV(format, args...) ALOGE_IF((gMediaLogLevel > 3), format, ##args)
#define MediaLOGD(format, args...) ALOGD_IF((gMediaLogLevel > 2), format, ##args)
#define MediaLOGW(format, args...) ALOGW_IF((gMediaLogLevel > 1), format, ##args)
#define MediaLOGI(format, args...) ALOGI_IF((gMediaLogLevel > 0), format, ##args)

#else

#define MediaLOGE(format, args...) ALOGE(format, ##args)
#define MediaLOGV(format, args...) ALOGV(format, ##args)
#define MediaLOGD(format, args...) ALOGD(format, ##args)
#define MediaLOGW(format, args...) ALOGW(format, ##args)
#define MediaLOGI(format, args...) ALOGI(format, ##args)

#endif


void updateMediaLogLevel();


#endif // _MEDIA_LOGS_H_

