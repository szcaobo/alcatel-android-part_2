# Copyright (C) 2016 Tcl Corporation Limited
LOCAL_PATH := $(my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE := libmedialog

LOCAL_SRC_FILES := \
        medialog.c

LOCAL_SHARED_LIBRARIES := \
        libcutils \
        liblog

LOCAL_CFLAGS := -Werror

include $(BUILD_SHARED_LIBRARY)

include $(call first-makefiles-under,$(LOCAL_PATH))
