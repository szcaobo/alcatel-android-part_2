/* Copyright (C) 2016 Tcl Corporation Limited */
#ifndef _MEDIA_LOGS_H_
#define _MEDIA_LOGS_H_

#include <stdlib.h>
#include <utils/Log.h>
#include <cutils/properties.h>

#include "medialog/medialog.h"


uint32_t gMediaLogLevel;

void updateMediaLogLevel() {


    #ifdef TCT_DEBUG_MEDIALOG
    char level[PROPERTY_VALUE_MAX];
    property_get("persist.debug.media.logs.level", level, "0");
    gMediaLogLevel = atoi(level);
    #endif
}

#endif // _MEDIA_LOGS_H_
