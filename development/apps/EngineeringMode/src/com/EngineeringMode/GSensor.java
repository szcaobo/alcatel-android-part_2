/******************************************************************************/
/*                                                               Date:09/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Qianbo Pan                                                        */
/* E-Mail:  Qianbo.Pan@tcl-mobile.com                                         */
/* Role  :  MMITest                                                           */
/* Reference documents :  Scribe 5_MMI_AUTO_Test_specfication_V3.0.Doc        */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/EngineeringMode/src/com/EngineeringMode/       */
/*                                                          GSensorTest.java  */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 12/09/12| Qianbo.Pan     |                    | Porting for EngineeringMode*/
/*         |                |                    |                            */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.EngineeringMode;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.TctLog;
import android.view.View;

class GSensorTest extends Test implements SensorEventListener {

    TestLayout1 tl;
    String mDisplayString;

    private enum position {
        UNDEF, UP, DOWN, LEFT, RIGHT, FACE_UP, FACE_DOWN
    };

    private int POS_BIT_UP = 0x1;
    private int POS_BIT_DOWN = 0x2;
    private int POS_BIT_LEFT = 0x4;
    private int POS_BIT_RIGHT = 0x8;
    private int POS_BIT_FACE_DOWN = 0x10;
    private int POS_BIT_FACE_UP = 0x20;
    private int POS_BIT_ALL = POS_BIT_UP | POS_BIT_DOWN | POS_BIT_LEFT
            | POS_BIT_RIGHT | POS_BIT_FACE_DOWN | POS_BIT_FACE_UP;

    private SensorManager mSensorManager;
    // private float[] mValues;
    private String TAG = "GSensor";
    private int mCount = 0;
    private position mPosition = position.UNDEF;
    int mPositionChecked = 0;

    private Sensor gsensor;

    Bitmap mBitmap;

    GSensorTest(ID pid, String s) {
        super(pid, s);
        TAG = Test.TAG + TAG;

    }

    GSensorTest(ID pid, String s, int timein) {
        super(pid, s, timein, 0);
        TAG = Test.TAG + TAG;

    }

    @Override
    protected void Run() {
        // this function executes the test
        switch (mState) {
        case INIT: // init the test, shows the first screen

            if (mSensorManager == null) {
                mSensorManager = (SensorManager) mContext
                        .getSystemService(Context.SENSOR_SERVICE);
            }
            // first screen is to check UP position only (for AUTO mode )
            mPositionChecked = POS_BIT_ALL & ~POS_BIT_UP;

            gsensor = mSensorManager
                    .getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

            if (gsensor != null) {
                TctLog.i(TAG, "GSensor opened : " + gsensor.getName());
                if (!mSensorManager.registerListener(this, gsensor,
                        SensorManager.SENSOR_DELAY_NORMAL)) {
                    TctLog.e(TAG,
                            "register listener for sensor " + gsensor.getName()
                                    + " failed");
                }
            } else {
                tl = new TestLayout1(mContext, mName, "No Sensor found");
                mContext.setContentView(tl.ll);
            }

            tl = new TestLayout1(mContext, mName, (View) new PositionView(
                    mContext));
            // tl.setAutoTestButtons(false);
            mContext.setContentView(tl.ll);

            // if (MMITest.mode == MMITest.AUTO_MODE){
            // SetTimer(35000, new CallBack (){
            // public void c(){
            // mState = TIMEOUT;
            // Run();
            // }
            // });
            // }
            mState++;

            break;

        case INIT + 1:
            // check UP is ok : reset all other positions status
            // TODO : in case of AUTO test, stop here
            if (mPositionChecked == POS_BIT_ALL) {
                mPositionChecked = POS_BIT_UP | POS_BIT_FACE_UP;// don't check
                                                                // face UP
                                                                // position

                // if (MMITest.mode == MMITest.AUTO_MODE){
                // StopTimer();
                // }

                mState++;
                /*
                 * trigger a new screen refresh as the display is only updated
                 * when a position change has been detected
                 */
                SetTimer(500, new CallBack() {
                    public void c() {
                        Run();
                    }
                });

            }

            break;

        case TIMEOUT:
            tl = new TestLayout1(mContext, mName,
                    "TIMEOUT : Gsensor test failed");
            // tl.setAutoTestButtons(true,false);
            mContext.setContentView(tl.ll);

            break;

        case END://
            mSensorManager.unregisterListener(this, gsensor);
            TctLog.d(TAG, "gsensor listener unregistered");

            break;

        default:
            if (mPositionChecked != POS_BIT_ALL) {
                tl = new TestLayout1(mContext, mName, (View) new PositionView(
                        mContext));
                // tl.setAutoTestButtons(true,false);
                mContext.setContentView(tl.ll);
            } else {
                /* all positions checked */
                StopTimer();
                tl = new TestLayout1(mContext, mName, "all positions checked");
                mContext.setContentView(tl.ll);
                mState = END;
            }

            break;
        }

    }

    class DDDVect {
        float Vx;
        float Vy;
        float Vz;

        DDDVect(float x, float y, float z) {
            Vx = x;
            Vy = y;
            Vz = z;
        }

        public float getYAngle() {
            return getAngle(Vy);
        }

        public float getXAngle() {
            return getAngle(Vx);
        }

        public float getZAngle() {
            return getAngle(Vz);
        }

        private float getAngle(float ref) {
            return (float) Math.toDegrees(Math.acos(ref
                    / Math.sqrt(Vx * Vx + Vy * Vy + Vz * Vz)));
        }

    }

    public void onSensorChanged(SensorEvent event) {

        float ThresholdHigh = (float) 8.0;
        float ThresholdLow = (float) 2.0;
        position mOldPosition = mPosition;

        TctLog.d(TAG, "onSensorChanged: (" + event.values[0] + ", "
                + event.values[1] + ", " + event.values[2] + ")");
        // mValues = event.values;

        DDDVect mDir = new DDDVect(event.values[0], event.values[1],
                event.values[2]);

        float mAngleToYaxis = mDir.getYAngle();
        float mAngleToXaxis = mDir.getXAngle();
        float mAngleToZaxis = mDir.getZAngle();

        mCount++;
        if (mAngleToYaxis < 10) {
            mPosition = position.UP;
            mPositionChecked |= POS_BIT_UP;
        } else if (mAngleToYaxis > 170) {
            mPosition = position.DOWN;
            mPositionChecked |= POS_BIT_DOWN;
        } else if (mAngleToXaxis < 10) {
            mPosition = position.LEFT;
            mPositionChecked |= POS_BIT_LEFT;
        } else if (mAngleToXaxis > 170) {
            mPosition = position.RIGHT;
            mPositionChecked |= POS_BIT_RIGHT;
        } else if (mAngleToZaxis > 170) {
            mPosition = position.FACE_DOWN;
            mPositionChecked |= POS_BIT_FACE_DOWN;
        } else if (mAngleToZaxis < 10) {
            mPosition = position.FACE_UP;
            mPositionChecked |= POS_BIT_FACE_UP;
        } else {
            mPosition = position.UNDEF;
        }

        if (mOldPosition != mPosition) {
            TctLog.d(TAG, "handest position changed :" + mPosition
                    + " (checked = " + mPositionChecked + ")");
            TctLog.d(TAG, "V : " + mAngleToYaxis + " deg , H : " + mAngleToXaxis
                    + " deg");
            Run();
        }

        TctLog.d(TAG, "V : " + mAngleToYaxis + " deg , H : " + mAngleToXaxis
                + " deg");

    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // TODO Auto-generated method stub
        // TctLog.i(TAG,"sensor"+sensor.getName()+" accuracy changed :"+accuracy);
    }

    private class PositionView extends View {
        private Paint mPaint = new Paint();

        public PositionView(Context context) {
            super(context);

            mBitmap = android.graphics.BitmapFactory.decodeResource(
                    getResources(), R.drawable.phone);
        }

        @Override
        protected void onDraw(Canvas canvas) {
            Paint paint = mPaint;

            canvas.drawColor(Color.WHITE);
            int length = 60, offset = 30, border = 10;

            paint.setAntiAlias(true);
            paint.setColor(Color.BLACK);
            paint.setStyle(Paint.Style.FILL);
            paint.setStrokeWidth(4);

            int w = canvas.getWidth();
            int h = canvas.getHeight();
            int cx = w / 2;
            int cy = h / 2 - 40;

            canvas.translate(cx, cy);

            /* UP LINE */
            if ((mPositionChecked & POS_BIT_UP) == 0) {
                canvas.drawLine(0, -offset, 0, -(length + offset), paint);
                canvas.drawLine(-border, -(offset + length - border), 0,
                        -(length + offset), paint);
                canvas.drawLine(border, -(offset + length - border), 0,
                        -(length + offset), paint);
                Matrix m = new Matrix();
                m.setTranslate(10, -length);
                paint.setColor(Color.BLUE);
                canvas.drawBitmap(mBitmap, m, paint);

                // canvas.drawBitmap(mBitmap, border , -length + border, null);
                canvas.drawText("put the handset face up and towards up",
                        -Lcd.width() / 2 + border, 0, paint);
            }

            if ((mPositionChecked & POS_BIT_FACE_DOWN) == 0) {
                canvas.drawLine(0, -offset, 0, -(length / 2 + offset), paint);
                canvas.drawLine(-border, -(offset + border), 0, -(offset),
                        paint);
                canvas.drawLine(border, -(offset + border), 0, -(offset), paint);
                // canvas.drawBitmap(mBitmap, border , -length + border, null);
                canvas.drawText("face down ", 0, -offset - border, paint);

                Rect r = new Rect(border, -length, border + mBitmap.getWidth(),
                        -length + 5);
                canvas.drawRect(r, paint);

            }
            /* DOWN LINE */
            if ((mPositionChecked & POS_BIT_DOWN) == 0) {
                Matrix m = new Matrix();
                m.setTranslate(10, -length);
                m.postRotate(180);
                canvas.drawLine(0, offset, 0, length + offset, paint);
                canvas.drawLine(-border, (offset + length - border), 0, length
                        + offset, paint);
                canvas.drawLine(border, (offset + length - border), 0, length
                        + offset, paint);
                canvas.drawText("DOWN", 20, length, paint);
                canvas.drawBitmap(mBitmap, m, null);

            }

            /* LEFT LINE */
            if ((mPositionChecked & POS_BIT_LEFT) == 0) {
                Matrix m = new Matrix();
                m.setTranslate(border, -length);
                m.postRotate(270);

                canvas.drawBitmap(mBitmap, m, paint);

                canvas.drawLine(-offset, 0, -(length + offset), 0, paint);
                canvas.drawLine(-(offset + length - border), -border,
                        -(length + offset), 0, paint);
                canvas.drawLine(-(offset + length - border), border,
                        -(length + offset), 0, paint);
                canvas.drawText("LEFT", -length, 20, paint);
                canvas.drawBitmap(mBitmap, m, null);

            }

            /* RIGHT LINE */
            if ((mPositionChecked & POS_BIT_RIGHT) == 0) {

                Matrix m = new Matrix();
                m.setTranslate(-mBitmap.getWidth() - border, -length);
                m.postRotate(90);

                canvas.drawBitmap(mBitmap, m, paint);

                canvas.drawLine(offset, 0, (length + offset), 0, paint);
                canvas.drawLine((offset + length - border), -border,
                        (length + offset), 0, paint);
                canvas.drawLine((offset + length - border), border,
                        (length + offset), 0, paint);
                canvas.drawText("RIGHT", length, 20, paint);
            }

        }

        @Override
        protected void onAttachedToWindow() {
            super.onAttachedToWindow();
        }

        @Override
        protected void onDetachedFromWindow() {
            super.onDetachedFromWindow();
        }
    }

    @Override
    protected void onTimeInFinished() {
        // if(tl!=null)
        // tl.setAutoTestButtons(true);
    }

}
