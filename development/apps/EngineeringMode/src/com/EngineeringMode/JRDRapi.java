/******************************************************************************/
/*                                                               Date:09/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Qianbo Pan                                                        */
/* E-Mail:  Qianbo.Pan@tcl-mobile.com                                         */
/* Role  :  MMITest                                                           */
/* Reference documents :  Scribe 5_MMI_AUTO_Test_specfication_V3.0.Doc        */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/EngineeringMode/src/com/EngineeringMode/       */
/*                                                              JRDRapi.java  */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 12/09/12| Qianbo.Pan     |                    | Porting for EngineeringMode*/
/* 08/10/12| Qianbo.Pan     |                    | change antenna test api    */
/*         |                |                    |                            */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.EngineeringMode;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PipedOutputStream;
import java.nio.channels.Pipe;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.util.TctLog;
import android.content.Context;
import android.os.SystemClock;
import android.util.TctLog;

public class JRDRapi {

    private final String TAG = "JRDRapi";

    public final static boolean DEBUG = true;
    // add by dingtang
    public static int last = 0;

    private final short OEM_RAPI_CLIENT_EVENT_NONE = 0;
    /* -------------------------------- */

    /* Enumerate OEM client events here : max is 0xFF !! */
    private final short OEM_RAPI_CLIENT_EVENT_GET_MODEM_VERSION = 16;
    private final short OEM_RAPI_CLIENT_AUDIO_LOOP_TEST = 17;
    private final short OEM_RAPI_CLIENT_SYS_OPRT_MODE_SET_MODE = 18;
    private final short OEM_RAPI_CLIENT_SYS_OPRT_MODE_GET_MODE = 19;
    // Primary path: ON, DRx: OFF, NV3851=0
    private final short OEM_RAPI_CLIENT_EVENT_RF_PRI_ON_DRX_OFF = 20;
    // Primary path: OFF, DRX: ON, NV3851=6
    private final short OEM_RAPI_CLIENT_EVENT_RF_PRI_OFF_DRX_ON = 21;
    // default value, Primay&DRX: All ON, NV3851=3,
    private final short OEM_RAPI_CLIENT_EVENT_RF_PRI_ON_DRX_ON = 22;
    /* -------------------------------- */

    public enum OprtMode {

        SYS_OPRT_MODE_NONE(-1),
        /** < FOR INTERNAL USE OF CM ONLY! */

        SYS_OPRT_MODE_PWROFF,
        /** < Phone is powering off */

        SYS_OPRT_MODE_FTM,
        /** < Phone is in factory test mode */

        SYS_OPRT_MODE_OFFLINE,
        /** < Phone is offline */

        SYS_OPRT_MODE_OFFLINE_AMPS,
        /** < Phone is offline analog */

        SYS_OPRT_MODE_OFFLINE_CDMA,
        /** < Phone is offline cdma */

        SYS_OPRT_MODE_ONLINE,
        /** < Phone is online */

        SYS_OPRT_MODE_LPM,
        /** < Phone is in LPM - Low Power Mode */

        SYS_OPRT_MODE_RESET,
        /** < Phone is resetting - i.e. power-cycling */

        SYS_OPRT_MODE_NET_TEST_GW,
        /** < Phone is conducting network test for GSM/WCDMA. */
        /** < This mode can NOT be set by the clients. It can */
        /** < only be set by the lower layers of the stack. */

        SYS_OPRT_MODE_OFFLINE_IF_NOT_FTM,
        /** < This mode is only set when there is offline */
        /** < request during powerup. */
        /** < This mode can not be set by the clients. It can */
        /** < only be set by task mode controller. */

        SYS_OPRT_MODE_PSEUDO_ONLINE,
        /** < Phone is pseudo online; tx disabled */

        SYS_OPRT_MODE_RESET_MODEM,
        /** < Phone is resetting the modem processor. */

        SYS_OPRT_MODE_MAX;
        /** < FOR INTERNAL USE OF CM ONLY! */

        private int value;
        private int last = 0;

        OprtMode(int val) {
            value = val;
            JRDRapi.last = val;
        }

        OprtMode() {
            JRDRapi.last++;
            value = JRDRapi.last;
        }

        public int getVal() {
            return value;
        }

        public static String valToString(int val) {
            for (OprtMode o : OprtMode.values())
                if (o.getVal() == val)
                    return o.toString().split("SYS_OPRT_MODE_")[1];

            return null;
        }

        public String print() {
            return this + " val = " + value + " ord = " + this.ordinal();
        }

    }

    /* snd devices */
    private final short SND_DEVICE_DEFAULT = 0;
    private final short SND_DEVICE_HANDSET = SND_DEVICE_DEFAULT + 0;
    private final short SND_DEVICE_HEADSET = SND_DEVICE_DEFAULT + 2; /*
                                                                      * Mono
                                                                      * headset
                                                                      */
    private final short SND_DEVICE_STEREO_HEADSET = SND_DEVICE_DEFAULT + 3; /*
                                                                             * Stereo
                                                                             * headset
                                                                             */
    private final short SND_DEVICE_IN_S_SADC_OUT_HANDSET = SND_DEVICE_DEFAULT + 16; /*
                                                                                     * Dual
                                                                                     * Mic
                                                                                     * handset
                                                                                     */

    private final short LOOP_ON = 1;
    private final short LOOP_OFF = 0;

    private final short MUTE = 1;
    private final short UNMUTE = 0;

    private String mPipeName = null;
    private File mPipeFile = null;

    public JRDRapi(Context c) throws FileNotFoundException {

        try {
            mPipeName = c.getFileStreamPath("fifo").getAbsolutePath();
            // mPipeOutName = c.getFileStreamPath("fifo_out").getAbsolutePath();

            // FIXME: temporary solution.
            // workaround for root permission issue.
            mPipeName = "/data/data/com.EngineeringMode/fifo";
            mPipeFile = new File(mPipeName + "_write");
            if (android.os.SystemProperties.get("init.svc.eng_rapiproxy") != "running") {
                System.out.println("android.os.SystemProperties.getinit.svc.eng_rapiproxy != running");
                android.os.SystemProperties.set("ctl.start", "eng_rapiproxy");
                SystemClock.sleep(500);
             }
        } catch (SecurityException e) {

        }

    }

    static {
        // The runtime will add "lib" on the front and ".o" on the end of
        // the name supplied to loadLibrary.
        System.loadLibrary("engrdrapijni");
    }

    private static native int sendcmd(String pipe, short cmd, short a, short b,
            short c);

    public void setAudioHandsetLoop(boolean enable) {
        sendToProxy(OEM_RAPI_CLIENT_AUDIO_LOOP_TEST, enable ? LOOP_ON
                : LOOP_OFF, SND_DEVICE_HANDSET, enable ? UNMUTE : MUTE);
    }

    public void setAudioHeadsetLoop(boolean enable) {
        sendToProxy(OEM_RAPI_CLIENT_AUDIO_LOOP_TEST, enable ? LOOP_ON
                : LOOP_OFF, SND_DEVICE_HEADSET, enable ? UNMUTE : MUTE);
    }

    public void setAudioStereoHeadsetLoop(boolean enable) {
        sendToProxy(OEM_RAPI_CLIENT_AUDIO_LOOP_TEST, enable ? LOOP_ON
                : LOOP_OFF, SND_DEVICE_STEREO_HEADSET, enable ? UNMUTE : MUTE);
    }

    public void setAudioDualMicHandsetLoop(boolean enable) {
        sendToProxy(OEM_RAPI_CLIENT_AUDIO_LOOP_TEST, enable ? LOOP_ON
                : LOOP_OFF, SND_DEVICE_IN_S_SADC_OUT_HANDSET, enable ? UNMUTE
                : MUTE);
    }

    public void setFTMMode() {
        sendToProxy(OEM_RAPI_CLIENT_SYS_OPRT_MODE_SET_MODE,
                (short) OprtMode.SYS_OPRT_MODE_FTM.getVal(), (short) 0,
                (short) 0);
    }

    public void setOnlineMode() {
        sendToProxy(OEM_RAPI_CLIENT_SYS_OPRT_MODE_SET_MODE,
                (short) OprtMode.SYS_OPRT_MODE_ONLINE.getVal(), (short) 0,
                (short) 0);
    }

    public void setDiversityAntennaPriOnDrxOn() {
        sendToProxy(OEM_RAPI_CLIENT_EVENT_RF_PRI_ON_DRX_ON, (short) 0, (short) 0,
                (short) 0);
    }

    public void setDiversityAntennaPriOnDrxOff() {
        sendToProxy(OEM_RAPI_CLIENT_EVENT_RF_PRI_ON_DRX_OFF, (short) 0,
                (short) 0, (short) 0);
    }

    public void setDiversityAntennaPriOffDrxOn() {
        sendToProxy(OEM_RAPI_CLIENT_EVENT_RF_PRI_OFF_DRX_ON, (short) 0,
                (short) 0, (short) 0);
    }

    public int getOprtMode() {
        StringBuffer sb = new StringBuffer();
        int mode;
        sendToProxy(OEM_RAPI_CLIENT_SYS_OPRT_MODE_GET_MODE, sb, (short) 0,
                (short) 0, (short) 0);

        Pattern p = Pattern.compile("\\s*=\\s*([-0-9]*)");
        Matcher m = p.matcher(sb);
        if (m.find()) {
            return Integer.parseInt(m.group(m.groupCount()), 10);
        } else {
            return -2;
        }
    }

    private void sendToProxy(short cmd, short a, short b, short c) {
        synchronized (mPipeFile) {
            CmdThread command = new CmdThread(cmd, a, b, c);
            android.os.SystemClock.sleep(500);
            try {
                command.join(2000); // block fot 2s max till command thread dies
            } catch (Exception e) {
                TctLog.e(TAG, "CmdThread interrupted");
            }

            // while(command.isAlive()) {
            // TctLog.e(TAG,"waiting for sendcmd()");
            // android.os.SystemClock.sleep(500);
            // }
        }
    }

    private void sendToProxy(short cmd, StringBuffer sb, short a, short b,
            short c) {
        synchronized (mPipeFile) {
            CallbackThread command = new CallbackThread(cmd, sb, a, b, c);
            android.os.SystemClock.sleep(500);
            try {
                command.join(2000); // block fot 2s max till command thread dies
            } catch (Exception e) {
                TctLog.e(TAG, "CmdThread interrupted");
            }

        }
    }

    class CmdThread extends Thread {

        private short[] para = new short[4];
        private int result = 0;

        CmdThread(short cmd, short a, short b, short c) {
            para[0] = cmd;
            para[1] = a;
            para[2] = b;
            para[3] = c;

            start();
        }

        public void run() {
            result = sendcmd(mPipeName, para[0], para[1], para[2], para[3]);
            TctLog.d(TAG, "sendcmd returned " + result);
        }
    }

    class CallbackThread extends Thread {

        private short[] para = new short[4];
        private int mResult;

        private CallBack cb;

        CallbackThread(short cmd, short a, short b, short c) {
            para[0] = cmd;
            para[1] = a;
            para[2] = b;
            para[3] = c;
            cb = new CallBack() {
                public void c() {
                    mResult = sendcmd(mPipeName, para[0], para[1], para[2],
                            para[3]);
                    TctLog.d(TAG, "sendcmd returned " + mResult);
                }
            };

            start();
        }

        CallbackThread(short cmd, final StringBuffer res, short a, short b,
                short c) {
            para[0] = cmd;
            para[1] = a;
            para[2] = b;
            para[3] = c;
            cb = new CallBack() {
                public void c() {
                    // res.append(sendcmdForStrResult(mPipeName, para[0],
                    // para[1], para[2], para[3]));
                    TctLog.d(TAG, "sendcmdForStrResult returned " + res);
                }
            };

            start();
        }

        public void run() {
            cb.c();
        }
    }
}
