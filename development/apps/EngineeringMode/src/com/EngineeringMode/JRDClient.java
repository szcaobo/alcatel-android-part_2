/******************************************************************************/
/*                                                               Date:09/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Qianbo Pan                                                        */
/* E-Mail:  Qianbo.Pan@tcl-mobile.com                                         */
/* Role  :  MMITest                                                           */
/* Reference documents :  Scribe 5_MMI_AUTO_Test_specfication_V3.0.Doc        */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/EngineeringMode/src/com/EngineeringMode/       */
/*                                                            JRDClient.java  */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 12/09/12| Qianbo.Pan     |                    | Porting for EngineeringMode*/
/*         |                |                    |                            */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.EngineeringMode;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.net.LocalSocket;
import android.net.LocalSocketAddress;
import android.util.TctLog;

public class JRDClient {
    private static final String TAG = "JRDClient";

    public static final String MMI_SOCKET_NAME = "mmi_proxy";

    // define MMI test commands
    public static final int ENGCMD_JRDRAPIPROXY = 1;

    private InputStream mIn;
    private OutputStream mOut;
    private LocalSocket mSocket;
    private byte cmd_buf[] = new byte[4];
    private byte reply_buf[] = new byte[4];

    public static boolean isSync = false;

    public JRDClient() {
    }

    public boolean connect(String socketName) {
        mSocket = new LocalSocket();
        if (mSocket == null) {
            return false;
        }

        TctLog.i(TAG, "connecting...");
        try {
            LocalSocketAddress address = new LocalSocketAddress(socketName,
                    LocalSocketAddress.Namespace.RESERVED);

            mSocket.connect(address);

            mIn = mSocket.getInputStream();
            mOut = mSocket.getOutputStream();
        } catch (IOException ex) {
            disconnect();
            return false;
        }
        TctLog.i(TAG, "connected");
        return true;
    }

    public void disconnect() {
        TctLog.i(TAG, "disconnecting...");
        try {
            if (mSocket != null)
                mSocket.close();
        } catch (IOException ex) {
            TctLog.e(TAG, "mSocket close IOException");
        }
        try {
            if (mIn != null)
                mIn.close();
        } catch (IOException ex) {
            TctLog.e(TAG, "mIn close IOException");
        }
        try {
            if (mOut != null)
                mOut.close();
        } catch (IOException ex) {
            TctLog.e(TAG, "mOut close IOException");
        }
        mSocket = null;
        mIn = null;
        mOut = null;
        TctLog.i(TAG, "disconnected");
    }

    public boolean readReply() {
        int count;
        try {
            count = mIn.read(reply_buf, 0, 4);
            if (count <= 0) {
                TctLog.e(TAG, "read error " + count);
            }
        } catch (IOException ex) {
            TctLog.e(TAG, "read exception");
            return false;
        }
        int reply = (((int) reply_buf[0]) & 0xff)
                | ((((int) reply_buf[1]) & 0xff) << 8);
        TctLog.i(TAG, "Reply is 0x" + Integer.toHexString(reply));

        return true;
    }

    public boolean sendCommand(int cmd) {
        cmd_buf[0] = (byte) (cmd & 0xff);
        cmd_buf[1] = (byte) ((cmd >> 8) & 0xff);
        cmd_buf[2] = (byte) ((cmd >> 16) & 0xff);
        cmd_buf[3] = (byte) ((cmd >> 24) & 0xff);

        try {
            mOut.write(cmd_buf, 0, 4);
        } catch (IOException ex) {
            TctLog.e(TAG, "write error");
            return false;
        }
        return true;
    }

}
