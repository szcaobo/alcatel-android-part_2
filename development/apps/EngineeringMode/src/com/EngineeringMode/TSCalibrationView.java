/******************************************************************************/
/*                                                               Date:09/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Qianbo Pan                                                        */
/* E-Mail:  Qianbo.Pan@tcl-mobile.com                                         */
/* Role  :  MMITest                                                           */
/* Reference documents :  Scribe 5_MMI_AUTO_Test_specfication_V3.0.Doc        */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/EngineeringMode/src/com/EngineeringMode/       */
/*                                                    TSCalibrationView.java  */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 12/09/12| Qianbo.Pan     |                    | Porting for EngineeringMode*/
/*         |                |                    |                            */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.EngineeringMode;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.View;

public class TSCalibrationView extends View {

    private class TargetPoint {
        public int x;
        public int y;

        public TargetPoint(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }

    private int mStep = 0;
    private TargetPoint mTargetPoints[];
    private TSCalibration mContext;

    public TSCalibrationView(TSCalibration context, int h, int w) {
        super(context);
        mContext = context;
        mTargetPoints = new TargetPoint[16];
        // mTargetPoints[15] add for saving currentX and currentY

        mTargetPoints[0] = new TargetPoint(25, 25);
        mTargetPoints[1] = new TargetPoint(w / 2, 25);
        mTargetPoints[2] = new TargetPoint(w - 25, 25);

        mTargetPoints[3] = new TargetPoint(w / 4, h / 6);
        mTargetPoints[4] = new TargetPoint(3 * w / 4, h / 6);

        mTargetPoints[5] = new TargetPoint(25, h / 3);
        mTargetPoints[6] = new TargetPoint(w / 2, h / 3);
        mTargetPoints[7] = new TargetPoint(w - 25, h / 3);

        mTargetPoints[8] = new TargetPoint(w / 4, h / 2);
        mTargetPoints[9] = new TargetPoint(3 * w / 4, h / 2);

        mTargetPoints[10] = new TargetPoint(25, 2 * h / 3);
        mTargetPoints[11] = new TargetPoint(w / 2, 2 * h / 3);
        mTargetPoints[12] = new TargetPoint(w - 25, 2 * h / 3);

        mTargetPoints[13] = new TargetPoint(w / 4, h - 50);
        mTargetPoints[14] = new TargetPoint(3 * w / 4, h - 50);

        mTargetPoints[15] = new TargetPoint(0, 0);

    }

    public void reset() {
        mStep = 0;
    }

    public boolean isFinished() {
        return mStep >= 15;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if (isFinished())
            return true;
        if (ev.getAction() != MotionEvent.ACTION_UP)
            return true;
        mTargetPoints[15].x = (int) ev.getRawX();
        mTargetPoints[15].y = (int) ev.getRawY();
        Rect rectD = new Rect();
        rectD.left = mTargetPoints[mStep].x - 25;
        rectD.top = mTargetPoints[mStep].y - 25;
        rectD.bottom = mTargetPoints[mStep].y + 25;
        rectD.right = mTargetPoints[mStep].x + 25;

        if (rectD.contains(mTargetPoints[15].x, mTargetPoints[15].y)) {

            mStep++;

        }

        mContext.onCalTouchEvent(ev);
        return true;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (isFinished())
            return;
        canvas.drawColor(Color.BLACK);
        drawTarget(canvas, mTargetPoints[mStep].x, mTargetPoints[mStep].y);
    }

    private void drawTarget(Canvas c, int x, int y) {
        Paint white = new Paint(Paint.ANTI_ALIAS_FLAG);
        Paint red = new Paint(Paint.ANTI_ALIAS_FLAG);
        white.setColor(Color.WHITE);
        red.setColor(Color.RED);
        c.drawCircle(x, y, 25, red);
        c.drawCircle(x, y, 21, white);
        c.drawCircle(x, y, 17, red);
        c.drawCircle(x, y, 13, white);
        c.drawCircle(x, y, 9, red);
        c.drawCircle(x, y, 5, white);
        c.drawCircle(x, y, 1, red);
    }
}
