/******************************************************************************/
/*                                                               Date:09/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Qianbo Pan                                                        */
/* E-Mail:  Qianbo.Pan@tcl-mobile.com                                         */
/* Role  :  MMITest                                                           */
/* Reference documents :  Scribe 5_MMI_AUTO_Test_specfication_V3.0.Doc        */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/EngineeringMode/src/com/EngineeringMode/       */
/*                                                           KeypadTest.java  */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 12/09/12| Qianbo.Pan     |                    | Porting for EngineeringMode*/
/*         |                |                    |                            */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.EngineeringMode;

import android.graphics.Typeface;
import android.util.TctLog;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

/*
 * Keyboard Test
 */
class KeypadTest extends Test {

    KeyPad kp = new KeyPad();

    Button blsk;
    Button brsk;

    private boolean turnOn = true;
    private BackLight bl = new BackLight();

    KeypadTest(ID pid, String s) {
        super(pid, s);

        hKey = new KeyHandler() {
            public boolean handleKey(KeyEvent event) {
                return kp.updateKeyState(event.getAction(), event.getKeyCode());
            }
        };
    }

    @Override
    protected void onTimeInFinished() {
        TctLog.d(TAG, "KeypadTest:onTimeInFinished()");
        // if(brsk != null)
        // brsk.setEnabled(true);
    }

    class KeyPad {

        KeyPad() {
        }

        class Key extends Object {
            String mName;
            Button b;
            boolean pressed;
            boolean is_padding;
            int keycode;

            Key(String s, int padding) { // add some virtual key
                mName = s;
                if (padding == 0) {
                    is_padding = false;
                    pressed = false;
                } else {
                    is_padding = true;
                    pressed = true; // only for isAllKeyPressed().
                }
            }

            Key(String s) {
                mName = s;
                pressed = false;
            }

            public String toString() {
                return mName;
            }
        }

        Key[][] KeyStates = { { new Key("Vol Down"), new Key("Vol Up"), }, {
        /* new Key("Call"), */new Key("Back"), new Key("Home"), /*
                                                                 * new Key
                                                                 * ("Search" ),
                                                                 */
        new Key("Menu"), },
        /*
         * { new Key("Q"), new Key("W"), new Key("E"), new Key("R"), new
         * Key("T"), new Key("Y"), new Key("U"), new Key("I"), new Key("O"), new
         * Key("P"), }, { new Key("A"), new Key("S"), new Key("D"), new
         * Key("F"), new Key("G"), new Key("H"), new Key("J"), new Key("K"), new
         * Key("L"), new Key("BSP"), }, { new Key("Z"), new Key("X"), new
         * Key("C"), new Key("V"), new Key("B"), new Key("N"), new Key("M"), new
         * Key("UP"), new Key("."), new Key("RET"), }, { new Key("Fn"), new
         * Key("SYM"), new Key("Sft"), new Key("Space"), new Key("Left"), new
         * Key("Down"), new Key("Right"), },
         */
        };

        final int[][] KeyCodes = {
                { KeyEvent.KEYCODE_VOLUME_DOWN, KeyEvent.KEYCODE_VOLUME_UP, },
                {
                /* KeyEvent.KEYCODE_CALL, */KeyEvent.KEYCODE_BACK,
                        KeyEvent.KEYCODE_HOME,/* KeyEvent.KEYCODE_SEARCH, */
                        KeyEvent.KEYCODE_MENU, },
        /*
         * { KeyEvent.KEYCODE_Q, KeyEvent.KEYCODE_W, KeyEvent.KEYCODE_E,
         * KeyEvent.KEYCODE_R, KeyEvent.KEYCODE_T, KeyEvent.KEYCODE_Y,
         * KeyEvent.KEYCODE_U, KeyEvent.KEYCODE_I, KeyEvent.KEYCODE_O,
         * KeyEvent.KEYCODE_P, }, { KeyEvent.KEYCODE_A, KeyEvent.KEYCODE_S,
         * KeyEvent.KEYCODE_D, KeyEvent.KEYCODE_F, KeyEvent.KEYCODE_G,
         * KeyEvent.KEYCODE_H, KeyEvent.KEYCODE_J, KeyEvent.KEYCODE_K,
         * KeyEvent.KEYCODE_L, KeyEvent.KEYCODE_DEL, }, { KeyEvent.KEYCODE_Z,
         * KeyEvent.KEYCODE_X, KeyEvent.KEYCODE_C, KeyEvent.KEYCODE_V,
         * KeyEvent.KEYCODE_B, KeyEvent.KEYCODE_N, KeyEvent.KEYCODE_M,
         * KeyEvent.KEYCODE_DPAD_UP, KeyEvent.KEYCODE_PERIOD,
         * KeyEvent.KEYCODE_ENTER, }, { KeyEvent.KEYCODE_ALT_LEFT,
         * KeyEvent.KEYCODE_SYM, KeyEvent.KEYCODE_SHIFT_LEFT,
         * KeyEvent.KEYCODE_SPACE, KeyEvent.KEYCODE_DPAD_LEFT,
         * KeyEvent.KEYCODE_DPAD_DOWN, KeyEvent.KEYCODE_DPAD_RIGHT, },
         */
        };

        boolean updateKeyState(int action, int keycode) {
            boolean handled = false;
            // hide the released buttons on key up
            for (int i = 0; i < KeyCodes.length; i++) {
                for (int j = 0; j < KeyCodes[i].length; j++) {
                    if (KeyCodes[i][j] == keycode) {
                        if (action == KeyEvent.ACTION_UP) {// hide the button
                            KeyStates[i][j].pressed = true;
                            KeyStates[i][j].b.setVisibility(Button.INVISIBLE);
                        }
                        handled = true;
                    }
                }
            }
            // TctLog.d("Dingtang","key "+keycode+"not handled");
            // update the current test status
            if (kp.isAllKeyPressed() == true) {
                Result = PASSED;
                blsk.setEnabled(true); // pass-button is on left now.

            } else {
                Result = FAILED;
                // brsk.setEnabled(false);
            }

            if (!handled) {
                TctLog.d(TAG, "key " + keycode + "not handled");
            }
            return handled;
        }

        boolean isAllKeyPressed() {

            for (int i = 0; i < KeyCodes.length; i++) {
                for (int j = 0; j < KeyCodes[i].length; j++) {
                    if (KeyStates[i][j].pressed == false) {
                        return false;
                    }
                }
            }

            return true;

        }

    }/* KeyPad */

    private class BLThread extends Thread {
        public void run() {
            // NPI requirement: Always turn on led backlight during test
            // But there is a limitation since the backlight on/off mechanism
            // defined in PowerManagerService.java
            // When key or screen touched, the backlight will turn off
            // automatically,so we have to turn it on and on again
            while (turnOn) {
                bl.setKbdBacklight(255);
                try {
                    sleep(1500);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    protected void Run() {

        switch (mState) {
        case INIT:
            // turn on baclight during test
            turnOn = true;
            new BLThread().start();

            // output = this.toString();
            // display the keyboard on the screen
            /*
             * Settings.Secure.putString(mContext.getContentResolver(),
             * Settings.Secure.ENABLED_INPUT_METHODS, null);
             *
             * Settings.Secure.putString(mContext.getContentResolver(),
             * Settings.Secure.DEFAULT_INPUT_METHOD, null);
             */
            LinearLayout ll = new LinearLayout(mContext);
            ll.setOrientation(LinearLayout.VERTICAL);

            LinearLayout.LayoutParams lllp = new LinearLayout.LayoutParams(
                    LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 0);
            TableLayout.LayoutParams tllp = new TableLayout.LayoutParams(
                    LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 1);
            TableRow.LayoutParams trlp = new TableRow.LayoutParams(
                    LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 1);

            /* add title to the view */
            TextView tvtitle = new TextView(mContext);
            tvtitle.setGravity(Gravity.CENTER);
            tvtitle.setTypeface(Typeface.MONOSPACE, 1);
            // tvtitle.setTextAppearance(mActivity,
            // android.R.style.TextAppearance_Large);
            tvtitle.setTypeface(Typeface.MONOSPACE, Typeface.BOLD);
            tvtitle.setTextSize(20);
            tvtitle.setText(mName);

            ll.addView(tvtitle, new LinearLayout.LayoutParams(
                    LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 3));

            kp = new KeyPad();

            for (int i = 0; i < kp.KeyStates.length; i++) {// create rows
                TableLayout tl = new TableLayout(mContext);
                TableRow tr = new TableRow(mContext);

                for (int j = 0; j < kp.KeyStates[i].length; j++) {
                    if ((!kp.KeyStates[i][j].mName.matches("End Call"))) {
                        if (kp.KeyStates[i][j].mName.matches(""))
                            break;
                        Button b = new Button(mContext);
                        b.setClickable(false);
                        b.setText(kp.KeyStates[i][j].mName);
                        // b.setMaxWidth(KeyNames[i][j].length()*48);
                        b.setPadding(0, 0, 0, 0);
                        kp.KeyStates[i][j].b = b;
                        tr.addView(b, trlp);

                        if (kp.KeyStates[i][j].is_padding == true) {
                            b.setVisibility(Button.INVISIBLE);
                        }
                        b.setTypeface(Typeface.MONOSPACE, Typeface.NORMAL);
                    }
                }
                tl.addView(tr, tllp);
                ll.addView(tl, lllp);
            }

            /* finally add the pass /failed buttons */
            blsk = new Button(mContext);
            brsk = new Button(mContext);
            blsk.setText("PASS");
            brsk.setText("FAIL");

            blsk.setEnabled(false);

            // TODO press OK or FAILED should have different behavior
            brsk.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    ExecuteTest.currentTest.Result = Test.FAILED;
                    ExecuteTest.currentTest.Exit();

                }
            });

            blsk.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    ExecuteTest.currentTest.Result = Test.PASSED;
                    ExecuteTest.currentTest.Exit();
                }
            });

            // create sub linear layout for the buttons and add the buttons to
            // it
            LinearLayout llsk = new LinearLayout(mContext);
            llsk.setOrientation(LinearLayout.HORIZONTAL);

            LinearLayout.LayoutParams llsklp = new LinearLayout.LayoutParams(
                    LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 1);
            llsk.addView(blsk, llsklp);
            llsk.addView(brsk, llsklp);
            llsk.setGravity(Gravity.CENTER);

            ll.addView(llsk, new LinearLayout.LayoutParams(
                    LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 3));

            ll.setGravity(Gravity.BOTTOM);
            WindowManager.LayoutParams wmlp = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.TYPE_KEYGUARD);
            mContext.setContentView(ll, wmlp);
            break;

        case END:
            turnOn = false;
            break;
        default:
            turnOn = false;
            break;
        }

    }

    // workaround, by wang.jian.
    CallBack Cb = new CallBack() {
        public void c() {
            int t = mContext.getWindow().getAttributes().type;
            TctLog.d(TAG, ">>>>>>>>>>>>>>>>>>" + t);
            // mContext.getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD);
            // HOME key has no effect.
        }
    };

    @Override
    public void Resume() {
        SetTimer(200, Cb); // miliseconds
    }

}
