/******************************************************************************/
/*                                                               Date:09/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Qianbo Pan                                                        */
/* E-Mail:  Qianbo.Pan@tcl-mobile.com                                         */
/* Role  :  MMITest                                                           */
/* Reference documents :  Scribe 5_MMI_AUTO_Test_specfication_V3.0.Doc        */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/EngineeringMode/src/com/EngineeringMode/       */
/*                                                      TouchWindowTest.java  */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 12/09/12| Qianbo.Pan     |                    | Porting for EngineeringMode*/
/*         |                |                    |                            */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.EngineeringMode;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.FontMetricsInt;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

public class TouchWindowTest extends Activity {

    private String currentFileName;

    @Override
    protected void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(new MyView(this));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        currentFileName = "TW_Coordinate_" + sdf.format(new Date());
        if (!checkSDCard()) {
            mMakeTextToast(getResources().getText(R.string.str_err).toString(),
                    true);
        }
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness = 1.0f;
        getWindow().setAttributes(lp);

    }

    private boolean checkSDCard() {

        if (android.os.Environment.getExternalStorageState().equals(
                android.os.Environment.MEDIA_MOUNTED)) {
            return true;
        } else {
            return false;
        }
    }

    public void mMakeTextToast(String str, boolean isLong) {
        if (isLong == true) {
            Toast.makeText(TouchWindowTest.this, str, Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(TouchWindowTest.this, str, Toast.LENGTH_SHORT)
                    .show();
        }
    }

    public static class PointerState {
        private final ArrayList<Float> mXs = new ArrayList<Float>();
        private final ArrayList<Float> mYs = new ArrayList<Float>();
        private boolean mCurDown;
        private int mCurX;
        private int mCurY;

    }

    public class MyView extends View {

        String TEXT = new String();
        String sString = new String();
        private final Paint mTextPaint;
        private final Paint mTextBackgroundPaint;
        private final Paint mPaint;
        private final Paint mTargetPaint;
        private final Paint mPathPaint;
        private final FontMetricsInt mTextMetrics = new FontMetricsInt();
        private int mHeaderBottom;
        private boolean mCurDown;
        private int mCurNumPointers;
        private int mMaxNumPointers;
        private final ArrayList<PointerState> mPointers = new ArrayList<PointerState>();

        public MyView(Context c) {
            super(c);

            // coordinate
            mTextPaint = new Paint();
            mTextPaint.setAntiAlias(true);
            mTextPaint
                    .setTextSize(18 * getResources().getDisplayMetrics().density);
            mTextPaint.setARGB(255, 0, 0, 0);

            // coordinate background
            mTextBackgroundPaint = new Paint();
            mTextBackgroundPaint.setAntiAlias(false);
            mTextBackgroundPaint.setARGB(255, 255, 255, 255);

            // draw coordinate
            mPaint = new Paint();
            mPaint.setAntiAlias(true);
            mPaint.setARGB(255, 255, 255, 255);
            mPaint.setStyle(Paint.Style.STROKE);
            mPaint.setStrokeWidth(0);

            // draw cross
            mTargetPaint = new Paint();
            mTargetPaint.setAntiAlias(false);
            mTargetPaint.setARGB(255, 255, 64, 128);

            // draw trail
            mPathPaint = new Paint();
            mPathPaint.setAntiAlias(false);
            mPathPaint.setARGB(255, 0, 96, 255);

            PointerState ps = new PointerState();
            mPointers.add(ps);

            mTextPaint.getFontMetricsInt(mTextMetrics);
            mHeaderBottom = -mTextMetrics.ascent + mTextMetrics.descent + 2;
        }

        @Override
        protected void onDraw(Canvas canvas) {

            final int itemW = getWidth() / 6;
            final int base = -mTextMetrics.ascent + 1;
            final int bottom = mHeaderBottom;

            final int NP = mPointers.size();

            // display X:Y:coordinate
            if (NP > 0) {
                final PointerState ps = mPointers.get(0);
                canvas.drawRect(0, 0, itemW - 1, bottom, mTextBackgroundPaint);
                canvas.drawText("X: " + ps.mCurX, 2, base, mTextPaint);
                canvas.drawRect(itemW, 0, itemW * 2 - 1, bottom,
                        mTextBackgroundPaint);
                canvas.drawText("Y: " + ps.mCurY, itemW + 1, base, mTextPaint);

            }

            // display cross
            for (int p = 0; p < NP; p++) {
                final PointerState ps = mPointers.get(p);
                if (mCurDown && ps.mCurDown) {
                    canvas.drawLine(0, (int) ps.mCurY, getWidth(),
                            (int) ps.mCurY, mTargetPaint);
                    canvas.drawLine((int) ps.mCurX, 0, (int) ps.mCurX,
                            getHeight(), mTargetPaint);

                }
            }

            // display lines
            for (int p = 0; p < NP; p++) {
                final PointerState ps = mPointers.get(p);
                final int N = ps.mXs.size();
                float lastX = 0, lastY = 0;
                boolean haveLast = false;
                StringBuilder sXY = new StringBuilder();
                mPaint.setARGB(255, 200, 255, 255);

                for (int i = 0; i < N; i++) {
                    float x = ps.mXs.get(i);
                    float y = ps.mYs.get(i);
                    if (Float.isNaN(x)) {
                        haveLast = false;
                        continue;
                    }
                    if (haveLast) {
                        canvas.drawLine(lastX, lastY, x, y, mPathPaint);
                        canvas.drawPoint(lastX, lastY, mPaint);
                        sXY.append("X:" + lastX + "\t" + "Y:" + lastY + "\r\n");
                    }
                    // else {
                    // lastX = x;
                    // lastY = y;
                    // sXY.append("X:" + lastX + "\t" + "Y:" + lastY + "\r\n");
                    // }
                    lastX = x;
                    lastY = y;
                    haveLast = true;
                }
                sString = sXY.toString();
            }
            TEXT = sString;
            // SDCardWrite("coordinate", sString);
        }

        public void SDCardWrite(String filename, String stext) {
            String sDStateString = android.os.Environment
                    .getExternalStorageState();

            if (sDStateString.equals(android.os.Environment.MEDIA_MOUNTED)) {

                try {
                    File SDFile = new File(
                            Environment.getExternalStorageDirectory(),
                            "TouchWindowTest");
                    if (!SDFile.exists()) {
                        SDFile.mkdirs();
                    }

                    File myFile = new File(SDFile.getAbsolutePath()
                            + File.separator + filename + ".txt");
                    if (!myFile.exists()) {
                        myFile.createNewFile();
                    }
                    // write data
                    FileOutputStream outputStream = new FileOutputStream(
                            myFile, true);
                    outputStream.write(stext.getBytes());
                    outputStream.flush();
                    outputStream.close();
                } catch (Exception e) {
                    // TODO: handle exception
                }
                // end
            }

        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            int action = event.getAction();
            int NP = mPointers.size();
            if (action == MotionEvent.ACTION_DOWN) {
                for (int p = 0; p < NP; p++) {
                    final PointerState ps = mPointers.get(p);
                    ps.mXs.clear();
                    ps.mYs.clear();
                    ps.mCurDown = false;
                }
                mPointers.get(0).mCurDown = true;
                mMaxNumPointers = 0;
            }

            mCurDown = action != MotionEvent.ACTION_UP
                    && action != MotionEvent.ACTION_CANCEL;

            if (mMaxNumPointers < mCurNumPointers) {
                mMaxNumPointers = mCurNumPointers;
            }
            {
                final PointerState ps = mPointers.get(0);
                ps.mXs.add(event.getX());
                ps.mYs.add(event.getY());
                ps.mCurX = (int) event.getX();
                ps.mCurY = (int) event.getY();
            }
            if (action == MotionEvent.ACTION_UP) {
                final PointerState ps = mPointers.get(0);
                if (ps.mCurDown) {
                    ps.mCurDown = false;
                    Log.i("Pointer", "Pointer " + (1) + ": UP");
                }
                StringBuilder NString = new StringBuilder();
                NString.append("X:" + event.getX() + "\t" + "Y:" + event.getY()
                        + "\r\n" + "Test Finished" + "\r\n\r\n");
                SDCardWrite(currentFileName, TEXT);
                SDCardWrite(currentFileName, NString.toString());
            }
            invalidate();
            return true;
        }

    }
}
