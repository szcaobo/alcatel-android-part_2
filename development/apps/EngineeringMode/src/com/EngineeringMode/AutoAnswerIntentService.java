/******************************************************************************/
/*                                                               Date:09/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Qianbo Pan                                                        */
/* E-Mail:  Qianbo.Pan@tcl-mobile.com                                         */
/* Role  :  MMITest                                                           */
/* Reference documents :  Scribe 5_MMI_AUTO_Test_specfication_V3.0.Doc        */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/EngineeringMode/src/com/EngineeringMode/       */
/*                                              AutoAnswerIntentService.java  */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 12/09/12| Qianbo.Pan     |                    | Porting for EngineeringMode*/
/*         |                |                    |                            */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.EngineeringMode;

import java.lang.reflect.Method;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.util.TctLog;
import android.view.KeyEvent;

import com.android.internal.telephony.ITelephony;

public class AutoAnswerIntentService extends IntentService {

    public AutoAnswerIntentService() {
        super("AutoAnswerIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Context context = getBaseContext();
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {

        }

        // Make sure the phone is still ringing
        TelephonyManager tm = (TelephonyManager) context
                .getSystemService(Context.TELEPHONY_SERVICE);
        if (tm.getCallState() != TelephonyManager.CALL_STATE_RINGING) {
            return;
        }

        // Answer the phone
        try {
            answerPhoneAidl(context);
        } catch (Exception e) {
            e.printStackTrace();
            TctLog.d("AutoAnswer",
                    "Error trying to answer using telephony service.  Falling back to headset.");
            answerPhoneHeadsethook(context);
        }

        return;
    }

    private void answerPhoneHeadsethook(Context context) {
        // Simulate a press of the headset button to pick up the call
        Intent buttonDown = new Intent(Intent.ACTION_MEDIA_BUTTON);
        buttonDown.putExtra(Intent.EXTRA_KEY_EVENT, new KeyEvent(
                KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_HEADSETHOOK));
        context.sendOrderedBroadcast(buttonDown,
                "android.permission.CALL_PRIVILEGED");

        // froyo and beyond trigger on buttonUp instead of buttonDown
        Intent buttonUp = new Intent(Intent.ACTION_MEDIA_BUTTON);
        buttonUp.putExtra(Intent.EXTRA_KEY_EVENT, new KeyEvent(
                KeyEvent.ACTION_UP, KeyEvent.KEYCODE_HEADSETHOOK));
        context.sendOrderedBroadcast(buttonUp,
                "android.permission.CALL_PRIVILEGED");
    }

    private void answerPhoneAidl(Context context) throws Exception {
        // Set up communication with the telephony service (thanks to Tedd's
        // Droid Tools!)
        TelephonyManager tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        @SuppressWarnings("rawtypes")
        Class c = Class.forName(tm.getClass().getName());
        Method m = c.getDeclaredMethod("getITelephony");
        m.setAccessible(true);
        ITelephony telephonyService;
        telephonyService = (ITelephony) m.invoke(tm);

        // Silence the ringer and answer the call!
        telephonyService.silenceRinger();
        telephonyService.answerRingingCall();
    }
}
