/******************************************************************************/
/*                                                               Date:09/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Qianbo Pan                                                        */
/* E-Mail:  Qianbo.Pan@tcl-mobile.com                                         */
/* Role  :  MMITest                                                           */
/* Reference documents :  Scribe 5_MMI_AUTO_Test_specfication_V3.0.Doc        */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/EngineeringMode/src/com/EngineeringMode/       */
/*                                                          AntennaTest.java  */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 12/09/12| Qianbo.Pan     |                    | Porting for EngineeringMode*/
/* 08/10/12| Qianbo.Pan     |                    | change antenna test api    */
/*         |                |                    |                            */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.EngineeringMode;

import com.EngineeringMode.JRDRapi;
import com.EngineeringMode.JRDClient;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.TctLog;
import android.view.View;
import android.view.View.OnClickListener;
import android.os.Build;

import java.io.IOException;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.TextView;

public class AntennaTest extends Activity implements OnClickListener{
    String TAG = "AntennaTest";

    private JRDRapi remote;
    private TextView messageText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.antenna);
        messageText = (TextView)findViewById(R.id.messageText);
        messageText.setText(TAG);

        Button btn_priondrxoff = (Button)findViewById(R.id.prion_drxoff);
        Button btn_prioffdrxon = (Button)findViewById(R.id.prioff_drxon);
        Button btn_priondrxon = (Button)findViewById(R.id.prion_drxon);
        Button btn_cancel = (Button)findViewById(R.id.cancel);

        btn_priondrxoff.setOnClickListener(this);
        btn_prioffdrxon.setOnClickListener(this);
        btn_priondrxon.setOnClickListener(this);
        btn_cancel.setOnClickListener(this);

        try {
            if ("scribe5".equals(Build.DEVICE)
                    || "msm7627a".equals(Build.DEVICE)) {
                remote = new JRDRapi(this);
            }
        } catch (IOException e) {
            TctLog.e(TAG, "error " + e);
        }

    }

    public void init() {
        JRDClient mClient = new JRDClient();
        if (!mClient.connect(JRDClient.MMI_SOCKET_NAME)) {
            TctLog.d(TAG, "Connecting MMI proxy fail!");
        }
        if (!mClient.sendCommand(JRDClient.ENGCMD_JRDRAPIPROXY)) {
            TctLog.d(TAG, "Send command to MMI proxy fail!");
            mClient.disconnect();
        }
        boolean success = mClient.readReply();
        mClient.disconnect();
    }

    public void onClick(View v) {
        // TODO Auto-generated method stub
        if(remote == null) {
            return;
        }
        switch (v.getId()) {
        case R.id.prion_drxoff:
            remote.setDiversityAntennaPriOnDrxOff();
            messageText.setText(TAG+"\n\n"+getString(R.string.strPriOnDrxOff));
            break;
        case R.id.prioff_drxon:
            remote.setDiversityAntennaPriOffDrxOn();
            messageText.setText(TAG+"\n\n"+getString(R.string.strPriOffDrxOn));
            break;
        case R.id.prion_drxon:
            remote.setDiversityAntennaPriOnDrxOn();
            messageText.setText(TAG+"\n\n"+getString(R.string.strPriOnDrxOn));
            break;
        case R.id.cancel:
            finish();
            break;

        default:
            break;
        }
    }

}
