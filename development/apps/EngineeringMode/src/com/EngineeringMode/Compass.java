/******************************************************************************/
/*                                                               Date:09/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Qianbo Pan                                                        */
/* E-Mail:  Qianbo.Pan@tcl-mobile.com                                         */
/* Role  :  MMITest                                                           */
/* Reference documents :  Scribe 5_MMI_AUTO_Test_specfication_V3.0.Doc        */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/EngineeringMode/src/com/EngineeringMode/       */
/*                                                          CompassTest.java  */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 12/09/12| Qianbo.Pan     |                    | Porting for EngineeringMode*/
/*         |                |                    |                            */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.EngineeringMode;

import com.android.internal.telephony.RILConstants;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.Message;
import android.util.TctLog;
import android.view.View;

class CompassTest extends Test implements SensorEventListener {

    TestLayout1 tl;
    String mDisplayString;

    private static SensorManager mSensorManager;
    private Sensor mCompass;
    private SampleView mView;
    private float[] mValues;
    private static final String TAG = "Compass";

    CompassTest(ID pid, String s) {
        super(pid, s);
    }

    CompassTest(ID pid, String s, int timein) {
        super(pid, s, timein, 0);
    }

    @Override
    protected void Run() {
        // this function executes the test
        switch (mState) {
        case INIT: // init the test, shows the first screen

            mTimeIn.start();

            if (mSensorManager == null)
                mSensorManager = (SensorManager) mContext
                        .getSystemService(Context.SENSOR_SERVICE);

            mCompass = mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);

            if (!mSensorManager.registerListener(this, mCompass,
                    SensorManager.SENSOR_DELAY_NORMAL)) {
                try {
                    TctLog.e(TAG,
                            "register listener for sensor "
                                    + mCompass.getName() + " failed");
                } catch (Exception e) {
                    // TODO: handle exception
                }
            }

            tl = new TestLayout1(mContext, mName,
                    "compass test start : \n move the phone in an 8 shape");
            tl.hideButtons();
            mContext.setContentView(tl.ll);
            mState++;

            SetTimer(500, new CallBack() {
                public void c() {
                    Run();
                }
            });

            break;
        case END://
            mSensorManager.unregisterListener(this, mCompass);
            TctLog.d(TAG, "compass sensor listener unregistered");
            break;

        default:
            /*
             * tl = new TestLayout1(mContext,mName,"compass:\n" +
             * mDisplayString); mContext.setContentView(tl.ll);
             */
            mView = new SampleView(this.mContext);
            tl = new TestLayout1(mContext, mName, (View) mView);
            // if(!mTimeIn.isFinished()) {
            // tl.setAutoTestButtons(false);
            // }
            mContext.setContentView(tl.ll/* mView */);

            break;
        }

    }

    public void onSensorChanged(SensorEvent event) {

        mValues = event.values;

        TctLog.d(TAG, "sensorChanged (" + mValues[0] + ", " + mValues[1] + ", "
                + mValues[2] + ")");

        if (mView != null) {
            tl.ll.invalidate();
            mView.invalidate();
        }
    }

    public void onAccuracyChanged(Sensor s, int accuracy) {
        // TODO Auto-generated method stub
        // TctLog.d(TAG, "sensor accuracy changed " + accuracy);

    }

    private class SampleView extends View {
        private Paint mPaint = new Paint();
        private Path mPath = new Path();
        private boolean mAnimate;
        private long mNextTime;

        public SampleView(Context context) {
            super(context);

            // Construct a wedge-shaped path
            mPath.moveTo(0, -50);
            mPath.lineTo(-20, 60);
            mPath.lineTo(0, 50);
            mPath.lineTo(20, 60);
            mPath.close();
        }

        @Override
        protected void onDraw(Canvas canvas) {
            Paint paint = mPaint;

            canvas.drawColor(Color.WHITE);

            paint.setAntiAlias(true);
            paint.setColor(Color.BLACK);
            paint.setStyle(Paint.Style.FILL);

            int w = canvas.getWidth();
            int h = canvas.getHeight();
            int cx = w / 2;
            int cy = h / 2;

            canvas.translate(cx, cy);
            if (mValues != null) {
                canvas.rotate(-mValues[0]);
            }
            canvas.drawPath(mPath, mPaint);
        }

        @Override
        protected void onAttachedToWindow() {
            mAnimate = true;
            super.onAttachedToWindow();
        }

        @Override
        protected void onDetachedFromWindow() {
            mAnimate = false;
            super.onDetachedFromWindow();
        }
    }

    private class MyHandler extends Handler {

        private static final int MESSAGE_GET_PREFERRED_NETWORK_TYPE = 0;
        private static final int MESSAGE_SET_PREFERRED_NETWORK_TYPE = 1;

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case MESSAGE_GET_PREFERRED_NETWORK_TYPE:
                TctLog.i(TAG, "MESSAGE_GET_PREFERRED_NETWORK_TYPE CALLBACK ======");
                // Looper.myLooper().quit();
                break;

            case MESSAGE_SET_PREFERRED_NETWORK_TYPE:
                TctLog.i(TAG, "MESSAGE_SET_PREFERRED_NETWORK_TYPE CALLBACK ======");
                // Looper.myLooper().quit();
                break;
            // ADD BY xielianghui for cell broadcast
            case RILConstants.RIL_REQUEST_GSM_GET_BROADCAST_CONFIG:
                TctLog.i(TAG,
                        "RIL_REQUEST_GSM_GET_BROADCAST_CONFIG CALLBACK ======");
                break;
            case RILConstants.RIL_REQUEST_GSM_BROADCAST_ACTIVATION:
                TctLog.i(TAG,
                        "RIL_REQUEST_GSM_BROADCAST_ACTIVATION  CALLBACK======");
                break;
            case RILConstants.RIL_REQUEST_GSM_SET_BROADCAST_CONFIG:
                TctLog.i(TAG,
                        "RIL_REQUEST_GSM_SET_BROADCAST_CONFIG  CALLBACK======");
                break;
            }
        }
    }

    @Override
    protected void onTimeInFinished() {
        // if(tl!=null)
        // tl.setAutoTestButtons(true);
    }

}
