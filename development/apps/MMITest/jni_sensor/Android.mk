
LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional

# This is the target being built.
LOCAL_MODULE:= libgyroscop_test

$(warning "HelloWorld")

# All of the source files that we will compile.
LOCAL_SRC_FILES:= \
  sensor_test.c

#LOCAL_SRC_FILES:= \
#  libtfa9890_mono.c \
#  common.c \

# All of the shared libraries we link against.
LOCAL_SHARED_LIBRARIES := \
    libutils liblog \
    libnativehelper \
    libsensor1 \
    libcutils

$(warning "hello" $(LOCAL_SHARED_LIBRARIES))
#LOCAL_SHARED_LIBRARIES = libtfa9890

# No static libraries.
#LOCAL_STATIC_LIBRARIES :=

# Also need the JNI headers.
LOCAL_C_INCLUDES += \
    $(JNI_H_INCLUDE)
LOCAL_C_INCLUDES += $(QC_PROP_ROOT)/qmi
LOCAL_C_INCLUDES += $(QC_PROP_ROOT)/qmi/src
LOCAL_C_INCLUDES += $(QC_PROP_ROOT)/sensors/dsps/api
LOCAL_C_INCLUDES += $(QC_PROP_ROOT)/data/dss_new/src/platform/inc

LOCAL_C_INCLUDES += $(LOCAL_PATH)/inc
LOCAL_C_INCLUDES += $(QC_PROP_ROOT)/sensors/dsps/sensordaemon/common/inc
LOCAL_C_INCLUDES += $(QC_PROP_ROOT)/sensors/dsps/sensordaemon/common/util/mathtools/inc

LOCAL_C_INCLUDES += vendor/qcom/proprietary/common/inc
LOCAL_C_INCLUDES += vendor/qcom/proprietary/qmi/core/lib/inc
LOCAL_C_INCLUDES += vendor/qcom/proprietary/sensors/dsps/sensortest/jni/inc
LOCAL_C_INCLUDES += vendor/qcom/proprietary/diag/include/
LOCAL_C_INCLUDES += vendor/qcom/proprietary/diag/src
LOCAL_C_INCLUDES += system/core/include/cutils/

$(warning "hello" $(LOCAL_C_INCLUDES))
# No special compiler flags.
#LOCAL_CFLAGS +=

# Don't prelink this library.  For more efficient code, you may want
# to add this library to the prelink map and set this to true. However,
# it's difficult to do this for applications that are not supplied as
# part of a system image.

LOCAL_PRELINK_MODULE := false

include $(BUILD_SHARED_LIBRARY)
