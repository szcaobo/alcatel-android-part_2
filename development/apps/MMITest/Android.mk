#
# Copyright (C) 2008 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

ifeq ($(TARGET_BUILD_MMITEST),true)
define mmitest-build-config
    cp $(dir $@)/AndroidManifest.xml.mmi $(dir $@)/AndroidManifest.xml
endef
else
define mmitest-build-config
    cp $(dir $@)/AndroidManifest.xml.sw $(dir $@)/AndroidManifest.xml
endef
endif

.PHONY: $(LOCAL_PATH)/AndroidManifest.xml
$(LOCAL_PATH)/AndroidManifest.xml:
	$(mmitest-build-config)
	$(product-build-config)
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := $(call all-java-files-under, src) \
                   src/com/android/mmi/aidl/mmiAidl.aidl

ifeq ($(TARGET_BUILD_MMITEST),false)
#SW build: we don't need to auto-startup MMITEST
LOCAL_SRC_FILES := $(filter-out src/com/android/mmi/StartupIntentReceiver.java,$(LOCAL_SRC_FILES))
LOCAL_SRC_FILES := $(filter-out %mini/BuildConfig.java,$(LOCAL_SRC_FILES))
else
LOCAL_SRC_FILES := $(filter-out %sw/BuildConfig.java,$(LOCAL_SRC_FILES))
endif

LOCAL_PROGUARD_ENABLED := disabled
LOCAL_STATIC_JAVA_LIBRARIES := libfpsynaptics opencsv_idol4s maxxclientbase_mmi maxxutil_client_mmi


LOCAL_PACKAGE_NAME := MMITest
LOCAL_CERTIFICATE := platform
LOCAL_JAVA_LIBRARIES := qcom.fmradio tct.feature_query
LOCAL_SHARED_LIBRARIES += libsensor_reg

LOCAL_PROGUARD_FLAG_FILES := proguard.flags
#LOCAL_JNI_SHARED_LIBRARIES := libmmirapijni

LOCAL_OVERRIDES_PACKAGES := Home

include $(BUILD_PACKAGE)

include $(CLEAR_VARS)
LOCAL_PREBUILT_STATIC_JAVA_LIBRARIES := libfpsynaptics:libs/synaptics-sys.jar \
                                        opencsv_idol4s:libs/opencsv_2.3.jar \
                                        maxxclientbase_mmi:libs/maxxclientbase_release_mmi.jar \
                                        maxxutil_client_mmi:libs/maxxutil_release_mmi.jar
include $(BUILD_MULTI_PREBUILT)

# Also build all of the sub-targets under this one: the shared library.
include $(call all-makefiles-under,$(LOCAL_PATH))
