/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/HardwareInfo.java  */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/08/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemProperties;
import android.provider.Settings;
import android.telephony.TelephonyManager;

import com.android.mmi.util.MMILog;

import android.widget.TextView;

import com.android.mmi.util.SysClassManager;
import com.android.mmi.util.TestItemManager;
//import android.telephony.MSimTelephonyManager;

public class HardwareInfo extends TestBase implements SensorEventListener{
    private SensorManager mSensorManager;
    private Handler mHandler;
    private String TAG = "HardwareInfo";
    private TextView mgsensortTextView;
    private TextView mcompassTextView;
    float[] compass_acceValues=new float[3];
    float[] compass_magValues=new float[3];
    float[] compass_vals=new float[3];
    float[] compass_rotate=new float[9];
    private TextView mlightsensorTextView;
    private TextView mproximityTextView;
    private TextView msimtextviewTextView;
    private TextView mTemperatureTextView;
    private TextView mBluetoothtTextView;
    private TextView mWifiTextView;
    private TextView mgpstTextView;
    //Sensor event
    private Sensor mAccSensor = null;
    //private Sensor mCompass=null;
    private Sensor mMAGSensor=null;
    private Sensor lsensor = null;
    private Sensor mproximity = null;
    private DDDVect mDir = null;
    private boolean mIsbright = false;
    private boolean mIsdark = false;
    private boolean mIsnear = false;
    private boolean mIsfar = false;
    //Temperature
    private BroadcastReceiver broadcastReceiver;
    private IntentFilter intentFilter;
    private final static int VALUE_TEMPERATURE_DEFALUT = 0;
    private final static int VALUE_TEMPERATURE_DEFAULT_UNIT = 10;
    private final static int VALUE_VOLTAGE_DEFALUT = 0;
    private NumberFormat numformat;
    private String batteryStr;
    //Bluetooth
    public static final int MESSAGE_BLUETOOTH_RESTART = 1030;
    public static final int LENGHT_BLUETOOTH_CHECK_TIME = 1000;
    public static enum BluetoothState {
        INIT_WAIT, INIT_FAIL, DETECT_WAIT, DETECT_FAIL, DETECT_SUCCESS;
    }
    private boolean isBluetoothOldState;
    private BluetoothAdapter bluetoothAdapter;
    private BroadcastReceiver bluetoothStateReceiver;
    private BroadcastReceiver bluetoothFindReceiver;
    private IntentFilter bluetoothStateIntentFilter;
    private IntentFilter bluetoothFindIntentFilter;
    private Handler Bthandler;
    private String btitems = "";
    private ArrayList<String> btlistnames;
    //Wifi
    public static final int MESSAGE_WIFI_DETECT_COMPLETED = 1025;
    public static final int MESSAGE_WIFI_RESTART_REQUEST = 1026;
    public static final int LENGTH_WIFI_DETECT_TIMEOUT = 100;
    public static final int LENGTH_WIFI_CHECK_TIME = 2000;
    public static enum WifiMessage {
        WIFI_INIT_WAIT, WIFI_INIT_FAIL, WIFI_DETECT_WAIT, WIFI_DETECT_FAIL, WIFI_DETECT_SUCCESS;
    }
    private boolean isOldWifiEnable;
    private WifiManager wifiManager;
    private WifiScanReceiver wifiScanReceiver;
    private WifiStatusReceiver wifiStatusReceiver;
    private IntentFilter wifiScanIntentFilter;
    private IntentFilter wifiStatusIntentFilter;
    private Handler wifihandler;
    private String wifiitems = "";
    private ArrayList<String> wifilistnames;
    //Gps
    private final static int MESSAGE_GPS_DETECT_COMPLETED = 1027;
    public final static int LENGTH_GPS_DETECT_TIMEOUT = 10000;
    public static final int LENGTH_NOTIFICATION_INTERVAL_TIME = 1000;
    public static final float LENGTH_NOTIFICATION_INTERVAL_DISTANCE = 5.0f;
    public static final int LENGTH_SATELLITE_PRN_STRING = 50;
    public static final int LENGTH_SATELLITE_SNR_STRING = 80;
    public static final int LENGTH_LOCATION_STRING = LENGTH_SATELLITE_PRN_STRING * 2;

    public static enum GpsState {
        INITIALIZE_SUCCESS, INITIALIZE_WAIT, INITIALIZE_FAIL;
    };
    private int gpsStatusUpdateCount;
    private int locationUpdateCount;
    private int gpsSatelliteCount;
    private GpsStatus currentGpsStatus;
    private Location currentLocation;
    private StringBuilder gpsStatusStringBuilder;
    private StringBuilder gpsSatellitePrnStringBuilder;
    private StringBuilder gpsSatelliteSnrStringBuilder;
    private Handler gpshandler;
    private LocationManager locationManager;
    private GpsStatus.Listener gpsStatusListener;
    private LocationListener locationListener;
    private boolean isGps = false;
    private int failCount = 4;
    private String gpsinfo = "";
    private String product;
    private SysClassManager AreaIdManager;

    protected int HIGH;
    protected int LOW;

    @Override
    public void create(CommonActivity a) {
        // TODO Auto-generated method stub
        super.create(a);
        setContentView(R.layout.test_hardwareinfo_singleline, LAYOUTTYPE.LAYOUT_CUST_WITHOUT_PASSFAIL);
    }

    protected void init() {
        HIGH = 20;
        LOW  = 10;
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
        init();
        mHandler = new Handler();
        product = SystemProperties.get("ro.tct.product").trim();
        //initialize view
        mgsensortTextView = (TextView)findViewById(R.id.mgsensor);
        mgsensortTextView.setEnabled(false);
        mcompassTextView = (TextView)findViewById(R.id.mcompass);

        if(TestItemManager.GetTestItemInfoIntFromClass("Compass")==0) {
            mcompassTextView.setVisibility(TextView.GONE);
        }
        else{
            mcompassTextView.setEnabled(false);
        }
        mlightsensorTextView = (TextView)findViewById(R.id.mlightsensor);
        mlightsensorTextView.setText("[Light Sensor]Initializing");
        mlightsensorTextView.setEnabled(false);
        mproximityTextView = (TextView)findViewById(R.id.mproximity);
        mproximityTextView.setText("[Proximity Sensor]Please shadow the sensor");
        mproximityTextView.setEnabled(false);
        msimtextviewTextView = (TextView)findViewById(R.id.simtextview);
        msimtextviewTextView.setEnabled(false);
        mTemperatureTextView = (TextView)findViewById(R.id.temperature);
        mTemperatureTextView.setEnabled(false);
        mBluetoothtTextView = (TextView)findViewById(R.id.bluetooth);
        mBluetoothtTextView.setEnabled(false);
        mWifiTextView = (TextView)findViewById(R.id.wifi);
        mWifiTextView.setEnabled(false);
        mgpstTextView = (TextView)findViewById(R.id.gps);
        mgpstTextView.setEnabled(false);
        //Sensor event
        mSensorManager = (SensorManager) mContext.getSystemService(Context.SENSOR_SERVICE);
        mAccSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        //mCompass = mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
        mMAGSensor=mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        lsensor = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        mproximity = mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);

        //SIM
        getSimCardState();

        //Temperature
        batteryStr = getString(R.string.hardwareinfo_battery);
        mTemperatureTextView.setText("[Battery Temperature]Initializing");
        broadcastReceiver = new MemberReceiver();
        intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_BATTERY_CHANGED);
        numformat=NumberFormat.getPercentInstance();
        numformat.setMaximumFractionDigits(2);
        //Bluetooth
        btlistnames = new ArrayList<String>();
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        bluetoothStateReceiver = new BluetoothStateReceiver();
        bluetoothFindReceiver = new BluetoothFindReceiver();
        bluetoothStateIntentFilter = new IntentFilter(
                BluetoothAdapter.ACTION_STATE_CHANGED);
        bluetoothFindIntentFilter = new IntentFilter(
                BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        bluetoothFindIntentFilter.addAction(BluetoothDevice.ACTION_FOUND);
        Bthandler = new MemberBluetoothHandler();
        saveBluetoothOldState();
        mHandler.postDelayed(minitBluetooth,0);

        //Wifi
        wifilistnames = new ArrayList<String>();
        wifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
        wifiScanReceiver = new WifiScanReceiver();
        wifiStatusReceiver = new WifiStatusReceiver();
        wifiScanIntentFilter = new IntentFilter(
                WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
        wifiStatusIntentFilter = new IntentFilter(
                WifiManager.WIFI_STATE_CHANGED_ACTION);
        wifihandler = new MemberWifiHandler();
        mHandler.postDelayed(minitWifi,1000);
        //Gps
        gpsStatusStringBuilder = new StringBuilder(LENGTH_LOCATION_STRING);
        gpsSatellitePrnStringBuilder = new StringBuilder(
                LENGTH_SATELLITE_PRN_STRING);
        gpsSatelliteSnrStringBuilder = new StringBuilder(
                LENGTH_SATELLITE_SNR_STRING);

        locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        gpsStatusListener = new MemberGpsStatusListener();
        locationListener = new MemberLocationListener();
        locationManager.addGpsStatusListener(gpsStatusListener);
        gpshandler = new MemberGpsHandler();
        setViewState(GpsState.INITIALIZE_WAIT);
        locationManager.sendExtraCommand(LocationManager.GPS_PROVIDER,
                "delete_aiding_data", null);
        isGps = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        MMILog.e(TAG, "Beginning...  isGps = "+isGps);
        mHandler.postDelayed(minitGps, 2000);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                LENGTH_NOTIFICATION_INTERVAL_TIME,
                LENGTH_NOTIFICATION_INTERVAL_DISTANCE, locationListener);
        gpshandler.sendEmptyMessageDelayed(MESSAGE_GPS_DETECT_COMPLETED,
                LENGTH_GPS_DETECT_TIMEOUT);
    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub
        super.resume();
        // Sensor event
        mSensorManager.registerListener(this, mAccSensor,
                SensorManager.SENSOR_DELAY_NORMAL);
//        if( product.compareToIgnoreCase(Value.PRO_NAME_TEN)!=0 &&
//                product.compareToIgnoreCase(Value.PRO_NAME_ELEVEN)!=0 ){
            mSensorManager.registerListener(this, mMAGSensor,
                    SensorManager.SENSOR_DELAY_NORMAL);
//        }
        mSensorManager.registerListener(this, lsensor,
                SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(this, mproximity,
                SensorManager.SENSOR_DELAY_NORMAL);
        mContext.registerReceiver(broadcastReceiver, intentFilter);
        registerBluetoothReceivers();
        registerWifiReceivers();
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub
        super.pause();
        if (mSensorManager != null) {
            mSensorManager.unregisterListener(this,mAccSensor);
            mSensorManager.unregisterListener(this,mMAGSensor);
            mSensorManager.unregisterListener(this,lsensor);
            mSensorManager.unregisterListener(this,mproximity);
        }
        mContext.unregisterReceiver(broadcastReceiver);
        unregisterBluetoothReceivers();
        unregisterWifiReceivers();
    }

    @Override
    public void destroy() {
        // TODO Auto-generated method stub
        super.destroy();
        mHandler.removeCallbacks(minitBluetooth);
        mHandler.removeCallbacks(minitWifi);
        mHandler.removeCallbacks(minitGps);
        loadBluetoothOldState();
        removeAllWifiMessages();
        enableAllAps();
        loadWifiOldState();
        locationManager.removeGpsStatusListener(gpsStatusListener);
        locationManager.removeUpdates(locationListener);
        isGps = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        MMILog.e(TAG, "End...  isGps = "+isGps);
        if (isGps)
            GpsOff();
        MMILog.e(TAG, "onDestroy");
    }

    private boolean isLatam(){
        int usb = -1;
        if (AreaIdManager != null) {
            AreaIdManager.setFileInputStream(AreaIdManager
                    .fileInputOpen(SysClassManager.AREA_ID));
            usb = AreaIdManager.readFileInputStream();
            AreaIdManager.fileInputClose();
        }
        if (usb == 49) {
            return true;
        }
        return false;
    }

    private Runnable minitBluetooth = new Runnable() {

        @Override
        public void run() {
             // TODO Auto-generated method stub
            requestBluetoothRestart();
        }
     };
    private Runnable minitWifi = new Runnable() {

        @Override
        public void run() {
             // TODO Auto-generated method stub
            initWifi();
        }
     };
     private Runnable minitGps = new Runnable() {

         @Override
         public void run() {
              // TODO Auto-generated method stub
             if (!isGps)
                 GpsOn();
         }
      };

    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public void onSensorChanged(SensorEvent event) {
        int type = event.sensor.getType();
        float[] value = event.values;
        String info = "";
        if(type==Sensor.TYPE_ACCELEROMETER){
            compass_acceValues=event.values;
        }
//        if( product.compareToIgnoreCase(Value.PRO_NAME_TEN)!=0 &&
//                product.compareToIgnoreCase(Value.PRO_NAME_ELEVEN)!=0 ){
            if(type==Sensor.TYPE_MAGNETIC_FIELD){
                compass_magValues=event.values;
            }
            SensorManager.getRotationMatrix(compass_rotate, null, compass_acceValues, compass_magValues);
            SensorManager.getOrientation(compass_rotate, compass_vals);
            //经过SensorManager.getOrientation(rotate, values);得到的values值为弧度
            //转换为角度
            compass_vals[0]=(float)Math.toDegrees(compass_vals[0]);
            info = "[E-Compass]";
            if (compass_vals[0] < 0) {
                info = info +  String.format("Angle: %.1f", -compass_vals[0]);
            } else {
                info = info +  String.format("Angle: -%.1f", compass_vals[0]);
            }
            mcompassTextView.setText(info);

            if(!mcompassTextView.isEnabled()){
                mcompassTextView.setEnabled(true);
            }
//        }

            switch (type) {
                case Sensor.TYPE_ACCELEROMETER:
                    mDir = new DDDVect(value[0], value[1],
                            value[2]);
                    float mAngleToYaxis = mDir.getYAngle();
                    float mAngleToXaxis = mDir.getXAngle();
                    info = "[G-Sensor]";
                    info = info + String.format("V: %.2f", mAngleToYaxis)
                            + String.format(", H: %.2f", mAngleToXaxis);
                    mgsensortTextView.setText(info);
                    if(!mgsensortTextView.isEnabled()){
                        mgsensortTextView.setEnabled(true);
                    }
                    break;
                case Sensor.TYPE_LIGHT:
                    info = "[Light Sensor]";
                    info = info +  "ambient light is " + value[0];
                    if (value[0] > HIGH ) {
                        mIsbright = true;
                    }else if (value[0] < LOW) {
                        mIsdark = true;
                    }
                    if (mIsbright) {
                        info = info + "/Bright:OK ";
                    }else{
                        info = info + "/Bright:not tested ";
                    }
                    if (mIsdark) {
                        info = info + "/Dark: OK";
                    }else{
                        info = info + "/Dark: not tested";
                    }
                    if (value[0] > HIGH ) {
                        info = info + "/current:Bright";
                    }else if (value[0] < LOW) {
                        info = info + "/current:Dark";
                    }
                    mlightsensorTextView.setText(info);
                    if(!mlightsensorTextView.isEnabled()){
                        mlightsensorTextView.setEnabled(true);
                    }
                    break;
                case Sensor.TYPE_PROXIMITY:
                    info = "[Proximity Sensor]";
                    float result = value[SensorManager.DATA_X];
                    if (result == 0) {
                        mIsnear = true;
                    } else if (result >= 1.0f) {
                        mIsfar = true;
                    }
                    if(mIsnear){
                        info = info + "Near: OK";
                    }else {
                        info = info + "Near: not tested";
                    }
                    if(mIsfar){
                        info = info + "/Far: OK";
                    }else {
                        info = info + "/Far: not tested";
                    }
                    if (result == 0) {
                        info = info + "/current:Near";
                    } else if (result >= 1.0f) {
                        info = info + "/current:Far";
                    }
                    mproximityTextView.setText(info);
                    if(!mproximityTextView.isEnabled()){
                        mproximityTextView.setEnabled(true);
                    }
                    break;
                default:
                    break;
            }
    }

    //SIM
    private void getSimCardState() {
        TelephonyManager tm = TelephonyManager.getDefault();
        String info="[SIM]";
        int simcount = tm.getPhoneCount();
        int oKflag = 0;
        String simStates[] = new String[2];
        simStates[0] = "";
        simStates[1] = "";

        for (int i = 0; i < simcount; i++) {
            int simcard = i + 1;

            switch (tm.getSimState(i)) {
            case TelephonyManager.SIM_STATE_ABSENT:
                simStates[i] = getString(R.string.sim_state_missing, simcard);
                break;
            case TelephonyManager.SIM_STATE_READY:
                simStates[i] = getString(R.string.sim_state_ok, simcard);
                oKflag++;
                break;

            case TelephonyManager.SIM_STATE_PIN_REQUIRED:
                /**
                 * SIM card state: Locked: requires the user's SIM PUK to unlock
                 */
            case TelephonyManager.SIM_STATE_PUK_REQUIRED:
                /** SIM card state: Locked: requires a network PIN to unlock */
            case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
                simStates[i] = getString(R.string.sim_state_deteected, simcard);
                break;

            case TelephonyManager.SIM_STATE_CARD_IO_ERROR:
                simStates[i] = getString(R.string.sim_state_ioerror, simcard);
                break;

            case TelephonyManager.SIM_STATE_UNKNOWN:
                /**
                 * SIM card state: Locked: requires the user's SIM PIN to unlock
                 */
            default:
                simStates[i] = getString(R.string.sim_state_unknown, simcard);
                oKflag--;
                break;
            }
        }
        if (oKflag == -simcount) {
            info = info + getString(R.string.sim_state_2nd_unknown);
            msimtextviewTextView.setText(info);
            msimtextviewTextView.setEnabled(true);
        } else {
            String strvar=null;
            if(simcount == 2){
                strvar=simStates[0] + "/" + simStates[1];
            }else{
                strvar=simStates[0];
                strvar=strvar.replace("1", "");
            }
            info = info +strvar;
            msimtextviewTextView.setText(info);
            if(!msimtextviewTextView.isEnabled()){
                msimtextviewTextView.setEnabled(true);
            }
        }
    }
    //Sensor:gensor
    class DDDVect {
        float Vx;
        float Vy;
        float Vz;

        DDDVect(float x, float y, float z) {
            Vx = x;
            Vy = y;
            Vz = z;
        }

        public float getYAngle() {
            return getAngle(Vy);
        }

        public float getXAngle() {
            return getAngle(Vx);
        }

        public float getZAngle() {
            return getAngle(Vz);
        }

        private float getAngle(float ref) {
            return (float) Math.toDegrees(Math.acos(ref
                    / Math.sqrt(Vx * Vx + Vy * Vy + Vz * Vz)));
        }
    }

     //Temperature
    private class MemberReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context arg0, Intent intent) {
            float voltage=(float)intent.getIntExtra(
             BatteryManager.EXTRA_VOLTAGE,VALUE_VOLTAGE_DEFALUT);
            int rawlevel=intent.getIntExtra("level",-1);
            int scale=intent.getIntExtra("scale",-1);
            String percent="";
            if(rawlevel>=0 && scale>0){
                percent=numformat.format((rawlevel*1.0)/scale);
            }
            mTemperatureTextView.setText(getStateString(intent,voltage,percent));
            if(!mTemperatureTextView.isEnabled()){
                mTemperatureTextView.setEnabled(true);
            }
        }
    }

    private String getStateString(Intent intent,float level,String percent) {
        return String.format(batteryStr, getTemperature(intent),level,percent);
    }

    private int getTemperature(Intent intent) {
        int temperature = (intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE,
                VALUE_TEMPERATURE_DEFALUT))/10;
        return temperature;//temperature /= VALUE_TEMPERATURE_DEFAULT_UNIT;
    }

    //Bluetooth
    private void saveBluetoothOldState() {
        isBluetoothOldState = bluetoothAdapter.isEnabled();
    }

    private void setViewState(BluetoothState state) {
        String info ="";
        switch (state) {
        case INIT_WAIT:
            info = "[Bluetooth]" + getString(R.string.bluetooth_init_wait);
            mBluetoothtTextView.setText(info);
            break;
        case INIT_FAIL:
            info = "[Bluetooth]" + getString(R.string.bluetooth_init_fail);
            mBluetoothtTextView.setText(info);
            break;
        case DETECT_WAIT:
            if(!mBluetoothtTextView.isEnabled() && btitems != ""){
                mBluetoothtTextView.setEnabled(true);
            }
            if(btitems != ""){
                info = "[Bluetooth]" + btitems;
            }else{
                info = "[Bluetooth]" + getString(R.string.bluetooth_detect_wait) + btitems;
            }
            mBluetoothtTextView.setText(info);
            break;
        case DETECT_FAIL:
            info = "[Bluetooth]" + getString(R.string.bluetooth_detect_fail);
            mBluetoothtTextView.setText(info);
            if(!mBluetoothtTextView.isEnabled()){
                mBluetoothtTextView.setEnabled(true);
            }
            break;
        case DETECT_SUCCESS:
            info = "[Bluetooth]" + btitems;
            if(!mBluetoothtTextView.isEnabled() && btitems != ""){
                mBluetoothtTextView.setEnabled(true);
            }
            mBluetoothtTextView.setText(info);
            break;
        }

    }

    private void restartBluetooth() {
        btitems = "";
        btlistnames.clear();
        bluetoothAdapter.enable();
        mBluetoothtTextView.setEnabled(false);
    }

    private class MemberBluetoothHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
            case MESSAGE_BLUETOOTH_RESTART:
                if (bluetoothAdapter.getState() == BluetoothAdapter.STATE_OFF) {
                    restartBluetooth();
                } else {
                    Bthandler.sendEmptyMessageDelayed(MESSAGE_BLUETOOTH_RESTART,
                            LENGHT_BLUETOOTH_CHECK_TIME);
                }
                break;
            }
        }
    }

    private void loadBluetoothOldState() {
        if (isBluetoothOldState) {
            bluetoothAdapter.enable();
        } else {
            bluetoothAdapter.cancelDiscovery();
            bluetoothAdapter.disable();
        }
    }

    private void registerBluetoothReceivers() {
        mContext.registerReceiver(bluetoothStateReceiver, bluetoothStateIntentFilter);
        mContext.registerReceiver(bluetoothFindReceiver, bluetoothFindIntentFilter);
    }

    private void unregisterBluetoothReceivers() {
        mContext.unregisterReceiver(bluetoothStateReceiver);
        mContext.unregisterReceiver(bluetoothFindReceiver);
    }

    private void requestBluetoothRestart() {
        setViewState(BluetoothState.INIT_WAIT);
        bluetoothAdapter.disable();
        Bthandler.sendEmptyMessageDelayed(MESSAGE_BLUETOOTH_RESTART,
                LENGHT_BLUETOOTH_CHECK_TIME);
    }

    class BluetoothStateReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context c, Intent intent) {
            final String action = intent.getAction();
            if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
                final int state = intent.getIntExtra(
                        BluetoothAdapter.EXTRA_STATE,
                        BluetoothAdapter.STATE_OFF);

                switch (state) {
                case BluetoothAdapter.STATE_ON:
                    setViewState(BluetoothState.INIT_WAIT);
                    bluetoothAdapter.startDiscovery();
                    break;
                case BluetoothAdapter.STATE_TURNING_ON:
                case BluetoothAdapter.STATE_TURNING_OFF:
                case BluetoothAdapter.STATE_OFF:
                    setViewState(BluetoothState.INIT_WAIT);
                    break;
                default:
                    setViewState(BluetoothState.INIT_FAIL);
                    break;
                }
            }
        }
    }

    class BluetoothFindReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context c, Intent intent) {
            final String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                setViewState(BluetoothState.DETECT_WAIT);
                BluetoothDevice device = intent
                        .getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                String strDevice = device.getAddress();
                short rssi = intent.getShortExtra(BluetoothDevice.EXTRA_RSSI,
                        Short.MIN_VALUE);
                if(!btlistnames.contains(strDevice)){
                    btitems = btitems + strDevice + "&" + rssi + "/";
                    btlistnames.add(strDevice);
                }
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED
                    .equals(action)) {
                if (btitems == "") {
                    setViewState(BluetoothState.DETECT_FAIL);
                } else {
                    setViewState(BluetoothState.DETECT_SUCCESS);
                }
            }
        }
    }

    //Wifi

    private void setWifiScanList() {
        List<ScanResult> wifiRawScanResultList = wifiManager.getScanResults();

        if (wifiRawScanResultList != null) {
            for (ScanResult scanResult : wifiRawScanResultList) {
                String strName = scanResult.SSID.toString();
                if(!wifilistnames.contains(strName)){
                    wifiitems = wifiitems + strName + "," + scanResult.level + "/";
                    wifilistnames.add(strName);
                }
            }
        }
        sendWifiDetectCompletedMessage();
    }

    private class WifiScanReceiver extends BroadcastReceiver {
        public void onReceive(Context context, Intent intent) {
            setWifiScanList();
        }
    }

    private class WifiStatusReceiver extends BroadcastReceiver {
        public void onReceive(Context context, Intent intent) {
            final int wifiState = intent.getIntExtra(
                    WifiManager.EXTRA_WIFI_STATE,
                    WifiManager.WIFI_STATE_UNKNOWN);

            switch (wifiState) {
            case WifiManager.WIFI_STATE_DISABLING:
            case WifiManager.WIFI_STATE_DISABLED:
            case WifiManager.WIFI_STATE_ENABLING:
                setViewState(WifiMessage.WIFI_INIT_WAIT);
                break;
            case WifiManager.WIFI_STATE_ENABLED:
                setViewState(WifiMessage.WIFI_DETECT_WAIT);
                disableAllAps();
                wifiManager.disconnect();
                wifiManager.startScan();
                break;
            case WifiManager.WIFI_STATE_UNKNOWN:
                setViewState(WifiMessage.WIFI_INIT_FAIL);
                break;
            }
        }
    }

    //[BUGFIX] START, take form m823 pr#1011441
    private void disableAllAps() {
        List<WifiConfiguration> configs = wifiManager.getConfiguredNetworks();
        if (null != configs) {
            for (WifiConfiguration config : configs) {
                if (config.networkId != -1 &&
                     config.status == WifiConfiguration.Status.ENABLED) {
                    wifiManager.disableNetwork(config.networkId);
                }
            }
        }
    }

    private void enableAllAps() {
        List<WifiConfiguration> configs = wifiManager.getConfiguredNetworks();
        if (null != configs) {
            for (WifiConfiguration config : configs) {
                if ( config.networkId != -1 &&
                    config.status == WifiConfiguration.Status.DISABLED) {
                    wifiManager.enableNetwork(config.networkId,false);
                }
            }
        }
    }
    //[BUGFIX] END

    private void setViewState(WifiMessage wifiMessage) {
        String info = "";
        switch (wifiMessage) {
        case WIFI_INIT_WAIT:
            info = "[WIFI]" + getString(R.string.wifi_init_wait);
            mWifiTextView.setText(info);
            break;
        case WIFI_INIT_FAIL:
            info = "[WIFI]" + getString(R.string.wifi_init_fail);
            mWifiTextView.setText(info);
            break;
        case WIFI_DETECT_WAIT:
            if(wifiitems != ""){
                info = "[WIFI]" + wifiitems;
            }else{
                info = "[WIFI]" + getString(R.string.wifi_detect_wait) + wifiitems;
            }
            mWifiTextView.setText(info);
            if(!mWifiTextView.isEnabled() && wifiitems != ""){
                mWifiTextView.setEnabled(true);
            }
            break;
        case WIFI_DETECT_FAIL:
            info = "[WIFI]" + getString(R.string.wifi_detect_fail);
            mWifiTextView.setText(info);
            if(!mWifiTextView.isEnabled()){
                mWifiTextView.setEnabled(true);
            }
            break;
        case WIFI_DETECT_SUCCESS:
            info = "[WIFI]" + wifiitems;
            mWifiTextView.setText(info);
            if(!mWifiTextView.isEnabled() && wifiitems != ""){
                mWifiTextView.setEnabled(true);
            }
            break;
        }
    }

    private void setWifiStartScan() {
        wifiitems = "";
        wifilistnames.clear();
        mWifiTextView.setEnabled(false);
        wifiManager.setWifiEnabled(true);
        wifiManager.startScan();
    }

    private void setViewStateForWifiScanResult() {
        if (wifiitems !="") {
            setViewState(WifiMessage.WIFI_DETECT_SUCCESS);
        } else {
            setViewState(WifiMessage.WIFI_DETECT_FAIL);
        }
    }

    private boolean isWifiDisabled() {
        if (wifiManager.getWifiState() == WifiManager.WIFI_STATE_DISABLED) {
            return true;
        } else {
            return false;
        }
    }

    private void sendWifiRestartMessage() {
        if (isWifiDisabled()) {
            setWifiStartScan();
        } else {
            wifihandler.sendEmptyMessageDelayed(MESSAGE_WIFI_RESTART_REQUEST,
                    LENGTH_WIFI_CHECK_TIME);
        }
    }

    private void sendWifiDetectCompletedMessage() {
        wifihandler.removeMessages(MESSAGE_WIFI_DETECT_COMPLETED);
        wifihandler.sendEmptyMessageDelayed(MESSAGE_WIFI_DETECT_COMPLETED,
                LENGTH_WIFI_DETECT_TIMEOUT);
    }

    private class MemberWifiHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
            case MESSAGE_WIFI_DETECT_COMPLETED:
                setViewStateForWifiScanResult();
                break;
            case MESSAGE_WIFI_RESTART_REQUEST:
                sendWifiRestartMessage();
                break;
            }
        }
    }

    private void requestWifiRestart() {
        wifiManager.setWifiEnabled(false);
        sendWifiRestartMessage();
    }

    private void saveWifiOldState() {
        isOldWifiEnable = wifiManager.isWifiEnabled();
    }

    private void loadWifiOldState() {
        wifiManager.setWifiEnabled(isOldWifiEnable);
    }

    private void initWifi() {
        saveWifiOldState();
        requestWifiRestart();
    }

    private void removeAllWifiMessages() {
        wifihandler.removeMessages(MESSAGE_WIFI_RESTART_REQUEST);
        wifihandler.removeMessages(MESSAGE_WIFI_DETECT_COMPLETED);
    }

    private void registerWifiReceivers() {
        mContext.registerReceiver(wifiStatusReceiver, wifiStatusIntentFilter);
        mContext.registerReceiver(wifiScanReceiver, wifiScanIntentFilter);
    }

    private void unregisterWifiReceivers() {
        mContext.unregisterReceiver(wifiScanReceiver);
        mContext.unregisterReceiver(wifiStatusReceiver);
    }
    //Gps


    private void updateSatelliteStatus(GpsStatus gpsStatus) {
        gpsSatelliteCount = 0;
        gpsSatellitePrnStringBuilder.setLength(0);
        gpsSatelliteSnrStringBuilder.setLength(0);
        Iterable<GpsSatellite> satelliteiIterable = gpsStatus.getSatellites();

        for (GpsSatellite gpsSatellite : satelliteiIterable) {
            if (gpsSatellite.getSnr() != 0) {
                gpsSatelliteCount++;
                gpsSatellitePrnStringBuilder.append(gpsSatellite.getPrn()).append(
                        "/");
                gpsSatelliteSnrStringBuilder // add by anshu.zhou for bug[244137]
                        .append(gpsSatellite.getSnr()).append("/");
            }
        }
    }

    private GpsStatus getGpsStatusUpdated() {
        return locationManager.getGpsStatus(currentGpsStatus);
    }

    private Location getLocationStatusUpdated() {
        if(currentLocation != null){
            return currentLocation;
        }else{
            Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            return location;
        }
    }

    private String getMessageString(int resourceId) {
        gpsStatusStringBuilder.setLength(0);
        gpsStatusStringBuilder.append(getString(resourceId));
        return gpsStatusStringBuilder.toString();
    }

    private String getMessageString(GpsStatus gpsStatus) {
        updateSatelliteStatus(gpsStatus);
        String location = getMessageString(getLocationStatusUpdated());

        gpsStatusStringBuilder.setLength(0);
        gpsStatusStringBuilder
                .append(gpsSatelliteCount + getString(R.string.gps_satellite) )
                .append("\n")
                .append(getString(R.string.gps_prn)
                        + gpsSatellitePrnStringBuilder.toString())
                .append("\n")
                .append(getString(R.string.gps_snr)
                        + gpsSatelliteSnrStringBuilder.toString())
                .append("\n")
                .append("Location:"+location)
                .append("\n")
                .append(locationUpdateCount + getString(R.string.gps_track))
                .append("\n")
                .append(getString(R.string.gps_status_update)
                        + gpsStatusUpdateCount);
        return gpsStatusStringBuilder.toString();
    }

    private String getMessageString(Location location) {
        if (location == null) {
            return getMessageString(R.string.gps_init_fail);
        }

        gpsStatusStringBuilder.setLength(0);
        gpsStatusStringBuilder
                .append("Longitude:").append(String.valueOf(location.getLongitude()))
                .append("/Latitude:").append(String.valueOf(location.getLatitude()));
        return gpsStatusStringBuilder.toString();
    }

    private void setViewState(GpsState state) {
         String info = "";
        switch (state) {
        case INITIALIZE_WAIT:
            info = "[GPS]" + getMessageString(R.string.gps_init_wait) + "Initializing";
            mgpstTextView.setText(info);
            MMILog.e(TAG, "GPS INITIALIZE_WAIT");
            break;
        case INITIALIZE_SUCCESS:
            failCount = 0;
            gpsinfo = getMessageString(getGpsStatusUpdated());
            info = "[GPS]" + gpsinfo;
            mgpstTextView.setText(info);
            if(!mgpstTextView.isEnabled() && gpsinfo != ""){
                mgpstTextView.setEnabled(true);
            }
            MMILog.e(TAG, "GPS INITIALIZE_SUCCESS");
            break;
        case INITIALIZE_FAIL:
            MMILog.e(TAG, "GPS INITIALIZE_FAIL Try again !");
            if (failCount > 0) {
                retest();
                failCount--;
                break;
            }
            info = "[GPS]" + getMessageString(R.string.gps_init_fail);
            mgpstTextView.setText(info);
            if(!mgpstTextView.isEnabled()){
                mgpstTextView.setEnabled(true);
            }
            MMILog.e(TAG, "GPS INITIALIZE_FAIL");
            break;
        }
    }

    private void retest() {
        locationManager.removeGpsStatusListener(gpsStatusListener);
        locationManager.removeUpdates(locationListener);
        gpsinfo = "";
        mgpstTextView.setEnabled(false);
        gpsStatusUpdateCount = 0;
        locationUpdateCount = 0;
        gpsSatelliteCount = 0;

        setViewState(GpsState.INITIALIZE_WAIT);

        gpsStatusStringBuilder.setLength(0);
        gpsSatellitePrnStringBuilder.setLength(0);

        gpshandler.sendEmptyMessageDelayed(MESSAGE_GPS_DETECT_COMPLETED,
                LENGTH_GPS_DETECT_TIMEOUT);

        locationManager.addGpsStatusListener(gpsStatusListener);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                LENGTH_NOTIFICATION_INTERVAL_TIME,
                LENGTH_NOTIFICATION_INTERVAL_DISTANCE, locationListener);
    }
    private class MemberGpsHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
            case MESSAGE_GPS_DETECT_COMPLETED:
                if (gpsinfo == "") {
                    setViewState(GpsState.INITIALIZE_FAIL);
                }
                break;
            }
        }
    }

    private class MemberGpsStatusListener implements GpsStatus.Listener {
        @Override
        public void onGpsStatusChanged(int event) {
            switch (event) {
            case GpsStatus.GPS_EVENT_STARTED:
                MMILog.e(TAG, "GPS_EVENT_STARTED");
                break;
            case GpsStatus.GPS_EVENT_STOPPED:
                MMILog.e(TAG, "GPS_EVENT_STOPPED");
                break;
            case GpsStatus.GPS_EVENT_FIRST_FIX:
                setViewState(GpsState.INITIALIZE_WAIT);
                MMILog.e(TAG, "GPS_EVENT_FIRST_FIX");
                break;
            case GpsStatus.GPS_EVENT_SATELLITE_STATUS:
                gpsStatusUpdateCount++;
                setViewState(GpsState.INITIALIZE_SUCCESS);
                MMILog.e(TAG, "GPS_EVENT_SATELLITE_STATUS");
                break;
            }
        }
    }

    public class MemberLocationListener implements LocationListener {
        public void onLocationChanged(Location location) {
            locationUpdateCount++;
            currentLocation = location;
        }

        public void onProviderDisabled(String provider) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    }

    private void GpsOn() {
        Settings.Secure.setLocationProviderEnabled(mContext.getContentResolver(),
                LocationManager.GPS_PROVIDER, true);
        MMILog.e("GPS_STATE", "GpsOn");
    }

    private void GpsOff() {
        Settings.Secure.setLocationProviderEnabled(mContext.getContentResolver(),
                LocationManager.GPS_PROVIDER, false);

        MMILog.e("GPS_STATE", "GpsOff");
    }

    private final String getString(int resId) {
        return mContext.getString(resId);
    }

    public final String getString(int resId, Object... formatArgs) {
        return mContext.getString(resId, formatArgs);
    }
}
