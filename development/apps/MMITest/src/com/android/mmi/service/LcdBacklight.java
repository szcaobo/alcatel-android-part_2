/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/KeypadBacklight.java*/
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 15/01/15| Shishun.Liu    |                    | create for test in selinux */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi.service;

import com.android.mmi.util.MMILog;

import android.content.ContentResolver;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
//import android.os.IHardwareService;
import android.os.Message;
import android.os.Messenger;
import android.os.PowerManager;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.provider.Settings;

public class LcdBacklight {
    private String TAG = "MMITest.Service_LcdBacklight";
    private boolean mLightEnabled = true;
//    private IHardwareService mLight;
    private int mCount = 1;
    private final int COUNT_MAX = 7;

    private int mSystemScreenBrightMode;
    private int mSystemScreenBrigntness;
    private PowerManager mPowerManager;

    private Handler mHandler = new Handler();

    private Messenger mClientMessenger;
    private ContentResolver mContentResolver;

    public LcdBacklight(Context context) {
        mContentResolver = context.getContentResolver();
        mPowerManager = (PowerManager) context
                .getSystemService(Context.POWER_SERVICE);
        mSystemScreenBrightMode = Settings.System.getInt(mContentResolver,
                Settings.System.SCREEN_BRIGHTNESS_MODE, -1);
    }

    public void setMessenger(Messenger toClientMsg) {
        mClientMessenger = toClientMsg;
    }

    private void sendStatus(boolean isPass) {
        if (mClientMessenger != null) {
            Message toClientMsg = new Message();
            Bundle bundle = new Bundle();
            bundle.putBoolean("ispass", isPass);
            toClientMsg.setData(bundle);
            try {
                MMILog.d(TAG, "service begin send msg to client");
                mClientMessenger.send(toClientMsg);
            } catch (RemoteException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

/*    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            // TODO Auto-generated method stub
            mCount++;
            MMILog.d(TAG, " " + mCount + " " + COUNT_MAX);
            if (mCount <= COUNT_MAX) {
                if (mPowerManager.isInteractive()) {
                    try {
                        MMILog.d(TAG, "set lcd ligth on/off");
                        mLight.setLCDLightEnabled((mCount % 2) == 1);
                    } catch (RemoteException e) {
                        MMILog.e(TAG,
                                "remote call for turn on lcd light failed.");

                        sendStatus(false);

                        return;
                    }
                } else {
                    MMILog.d(TAG, "isInteractive=false, skip lcd setting");
                }
                if (mCount < COUNT_MAX) {
                    mHandler.postDelayed(mRunnable, 1000);
                } else {
                    sendStatus(true);
                }
            }
        }
    };*/

    public void run() {
        // TODO Auto-generated method stub
        mSystemScreenBrightMode = Settings.System.getInt(mContentResolver,
                Settings.System.SCREEN_BRIGHTNESS_MODE, -1);
        if (mSystemScreenBrightMode == Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL) {
            mSystemScreenBrigntness = Settings.System.getInt(mContentResolver,
                    Settings.System.SCREEN_BRIGHTNESS, -1);
        }

/*        mLight = IHardwareService.Stub.asInterface(ServiceManager
                .getService("hardware"));
        mLightEnabled = (mLight != null);
        MMILog.d(TAG, "mLightEnabled = " + mLightEnabled);
        sendStatus(false);
        if (mLightEnabled) {
            // Start LCD backlight flash
            mHandler.postDelayed(mRunnable, 1000);
        }*/
    }

    public void resume() {
        // TODO Auto-generated method stub
        // Disable auto brightness mode
        if (mSystemScreenBrightMode == Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC) {
            Settings.System.putInt(mContentResolver,
                    Settings.System.SCREEN_BRIGHTNESS_MODE,
                    Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);
        }
    }

    public void pause() {
        // TODO Auto-generated method stub
/*        if (mLightEnabled) {
            mHandler.removeCallbacks(mRunnable);
            try {
                mLight.setLCDLightEnabled(true);
            } catch (RemoteException e) {
                MMILog.e(TAG, "remote call for turn on lcd light failed.");
            }
        }*/

        if (mSystemScreenBrightMode == Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC) {
            Settings.System.putInt(mContentResolver,
                    Settings.System.SCREEN_BRIGHTNESS_MODE,
                    mSystemScreenBrightMode);
        } else {
            Settings.System.putInt(mContentResolver,
                    Settings.System.SCREEN_BRIGHTNESS, mSystemScreenBrigntness);
        }
    }
}
