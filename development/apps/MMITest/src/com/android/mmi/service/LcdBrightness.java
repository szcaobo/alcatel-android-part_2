package com.android.mmi.service;

import android.content.ContentResolver;
import android.content.Context;
import android.provider.Settings;

public class LcdBrightness {

    private ContentResolver mContentResolver;
    private int mSystemScreenBrightMode;
    private int mSystemScreenBrigntness = -1;

    public LcdBrightness(Context context) {
        mContentResolver = context.getContentResolver();
        mSystemScreenBrightMode = Settings.System.getInt(mContentResolver,
                Settings.System.SCREEN_BRIGHTNESS_MODE, -1);
    }

    public void setLcdBrightness(int value) {
        if (mSystemScreenBrightMode == Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL) {
            mSystemScreenBrigntness = Settings.System.getInt(mContentResolver,
                    Settings.System.SCREEN_BRIGHTNESS, -1);
        } else if (mSystemScreenBrightMode == Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC) {
            Settings.System.putInt(mContentResolver,
                    Settings.System.SCREEN_BRIGHTNESS_MODE,
                    Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);
        }
        Settings.System.putInt(mContentResolver,
                Settings.System.SCREEN_BRIGHTNESS, value);
    }

    public void resetLcdBrightness() {
        if (mSystemScreenBrightMode == Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC) {
            Settings.System.putInt(mContentResolver,
                    Settings.System.SCREEN_BRIGHTNESS_MODE,
                    mSystemScreenBrightMode);
        } else {
            if (mSystemScreenBrigntness != -1) {
                Settings.System.putInt(mContentResolver,
                        Settings.System.SCREEN_BRIGHTNESS,
                        mSystemScreenBrigntness);
            }
        }
    }
}
