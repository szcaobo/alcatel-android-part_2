/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/MicroSD.java       */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/08/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import com.android.mmi.R;
import com.android.mmi.Value;
import com.android.mmi.util.MMILog;

import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.os.storage.StorageEventListener;
import android.os.storage.StorageManager;
import android.os.storage.StorageVolume;
import libcore.io.IoUtils;

public class MicroSD {
    private String TAG = "MMITest.Service_MicroSD";

    static final String STRING_TESTFILE_NAME = "/test.txt";
    public static final byte VALUE_TEST_BYTE = 9;

    public static String SD_DIR = "/storage/sdcard1";

    private StorageManager storageManager;
    private boolean isReadDataSame;

    private StorageEventListener storageEventListener;

    protected int mUserSelectMode = Value.USER_SELECT_MODE_UNKNOW;

    private Context mContext;

    private Messenger mClientMessenger;

    public MicroSD(Context context) {
        mContext = context;
    }

    public void setMessenger(Messenger toClientMsg) {
        mClientMessenger = toClientMsg;
    }

    public void run() {
        // TODO Auto-generated method stub
        storageManager = (StorageManager) mContext.getSystemService(Context.STORAGE_SERVICE);

        StorageVolume[] storageVolumeList = storageManager.getVolumeList();
        if (storageVolumeList != null) {
            for (StorageVolume volume : storageVolumeList) {
                String path = volume.getPath();
                boolean removeable = volume.isRemovable();
                MMILog.d(TAG, "path: " + path + " removeable: " + removeable);
                if(removeable){
                    SD_DIR = path;
                    break;
                }
            }
        }

        storageEventListener = new MemberStorageEventListener();

        runTest();

        storageManager.registerListener(storageEventListener);
    }

    private void runTest() {
        if (isSDcardMounted()) {
            MMILog.d(TAG, "RESULT:SD CARD ALREADY MOUNTED");
            if (startFileReadTest()) {
                MMILog.d(TAG, "RESULT:WRITE/READ OK");
                sendStatus(mContext.getString(R.string.sdcard_read_success), true);
            } else {
                MMILog.e(TAG, "RESULT:WRITE/READ FAIL");
                sendStatus(mContext.getString(R.string.sdcard_read_fail), false);
            }
        } else {
            MMILog.d(TAG, "RESULT:NO SD CARD");
            sendStatus(mContext.getString(R.string.sdcard_mount_fail), false);
        }
    }

    public void destroy() {
        storageManager.unregisterListener(storageEventListener);
    }

    private boolean isSDcardMounted() {
        String state = "";
        state=storageManager.getVolumeState(SD_DIR);
        MMILog.d(TAG, "storagevolume:"+state);
        if (state.equals(
                Environment.MEDIA_MOUNTED)) {
            return true;
        } else {
            return false;
        }
    }

    /*
     * *************************************************************************
     * **** METHOD process external file's life cycle
     * ********************************
     * *******************************************
     */
    private File setExternalFileCreate(String path) throws IOException {

        File externalFile = new File(path);

        if (!externalFile.exists()) {
            MMILog.d(TAG, "before create!");
            externalFile.createNewFile();
            MMILog.d(TAG, "end create!");
        }
        if (externalFile.exists()) {
            MMILog.d(TAG, "create sicess!");
        } else
            MMILog.d(TAG, "create fail!");
        return externalFile;
    }

    private File setExternalFileDelete(File externalFile) throws IOException {
        if (externalFile.exists()) {
            externalFile.delete();
        }
        return externalFile;
    }

    private FileOutputStream setExternalFileWrite(File externalFile) throws FileNotFoundException, IOException {
        FileOutputStream externalFileOutputStream;
        externalFileOutputStream = new FileOutputStream(externalFile);
        externalFileOutputStream.write(VALUE_TEST_BYTE);
        externalFileOutputStream.close();
        return externalFileOutputStream;
    }

    private FileInputStream setExternalFileRead(File externalFile) throws FileNotFoundException, IOException {
        FileInputStream externalFileInputStream;
        externalFileInputStream = new FileInputStream(externalFile);
        isReadDataSame = isReadDataSame(externalFileInputStream.read());
        externalFileInputStream.close();
        return externalFileInputStream;
    }

    /*
     * *************************************************************************
     * **** METHOD check external memory read test
     * ***********************************
     * ****************************************
     */
    private boolean startFileReadTest() {
        String filePath;
        File externalFile = null;
        FileInputStream externalFileInputStream = null;
        FileOutputStream externalFileOutputStream = null;

        if (!isSDcardMounted()) {
            return false;
        }

        filePath = SD_DIR + STRING_TESTFILE_NAME;

        try {
            externalFile = setExternalFileCreate(filePath);
            externalFileOutputStream = setExternalFileWrite(externalFile);
            MMILog.d(TAG, "write");
            externalFileInputStream = setExternalFileRead(externalFile);
            MMILog.d(TAG, "read");
            externalFile = setExternalFileDelete(externalFile);
        } catch (FileNotFoundException e) {
            MMILog.e(TAG, e + ", externalFile: " + externalFile);
            return false;
        } catch (IOException e) {
            MMILog.e(TAG, e + ", isSDcardMounted: " + isSDcardMounted());
            return false;
        } catch (Exception e) {
            MMILog.e(TAG, e.toString());
            return false;
        } finally {
            try {
                if (externalFileInputStream != null) {
                    externalFileInputStream.close();
                }
                if (externalFileOutputStream != null) {
                    externalFileOutputStream.close();
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            IoUtils.closeQuietly(externalFileInputStream);
            IoUtils.closeQuietly(externalFileOutputStream);
        }

        return isReadDataSame;
    }

    private boolean isReadDataSame(int readData) {
        MMILog.d(TAG, "isReadDataSame: " + readData);
        return readData == VALUE_TEST_BYTE;
    }

    private class MemberStorageEventListener extends StorageEventListener {
        @Override
        public void onStorageStateChanged(String path, String oldState,
                String newState) {
            super.onStorageStateChanged(path, oldState, newState);
            if (Environment.MEDIA_MOUNTED.equals(newState)) {
                runTest();
            }
        }
    }

    private void sendStatus(String result, boolean pass) {
        if (mClientMessenger != null) {

            Message toClientMsg = new Message();
            Bundle bundle = new Bundle();

            bundle.putString("micro_sd", result);
            bundle.putBoolean("ispass", pass);

            toClientMsg.setData(bundle);

            try {
                MMILog.d(TAG, "service begin send msg to client");
                mClientMessenger.send(toClientMsg);
            } catch (RemoteException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
}