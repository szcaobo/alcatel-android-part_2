/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/Audio.java         */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/08/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import java.io.IOException;

import com.android.mmi.traceability.TraceabilityStruct;
import com.android.mmi.util.JRDClient;
import com.android.mmi.util.MMILog;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.AudioSystem;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnErrorListener;
import android.os.Handler;
import android.provider.Settings;
import android.widget.Toast;

public class Audio extends TestBase {
    public final static float LENGTH_VOLUME_MAX = 1.0f;
    public final static float LENGTH_VOLUME_HALF = 0.5f;
    public final static float LENGTH_VOLUME_MINOR = 0.01f;
    public final static float LENGTH_VOLUME_MIN = 0.0f;

    private final int AUDIO_RECEIVER_DISCRETE   = 0;
    private final int AUDIO_RECEIVER_DISCRETE_DOWN   = 1;
    private final int AUDIO_SPEAKER_L_MELODY      = 2;
    private final int AUDIO_SPEAKER_R_MELODY      = 3;
    private final int AUDIO_LOOP_MAIN_MIC       = 4;
    private final int AUDIO_LOOP_MAIN_MIC_END   = 5;
    private final int AUDIO_LOOP_SEC_MIC        = 6;
    private final int AUDIO_LOOP_SEC_MIC_END    = 7;
    private int mState = AUDIO_RECEIVER_DISCRETE;

    private int oldAudioMode;
    private AudioManager audioManager;
    private MediaPlayer beepMediaPlayer;
    private MediaPlayer beepMediaPlayerdown;
    private MediaPlayer ringtoneMediaPlayerL;
    private MediaPlayer ringtoneMediaPlayerR;
    private MediaPlayer currentMediaPlayer;
    private JRDClient mClient;
    private Handler objHandler = new Handler();
    private boolean loopModeOn = false;

    private boolean haveSecMic = false;

    private int mSlientMode;

    protected void enableSecMic() {
        haveSecMic = true;
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
        audioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);

        mSlientMode = Settings.System.getInt(mContext.getContentResolver(), Settings.System.SOUND_EFFECTS_ENABLED, -1);

        mClient = new JRDClient();
        initAudioMode();

        setDefTextMessage(R.string.audio_receiver1);
        setPassButtonEnable(true);
        setButtonAnimateVisible(2000);

        new Thread(new Runnable() {
            public void run() {
                playTopReceiver();
            }
        }).start();
    }

    private void playTopReceiver(){
        MMILog.d(TAG, "=======playTopReceiver()========== ");
        setReceiverMode();

        beepMediaPlayer = new MediaPlayer();
        setDataSourceFromResource(beepMediaPlayer, R.raw.receiver);
        beepMediaPlayer.setVolume(LENGTH_VOLUME_MAX, LENGTH_VOLUME_MAX);
        beepMediaPlayer.setLooping(true);
        beepMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                MMILog.i(TAG, "beepMediaPlayer onPrepared");
                beepMediaPlayer.start();
                currentMediaPlayer = beepMediaPlayer;
            }
        });
        beepMediaPlayer.setOnErrorListener(new OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                MMILog.e(TAG, "beepMediaPlayer had error " + what + ", " + extra);
                return true;
            }
        });
        try {
            beepMediaPlayer.prepare();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            MMILog.e(TAG, "beepMediaPlayer prepare fail, " + e.getMessage());
        }
    }

    private void playBtmReceiver(){
        MMILog.d(TAG, "=======playBtmReceiver()========== ");
        beepMediaPlayerdown = new MediaPlayer();
        setDataSourceFromResource(beepMediaPlayerdown, R.raw.receiver);
        beepMediaPlayerdown.setVolume(LENGTH_VOLUME_MAX, LENGTH_VOLUME_MAX);
        beepMediaPlayerdown.setLooping(true);
        beepMediaPlayerdown.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                MMILog.i(TAG, "beepMediaPlayerdown onPrepared");
                beepMediaPlayerdown.start();
                currentMediaPlayer = beepMediaPlayerdown;
            }
        });
        beepMediaPlayerdown.setOnErrorListener(new OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                MMILog.e(TAG, "beepMediaPlayerdown had error " + what + ", " + extra);
                return true;
            }
        });
        try {
            beepMediaPlayerdown.prepare();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            MMILog.e(TAG, "beepMediaPlayerdown prepare fail, " + e.getMessage());
        }
    }

    private void playTopSpeaker(){
        MMILog.d(TAG, "=======playTopSpeaker()========== ");
        ringtoneMediaPlayerL = new MediaPlayer();
        setDataSourceFromResource(ringtoneMediaPlayerL, R.raw.speakerl);
        ringtoneMediaPlayerL.setVolume(LENGTH_VOLUME_MAX, LENGTH_VOLUME_MAX);
        ringtoneMediaPlayerL.setLooping(true);
        ringtoneMediaPlayerL.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                MMILog.i(TAG, "ringtoneMediaPlayerL onPrepared");
                ringtoneMediaPlayerL.start();
                currentMediaPlayer = ringtoneMediaPlayerL;
            }
        });
        ringtoneMediaPlayerL.setOnErrorListener(new OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                MMILog.e(TAG, "ringtoneMediaPlayerL had error " + what + ", " + extra);
                return true;
            }
        });
        try {
            ringtoneMediaPlayerL.prepare();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            MMILog.e(TAG, "ringtoneMediaPlayerL prepare fail, " + e.getMessage());
        }
    }

    private void playBtmSpeaker(){
        MMILog.d(TAG, "=======playBtmSpeaker()========== ");
        ringtoneMediaPlayerR = new MediaPlayer();
        setDataSourceFromResource(ringtoneMediaPlayerR, R.raw.speakerr);
        ringtoneMediaPlayerR.setVolume(LENGTH_VOLUME_MAX, LENGTH_VOLUME_MAX);
        ringtoneMediaPlayerR.setLooping(true);
        ringtoneMediaPlayerR.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                MMILog.i(TAG, "ringtoneMediaPlayerR onPrepared");
                ringtoneMediaPlayerR.start();
                currentMediaPlayer = ringtoneMediaPlayerR;
            }
        });
        ringtoneMediaPlayerR.setOnErrorListener(new OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                MMILog.e(TAG, "ringtoneMediaPlayerR had error " + what + ", " + extra);
                return true;
            }
        });
        try {
            ringtoneMediaPlayerR.prepare();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            MMILog.e(TAG, "ringtoneMediaPlayerR prepare fail, " + e.getMessage());
        }
    }

    private void setDataSourceFromResource(MediaPlayer player, int res) {
        AssetFileDescriptor afd = mContext.getResources().openRawResourceFd(res);
        if (afd != null) {
            try {
                player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                afd.close();
            } catch (IllegalArgumentException e) {
                // TODO Auto-generated catch block
                MMILog.e(TAG, "setDataSourceFromResource fail, " + e.getMessage());
            } catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                MMILog.e(TAG, "setDataSourceFromResource fail, " + e.getMessage());
            } catch (IOException e) {
                // TODO Auto-generated catch block
                MMILog.e(TAG, "setDataSourceFromResource fail, " + e.getMessage());
            }
        }
    }

    public void resume() {
        super.resume();
        if(mSlientMode != -1){
            Settings.System.putInt(mContext.getContentResolver(), Settings.System.SOUND_EFFECTS_ENABLED, 0);
        }
    }

    public void pause() {
        super.pause();
        if(mSlientMode != -1){
            Settings.System.putInt(mContext.getContentResolver(), Settings.System.SOUND_EFFECTS_ENABLED, mSlientMode);
        }
    }

    @Override
    public void destroy() {
        // TODO Auto-generated method stub
        new Thread(new Runnable() {
            public void run() {
                AudioSystem.setParameters("rotation=0");
                MMILog.d(TAG, "rotation=0");
            }
        }).start();

        clearAudioMode();
        super.destroy();
    }

    @Override
    public void onPassClick() {
        mState++;

        if (currentMediaPlayer != null) {
            currentMediaPlayer.stop();
            currentMediaPlayer.release();
            currentMediaPlayer = null;
        }

        switch (mState) {
        case AUDIO_RECEIVER_DISCRETE_DOWN:
            setButtonAnimateVisible(2000);
            setDefTextMessage(R.string.audio_receiver2);

            new Thread(new Runnable() {
                public void run() {
                    AudioSystem.setParameters("rotation=180");
                    MMILog.d(TAG, "rotation=180");
                }
            }).start();

            new Thread(new Runnable() {
                public void run() {
                    setSpeakerMode();
                    setReceiverMode();
                    playBtmReceiver();
                }
            }).start();

            break;
        case AUDIO_SPEAKER_L_MELODY:
            setButtonAnimateVisible(2000);
            setDefTextMessage(R.string.audio_speaker1);

            new Thread(new Runnable() {
                public void run() {
                    setSpeakerMode();
                    playTopSpeaker();
                }
            }).start();

            break;
        case AUDIO_SPEAKER_R_MELODY:
            setButtonAnimateVisible(2000);
            setDefTextMessage(R.string.audio_speaker2);

            new Thread(new Runnable() {
                public void run() {
                    playBtmSpeaker();
                }
            }).start();

            break;
        case AUDIO_LOOP_MAIN_MIC:
            setButtonAnimateVisible(2000);
            objHandler.postDelayed(micTask, 3500);
            setDefTextMessage(R.string.audio_loop);
            setPassButtonEnable(false);
            break;
        case AUDIO_LOOP_MAIN_MIC_END:
            if(haveSecMic) {
                mClient.main_end();
                loopModeOn = false;
                mState++;
            }
            else {
                super.onPassClick();
                break;
            }
        case AUDIO_LOOP_SEC_MIC:
            setButtonAnimateVisible(1000);
            objHandler.postDelayed(secTask, 2000);
            setDefTextMessage(R.string.audio_loop_sec);
            setPassButtonEnable(false);
            break;
        default:
            super.onPassClick();
            break;
        }
    }

    private void initAudioMode() {
        saveOldAudioMode();
    }

    // //////////////////////////////////////////////////////////////////////////////
    // MARK Methods
    // //////////////////////////////////////////////////////////////////////////////
    /* *****************************************************************************
     * METHOD save/load old audio mode
     * ******************************************
     * *********************************
     */
    private void saveOldAudioMode() {
        oldAudioMode = audioManager.getMode();
    }

    private void loadOldAudioMode() {
        audioManager.setMode(oldAudioMode);
    }

    /* *****************************************************************************
     * METHOD set audio mode
     * ****************************************************
     * ***********************
     */
    private void setSpeakerMode() {
        int maxVolume = audioManager
                .getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        audioManager.setMode(AudioManager.MODE_NORMAL);
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, maxVolume - 2, 0);
    }

    private void setReceiverMode() {
        int maxVolume = audioManager
                .getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        audioManager.setMode(AudioManager.MODE_IN_CALL);
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, maxVolume,
                0);
    }

    private void clearAudioMode() {
        objHandler.removeCallbacksAndMessages(null);
        if(currentMediaPlayer != null){
            currentMediaPlayer.release();
            currentMediaPlayer = null;
        }

        switch (mState) {
        case AUDIO_LOOP_MAIN_MIC:
        case AUDIO_LOOP_MAIN_MIC_END:
             if(loopModeOn) {
                 mClient.main_end();
                 loopModeOn = false;
             }
             audioManager.setMode(AudioManager.MODE_NORMAL);
            break;
        case AUDIO_LOOP_SEC_MIC:
        case AUDIO_LOOP_SEC_MIC_END:
             if(loopModeOn) {
                 mClient.sec_end();
                 loopModeOn = false;
             }
             audioManager.setMode(AudioManager.MODE_NORMAL);
             break;
        default:
            break;
        }
        //SystemProperties.set("ctl.stop", "loophandset");
        //SystemProperties.set("ctl.start", "loophandsetoff");

        loadOldAudioMode();
    }

    private Runnable micTask = new Runnable() {

        @Override
        public void run() {
             // TODO Auto-generated method stub
            mClient.mic_start();
            loopModeOn = true;
            setPassButtonEnable(true);
        }
    };

    private Runnable secTask = new Runnable() {

        @Override
        public void run() {
             // TODO Auto-generated method stub
            mClient.sec_start();
            loopModeOn = true;
            setPassButtonEnable(true);
        }
     };
}
