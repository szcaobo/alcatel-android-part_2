package com.android.mmi.util;

import android.view.KeyEvent;

public class KeyCodeFilter {
    public static boolean isUserCustomizableKeycode(int keyCode) {
        // filter mobile-useful keyCode
        switch (keyCode) {
        case 0: // keyCode 0 means no action definition in array.xml
        case KeyEvent.KEYCODE_MENU:
        case KeyEvent.KEYCODE_HOME:
        case KeyEvent.KEYCODE_POWER:
        case KeyEvent.KEYCODE_BACK:
        case KeyEvent.KEYCODE_CALL:
        case KeyEvent.KEYCODE_ENDCALL:
        case KeyEvent.KEYCODE_VOLUME_UP:
        case KeyEvent.KEYCODE_VOLUME_DOWN:
        // HALL key event
        case 250:
        case 251:
        case 252:
        case 254:
            return false;
        default:
            return true;
        }
    }
}
