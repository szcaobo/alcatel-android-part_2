/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/util/              */
/*                                                            VersionAPI.java */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/08/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import android.os.SystemProperties;

public class VersionAPI {

    private String LOG_TAG="MMITest.VersionAPI";

    private int    ModemVerBuffer = 256;/* should be sync with jrd_version_name in hw.c */
    private String BootVerFile ="boot.ver";
    private String SystemVerFile ="/system/system.ver";
    private String UserdataVerFile ="/data/userdata.ver";
    private String CustpackVerFile ="/custpack/custpack.ver";
    private String ModemDevVerFile ="/sys/devices/system/soc/soc0/jrd_ver";
    //[BUGFIX]-Add-BEGIN by Miao FR-346362, 2013/01/03
    private String RecoveryVerFile ="/data/local/tmp/recovery.ver";
    private String ModemVerFile ="/data/local/tmp/modem.ver";
    //[BUGFIX]-Add-END by Miao
    private String READ_ERROR_STR = "????????";

    private String mVersionNames = null;

    enum file_type_t {
        PARTITION      ,
        QCSBL          ,
        QCSBLHD        ,
        OEMSBL         ,
        OEMSBLHD       ,
        AMSS           ,
        AMSSHD         ,
        APPSBOOT       ,
        APPSBOOTHD     ,
        STUDY_PARA     ,
        TUNING_PARA    ,
        PERSO          ,
        SIMLOCK        ,
        FILE_TYPE_MAX
    };

    static {
        // The JNI library reading datas from modem in the fifo
        // The runtime will add "lib" on the front and ".o" on the end of
        // the name supplied to loadLibrary.
        /*[BUGFIX]-Del-BEGIN by TCTNB.(Qianbo Pan), 2012/12/03, for PR367279 Enable *#3228# to check SW versio*/
        //System.loadLibrary("mver_jni");
        /*[BUGFIX]-Del-END by TCTNB.(Qianbo Pan)*/
    }

    private static native String get_modem_ver() throws RuntimeException ;

    public VersionAPI() {
            MMILog.i(LOG_TAG, "VersionAPI Service Created Successfully");
    }


    private String getVerFromFile(String FileName)/*throws IOException, FileNotFoundException*/ {

        String Output="", Input;
        MMILog.i(LOG_TAG,"opening file:"+ FileName);

        try{
            BufferedReader buffer = new BufferedReader( new FileReader(FileName), ModemVerBuffer);

            do{// read all the lines till the end
                Input = buffer.readLine();
                Output+=(Input == null ? "" : Input);
            }while(Input!= null);
            buffer.close();
        }catch(FileNotFoundException e) {
            MMILog.e(LOG_TAG,"cannot open file" + FileName );
            return READ_ERROR_STR;
        }catch(IOException e) {
            MMILog.e(LOG_TAG,"file read error");
            return READ_ERROR_STR;
        }finally {
            ;
        }
        return Output;

    }

    public String getPTABLEVer() {
      return getModemVer(file_type_t.PARTITION.ordinal());
    }

    public String getAMSSHDVer() {
      return getModemVer(file_type_t.AMSSHD.ordinal());
    }
    public String getAMSSVer() {
      return getModemVer(file_type_t.AMSS.ordinal());
    }

    public String getJRDParttionsVer() {
      String result=getModemVer(file_type_t.STUDY_PARA.ordinal())+"\n";
      result+=getModemVer(file_type_t.TUNING_PARA.ordinal())+"\n";
      result+=getModemVer(file_type_t.SIMLOCK.ordinal())+"\n";
      return result;
    }

    private String getModemVer(int bintype) throws RuntimeException {
        /*[BUGFIX]-Mod-BEGIN by TCTNB.(Qianbo Pan), 2012/12/03, for PR367279 Enable *#3228# to check SW versio*/
        /*if(mVersionNames==null) {
            mVersionNames = get_modem_ver();
        }

        String[] VersionListNames = new String(mVersionNames).split("\n");*/
       return getVerFromFile(ModemVerFile);
//        return "";//VersionListNames[bintype];
        /*[BUGFIX]-Mod-END by TCTNB.(Qianbo Pan)*/
    }

    public String getModemVer() throws RuntimeException {

        String result = getVerFromFile(ModemVerFile);
        if(result.contains("??")) {
            result = SystemProperties.get("ro.tct.non.ver", READ_ERROR_STR);
        }
        return result;
    }

    public String getBootVer() throws RuntimeException {
        String result = getVerFromFile(BootVerFile);
        if(result.contains("??")) {
            result = SystemProperties.get("ro.tct.boot.ver", READ_ERROR_STR);
        }
        return result;
    }
    public String getSystemVer() throws RuntimeException {
        String result = getVerFromFile(SystemVerFile);
        if(result.contains("??")) {
            result = SystemProperties.get("ro.tct.sys.ver", READ_ERROR_STR);
        }
        return result;
    }
    public String getUserdataVer() throws RuntimeException {

        return getVerFromFile(UserdataVerFile);
    }
    public String getCustpackVer() throws RuntimeException {
        String result = getVerFromFile(CustpackVerFile);
        if(result.contains("??")) {
            result = SystemProperties.get("ro.tct.cust.ver", READ_ERROR_STR);
        }
        return result;
    }
    public String getRecoveryVer() throws RuntimeException {
        String result = getVerFromFile(RecoveryVerFile);
        if(result.contains("??")) {
            result = SystemProperties.get("ro.tct.reco.ver", READ_ERROR_STR);
        }
        return result;
    }

    public String getBootloaderVer() {
        return SystemProperties.get("ro.bootloader");
    }

    public String getBootloaderHdVer() {
        char[] bootloaderHdVer = SystemProperties.get("ro.bootloader").toCharArray();
        bootloaderHdVer[5] = 'H';
        return new String(bootloaderHdVer);
    }

    public String getQCSBLVer() {
        return getModemVer(file_type_t.QCSBL.ordinal());
    }

    public String getQCSBLHDVer() {
      return getModemVer(file_type_t.QCSBLHD.ordinal());
    }

    public String getOEMSBLVer() {
       return getModemVer(file_type_t.OEMSBL.ordinal());
    }

    public String getOEMSBLHDVer() {
       return getModemVer(file_type_t.OEMSBLHD.ordinal());
    }

}
