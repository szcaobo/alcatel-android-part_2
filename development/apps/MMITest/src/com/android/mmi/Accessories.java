/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/Accessories.java   */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/08/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.AudioSystem;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.os.SystemProperties;
import com.android.mmi.util.JRDClient;
import com.android.mmi.util.MMILog;
import com.android.mmi.util.TestItemManager;

public class Accessories extends TestBase implements OnClickListener {
    public final static int MESSAGE_USERDEFINE = 1024;
    public final static int MESSAGE_TIMEOUT = MESSAGE_USERDEFINE + 1;
    public final static int MESSAGE_TIMECOUNT = MESSAGE_TIMEOUT + 1;
    public final static float LENGTH_VOLUME_MAX = 1.0f;
    public final static float LENGTH_VOLUME_HALF = 0.5f;
    public final static float LENGTH_VOLUME_MINOR = 0.1f;
    public final static float LENGTH_VOLUME_MIN = 0.0f;
    public final static int LENGTH_TIMEOUT = 15000;
    public final static int LENGTH_SECOND = 1000;
    public final static int LENGTH_SECOND_TIMECOUNT = LENGTH_TIMEOUT
            / LENGTH_SECOND;

    private final int ACCESSORIES_HEADSET_INSERT    = 0;
    private final int ACCESSORIES_HEADSET_DISCRETE_LEFT     = 1;
    private final int ACCESSORIES_HEADSET_DISCRETE_RIGHT    = 2;
    private final int ACCESSORIES_HEADSET_MIC_LOOP  = 3;
    private final int ACCESSORIES_HEADSET_REMOVE    = 4;
    private int mState = ACCESSORIES_HEADSET_INSERT;

    private boolean isHeadsetPlugged;
    private boolean isHeadsetOn = false;
    private boolean isLastItem=false;
    private int timeCount;
    private BroadcastReceiver broadcastReceiver;
    private IntentFilter intentFilter;
    private Handler handler;
    private AudioManager audioManager;
    private MediaPlayer beepLeftMediaPlayer;
    private MediaPlayer beepRightMediaPlayer;
    private TextView messageTextview;
    private View buttonsView;
    private Button buttonUp;
    private Button buttonDown;
    private int volumeMax;
    private int volumeCurrent;
    private int oldAudioMode;
    private int mSilentMode;

    private JRDClient mClient;
    private Handler objHandler = new Handler();
    private boolean loopModeOn = false;


    @Override
    public void create(CommonActivity a) {
        // TODO Auto-generated method stub
        super.create(a);
        setContentView(R.layout.test_accessories, LAYOUTTYPE.LAYOUT_CUST_WITH_PASSFAIL);

        mSilentMode = Settings.System.getInt(mContext.getContentResolver(),
                Settings.System.SOUND_EFFECTS_ENABLED, -1);
        if(mSilentMode == 1) {
            Settings.System.putInt(mContext.getContentResolver(),
                    Settings.System.SOUND_EFFECTS_ENABLED, 0);
        }
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
        messageTextview = (TextView) findViewById(R.id.accessories_message);

        isHeadsetPlugged = false;
        timeCount = LENGTH_SECOND_TIMECOUNT;

        broadcastReceiver = new MemberReceiver();
        intentFilter = new IntentFilter(Intent.ACTION_HEADSET_PLUG);
        handler = new MemberHandler();

        audioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);

        beepLeftMediaPlayer = MediaPlayer.create(mContext, R.raw.speaker);//beep_left
        beepLeftMediaPlayer.setVolume(LENGTH_VOLUME_MAX, LENGTH_VOLUME_MIN);
        beepLeftMediaPlayer.setLooping(true);
        beepRightMediaPlayer = MediaPlayer.create(mContext, R.raw.speaker);//beep_right
        beepRightMediaPlayer.setVolume(LENGTH_VOLUME_MIN, LENGTH_VOLUME_MAX);
        beepRightMediaPlayer.setLooping(true);

        buttonsView = findViewById(R.id.key_buttons);
        buttonUp = (Button) findViewById(R.id.key_up);
        buttonDown = (Button) findViewById(R.id.key_down);
        buttonUp.setOnClickListener(this);
        buttonDown.setOnClickListener(this);
        buttonUp.setEnabled(false);

        mClient = new JRDClient();

        //set audio mode
        saveOldAudioMode();
        messageTextview.setText(R.string.accessories_insert_wait);
        setButtonAnimateVisible(1000);
    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub
        super.resume();
        mContext.registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub
        super.pause();
        mContext.unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void destroy() {
        // TODO Auto-generated method stub
        super.destroy();
        clearAudioMode();
        if(mSilentMode!=-1) {
            Settings.System.putInt(mContext.getContentResolver(),
                    Settings.System.SOUND_EFFECTS_ENABLED, mSilentMode);
        }
    }

    private void saveOldAudioMode() {
        oldAudioMode = audioManager.getMode();
    }
    private void loadOldAudioMode() {
        audioManager.setMode(oldAudioMode);
    }

    private void onHeadsetPlugChanged() {
        switch (mState) {
        case ACCESSORIES_HEADSET_INSERT:
            if (isHeadsetPlugged) {
                volumeMax = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
                volumeCurrent = volumeMax;
                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, volumeCurrent, 0);
                volumeCurrent = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
                if(volumeCurrent < volumeMax){
                       volumeMax = 10;  // if set volume>10, it will popup a dialog to confirm
                       volumeCurrent = volumeMax;
                       audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, volumeMax, 0);
                }

                messageTextview.setText(R.string.accessories_insert_success);
                setPassButtonEnable(true);
                //stopHeadsetPlugCheck();
                onPassClick();
            } else {
                messageTextview.setText(R.string.accessories_insert_wait);
            }
            break;
        case ACCESSORIES_HEADSET_REMOVE:
            if (isHeadsetPlugged) {
                messageTextview.setText(R.string.accessories_remove_wait);
            } else {
                messageTextview.setText(R.string.accessories_remove_success);
                //stopHeadsetPlugCheck();
                onPassClick();
            }
            break;
        }
    }

    /* *****************************************************************************
     * METHOD set audio mode
     * ****************************************************
     * ***********************
     */
    private void setSpeakerMode() {
        int maxVolume = audioManager
                .getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        audioManager.setMode(AudioManager.MODE_NORMAL);
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, maxVolume,
                0);
    }


    private void setLoopbackMode() {
        if(loopModeOn==false) {
            String cmd="cc";
            mClient.commandCmd(cmd.getBytes());
            loopModeOn = true;
        }
        setPassButtonEnable(true);
    }

    private void clearLoopbackMode() {
        objHandler.removeCallbacksAndMessages(null);
        if(loopModeOn) {
            String cmd = "c#";
            mClient.commandCmd(cmd.getBytes());
            loopModeOn = false;
        }
    }

    private void clearAudioMode() {
        beepLeftMediaPlayer.release();
        beepRightMediaPlayer.release();

        if( mState==ACCESSORIES_HEADSET_REMOVE ||
                mState==ACCESSORIES_HEADSET_MIC_LOOP) {
            clearLoopbackMode();
        }
        loadOldAudioMode();
    }

    /* *****************************************************************************
     * METHOD check headset insert
     * **********************************************
     * *****************************
     */
    private void startHeadsetPlugCheck() {
        handler.sendEmptyMessageDelayed(MESSAGE_TIMEOUT, LENGTH_TIMEOUT);
        handler.sendEmptyMessageDelayed(MESSAGE_TIMECOUNT, LENGTH_SECOND);
    }

    private void stopHeadsetPlugCheck() {
        handler.removeMessages(MESSAGE_TIMEOUT);
        handler.removeMessages(MESSAGE_TIMECOUNT);
        timeCount = LENGTH_SECOND_TIMECOUNT;
    }

    /* *****************************************************************************
     * METHOD get headset plug state
     * ********************************************
     * *******************************
     */
    private boolean isHeadsetPlugged(Intent intent) {
        int numHeadsetState = intent.getIntExtra(
                Value.KEY_INTENT_STATE_HEADSET,
                Value.STATE_INTENT_HEADSET_UNPLUGGED);
        isHeadsetOn = audioManager.isWiredHeadsetOn();
        if(numHeadsetState == Value.STATE_INTENT_HEADSET_PLUGGED){
            return true;
        }
        else if(isHeadsetOn){
            return true;
        }
        else{
            return false;
        }
    }

    /* *****************************************************************************
     * METHOD select pass action
     * ************************************************
     * ***************************
     */
    @Override
    public void onPassClick() {
        mState++;
        switch (mState) {
        case ACCESSORIES_HEADSET_DISCRETE_LEFT:
            messageTextview.setText(R.string.accessories_sound_left);
            setSpeakerMode();
            beepLeftMediaPlayer.start();
            buttonsView.setVisibility(View.VISIBLE);
            break;
        case ACCESSORIES_HEADSET_DISCRETE_RIGHT:
            messageTextview.setText(R.string.accessories_sound_right);
            beepLeftMediaPlayer.stop();
            beepRightMediaPlayer.start();
            break;
        case ACCESSORIES_HEADSET_MIC_LOOP:
            setPassButtonEnable(false);
            messageTextview.setText(R.string.accessories_loopback);
            beepRightMediaPlayer.stop();
            buttonsView.setVisibility(View.GONE);
            audioManager.setMode(AudioSystem.MODE_IN_CALL);
            objHandler.postDelayed(mcmdstart, 3000);
            setPassButtonEnable(false);
            break;
        case ACCESSORIES_HEADSET_REMOVE:
            clearAudioMode();
            if (Value.isAutoTest && !TestItemManager.getProduct().equals("idol4s_cn")
                    && !TestItemManager.getProduct().equals("argon")) {
                //setPassButtonEnable(false);
                //setFailButtonEnable(false);
                objHandler.postDelayed(mRestartTask, 3000);
                messageTextview.setText(R.string.fm_init);
            } else {
                setPassButtonEnable(false);
                messageTextview.setText(R.string.accessories_remove_wait);
                //startHeadsetPlugCheck();
            }
            break;
        default:
            super.onPassClick();
            break;
        }
    }

    // //////////////////////////////////////////////////////////////////////////////
    // MARK Inner Classes
    // //////////////////////////////////////////////////////////////////////////////
    private class MemberHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
            case MESSAGE_TIMEOUT:
                switch (mState) {
                case ACCESSORIES_HEADSET_INSERT:
                    messageTextview.setText(R.string.accessories_insert_fail);
                    break;
                case ACCESSORIES_HEADSET_REMOVE:
                    messageTextview.setText(R.string.accessories_remove_fail);
                }
                break;
            case MESSAGE_TIMECOUNT:
                // TODO display timecount(low priority)
                timeCount--;
                handler.sendEmptyMessageDelayed(MESSAGE_TIMECOUNT,
                        LENGTH_SECOND);

                if ((timeCount % 3) == 0) {
                    MMILog.d(TAG, "remain time count: " + timeCount);
                }
                break;
            }
        }
    }

    private class MemberReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context arg0, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Intent.ACTION_HEADSET_PLUG)) {
                isHeadsetPlugged = isHeadsetPlugged(intent);
                onHeadsetPlugChanged();
            }
        }
    }

    private Runnable mRestartTask = new Runnable() {

        @Override
        public void run() {
             // TODO Auto-generated method stub
            onPassClick();
        }
    };

    private Runnable mcmdstart = new Runnable() {

        @Override
        public void run() {
            // TODO Auto-generated method stub
            setLoopbackMode();
            setPassButtonEnable(true);
        }
    };

    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch(v.getId()) {
        case R.id.key_up:
            if (volumeCurrent == 0) {
                buttonDown.setEnabled(true);
            }
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, ++volumeCurrent, 0);
            if (volumeCurrent == volumeMax) {
                buttonUp.setEnabled(false);
            }
            buttonDown.setEnabled(true);
            break;
        case R.id.key_down:
            if (volumeCurrent == volumeMax) {
                buttonUp.setEnabled(true);
            }
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, --volumeCurrent, 0);
            if (volumeCurrent == 0) {
                buttonDown.setEnabled(false);
            }
            break;
        default:
            break;
        }
    }
}
