/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/EmergencyCall.java */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/08/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import android.content.Intent;
import android.os.UserHandle;

import com.android.mmi.util.MMILog;

public class EmergencyCall extends TestBase {

    @Override
    public void run() {
        // TODO Auto-generated method stub
        Intent intent = new Intent();
        //intent.setAction("android.intent.action.CALL");
        intent.setAction(Intent.ACTION_CALL_EMERGENCY);
        intent.setData(android.net.Uri.fromParts("tel", "112", null));
        mContext.startActivityForResult(intent, Value.SCENARIO_AUTO);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        MMILog.d(TAG, "onActivityResult");
        this.onPassClick();
    }

}
