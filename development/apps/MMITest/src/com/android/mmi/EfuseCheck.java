/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/EfuseCheck.java    */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 08/14/14| Shishun.Liu    |                    | Create for Efuse check     */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import com.android.mmi.util.JRDClient;
import com.android.mmi.util.MMILog;

import android.os.SystemProperties;

public class EfuseCheck extends TestBase {
    private String mOemfuseStatus ="Mobile EFuse: ";
    private String mTctDump = "TCT Dump: ";

    @Override
    public void run() {
        // TODO Auto-generated method stub
        getOemfuseStatus();
        getTctDumpStatus();
        this.setDefTextMessage( mOemfuseStatus + "\n"
                + mTctDump);

        if(mOemfuseStatus.contains("Enable")
                && mTctDump.contains("Disable")){
            setPassButtonEnable(true);
        }
    }

    private void getOemfuseStatus(){

        JRDClient jrdClient = new JRDClient();
        long status = 0;
        byte[] byteStatus = jrdClient.readEfuseStatus();
        if (byteStatus.length == 4) {
            status += (byteStatus[0]);
            status += (byteStatus[1] << 8);
            status += (byteStatus[2] << 16);
            status += (byteStatus[3] << 24);
        }

        if (status == 0x00000001) {
            mOemfuseStatus += "Enable";
        } else if (status == 0x00000000) {
            mOemfuseStatus += "Disable";
        } else {
            mOemfuseStatus += "Unknow";
        }
    }

    private void getTctDumpStatus() {
        if(SystemProperties.getBoolean("persist.sys.dload.enable", false)) {
            mTctDump += "Enable";
        }
        else {
            mTctDump += "Disable";
        }
    }
}
