/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/Vibrator.java      */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/08/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

public class Vibrator extends TestBase {
    private android.os.Vibrator vibratorManager;
    public static final long LENGTH_VIBRATION_TIME = 500L;
    public static final long[] VIBRATOR_PATTERN = { 0, LENGTH_VIBRATION_TIME };

    @Override
    public void run() {
        // TODO Auto-generated method stub
        vibratorManager = (android.os.Vibrator) mContext.getSystemService(
                android.content.Context.VIBRATOR_SERVICE);

        vibratorManager.vibrate(VIBRATOR_PATTERN, 0);
        mContext.mTextMessage.setText(R.string.vibrator_message);
        setPassButtonEnable(true);
        setButtonAnimateVisible();
    }

    @Override
    public void destroy() {
        // TODO Auto-generated method stub
        super.destroy();
        vibratorManager.cancel();
    }

}
