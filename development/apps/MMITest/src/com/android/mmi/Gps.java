/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/Gps.java          */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/08/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import android.content.Context;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;

import com.android.mmi.util.MMILog;

public class Gps extends TestBase {
    public static final int LENGTH_NOTIFICATION_INTERVAL_TIME = 1000;
    public static final float LENGTH_NOTIFICATION_INTERVAL_DISTANCE = 5.0f;
    public static final int LENGTH_SATELLITE_PRN_STRING = 50;
    public static final int LENGTH_SATELLITE_SNR_STRING = 80; // add by
                                                              // anshu.zhou
                                                              // for
                                                              // bug[244137]
    public static final int LENGTH_LOCATION_STRING = LENGTH_SATELLITE_PRN_STRING * 2;

    public static enum GpsState {
        INITIALIZE_SUCCESS, INITIALIZE_WAIT, INITIALIZE_FAIL;
    };

    private final static int MESSAGE_USER_DEFINE = 1024;
    private final static int MESSAGE_GPS_DETECT_COMPLETED = MESSAGE_USER_DEFINE + 1;
    public final static int LENGTH_GPS_DETECT_TIMEOUT = 10000;

    private int gpsStatusUpdateCount;
    private int locationUpdateCount;
    private int gpsSatelliteCount;
    private GpsStatus currentGpsStatus;
    private Location currentLocation;
    private StringBuilder gpsStatusStringBuilder;
    private StringBuilder gpsSatellitePrnStringBuilder;
    private StringBuilder gpsSatelliteSnrStringBuilder; // add by anshu.zhou for
                                                        // bug[244137]

    private Handler handler;

    private LocationManager locationManager;
    private GpsStatus.Listener gpsStatusListener;
    private LocationListener locationListener;

    private boolean isGps = false;
    private int failCount = 4;

    long fixLocationTime;
    boolean firstLocation;

    @Override
    public void run() {
        // TODO Auto-generated method stub
        this.setButtonAnimateVisible(3000);

        gpsStatusStringBuilder = new StringBuilder(LENGTH_LOCATION_STRING);
        gpsSatellitePrnStringBuilder = new StringBuilder(
                LENGTH_SATELLITE_PRN_STRING);
        gpsSatelliteSnrStringBuilder = new StringBuilder(
                LENGTH_SATELLITE_SNR_STRING);

        locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        gpsStatusListener = new MemberGpsStatusListener();
        locationListener = new MemberLocationListener();
        locationManager.addGpsStatusListener(gpsStatusListener);

        handler = new MemberHandler();

        setViewState(GpsState.INITIALIZE_WAIT);

        locationManager.sendExtraCommand(LocationManager.GPS_PROVIDER,
            "delete_aiding_data", null);

        isGps = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (!isGps)
            GpsOn();

        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                LENGTH_NOTIFICATION_INTERVAL_TIME,
                LENGTH_NOTIFICATION_INTERVAL_DISTANCE, locationListener);
        handler.sendEmptyMessageDelayed(MESSAGE_GPS_DETECT_COMPLETED,
                LENGTH_GPS_DETECT_TIMEOUT);

        firstLocation = true;
        fixLocationTime = System.nanoTime();

    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub
        super.resume();
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub
        super.pause();
    }

    @Override
    public void destroy() {
        // TODO Auto-generated method stub
        super.destroy();
        locationManager.removeGpsStatusListener(gpsStatusListener);
        locationManager.removeUpdates(locationListener);

        isGps = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (isGps)
            GpsOff();
    }

    private void setViewState(GpsState state) {
        switch (state) {
        case INITIALIZE_WAIT:
            setDefTextMessage(getMessageString(R.string.gps_init_wait));
            // retestButton.setVisibility(View.INVISIBLE);
            setPassButtonEnable(false);
            setFailButtonEnable(true);
            MMILog.e(TAG, "INITIALIZE_WAIT");
            break;
        case INITIALIZE_SUCCESS:
            handler.removeMessages(MESSAGE_GPS_DETECT_COMPLETED);
            failCount = 0;
            setPassButtonEnable(true);
            setFailButtonEnable(true);
            setDefTextMessage(getMessageString(getGpsStatusUpdated()));
            MMILog.e(TAG, "INITIALIZE_SUCCESS");
            break;
        case INITIALIZE_FAIL:
            isGps = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            MMILog.e(TAG, "INITIALIZE_FAIL Try again !");
            if (failCount > 0) {
                retest();
                failCount--;
                break;
            }
            setDefTextMessage(R.string.gps_init_fail);

            setPassButtonEnable(false);
            setFailButtonEnable(true);
            MMILog.e(TAG, "INITIALIZE_FAIL");
            break;
        }
    }

    /* *****************************************************************************
     * METHOD get message string
     * ************************************************
     * ***************************
     */
    private String getMessageString(int resourceId) {
        gpsStatusStringBuilder.setLength(0);
        gpsStatusStringBuilder.append(getString(R.string.gps_data_title))
                .append("\n\n").append(getString(resourceId));
        return gpsStatusStringBuilder.toString();
    }

    private String getMessageString(GpsStatus gpsStatus) {
        updateSatelliteStatus(gpsStatus);

        gpsStatusStringBuilder.setLength(0);
        gpsStatusStringBuilder
                // M7-U-戚慎薦 11010 : Gpa fail string issue
                // .append(getString(R.string.gps_data_title))
                .append("\n\n")
                .append(gpsSatelliteCount + getString(R.string.gps_satellite))
                .append("\n")
                .append(getString(R.string.gps_prn)
                        + gpsSatellitePrnStringBuilder.toString())
                .append("\n")
                .append(getString(R.string.gps_snr)
                        + gpsSatelliteSnrStringBuilder.toString())
                // add by anshu.zhou for bug[244137]
                .append("\n")
                .append(locationUpdateCount + getString(R.string.gps_track))
                .append("\n")
                .append(getString(R.string.gps_status_update)
                        + gpsStatusUpdateCount);
        return gpsStatusStringBuilder.toString();
    }

    private String getMessageString(Location location) {
        if (location == null) {
            return getMessageString(R.string.gps_init_fail);
        }

        gpsStatusStringBuilder.setLength(0);
        gpsStatusStringBuilder.append(getString(R.string.gps_data_title))
                .append("\n\n").append(String.valueOf(location.getLongitude()))
                .append("\n").append(String.valueOf(location.getLatitude()));
        return gpsStatusStringBuilder.toString();
    }

    /* *****************************************************************************
     * METHOD get satellite status
     * **********************************************
     * *****************************
     */
    private GpsStatus getGpsStatusUpdated() {
        return locationManager.getGpsStatus(currentGpsStatus);
    }

    private void updateSatelliteStatus(GpsStatus gpsStatus) {
        gpsSatelliteCount = 0;
        gpsSatellitePrnStringBuilder.setLength(0);
        gpsSatelliteSnrStringBuilder.setLength(0);
        Iterable<GpsSatellite> satelliteiIterable = gpsStatus.getSatellites();

        for (GpsSatellite gpsSatellite : satelliteiIterable) {
            if (gpsSatellite.getSnr() != 0) {
                gpsSatelliteCount++;
                gpsSatellitePrnStringBuilder.append(gpsSatellite.getPrn()).append(
                        "/");
                gpsSatelliteSnrStringBuilder // add by anshu.zhou for bug[244137]
                        .append(gpsSatellite.getSnr()).append("/");
            }
        }
    }

    // //////////////////////////////////////////////////////////////////////////////
    // MARK Inner Classes
    // //////////////////////////////////////////////////////////////////////////////
    private class MemberGpsStatusListener implements GpsStatus.Listener {
        @Override
        public void onGpsStatusChanged(int event) {
            switch (event) {
            case GpsStatus.GPS_EVENT_STARTED:
                MMILog.e(TAG, "GPS_EVENT_STARTED");
                break;
            case GpsStatus.GPS_EVENT_STOPPED:
                MMILog.e(TAG, "GPS_EVENT_STOPPED");
                break;
            case GpsStatus.GPS_EVENT_FIRST_FIX:
                setViewState(GpsState.INITIALIZE_WAIT);
                MMILog.e(TAG, "GPS_EVENT_FIRST_FIX");
                break;
            case GpsStatus.GPS_EVENT_SATELLITE_STATUS:
                gpsStatusUpdateCount++;
                setViewState(GpsState.INITIALIZE_SUCCESS);
                MMILog.e(TAG, "GPS_EVENT_SATELLITE_STATUS");
                break;
            }
        }
    }

    public class MemberLocationListener implements LocationListener {
        public void onLocationChanged(Location location) {
            locationUpdateCount++;
            currentLocation = location;
            UpdateLocation(location);
        }

        public void onProviderDisabled(String provider) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    }

    private void retest() {
        locationManager.removeGpsStatusListener(gpsStatusListener);
        locationManager.removeUpdates(locationListener);

        gpsStatusUpdateCount = 0;
        locationUpdateCount = 0;
        gpsSatelliteCount = 0;

        //setButtonAnimateVisibleGPS();

        setViewState(GpsState.INITIALIZE_WAIT);

        gpsStatusStringBuilder.setLength(0);
        gpsSatellitePrnStringBuilder.setLength(0);

        handler.sendEmptyMessageDelayed(MESSAGE_GPS_DETECT_COMPLETED,
                LENGTH_GPS_DETECT_TIMEOUT);

        locationManager.addGpsStatusListener(gpsStatusListener);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                LENGTH_NOTIFICATION_INTERVAL_TIME,
                LENGTH_NOTIFICATION_INTERVAL_DISTANCE, locationListener);
    }

    private class MemberHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
            case MESSAGE_GPS_DETECT_COMPLETED:
                setViewState(GpsState.INITIALIZE_FAIL);

                MMILog.e(TAG, "GpsState.INITIALIZE_FAIL");
                break;
            }
        }
    }

    private void GpsOn() {
        Settings.Secure.setLocationProviderEnabled(mContext.getContentResolver(),
                LocationManager.GPS_PROVIDER, true);
        MMILog.e(TAG, "GpsOn");
    }

    private void GpsOff() {
        Settings.Secure.setLocationProviderEnabled(mContext.getContentResolver(),
                LocationManager.GPS_PROVIDER, false);

        MMILog.e(TAG, "GpsOff");
    }

    private void UpdateLocation(Location location) {
        if (location!=null) {
            if (firstLocation) {
                fixLocationTime = (System.nanoTime()-fixLocationTime)/1000000000;
                firstLocation = false;
            }
            this.setDefActionMessage("Latitude: "+location.getLatitude()+
                    "\nLongitude: "+location.getLongitude()+"\nFix time: "+
                     String.format("%d s", fixLocationTime));
        }
        else {
            setDefActionMessage("location=null");
        }
    }

    public final String getString(int resId) {
        return mContext.getResources().getString(resId);
    }
}
