/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/AudioInit.java     */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 11/23/15| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.FileNotFoundException;
import android.os.AsyncTask;
import java.util.Arrays;

import com.android.mmi.traceability.TraceabilityStruct;
import com.android.mmi.util.MMILog;

public class AACLC extends TestBase {
    private TraceabilityStruct traceabilityStruct;
    private boolean bPass = false;

    @Override
    public void create(CommonActivity a) {
        // TODO Auto-generated method stub
        super.create(a);
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
        setFailButtonEnable(false);
        setDefTextMessage("please wait...");
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... unused) {
                return getSpeakerStatus();
            }

            @Override
            protected void onPostExecute(String result) {
                setDefTextMessage(result);
                setPassButtonEnable(bPass);
                setFailButtonEnable(true);
            }
        }.execute();

    }

    @Override
    public void onBackPressed() {
        // not allowed to finish here
    }

    private String getSpeakerStatus() {
        String result = "Flag: Unknown";
        byte[] flag_AAC = {0x57, 0x68, 0x79, (byte)0x8a};
        byte[] flag_LC = {0x1f, 0x2e, 0x3c, 0x4b};
        byte flag = 'u'; // unknown
        byte gpio = 'u'; // unknown
        try {
            traceabilityStruct = new TraceabilityStruct();
            byte[] flag_read = traceabilityStruct.getItem(TraceabilityStruct.ID.INFO_SPEAKER);
            if(flag_read!=null && flag_read.length==4) {
                if(Arrays.equals(flag_read, flag_AAC)) {
                    result = "Flag: AAC";
                    flag = 'a';
                }
                else if(Arrays.equals(flag_read, flag_LC)) {
                    result = "Flag: LC";
                    flag = 'l';
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            BufferedReader bufReader = new BufferedReader(new FileReader("/sys/class/tfa9890/control/spk_box_type"));
            String info = bufReader.readLine();
            if(info!=null) {
                if(info.equals("AAC_IDOL4S")) {
                    result += "\nGPIO: AAC";
                    gpio = 'a';
                }
                else if(info.equals("LC_IDOL4S")) {
                    result += "\nGPIO: LC";
                    gpio = 'l';
                }
                else {
                    result += "\nGPIO: Unknown";
                }
            }
            else {
                result += "\nGPIO: Unknown";
            }
            bufReader.close();
        }
        catch(FileNotFoundException e) {
            e.printStackTrace();
            result += "\nGPIO: Unsupported";
        }
        catch(IOException e) {
            e.printStackTrace();
            result += "\nGPIO: Unsupported";
        }

        if (flag != 'u') {
            bPass = true;
        }

        return result;
    }
}
