/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/Vibrator.java      */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/08/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import com.android.mmi.util.MMILog;

import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Color;
import android.provider.Settings;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Hover extends TestBase {
    int MATCH_PARENT = LinearLayout.LayoutParams.MATCH_PARENT;
    public static final long LENGTH_VIBRATION_TIME = 500L;
    public static final long[] VIBRATOR_PATTERN = { 0, LENGTH_VIBRATION_TIME };
    private int mHoverEnable = 0;
    private ContentResolver mContentResolver;
    private int mTotalHoverView = 0;
    private int mTotalHoverView_NeedTest = 0;
    private int mPassedHoverView = 0;

    @Override
    public void create(CommonActivity a) {
        // TODO Auto-generated method stub
        super.create(a);
        setContentView(R.layout.test_hover, LAYOUTTYPE.LAYOUT_CUST_WITHOUT_PASSFAIL);

        LinearLayout llRoot = (LinearLayout)findViewById(R.id.rootLayout);
        if(llRoot!=null) {
            LinearLayout ll;
            int numx = 5;
            int numy = 9;

            for(int i=0; i<numy-1; i++) {
                ll = createLinearLayout(LinearLayout.HORIZONTAL, MATCH_PARENT, 0, 1);
                for(int j=0; j<numx; j++) {
                    ll.addView(createHoverView(0, MATCH_PARENT, 1));
                }
                llRoot.addView(ll);
            }
            llRoot.addView(createSpecialLayout());
        }
    }

    private LinearLayout createLinearLayout(int direction, int width, int height, int weight) {
        LinearLayout ll = new LinearLayout(mContext);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width, height, weight);
        ll.setLayoutParams(params);
        ll.setOrientation(direction);
        ll.setBackgroundColor(Color.BLACK);
        //ll.setGravity(Gravity.CENTER);
        return ll;
    }

    private HoverView createHoverView(int width, int height, int weight) {
        HoverView mtv = new HoverView(mContext);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width, height, weight);
        mtv.setLayoutParams(params);
        mtv.setGravity(Gravity.CENTER);

        return mtv;
    }

    private Button createnButton(int width, int height, int weight, boolean isPassBtn) {
        Button btn = new Button(mContext);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width, height, weight);
        params.gravity = Gravity.BOTTOM;
        btn.setLayoutParams(params);
        btn.setGravity(Gravity.CENTER);
        btn.setOnClickListener(mContext);
        btn.setBackgroundResource(android.R.drawable.btn_default_small);
        if(isPassBtn) {
            btn.setText(R.string.common_button_test_left);
            btn.setTextAppearance(R.style.CommonButton_Pass);
            btn.setEnabled(false);
            mContext.mPassButton = btn;
        }
        else {
            btn.setText(R.string.common_button_test_right);
            btn.setTextAppearance(R.style.CommonButton_Fail);
            btn.setEnabled(true);
            mContext.mFailButton = btn;
        }

        return btn;
    }

    private LinearLayout createSpecialLayout() {
        LinearLayout ll = createLinearLayout(LinearLayout.HORIZONTAL, MATCH_PARENT, 0, 1);

        // add pass btn
        ll.addView(createnButton(0, LinearLayout.LayoutParams.WRAP_CONTENT, 2, true));
        // add hover view
        ll.addView(createHoverView(0, MATCH_PARENT, 1));
        // add pass btn
        ll.addView(createnButton(0, LinearLayout.LayoutParams.WRAP_CONTENT, 2, false));

        return ll;
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
        mContentResolver = mContext.getContentResolver();
    }

    @Override
    public void destroy() {
        // TODO Auto-generated method stub
        super.destroy();
    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub
        super.resume();
        mHoverEnable = Settings.System.getInt(mContentResolver, "tp_touchless_enable", 0);
        if(mHoverEnable==0) {
            Settings.System.putInt(mContentResolver, "tp_touchless_enable", 1);
        }
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub
        if(mHoverEnable==0) {
            Settings.System.putInt(mContentResolver, "tp_touchless_enable", 0);
        }
        super.pause();
    }

    private class HoverView extends TextView {
        private boolean mPassed = false;

        public HoverView(Context context) {
            super(context);
            // TODO Auto-generated constructor stub
            mTotalHoverView++;
            if(mTotalHoverView==3
                    ||mTotalHoverView==11
                    ||mTotalHoverView==15
                    ||mTotalHoverView==23
                    ||mTotalHoverView==31
                    ||mTotalHoverView==35
                    ||mTotalHoverView==41) {
                mTotalHoverView_NeedTest++;
                this.setText(Integer.toString(mTotalHoverView_NeedTest));
                this.setBackgroundResource(R.drawable.hoverview_border_yellow);
                this.setOnHoverListener(new OnHoverListener() {
                    @Override
                    public boolean onHover(View v, MotionEvent event) {
                        // TODO Auto-generated method stub
                        int type = event.getToolType(event.getPointerCount()-1);
                        MMILog.d(TAG, "onHover: "+event.getActionMasked() + " type:"+type);
                        if(event.getActionMasked()==MotionEvent.ACTION_HOVER_ENTER
                                && type==MotionEvent.TOOL_TYPE_FINGER) {
                            if(mPassed==false) {
                                mPassed = true;
                                HoverView.this.setBackgroundResource(R.drawable.hoverview_border_green);
                                mPassedHoverView++;
                                if(mPassedHoverView == mTotalHoverView_NeedTest) {
                                    Hover.this.setPassButtonEnable(true);
                                }
                            }
                        }

                        return false;
                    }
                });
            }
            else {
                this.setBackgroundResource(R.drawable.hoverview_border_black);
            }
        }
    }
}
