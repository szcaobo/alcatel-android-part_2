/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/Value.java         */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/08/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

public class Value {
    public static boolean isAutoTest = false;
    public static boolean isAuto2Test = false;

    public final static int SCENARIO_AUTO = 0;
    public final static int SCENARIO_FULL_AUTO = 1;

    public final static int RESULT_CODE_FAIL = 0;
    public final static int RESULT_CODE_PASS = RESULT_CODE_FAIL - 1;
    public final static int RESULT_CODE_ERROR = RESULT_CODE_PASS - 1;
    public final static int RESULT_CODE_RESTART = RESULT_CODE_ERROR - 1;

    // extra info in intent to determine which mode the test activity starts
    public final static String USER_SELECT = "user_select";
    public final static String USER_SELECT_AUTO2 = "user_select_auto2";
    // test activity starts in UNKNOW mode
    public final static int USER_SELECT_MODE_UNKNOW = 0;
    // test activity starts in AUTO mode
    public final static int USER_SELECT_MODE_AUTO = 1;
    // test activity starts in MANU mode
    public final static int USER_SELECT_MODE_MANU = 2;
    // test activity starts in key event mode
    public final static int USER_SELECT_MODE_KEYEVENT = 3;

    public final static String USER_TITLE = "title";

    public final static String KEY_INTENT_STATE_HEADSET = "state";
    public final static int STATE_INTENT_HEADSET_PLUGGED = 1;
    public final static int STATE_INTENT_HEADSET_UNPLUGGED = 2;

    public final static int REQ_ROBUST_TP_RAW = 1000;
}
