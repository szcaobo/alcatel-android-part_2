/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/Firmware.java      */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 06/05/14| Shishun.Liu    |                    | FR-694468                  */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import java.io.IOException;

import com.android.mmi.util.MMILog;
import com.android.mmi.util.SysClassManager;
import com.nxp.nfc.NxpNfcAdapter;
import android.nfc.NfcAdapter;
import android.os.AsyncTask;

public class Firmware extends TestBase {
    private boolean mNFCEnable;
    private AsyncTask asynTask;

    private NfcAdapter mNfcAdapter = null;

    @Override
    public void run() {
        // TODO Auto-generated method stub
        setDefTextMessage("reading...");
        mNFCEnable = isNFCEnable();

        try{
            asynTask = new AsyncTask<Void, Void, String>() {
                @Override
                protected String doInBackground(Void... unused) {
                    return  getTPFirmware() + "\n"
                            + getNFCFirmware();
                }

                @Override
                protected void onPostExecute(String result) {
                    if(isCancelled()==false) {
                        setDefTextMessage(result);
                        setPassButtonEnable(true);
                    }
                }
            }.execute();
        }catch(Exception e){
            setDefTextMessage("Firmware read error");
        }
    }

    @Override
    public void destroy() {
        if(!mNFCEnable){
            if(isNFCEnable()){
                enableNFC(false);
            }
        }
        if(asynTask!=null) {
            asynTask.cancel(true);
        }
        super.destroy();
    }

    private String getTPFirmware() {
        SysClassManager fileManager = new SysClassManager();
        byte result[];

        fileManager.setFileInputStream(
               fileManager.fileInputOpen(SysClassManager.TP_FIRMWARE_FILE_IDOL4S));

        result=fileManager.readFileInputByte();
        fileManager.fileInputClose();
        if( result != null ) {
            return "TP: "+new String(result).trim();
        }
        else {
            return "TP: error";
        }
    }

    private String getNFCFirmware() {
        int count = 0;

        if (!mNFCEnable) {
            enableNFC(true);
        }

        while(!isNFCEnable()) {
            if(count < 10) {
                MMILog.d(TAG, "NFC adapter is disable, sleep(500)");
                count += 1;
                android.os.SystemClock.sleep(500);
            }
            else {
                MMILog.d(TAG, "NFC adapter is disable, getNFCFirmware return null");
                return "NFC: error";
            }
        }

        NfcAdapter mNfcAdapter = NfcAdapter.getDefaultAdapter(mContext);
        NxpNfcAdapter mNxpNfcAdapter = NxpNfcAdapter.getNxpNfcAdapter(mNfcAdapter);
        try {
            byte[] result = mNxpNfcAdapter.getFwVersion();
            return "NFC: " + result[0] + result[1];
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

         return "NFC: error";
    }

    private void enableNFC(boolean enable) {
//      NfcAdapter mNfcAdapter;
//      mNfcAdapter = NfcAdapter.getDefaultAdapter(mContext);
      if(mNfcAdapter!=null) {
          if(enable) {
              MMILog.d(TAG, "enable NFC Adatper");
              mNfcAdapter.enable();
          }
          else {
              MMILog.d(TAG, "disable NFC Adatper");
              mNfcAdapter.disable();
          }
      }
  }

  private boolean isNFCEnable() {
//      NfcAdapter mNfcAdapter;
      mNfcAdapter = NfcAdapter.getDefaultAdapter(mContext);
      if(mNfcAdapter!=null) {
          return mNfcAdapter.isEnabled();
      }
      return false;
  }
}
