/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/Nfc.java           */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/08/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import java.io.File;
import java.io.FilenameFilter;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.nfc.tech.NfcA;
import android.nfc.tech.NfcB;
import android.nfc.tech.NfcF;
import android.nfc.tech.NfcV;
import android.os.Handler;
import android.os.Message;

public class Nfc extends TestBase {
    public static final int MESSAGE_NFC_TIMEOUT = 1024;
    public static final int LENGTH_NFC_TIMEOUT = 20000;

    public static enum NfcState {
        NO_SUPPORT, TIME_OUT;
    }

    private boolean isNfcOldState;
    private PendingIntent pendingIntent;
    private IntentFilter ndef;
    private IntentFilter tech;
    private IntentFilter tag;
    private IntentFilter[] intentFiltersArray;
    private String[][] techListsArray;

    private NfcAdapter mNfcAdapter;
    private Handler handler;
    private boolean isEnable;

    @Override
    public void run() {
        // TODO Auto-generated method stub
        pendingIntent = PendingIntent.getActivity(mContext, 0, new Intent(
                mContext, mContext.getClass())
                .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);

        tech = new IntentFilter(NfcAdapter.ACTION_TECH_DISCOVERED);
        ndef = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
        tag = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);

        intentFiltersArray = new IntentFilter[] { ndef, tech, tag };

        techListsArray = new String[][] {
                new String[] { NfcF.class.getName() },
                new String[] { NfcA.class.getName() },
                new String[] { NfcB.class.getName() },
                new String[] { NfcV.class.getName() }, };

        handler = new MemberHandler();

        mNfcAdapter = NfcAdapter.getDefaultAdapter(mContext);
        if (mNfcAdapter != null) {
            setDefTextMessage("NFC: Mobile read...");
            enableNfc();
            handler.sendEmptyMessageDelayed(MESSAGE_NFC_TIMEOUT,
                    LENGTH_NFC_TIMEOUT);
        } else {
            setViewState(NfcState.NO_SUPPORT);
        }
//        if(isFileExist("/dev", "bcm2079x", true)) {
//            setDefTextMessage("NFC init OK");
//            setPassButtonEnable(true);
//            setFailButtonEnable(true);
//        }
//        else {
//            setDefTextMessage("NFC init FAIL");
//            setPassButtonEnable(false);
//            setFailButtonEnable(true);
//        }
    }

    private boolean isFileExist(String path, final String keyWord, final boolean matchFullName) {
        File file   = new File(path);

        FilenameFilter filter = new FilenameFilter() {
            @Override
            public boolean accept(File dir, String filename) {
                // TODO Auto-generated method stub
                if(matchFullName) {
                    return filename.equals(keyWord);
                }
                else {
                    return filename.contains(keyWord);
                }
            }
        };

        String[] list = file.list(filter);
        if(null!=list && list.length>0) {
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub
        super.resume();
        if (mNfcAdapter != null) {
            mNfcAdapter.enableForegroundDispatch(mContext, pendingIntent,
                    intentFiltersArray, techListsArray);
        }
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub
        super.pause();
        if (mNfcAdapter != null) {
            mNfcAdapter.disableForegroundDispatch(mContext);
        }
    }

    @Override
    public void destroy() {
        // TODO Auto-generated method stub
        super.destroy();
        handler.removeCallbacksAndMessages(null);
        loadOldState();
    }

    private void setViewState(NfcState state) {
        switch (state) {
        case NO_SUPPORT:
            setDefTextMessage(R.string.nfc_not_support);
            setPassButtonEnable(false);
            setFailButtonEnable(true);
            break;
        case TIME_OUT:
            setDefTextMessage(R.string.nfc_timeout);
            setPassButtonEnable(false);
            setFailButtonEnable(true);
            break;
        }
    }

    private void enableNfc() {
        isEnable = mNfcAdapter.isEnabled();
        if (!isEnable)
            mNfcAdapter.enable();
    }

    private void loadOldState() {
        if (mNfcAdapter != null && !isEnable)
            mNfcAdapter.disable();
    }

    @Override
    public void onNewIntent(Intent intent) {
        String textString = "NFC: Detected\n";

        // add by shishun.liu for bug[519305]
        Tag tagFromIntent = (Tag) intent
                .getParcelableExtra(NfcAdapter.EXTRA_TAG);
        if (tagFromIntent != null) {
            // read id
            byte[] bytesId = tagFromIntent.getId();
            String stringId = bytesToHexAddressString(bytesId);
            if (stringId != null) {
                textString += "ID: " + stringId + "\n";
            } else {
                textString += "ID: none\n";
            }

            // read type
            Ndef ndef = Ndef.get(tagFromIntent);
            if (ndef != null) {
                String nfcForumType = ndef.getType();
                if (nfcForumType.equals(Ndef.NFC_FORUM_TYPE_4)) {
                    textString += "Type: 4\n";
                } else if (nfcForumType.equals(Ndef.NFC_FORUM_TYPE_3)) {
                    textString += "Type: 3\n";
                } else if (nfcForumType.equals(Ndef.NFC_FORUM_TYPE_2)) {
                    textString += "Type: 2\n";
                } else if (nfcForumType.equals(Ndef.NFC_FORUM_TYPE_1)) {
                    textString += "Type: 1\n";
                } else {
                    textString += "Type: unknow\n";
                }
            } else {
                textString += "Type: unknow\n";
            }
        }

        setDefTextMessage(textString);
        handler.removeMessages(MESSAGE_NFC_TIMEOUT);
        setPassButtonEnable(true);
    }

    // //////////////////////////////////////////////////////////////////////////////
    // MARK Inner Classes
    // //////////////////////////////////////////////////////////////////////////////
    private class MemberHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
            case MESSAGE_NFC_TIMEOUT:
                setViewState(NfcState.TIME_OUT);
                break;
            }
        }
    }

    private String bytesToHexAddressString(byte[] src) {
        StringBuilder stringBuilder = new StringBuilder("");
        if (src == null || src.length <= 0) {
            return null;
        }

        char[] buffer = new char[3];
        buffer[2] = ':';
        for (int i = 0; i < src.length; i++) {
            buffer[0] = Character.forDigit((src[i] >>> 4) & 0x0f, 16);
            buffer[1] = Character.forDigit(src[i] & 0x0f, 16);
            stringBuilder.append(buffer);
        }
        stringBuilder.deleteCharAt(stringBuilder.length() - 1);

        return stringBuilder.toString().toUpperCase();
    }
}
