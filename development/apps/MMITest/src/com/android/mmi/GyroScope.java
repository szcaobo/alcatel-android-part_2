/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/GyroScope.java     */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/23/14| Shishun.Liu    |                    | create for GypoScope test  */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import android.content.ContentResolver;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;

import com.android.mmi.util.GyroScopeJNI;


public class GyroScope extends TestBase implements SensorEventListener {
    private Sensor mGyroScopeSensor = null;
    private SensorManager mSensorManager = null;
    private double timestamp;
    private static final double R2D = 180.0f/(double)Math.PI;
    private static final double NS2S = 1.0f / 1000000000.0f;
    private double integratedValues[] = new double[3];
    private double lastValues[] = new double[3];

    private AsyncTask<Object, Void, Integer> asynTask;

    private int degreeRotation_90 = 0;
    private int degreeRotation_180 = 0;
    private ContentResolver mContentResolver = null;

    @Override
    public void run() {
        // TODO Auto-generated method stub
        mContentResolver = mContext.getContentResolver();
        try {
            degreeRotation_90 = Settings.System.getInt(mContentResolver,
                    Settings.System.ACCELEROMETER_ROTATION);
            degreeRotation_180 = Settings.System.getInt(mContentResolver,
                    "degree_rotation");
        } catch (SettingNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

//        GyroScope.this.setDefTextMessage("BIST test: Waitting...");

        timestamp = 0;
        integratedValues[0] = 0;
        integratedValues[1] = 0;
        integratedValues[2] = 0;
        mSensorManager = (SensorManager) mContext.getSystemService(Context.SENSOR_SERVICE);
        mGyroScopeSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        if( mGyroScopeSensor==null ){
            setDefTextMessage("Not supported!");
        }
        else {
            setPassButtonEnable(true);
        }
    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub
        super.resume();
        if( mGyroScopeSensor!=null ){
           // mSensorManager.registerListener(this, mGyroScopeSensor,
           //  SensorManager.SENSOR_DELAY_NORMAL);
        }

        if (degreeRotation_90 == 1) {
            Settings.System.putInt(mContentResolver,
                    Settings.System.ACCELEROMETER_ROTATION, 0);
        }

        if (degreeRotation_180 == 1) {
            Settings.System.putInt(mContentResolver, "degree_rotation", 0);
        }

        asynTask = new SensorTestTask().execute();
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub
        super.pause();

        asynTask.cancel(true);

        if( mGyroScopeSensor!=null ){
            mSensorManager.unregisterListener(this);
        }

        if (degreeRotation_90 == 1) {
            Settings.System.putInt(mContentResolver,
                    Settings.System.ACCELEROMETER_ROTATION, 1);
        }

        if (degreeRotation_180 == 1) {
            Settings.System.putInt(mContentResolver, "degree_rotation", 1);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        // TODO Auto-generated method stub
        if (timestamp == 0) {
            timestamp = event.timestamp;
            lastValues[0] = event.values[0];
            lastValues[1] = event.values[1];
            lastValues[2] = event.values[2];
        }
        else {
            final double dT = (event.timestamp - timestamp) * NS2S;
            integratedValues[0] += (event.values[0]+lastValues[0])/2.0f*dT;
            integratedValues[1] += (event.values[1]+lastValues[1])/2.0f*dT;
            integratedValues[2] += (event.values[2]+lastValues[2])/2.0f*dT;
            this.setDefActionMessage( String.format("Vx: %f\nVy: %f\nVz: %f\n\ndx: %.2f\ndy: %.2f\ndz: %.2f",
                    event.values[0], event.values[1], event.values[2],
                    integratedValues[0]*R2D, integratedValues[1]*R2D, integratedValues[2]*R2D) );

            timestamp = event.timestamp;
            lastValues[0] = event.values[0];
            lastValues[1] = event.values[1];
            lastValues[2] = event.values[2];
        }
    }

    private class SensorTestTask extends AsyncTask<Object, Void, Integer> {

            @Override
            protected Integer doInBackground(Object... params) {

//             int rv;
//             rv = GyroScopeJNI.runSensorTest();
//
//             return rv;

                return 0;
            }

            @Override
             protected void onPostExecute(Integer testResult) {
              if(isCancelled()){
                  return;
              }

              if(testResult == 0){
//                  GyroScope.this.setDefTextMessage("BIST test: OK");
                  if( mGyroScopeSensor!=null ){
                       mSensorManager.registerListener(GyroScope.this, mGyroScopeSensor,
                               SensorManager.SENSOR_DELAY_NORMAL);
                  }
                  setPassButtonEnable(true);
              }
              else{
//                  GyroScope.this.setDefTextMessage("BIST test: NOK");
                  setPassButtonEnable(false);
              }
            }
     }
}
