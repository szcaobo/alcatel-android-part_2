/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/DualSim.java       */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/08/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.nfc.NfcAdapter;
import android.os.AsyncTask;
import android.telephony.TelephonyManager;

import java.io.IOException;

import com.android.mmi.util.MMILog;
import com.android.mmi.util.TestItemManager;
import com.nxp.nfc.NxpNfcAdapter;
public class DualSim extends TestBase {

    private final static String ACTION_SIM_STATE_CHANTED = "android.intent.action.SIM_STATE_CHANGED";
    private BroadcastReceiver broadcastReceiver;
    private IntentFilter intentFilter;

    private boolean simOk;
    private boolean mNFCEnable;
    private NfcAdapter mNfcAdapter = null;
    private AsyncTask asynTask = null;
    private boolean mSupportSimNFC = true;

    protected void diableSimNFCTest() {
        mSupportSimNFC = false;
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
        if(mSupportSimNFC) {
            mNFCEnable = !isNFCDisable();
            setDefActionMessage("reading...");
        }

        broadcastReceiver = new SimReceiver();
        intentFilter = new IntentFilter(ACTION_SIM_STATE_CHANTED);

        getSimCardState();
    }

    private class SimReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context arg0, Intent intent) {
            String action = intent.getAction();
            if (action.equals(ACTION_SIM_STATE_CHANTED)) {
                getSimCardState();
                if(mSupportSimNFC) {
                    getNfcSim();
                }
            }
        }
    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub
        super.resume();
        mContext.registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub
        super.pause();
        mContext.unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void destroy() {
        if(mSupportSimNFC) {
            if (mNFCEnable) {
                if (isNFCDisable()) {
                    enableNFC(true);
                }
            }
            if (asynTask != null) {
                asynTask.cancel(true);
                asynTask = null;
            }
        }
        super.destroy();
    }

    private void getSimCardState() {
        TelephonyManager tm = TelephonyManager.getDefault();
        int simcount = tm.getPhoneCount();
        int oKflag = 0;
        String simStates[] = new String[2];
        simStates[0] = "";
        simStates[1] = "";
        String adbMessage = "";

        for (int i = 0; i < simcount; i++) {
            int simcard = i + 1;
            switch (tm.getSimState(i)) {
            case TelephonyManager.SIM_STATE_ABSENT:
                if(adbMessage.isEmpty()) {
                    adbMessage += String.format("RESULT:SIM%d CARD IS MISSING", simcard);
                }
                else {
                    adbMessage += String.format("|SIM%d CARD IS MISSING", simcard);
                }
                simStates[i] = mContext.getString(R.string.sim_state_missing, simcard);
                setPassButtonEnable(false);
                break;
            case TelephonyManager.SIM_STATE_READY:
                if(adbMessage.isEmpty()) {
                    adbMessage += String.format("RESULT:SIM%d CARD IS DETECTED", simcard);
                }
                else {
                    adbMessage += String.format("|SIM%d CARD IS DETECTED", simcard);
                }
                simStates[i] = mContext.getString(R.string.sim_state_ok, simcard);
                oKflag++;
                break;

            case TelephonyManager.SIM_STATE_PIN_REQUIRED:
                /**
                 * SIM card state: Locked: requires the user's SIM PUK to unlock
                 */
            case TelephonyManager.SIM_STATE_PUK_REQUIRED:
                /** SIM card state: Locked: requires a network PIN to unlock */
            case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
                if(adbMessage.isEmpty()) {
                    adbMessage += String.format("RESULT:SIM%d CARD IS DETECTED", simcard);
                }
                else {
                    adbMessage += String.format("|SIM%d CARD IS DETECTED", simcard);
                }
                simStates[i] = mContext.getString(R.string.sim_state_deteected, simcard);
                break;

            /*case TelephonyManager.SIM_STATE_CARD_IO_ERROR:
                simStates[i] = getString(R.string.sim_state_ioerror, simcard);
                break;*/

            case TelephonyManager.SIM_STATE_UNKNOWN:
                /**
                 * SIM card state: Locked: requires the user's SIM PIN to unlock
                 */
            default:
                if(adbMessage.isEmpty()) {
                    adbMessage += String.format("RESULT:SIM%d CARD STATE UNKNOW", simcard);
                }
                else {
                    adbMessage += String.format("|SIM%d CARD STATE UNKNOW", simcard);
                }
                simStates[i] = mContext.getString(R.string.sim_state_unknown, simcard);
                oKflag--;
                break;
            }

        }

        if(simcount==1) {
            adbMessage = adbMessage.replace("1", "");
        }
        MMILog.i("MMITEST_GIM_SIM", adbMessage);

        if (oKflag == -simcount) {
            setDefTextMessage(R.string.sim_state_2nd_unknown, Color.RED);
        } else {
            String strvar=null;
            if(simcount == 2){
                strvar=simStates[0] + "\n" + simStates[1];
            }else{
                strvar=simStates[0];
                strvar=strvar.replace("1", "");
            }
            if(oKflag == simcount) {
                setDefTextMessage(strvar, Color.WHITE);
                if(mSupportSimNFC==false) {
                    setPassButtonEnable(true);
                }
                simOk = true;
            }
            else {
                setDefTextMessage(strvar, Color.RED);
            }
        }
    }

    private boolean isNFCDisable() {
        mNfcAdapter = NfcAdapter.getDefaultAdapter(mContext);
        if (mNfcAdapter != null) {
            return (mNfcAdapter.getAdapterState()==NfcAdapter.STATE_OFF);
        }
        return true;
    }

    private void enableNFC(boolean enable) {
        if (mNfcAdapter != null) {
            if (enable) {
                MMILog.d(TAG, "enable NFC Adatper");
                mNfcAdapter.enable();
            } else {
                MMILog.d(TAG, "disable NFC Adatper");
                mNfcAdapter.disable();
            }
        }
    }

    private void getNfcSim() {
        try {
            asynTask = new AsyncTask<Void, Void, String>() {
                @Override
                protected String doInBackground(Void... unused) {
                    int count = 0;

                    if (mNFCEnable) {
                        enableNFC(false);
                    }

                    while (!isNFCDisable()) {
                        if (count < 10) {
                            MMILog.d(TAG, "NFC adapter is enable, sleep(500)");
                            count += 1;
                            android.os.SystemClock.sleep(500);
                        } else {
                            MMILog.d(TAG, "NFC adapter is enable, getNfcSim return -1");
                            return "FAIL";
                        }
                    }

                    NfcAdapter mNfcAdapter = NfcAdapter.getDefaultAdapter(mContext);
                    NxpNfcAdapter mNxpNfcAdapter = NxpNfcAdapter.getNxpNfcAdapter(mNfcAdapter);
                    int result = -1;
                    try {
                        MMILog.d(TAG, "testNfcSwp() start");
                        result = mNxpNfcAdapter.testNfcSwp();
                        MMILog.d(TAG, "testNfcSwp() end");
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        MMILog.d(TAG, "testNfcSwp() error: " + e.getMessage());
                    }
                    MMILog.d(TAG, "testNfcSwp(), result: " + result);

                    if (result == 0) {
                        return "PASS";
                    } else {
                        return "FAIL";
                    }
                }

                @Override
                protected void onPostExecute(String result) {
                    if (isCancelled() == false) {
                        if ("PASS".equals(result)) {
                            setDefActionMessage("nfc sim is OK", Color.WHITE);
                            if(simOk) {
                                setPassButtonEnable(true);
                            }
                        } else {
                            setDefActionMessage("nfc sim is NOK", Color.RED);
                        }

                    }
                }
            }.execute();
        } catch (Exception e) {
            setDefTextMessage("read error");
        }

    }
}
