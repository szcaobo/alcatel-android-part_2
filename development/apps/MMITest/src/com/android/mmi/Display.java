/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/Display.java       */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/08/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import android.graphics.Point;
import android.util.DisplayMetrics;

import com.android.mmi.util.DisplayView;
import com.android.mmi.util.DisplayView.DisplayItem;
import com.android.mmi.util.TestItemManager;

public class Display extends TestBase {
    private DisplayView displayView;
    private DisplayItem mDisplayItem;
    private DisplayItem displayItems[] = DisplayItem.values();

    @Override
    public void create(CommonActivity a) {
        // TODO Auto-generated method stub
        super.create(a);

        setContentView(R.layout.test_display, LAYOUTTYPE.LAYOUT_CUST_WITH_PASSFAIL);

        hideNavigationBar();
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub

        String product = TestItemManager.getProduct();
        int displayWidth = 0;
        int displayHeight = 0;

        DisplayMetrics dm = getRealMetrics();
        displayWidth = dm.widthPixels;
        displayHeight = dm.heightPixels;

        mDisplayItem = displayItems[0];
        displayView = (DisplayView) findViewById(R.id.displayview);
        displayView.setViewSize(displayWidth, displayHeight);
        displayView.setDisplayItem(mDisplayItem);
        setButtonAnimateVisible();
        setPassButtonEnable(true);
        setButtonTransparent();
    }

    @Override
    public void onPassClick() {
        // TODO Auto-generated method stub
        if( mDisplayItem.ordinal()<displayItems.length-1) {
            mDisplayItem = displayItems[mDisplayItem.ordinal()+1];
            displayView.setDisplayItem(mDisplayItem);
            setButtonAnimateVisible();
        }
        else {
            super.onPassClick();
        }
    }

    @Override
    public void destroy() {
        super.destroy();
        resetConfirmed();
    }

}
