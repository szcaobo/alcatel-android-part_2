/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/TestBase.java      */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/08/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import java.lang.reflect.Method;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Handler;
import android.os.Message;
import android.os.UserHandle;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.android.mmi.util.KeyCodeFilter;
import com.android.mmi.util.MMILog;
import com.android.mmi.util.TestItemManager;

public abstract class TestBase {
    protected String TAG;
    protected CommonActivity mContext;
    public String mTitle = "NOT SET";
    public LAYOUTTYPE mLayoutType = LAYOUTTYPE.LAYOUT_DEF;
    private Handler handler;
    private final int MESSAGE_BUTTON_VISIBLE_ANIMATE = 1024;
    private Animation fadeinAnimation;
    private String mClassShortName;
    protected int mUserSelectMode = Value.USER_SELECT_MODE_UNKNOW;

    private long mStartTime;

//    protected static final String TCT_MMITEST = "tct_mmitest";

    public enum LAYOUTTYPE{
        LAYOUT_DEF,
        LAYOUT_CUST_WITH_PASSFAIL,
        LAYOUT_CUST_WITHOUT_PASSFAIL
    };

    public TestBase() {
        // TODO Auto-generated constructor stub
        String className = this.getClass().getName();
        int index = className.lastIndexOf('.');
        mClassShortName = className.substring(index<0?0:index+1);
        TAG = "MMITest."+mClassShortName;
    }

    // the child class should setContentView in create if used special layout
    public void create(CommonActivity a) {
        mContext = a;
        Intent intent = mContext.getIntent();
        if(intent!=null) {
            mUserSelectMode = intent.getIntExtra(Value.USER_SELECT, Value.USER_SELECT_MODE_UNKNOW);
        }
     }

    // all test code should be here
    public abstract void run();

    public void resume() {
        //
    }

    public void pause() {
        //
    }

    public void destroy() {
        //
    }

    public void onBackPressed() {
        if(Value.isAutoTest==false && Value.isAuto2Test==false) {
            mContext.finish();
        }
    }

    public void onPassClick() {
        Intent intent = mContext.getIntent();
        mContext.setResult(Value.RESULT_CODE_PASS, intent);
        mContext.finish();
    }

    public void onFailClick() {
        Intent intent = mContext.getIntent();
        mContext.setResult(Value.RESULT_CODE_FAIL, intent);
        mContext.finish();
    }

    public void onAuto2Click() {

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return mContext.super_onKeyDown(keyCode, event);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        return mContext.super_onKeyUp(keyCode, event);
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
/*        int keycode = event.getKeyCode();
        if (event.isLongPress()) {
            // long press just manages some exit keys
            switch (keycode) {
            case KeyEvent.KEYCODE_HOME:
                if (!mClassShortName.equals("MainScreen")
                        && !mClassShortName.equals("AutoTestMain")
                        && !mClassShortName.equals("MiniTestMenu")
                        && mUserSelectMode == Value.USER_SELECT_MODE_AUTO) {
                    MMILog.d(TAG, mClassShortName+" finish when home key long press");
                    Intent intent = mContext.getIntent();
                    mContext.setResult(Value.RESULT_CODE_RESTART, intent);
                    mContext.finish();
                }
                break;
            }
        }*/

        boolean isPressHome = false;
        switch (event.getKeyCode()) {
        case KeyEvent.KEYCODE_HOME:
            isPressHome = true;
            break;
        }

        /*else*/ if(event.getAction()==KeyEvent.ACTION_DOWN) {
            if(isPressHome){
                mStartTime = System.currentTimeMillis();
            }

            if(mUserSelectMode == Value.USER_SELECT_MODE_KEYEVENT &&
                    KeyCodeFilter.isUserCustomizableKeycode(event.getKeyCode())) {
                Intent intent = mContext.getIntent();
                mContext.setResult(event.getKeyCode(), intent);
                mContext.finish();
                return true;
            }
            else {
                MMILog.d(TAG, "not in keyevent mode or not user customizable keycode");
            }
        }else if(event.getAction()==KeyEvent.ACTION_UP){
            if(isPressHome){
                long elapsed = System.currentTimeMillis() - mStartTime;
                MMILog.d(TAG, "elapsed: " + elapsed);
                if(elapsed > 3000){
                    if (!mClassShortName.equals("MainScreen")
                            && !mClassShortName.equals("AutoTestMain")
                            && !mClassShortName.equals("MiniTestMenu")
                            && mUserSelectMode == Value.USER_SELECT_MODE_AUTO) {
                        MMILog.d(TAG, mClassShortName+" finish when home key long press");
                        Intent intent = mContext.getIntent();
                        mContext.setResult(Value.RESULT_CODE_RESTART, intent);
                        mContext.finish();
                    }
                }
                mStartTime = 0;
            }
        }
        return mContext.super_dispatchKeyEvent(event);
    }

    public void onNewIntent(Intent intent) {
        //
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////

    protected View findViewById(int id) {
        return mContext.findViewById(id);
    }

    protected void setContentView(int id, LAYOUTTYPE layoutType) {
        mContext.setContentView(id);
        mLayoutType = layoutType;
    }

    protected void setContentView(View view, LAYOUTTYPE layoutType) {
        mContext.setContentView(view);
        mLayoutType = layoutType;
    }

    protected void setFailButtonText(int resId) {
        if(mContext.mFailButton!=null){
            mContext.mFailButton.setText(resId);
        }
    }

    protected void setPassButtonText(int resId) {
        if(mContext.mPassButton!=null){
            mContext.mPassButton.setText(resId);
        }
    }

    protected void setFailButtonText(String sText) {
        if(mContext.mFailButton!=null){
            mContext.mFailButton.setText(sText);
        }
    }

    protected void setPassButtonText(String sText) {
        if(mContext.mPassButton!=null){
            mContext.mPassButton.setText(sText);
        }
    }

    protected void setFailButtonEnable(boolean enable) {
        if(mContext.mFailButton!=null){
            mContext.mFailButton.setEnabled(enable);
        }
    }

    protected void setPassButtonEnable(boolean enable) {
        if(mContext.mPassButton!=null){
            mContext.mPassButton.setEnabled(enable);
        }
    }

    protected void setAuto2ButtonEnable(boolean enable) {
        if(mContext.mAuto2Button!=null){
            mContext.mAuto2Button.setEnabled(enable);
        }
    }

    protected void setButtonTransparent() {
        if(mContext.mFailButton!=null && mContext.mPassButton!=null) {
            mContext.mFailButton.setBackgroundColor(Color.TRANSPARENT);
            mContext.mFailButton.setTextColor(Color.RED);
            mContext.mPassButton.setBackgroundColor(Color.TRANSPARENT);
            mContext.mPassButton.setTextColor(Color.GREEN);
        }
    }

    protected void setButtonGone() {
        if(mContext.mFailButton!=null && mContext.mPassButton!=null) {
            mContext.mFailButton.setVisibility(View.GONE);
            mContext.mPassButton.setVisibility(View.GONE);
        }
    }

    private void setButtonVisible(Animation animation) {
        if(mContext.mFailButton!=null && mContext.mPassButton!=null) {
            mContext.mFailButton.startAnimation(animation);
            mContext.mPassButton.startAnimation(animation);
            mContext.mFailButton.setVisibility(View.VISIBLE);
            mContext.mPassButton.setVisibility(View.VISIBLE);
        }
    }

    private void setButtonInvisible() {
        if(mContext.mFailButton!=null && mContext.mPassButton!=null) {
            mContext.mFailButton.setVisibility(View.INVISIBLE);
            mContext.mPassButton.setVisibility(View.INVISIBLE);
        }
    }

    protected void setButtonVisible() {
        if(mContext.mFailButton!=null && mContext.mPassButton!=null) {
            mContext.mFailButton.setVisibility(View.VISIBLE);
            mContext.mPassButton.setVisibility(View.VISIBLE);
        }
    }

    protected void setButtonAnimateVisible() {
        setButtonAnimateVisible(500);
    }

    protected void setButtonAnimateVisible(int mTimeDelay) {
        if (mTimeDelay >0) {
            setButtonInvisible();
            if(handler==null) {
                handler = new TestBaseHandler();
            }
            handler.sendEmptyMessageDelayed(MESSAGE_BUTTON_VISIBLE_ANIMATE, mTimeDelay);
        }
    }

    protected void setDefTextMessage(int resId) {
        if(mContext.mTextMessage!=null) {
            mContext.mTextMessage.setText(resId);
        }
    }

    protected void setDefTextMessage(int resId, int color) {
        if(mContext.mTextMessage!=null) {
            mContext.mTextMessage.setText(resId);
            mContext.mTextMessage.setTextColor(color);
        }
    }

    protected void setDefTextMessage(String sMessage) {
        if(mContext.mTextMessage!=null) {
            mContext.mTextMessage.setText(sMessage);
        }
    }

    protected void setDefTextMessage(String sMessage, int color) {
        if(mContext.mTextMessage!=null) {
            mContext.mTextMessage.setText(sMessage);
            mContext.mTextMessage.setTextColor(color);
        }
    }

    protected void setDefActionMessage(int resId) {
        if(mContext.mTextAction!=null) {
            mContext.mTextAction.setText(resId);
        }
    }

    protected void setDefActionMessage(String sMessage) {
        if(mContext.mTextAction!=null) {
            mContext.mTextAction.setText(sMessage);
        }
    }

    protected void setDefActionMessage(String sMessage, int color) {
        if(mContext.mTextAction!=null) {
            mContext.mTextAction.setText(sMessage);
            mContext.mTextAction.setTextColor(color);
        }
    }

    private class TestBaseHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == MESSAGE_BUTTON_VISIBLE_ANIMATE) {
                if(fadeinAnimation==null) {
                    fadeinAnimation = AnimationUtils.loadAnimation(mContext, R.anim.fadein);
                }
                setButtonVisible(fadeinAnimation);
            }
        }
    }

    protected void getDisplaySize(Point size) {
        mContext.getWindowManager().getDefaultDisplay().getSize(size);
    }

    private boolean needResetConfirm = false;

    protected void hideNavigationBar() {
        String value = Settings.Secure.getStringForUser(mContext.getContentResolver(),
                Settings.Secure.IMMERSIVE_MODE_CONFIRMATIONS,
                UserHandle.USER_CURRENT);

        if(!"confirmed".equals(value)){
            Settings.Secure.putStringForUser(mContext.getContentResolver(),
                    Settings.Secure.IMMERSIVE_MODE_CONFIRMATIONS,
                    "confirmed",
                    UserHandle.USER_CURRENT);
            needResetConfirm = true;
        }

        final View mDecorView = mContext.getWindow().getDecorView();
        final int flags = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        mDecorView.setSystemUiVisibility(flags);
        mDecorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                if ((visibility & View.SYSTEM_UI_FLAG_HIDE_NAVIGATION) == 0) {
                    mDecorView.setSystemUiVisibility(flags);
                }
            }
        });
    }

    protected void resetConfirmed() {
        if(needResetConfirm){
            Settings.Secure.putStringForUser(mContext.getContentResolver(),
                    Settings.Secure.IMMERSIVE_MODE_CONFIRMATIONS,
                    null,
                    UserHandle.USER_CURRENT);
        }
    }

    protected DisplayMetrics getRealMetrics() {
        DisplayMetrics dm = new DisplayMetrics();
        @SuppressWarnings("rawtypes")
        Class c;
        try {
            c = Class.forName("android.view.Display");
            @SuppressWarnings("unchecked")
            Method method = c.getMethod("getRealMetrics", DisplayMetrics.class);
            method.invoke(mContext.getWindowManager().getDefaultDisplay(), dm);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dm;
    }
}
