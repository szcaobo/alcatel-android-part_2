/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/Exit.java          */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/08/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import java.lang.reflect.Method;

import android.app.ActivityManager;
import android.content.Context;

public class Exit extends TestBase {

    @Override
    public void run() {
        // TODO Auto-generated method stub
        ActivityManager am=(ActivityManager)mContext.getSystemService(Context.ACTIVITY_SERVICE);

        try {
            Method forceStopPackage = am.getClass()
                    .getDeclaredMethod("forceStopPackage",
                            String.class);
            forceStopPackage.setAccessible(true);
            forceStopPackage.invoke(am, "com.android.mmi");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
