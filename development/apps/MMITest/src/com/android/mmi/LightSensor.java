/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/LightSensor.java   */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/08/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;


import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.widget.TextView;

import com.android.mmi.util.MMILog;


public class LightSensor extends TestBase implements SensorEventListener {
    private SensorManager mSensorManager;
    private Sensor lsensor;

    private TextView mLsensorMain;
    private TextView mLsensorDark;
    private TextView mLsensorBright;
    private TextView mLightStatus;

    private String mMain = null;
    private String mDark = null;
    private String mBright = null;

    protected int HIGH;
    protected int LOW;
    private int fh = -1;
    private int fl = -1;

    @Override
    public void create(CommonActivity a) {
        super.create(a);
        setContentView(R.layout.test_lightsensor, LAYOUTTYPE.LAYOUT_CUST_WITH_PASSFAIL);
        mLsensorMain = (TextView) findViewById(R.id.lsensor_main);
        mLsensorDark = (TextView) findViewById(R.id.lsensor_dark);
        mLsensorBright = (TextView) findViewById(R.id.lsensor_bright);
        mLightStatus = (TextView) findViewById(R.id.light_status);
    }

    protected void init() {
        HIGH = 20;
        LOW  = 10;
    }

    @Override
    public void run() {
        init();
        mSensorManager = (SensorManager) mContext.getSystemService(mContext.SENSOR_SERVICE);
        lsensor = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub
        mSensorManager.registerListener(this,
                mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT),
                SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void pause() {
        if (mSensorManager != null) {
            mSensorManager.unregisterListener(this);
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        // TODO Auto-generated method stub
        MMILog.d(TAG, "onSensorChanged: (" + event.values[0] + ", " + event.values[1]
                + ", " + event.values[2] + ")");

        boolean isDark = false;
        boolean isBright = false;
        int value = (int) event.values[0];
        if (value < LOW) {
            fl = 1;
            isDark = true;
        } else if (value > HIGH) {
            fh = 1;
            isBright =true;
        }
        mLsensorMain.setText("ambient light is " + value);
        mLsensorMain.setTextSize(20);
        mLsensorMain.setTextColor(Color.WHITE);
        if (fl > 0) {
            mLsensorDark.setText("dark : OK");
        }

        if (fh > 0) {
            mLsensorBright.setText("bright : OK");
        }

        if (fl > 0 && fh > 0) {
            setPassButtonEnable(true);
        }

        if (isBright) {
            mLightStatus.setText("Bright");
        } else if (isDark) {
            mLightStatus.setText("Dark");
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // TODO Auto-generated method stub

    }

}
