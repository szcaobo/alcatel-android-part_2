/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/MainCamera.java    */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/08/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.app.Dialog;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusMoveCallback;

import com.android.mmi.camera.CameraDialog;
import com.android.mmi.camera.ui.FocusIndicatorRotateLayout;
import com.android.mmi.util.JRDClient;
import com.android.mmi.util.MMILog;
import android.os.SystemProperties;//jeffzhang add

public class MainCamera extends FrontCamera {
    private FocusIndicatorRotateLayout mFocusIndicatorRotateLayout;
    private AutoFocusMoveCallback mAutoFocusMoveCallback;

    private static String CAMERA_TYPE = "/sys/class/vcm_type/value";

    @Override
    public void create(CommonActivity a) {
        super.create(a);
        mTargetCameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
        super.run();
        mFocusIndicatorRotateLayout = (FocusIndicatorRotateLayout) findViewById(R.id.focus_indicator_rotate_layout);
        if(mFocusIndicatorRotateLayout!=null) {
            mAutoFocusMoveCallback = new AutoFocusMoveCallback() {
                @Override
                public void onAutoFocusMoving(boolean start, Camera camera) {
                    // TODO Auto-generated method stub
                    if (!start) {
                        mFocusIndicatorRotateLayout.showStart();
                    } else {
                        mFocusIndicatorRotateLayout.showSuccess(true);
                    }
                }
            };

            if(mCameraTextureView!=null) {
                mCameraTextureView.setAutoFocusMoveCallback(mAutoFocusMoveCallback);
            }
            else {
                MMILog.d(TAG, "mCameraTextureView=null");
            }
        }

        String cameraType = getCameraType();
        String waringTitle = "";
        int nv = getAreaType();
        MMILog.i(TAG,"NV95 =====>" + nv);

        if((nv&0x18) == 0x0){
            match_result=true;
        }else if (((nv & 0x10) == 0x10 &&(cameraType.contains("FF"))) || ((nv & 0x8)== 0x8 && cameraType.contains("AF"))){
            waringTitle = "MATCH";
            match_result=true;
        }else {
            match_result=false;
            waringTitle = "NOT MATCH";
        }

        if(!match_result){
            setPassButtonEnable(false);
            Dialog dialog = new CameraDialog(mContext,waringTitle,cameraType,nv);
            dialog.show();
        }
    }

    private String getCameraType() {
        String cameraType = null;
        try {
            InputStream is = new FileInputStream(CAMERA_TYPE);
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            cameraType = reader.readLine();
            reader.close();
            is.close();
        } catch (Exception e){
            e.printStackTrace();
            MMILog.e(TAG, "get camera type fail");
        }
        if (cameraType != null && !cameraType.equals("")) {
            if (cameraType.contains("FF")){
                return new String("TYPE: FF");
            }else{
                return new String("TYPE: AF");
            }
        } else {
            return new String("TYPE: ERROR");
        }

    }

    private int getAreaType() {
        int area = 0;
        JRDClient rpcClient = new JRDClient();
        byte nv[] = rpcClient.readNv(95);
        if (nv != null && nv.length > 0) {
            String AreaType = Byte.toString(nv[0]);
            try {
                area = Integer.parseInt(AreaType);
            } catch (Exception e) {
                // TODO: handle exception
            }
        }
        return area;
    }
}
