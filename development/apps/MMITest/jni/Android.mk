
LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

#commonIncludes := vendor/qcom/proprietary/common/inc
#commonIncludes += vendor/qcom/proprietary/oncrpc/inc/
#commonIncludes += vendor/qcom/proprietary/modem-apis/msm7627a/api/libs/remote_apis/oem_rapi/inc/
#commonIncludes += vendor/qcom/proprietary/modem-apis/msm7627a/api/libs/remote_apis/snd/inc/
#commonIncludes += vendor/qcom/proprietary/modem-apis/msm7627a/api/libs/remote_apis/cm/inc
#commonIncludes += vendor/jrdcom/system/trace_util/
commonIncludes += $(LOCAL_PATH)/..


LOCAL_MODULE_TAGS := optional

# This is the target being built.
LOCAL_MODULE:= libmmijni


# All of the source files that we will compile.
LOCAL_SRC_FILES:= \
    mmijni.cpp

# All of the shared libraries we link against.
LOCAL_SHARED_LIBRARIES := \
    libutils \
    liblog


# No static libraries.
LOCAL_STATIC_LIBRARIES :=

# Also need the JNI headers.
LOCAL_C_INCLUDES += \
    $(JNI_H_INCLUDE) \
    $(commonIncludes)

# No special compiler flags.
#LOCAL_CFLAGS += -DFEATURE_VOC_PCM_INTERFACE -DFEATURE_VOICE_PLAYBACK

LOCAL_PRELINK_MODULE := false

include $(BUILD_SHARED_LIBRARY)

