package com.tct.android.tools.feature.sdm2;

import java.util.HashMap;
import java.util.Map;

import com.tct.android.tools.feature.app.Main;

public class SdmTable {
	private static final Map<String, VarInfo> SDMs = new HashMap<String, SdmTable.VarInfo>();
	private SdmTable(){}
	
	public static void addOrUpdateVar(String sdmid, String type, String value) {
		SDMs.put(sdmid.trim(), new VarInfo(sdmid, type, value));
	}
	
	public static void updateVar(String sdmid, String value) {
		sdmid = sdmid.trim();
		if (SDMs.containsKey(sdmid)) {
			VarInfo vInfo = SDMs.get(sdmid);
			vInfo.value = vInfo.getStandardValue(value);
		} else {
			Main.LOG.e("updateVar: sdmid NOT found! (" + sdmid + ")");
		}
	}
	
	public static VarInfo getVar(String sdmid) {
		sdmid = sdmid.trim();
		if (SDMs.containsKey(sdmid)) {
			return SDMs.get(sdmid);
		}
		return null;
	}
	
	static void print() {
		System.out.println(SDMs);
	}
	
	public static class VarInfo {
		public static final String BOOLEAN = "boolean";
		public static final String INT = "int";
		public static final String STRING = "String";
		private static final String TP_BOOLEAN = "boolean";
		private static final String TP_BYTE = "byte";
		private static final String TP_WORD = "word";
		private static final String TP_DWORD = "dword";
		private static final String TP_SBYTE = "sbyte";
		private static final String TP_SWORD = "sword";
		private static final String TP_SDWORD = "sdword";
		private static final String TP_ASCIISTRING = "asciistring";		
		
		private String sdmid;
		private String value;
		private String type;
		private boolean isNum;

		public VarInfo(String id, String t, String v) {
			t = t.toLowerCase().trim();
			if (checkType(t)) {
				sdmid = id.trim();
				type  = getStandardType(t);
				isNum = checkNum();
				value = getStandardValue(v);
			}else{
				Main.LOG.e("Wrong Type: " + t + "  (SDMID: " + id + ")");
			}			
		}
		
		public String sdmid() {return sdmid;}
		public String value() {return value;}
		public String type() {return type;}
		public boolean isNum() {return isNum;}
		
		
		private boolean checkType(String type){
			if (type.startsWith(TP_BOOLEAN)) {return true;}
			if (type.startsWith(TP_BYTE)) {return true;}
			if (type.startsWith(TP_WORD)) {return true;}
			if (type.startsWith(TP_DWORD)) {return true;}
			if (type.startsWith(TP_SBYTE)) {return true;}
			if (type.startsWith(TP_SWORD)) {return true;}
			if (type.startsWith(TP_SDWORD)) {return true;}
			if (type.startsWith(TP_ASCIISTRING)) {return true;}
			return false;
		}
		private String getStandardType(String type){
			if (type.startsWith(TP_ASCIISTRING)) {
				return STRING;
			} else if (type.startsWith(TP_BOOLEAN)) {
				return BOOLEAN;
			} else {
				return INT;
			}
		}
 		private boolean checkNum() {
			if (type.equals(STRING)) {
				return false;
			} else {
				return true;
			}
		}
 		private String getStandardValue(String value) {
 			value = value.trim();
 			if (value.startsWith("\"")) {
 				value = value.substring(1);
 			}
 			if (value.endsWith("\"")) {
 				value = value.substring(0, value.length()-1);
 			}
 			if (type.equals(BOOLEAN)) {
 				value = value.toLowerCase();
 				if (value.equals("1") || value.equals("0x01") || value.equals("true") || value.equals("yes")) {
 					return "true";
 				}else{
 					return "false";
 				}
 			}
 			return value;
 		}

		@Override
		public String toString() {
			return "public static final " + type  + " " + sdmid + ";";
		} 		
	}
}
