package com.tct.android.tools.feature.sdm2;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.tct.android.tools.feature.app.Main;


public class PLF_File {
	private File mFile;
	private boolean isPlf;
	private OnSdmListener mListener;
	public PLF_File(File plf_xplf, OnSdmListener listener) {
//		if (plf_xplf.getName().endsWith(".plf") || plf_xplf.getName().endsWith(".xplf")) {
		    isPlf = plf_xplf.getName().endsWith(".plf");
			mFile = plf_xplf;
			mListener = listener;
//		}
	}
	
	public boolean parse(){
		if (mFile == null) return false;
		SAXParserFactory factory = SAXParserFactory.newInstance();
        try {
            InputStream is = new FileInputStream(mFile);
            SAXParser parser = factory.newSAXParser();
            parser.parse(is, new DefaultHandler() {
                private boolean inSdm;
                private Var var;
                private String tag;
                private String keySdmid = null;

                @Override
                public void startElement(String uri, String localName,
                        String name, Attributes atrbts) throws SAXException {
                    super.startElement(uri, localName, name, atrbts);
                    if (name.equals(Var.KEY_VAR)) {
                        inSdm = true;
                        var = new Var();
                    }
                    tag = name;
                }

                @Override
                public void characters(char[] chars, int i, int i1)
                        throws SAXException {
                    super.characters(chars, i, i1);
                    String value = new String(chars, i, i1);
                    if (inSdm) {
                        if (tag.equals(Var.KEY_SDMID)) {
                            keySdmid = value;
                            var.sdmid = value;
                            return;
                        } 
                        if (tag.equals(Var.KEY_C_NAME)) {
                            var.c_name = value;
                            return;
                        }
                        if (tag.equals(Var.KEY_C_TYPE)) {
                            var.c_type = value;
                            return;
                        }
                        if (tag.equals(Var.KEY_ARRAY)) {
                            var.array = value;
                            return;
                        }
                        if (tag.equals(Var.KEY_METATYPE)) {
                            var.metatype = value;
                            return;
                        }
                        if (tag.equals(Var.KEY_IS_CUSTO)) {
                            var.is_custo = value;
                            return;
                        }
                        if (tag.equals(Var.KEY_FEATURE)) {
                            var.feature = value;
                            return;
                        }
                        if (tag.equals(Var.KEY_DESC)) {
                            value = escapeXML(value);
                            var.desc = var.desc + value;
                            return;
                        }
                        if (tag.equals(Var.KEY_VALUE)) {
                            value = escapeXML(value);
                            var.value = var.value + value;
                            return;
                        }
                        
                    }
                }

                @Override
                public void endElement(String uri, String localName, String name)
                        throws SAXException {
                    super.endElement(uri, localName, name);
                    if (name.equals(Var.KEY_VAR)) {
                        inSdm = false;
                        if (keySdmid != null) {
                        	mListener.onNewSdm(isPlf, var.sdmid, var.metatype, var.value);
                            keySdmid = null;
                            var = null;
                        }
                    }
                    tag = "";
                }
            });
        } catch (Exception ex) {
            Main.LOG.e("PLF_File parse Error:" + ex);
            return false;
        }
		return true;
	}
	
	
	
	static class Var {
        static final String KEY_VAR       = "VAR";
        static final String KEY_SIMPLE     = "SIMPLE_VAR";
        static final String KEY_SDMID     = "SDMID";
        static final String KEY_C_NAME    = "C_NAME";
        static final String KEY_C_TYPE    = "C_TYPE";
        static final String KEY_ARRAY     = "ARRAY";
        static final String KEY_METATYPE  = "METATYPE";
        static final String KEY_IS_CUSTO  = "IS_CUSTO";
        static final String KEY_FEATURE   = "FEATURE";
        static final String KEY_DESC      = "DESC";
        static final String KEY_VALUE     = "VALUE";
        String sdmid = "";
        String c_name = "";
        String c_type = "";
        String array = "";
        String metatype = "";
        String is_custo = "";
        String feature = "";
        String desc = "";
        String value = "";
	}
	
	static String escapeXML(String value) {
        if (value == null) {
            return null;
        }
        char content[] = new char[value.length()];
        value.getChars(0, value.length(), content, 0);
        StringBuffer result = new StringBuffer(content.length+50);
        for(int i=0; i<content.length; i++) {
            switch(content[i]) {
            case 60: // '<'
                result.append("&lt;");
                break;
            case 62: // '>'
                result.append("&gt;");
                break;
            case 38: // '&'
                result.append("&amp;");
                break;
            default:
                result.append(content[i]);
                break;
            }
        }
        return result.toString();
    }
}
