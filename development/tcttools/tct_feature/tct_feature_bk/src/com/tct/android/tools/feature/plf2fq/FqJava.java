package com.tct.android.tools.feature.plf2fq;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.tct.android.tools.feature.app.Main;

/*
 * Format: plfmap.xml 
 * <plf2fq>
 *  <plf path="device/tct/common/perso/plf/packages/apps/Contacts">
 *   <sdm id="AAA"/>
 *  </plf> 
 *  <plf path="device/tct/common/perso/plf/frameworks/base/packages/SettingsProvider" >
 *   <sdm id="BBB"/>
 *   <sdm id="CCC"/>
 *  </plf>
 * </plf2fq>
 */
public class FqJava {
    private static final String TAG_PLF = "plf";
    private static final String ATTR_PATH = "path";
    private static final String ATTR_NAME = "name";
    private static final String TAG_SDM = "sdm";
    private static final String ATTR_ID = "id";
    private static FqJava _FQJAVA = null;
    private final HashMap<String, HashMap<String, Field>> allModules = new HashMap<String, HashMap<String, Field>>();
    private String plf2fq = null;
    private FqJava() {
    }
    public synchronized static FqJava create(String plf2fqXML) {
        if (_FQJAVA == null) {
            if (new File(plf2fqXML).exists()) {
                _FQJAVA = new FqJava();
                _FQJAVA.plf2fq = plf2fqXML;
                return _FQJAVA;
            }
        }
        return null;
    }

    public void init(final Listener l) {
        if (plf2fq != null && l != null) {
            Main.LOG.d(plf2fq);
            SAXParserFactory factory = SAXParserFactory.newInstance();
            try {
                InputStream is = new FileInputStream(plf2fq);
                SAXParser parser = factory.newSAXParser();
                parser.parse(is, new DefaultHandler() {
                    String curPath = null;
                    String curModule = null;
                    String curName = null;
                    @Override
                    public void startElement(String uri, String localName,
                            String name, Attributes atrbts) throws SAXException {
                        super.startElement(uri, localName, name, atrbts);
                        if (name.equals(TAG_PLF)) {
                            curPath = atrbts.getValue(ATTR_PATH);
                            curName = atrbts.getValue(ATTR_NAME);

                            Main.LOG.d(curPath);
                            if (curPath.endsWith("/")) {
                                curPath = curPath.substring(0, curPath.length()-1);
                            }
                            curModule = curPath.substring(curPath.lastIndexOf("/")+1).trim();
                            if(curName != null) {
                                curModule = curName;
                            }
                            allModules.put(curModule, new HashMap<String,Field>());
                            Main.LOG.d(curModule);
                        } else if (name.equals(TAG_SDM)) {
                            String sdmid = atrbts.getValue(ATTR_ID);
                            Field f = new Field();
                            f.name = sdmid;
                            allModules.get(curModule).put(sdmid, f);
                            Main.LOG.d(sdmid);
                        }
                    }

                    @Override
                    public void endElement(String uri, String localName, String name)
                            throws SAXException {
                        super.endElement(uri, localName, name);
                        if (name.equals(TAG_PLF)) {
                            l.onNewPlf(curModule, curPath);
                            curPath = null;
                            curModule = null;
                        }
                    }
                });
            } catch (Exception ex) {
                Main.LOG.e("PlfParser Error:" + ex);
            }
        }
    }

    public void updateField(String module, Field f) {
        Main.LOG.d("M:" + module + "  F:" + f.name + " " + f.type + " " + f.value);
        if (module != null && module.length() > 0 && f != null
                && f.name != null && f.name.length() > 0) {
            String sdmid = f.name;
            if (allModules.containsKey(module)) {
                HashMap<String, Field> fields = allModules.get(module);
                if (fields != null && fields.containsKey(sdmid)) {
                    fields.put(sdmid, f); //Cover
                }
            }
        }
    }
    FileWriter fw = null;
    private void write(String str) throws IOException {
        if (str.contains("{")) {
            fw.write(SPACE.getSpaces() + str +"\n");
            SPACE.increase();
        } else if (str.contains("}")) {
            SPACE.decrease();
            fw.write(SPACE.getSpaces() + str +"\n");
        } else {
            fw.write(SPACE.getSpaces() + str +"\n");
        }
    }
    public void genJava(String outputJavaDir) {
        Main.LOG.d(outputJavaDir);
        File file  = new File(outputJavaDir + "/com/tct/feature");
        if (!file.exists()) {
            file.mkdirs();
        }
        String outputJavaFile = file.getPath() + "/PLF.java";
        try {
            fw = new FileWriter(outputJavaFile);
            write(SPACE.getSpaces() + "package com.tct.feature;");
            write(SPACE.getSpaces() + "public final class PLF {");
            for(String module : allModules.keySet()) {
                write("public final static class " + module + "{");
                write("public final static class R {");
                HashMap<String, Field> fields = allModules.get(module);
                if (fields != null) {
                    ArrayList<String> intArray = new ArrayList<String>();
                    ArrayList<String> boolArray = new ArrayList<String>();
                    ArrayList<String> stringArray = new ArrayList<String>();
                    ArrayList<String> tmp = null;
                    for (Field f : fields.values()) {
                        if (Field.BOOL.equals(f.type)) {
                            tmp = boolArray;
                        } else if (Field.STRING.equals(f.type)) {
                            tmp = stringArray;
                        } else if (Field.INT.equals(f.type)) {
                            tmp = intArray;
                        }
                        String stmt = f.toString().trim();
                        if(stmt != null && stmt.length() > 0) {
                            tmp.add(stmt);
                        }
                    }
                    write("public final static class integer {");
                    for (String stmt : intArray) {
                        write(stmt);
                    }
                    write("}");
                    
                    write("public final static class bool {");
                    for (String stmt : boolArray) {
                          write(stmt);
                      }
                    write("}");
                    
                    write("public final static class string {");
                    for (String stmt : stringArray) {
                          write(stmt);
                      }
                    write("}");
                }
                write("}");
                write("}");
            }
            write("}");
        } catch (IOException e) {
            Main.LOG.e("Open File Error:" + outputJavaFile + e.toString());
        } finally {
            try {
                fw.close();
            } catch (IOException e) {
            }
        }
        
    }

    public static class Field {
        static final String STRING = "String";
        static final String BOOL = "boolean";
        static final String INT = "int";
        public String type;
        String name;
        String value;
        
        @Override
        public String toString() {
            String ret = "";
            if (STRING.equals(type)) {
                if (validName()) {
                    if (value == null) {
                        ret = "public static String " + name + " = null;";
                    } else {
                        ret = "public static String " + name + " = \"" + value
                                + "\";";
                    }
                }
                return ret;
            }
            if (BOOL.equals(type) || INT.equals(type)) {
                if (validName() && validValue()) {
                    ret = "public static " + type + " " + name + " = " + value
                            + ";";
                }
                return ret;
            }
            return ret;
        }

        private boolean validName() {
            return name != null && name.length() > 0;
        }

        private boolean validValue() {
            return value != null && value.length() > 0;
        }
    }
    private static class SPACE {
        private static String getSpaces() {
            return SPACES[mSpaceNum];
        }
        private static void increase() {
            mSpaceNum++;
            if (mSpaceNum >= 10) {
                mSpaceNum = 10;
            }
        }
        private static void decrease() {
            mSpaceNum--;
            if (mSpaceNum < 0) {
                mSpaceNum = 0;
            }
        }
        private static int mSpaceNum = 0;
        private final static String[] SPACES = {
          "",
          "  ",
          "    ",
          "      ",
          "        ",
          "          ",
          "            ",
          "              ",
          "                ",
          "                  ",
          "                    ",
        };
    }
    public static interface Listener {
        void onNewPlf(String module, String path);
    }
}
