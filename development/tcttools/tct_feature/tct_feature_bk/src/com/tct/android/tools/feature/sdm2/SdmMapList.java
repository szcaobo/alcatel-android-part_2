package com.tct.android.tools.feature.sdm2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.tct.android.tools.feature.app.Main;


public class SdmMapList {
	private static final String MAP_FILE_PATH = "device/tct/common/tct_feature.sdm2hj";
	public static final List<String> LIST;
	private SdmMapList(){};
	static {
		LIST = new ArrayList<String>();
		File sdm2hj = new File(MAP_FILE_PATH);
		if (sdm2hj.exists()) {
			BufferedReader br = null;
	        try {
	            br = new BufferedReader(new FileReader(sdm2hj));
	            String line = null;
	            while ((line = br.readLine()) != null) {
	            	line = line.trim();
	            	if (!line.startsWith("#")) {
	            		LIST.add(line);
	            	}
	            }
	        } catch (Exception e) {
	            Main.LOG.e("Read SDM Map File ERROR! e:" + e.toString());
	        } finally {
	            try {
	                br.close();
	            } catch (IOException e) {
	            }
	        }
		}
	}
}
