package com.tct.android.tools.feature.plf2fq;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Perso {
    private final static ArrayList<String> booleanTrueList= new ArrayList<String>();
    //Support META_TYPE
    private final static Map<String,String> metatypeMap = new HashMap<String,String>();
    
    static {
        booleanTrueList.add("true");
        booleanTrueList.add("yes");
        booleanTrueList.add("0x01");
        booleanTrueList.add("1");
        //Supported METATYPE
        metatypeMap.put("AsciiString", "String");
        metatypeMap.put("Boolean", "boolean");
        metatypeMap.put("Byte", "int");
        metatypeMap.put("Dword", "int");
        metatypeMap.put("Ucs2StringWLen", "String");
        metatypeMap.put("Word", "int");
    }

    public static String convertType(String metatype) {
        if (metatypeMap.containsKey(metatype)) {
            return metatypeMap.get(metatype);
        }
        return "String";
    }
    
    static boolean isEnable(String cType, String value) {
        value = value.toLowerCase().replace("\"", "");
        return booleanTrueList.contains(value);
    }

    static String convertValue (String plfType, String value) {
        value = trimQuotes(value);
        if ("Boolean".equalsIgnoreCase(plfType)) {
            value = isEnable(plfType, value) ? "true" : "false";
        }
        if ("Ucs2StringWLen".equalsIgnoreCase(plfType)) {
            value = decodeUCS2(value.toLowerCase().replace("0x", "")
                                                  .replace(",","")
                                                  .replace(")", "")
                                                  .split("\\(")[1]);
        }
        return value;
    }

    private static String trimQuotes(String str) {
        if (str.startsWith("\"")) {
            str = str.substring(1);
        }
        if (str.endsWith("\"")) {
            str = str.substring(0, str.length()-1);
        }
        return str;
    }

    private static String decodeUCS2(String ucs2) {
        byte[] bytes = new byte[ucs2.length()/2];
        for (int i=0; i< ucs2.length(); i+=2) {
            bytes[i/2] = (byte)(Integer.parseInt(ucs2.substring(i, i+2), 16));
        }
        String reVal = "";
        try {
            reVal = new String(bytes, "UTF-16BE");
        } catch(Exception e) {
        }
        return reVal;
    }
}
